<?php
error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
    '/bank',
    '/configuration',
    '/scripts',
    '/payReport'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/response.php";
require_once "utils/helpers/funcs.php";
require_once "utils/config/driverPayReportConfig.php";
require_once "utils/config/driverConfig.php";
require_once "classes/driverPayoutReportClass.php";
require_once "classes/driverPayoutReportStatusClass.php";
require_once "classes/driverPayoutDatestampClass.php";

session_start();

function get_all_drivers($db, $driver_type) {
	$ret_arr = array();
    $params  = array($driver_type);
    
    $query = 'SELECT 
			driver.*,
			gett.*,
			telephone_book.*,
			vehicle.*,
			bank.*,
			pay_project.*
		FROM
			postgres.public.driver as driver
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.gett
		) AS gett ON driver.driver_id=gett.gett_driver_id
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.telephone_book
		) AS telephone_book ON driver.driver_telephone_book_id=telephone_book.telephone_book_id
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.vehicle
		) AS vehicle ON driver.driver_vehicle_id=vehicle.vehicle_id
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.bank
		) AS bank ON driver.driver_id=bank.bank_driver_id
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.pay_project
		  ) AS pay_project ON pay_project.pay_project_id=bank.bank_pay_project_id
		WHERE
			driver.driver_type = $1
		';
    
    $query_name = "get_drivers_by_type_query";
    $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
        $query_name
    ));
    
    if (!$result || pg_num_rows($result) == 0) {
        $result = pg_prepare($db, $query_name, $query);
    } //!$result || pg_num_rows( $result ) == 0
    
    $result = pg_execute($db, $query_name, $params);
    
    if ($result) {
        while ($row = pg_fetch_assoc($result)) {
            $ret_arr[] = $row;
        } //$row = pg_fetch_assoc($result)
        
        pg_free_result($result);
    } //$result
    
    return $ret_arr;
}

function get_all_orders($db, $gett_id, $date_beg, $date_end) {
	$ret_arr = array();
    $params  = array(
    	$gett_id,
    	$date_beg,
    	$date_end
    );
    
    $query = 'SELECT 
		ord.*,
		gett.*,
		driver.*,
		vehicle.*,
		telephone_book.*
		FROM postgres.public."order" as ord
		LEFT OUTER JOIN (SELECT * FROM postgres.public.gett)
			AS gett ON  gett.gett_id = ord.order_gett_id
		LEFT OUTER JOIN (SELECT * FROM postgres.public.driver)
			AS driver ON gett.gett_driver_id = driver.driver_id
		LEFT OUTER JOIN (SELECT * FROM postgres.public.vehicle)
			AS vehicle ON driver.driver_vehicle_id = vehicle.vehicle_id
		LEFT OUTER JOIN (SELECT * FROM postgres.public.telephone_book) 
			AS telephone_book ON driver.driver_telephone_book_id=telephone_book.telephone_book_id
		WHERE ord.order_gett_id = $1 AND ord.order_datetime_start >= $2 AND ord.order_datetime_start <= $3
		ORDER BY ord.order_datetime_start desc
		';
    
    $query_name = "get_orders_by_driver_id_query";
    $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
        $query_name
    ));
    
    if (!$result || pg_num_rows($result) == 0) {
        $result = pg_prepare($db, $query_name, $query);
    } //!$result || pg_num_rows( $result ) == 0
    
    $result = pg_execute($db, $query_name, $params);
    
    if ($result) {
        while ($row = pg_fetch_assoc($result)) {
            $ret_arr[] = $row;
        } //$row = pg_fetch_assoc($result)
        
        pg_free_result($result);
    } //$result
    
    return $ret_arr;
}

function get_all_orders_with_fraud($db, $gett_id, $date_beg, $date_end) {
	$ret_arr = array();
    $params  = array(
    	$gett_id,
    	$date_beg,
    	$date_end
    );
    
    $query = 'SELECT 
		ord.*,
		gett.*,
		driver.*,
		vehicle.*,
		telephone_book.*,
		order_fraud.*
		FROM postgres.public."order" as ord
		LEFT OUTER JOIN (SELECT DISTINCT ON (order_fraud_order_id) * FROM postgres.public.order_fraud)
			AS order_fraud ON  order_fraud.order_fraud_order_id = ord.order_id
		LEFT OUTER JOIN (SELECT * FROM postgres.public.gett)
			AS gett ON  gett.gett_id = ord.order_gett_id
		LEFT OUTER JOIN (SELECT * FROM postgres.public.driver)
			AS driver ON gett.gett_driver_id = driver.driver_id
		LEFT OUTER JOIN (SELECT * FROM postgres.public.vehicle)
			AS vehicle ON driver.driver_vehicle_id = vehicle.vehicle_id
		LEFT OUTER JOIN (SELECT * FROM postgres.public.telephone_book) 
			AS telephone_book ON driver.driver_telephone_book_id=telephone_book.telephone_book_id
		WHERE ord.order_gett_id = $1 AND ord.order_datetime_start >= $2 AND ord.order_datetime_start <= $3
		ORDER BY ord.order_datetime_start desc
		';
    
    $query_name = "get_orders_by_driver_id_query";
    $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
        $query_name
    ));
    
    if (!$result || pg_num_rows($result) == 0) {
        $result = pg_prepare($db, $query_name, $query);
    } //!$result || pg_num_rows( $result ) == 0
    
    $result = pg_execute($db, $query_name, $params);
    
    if ($result) {
        while ($row = pg_fetch_assoc($result)) {
            $ret_arr[] = $row;
        } //$row = pg_fetch_assoc($result)
        
        pg_free_result($result);
    } //$result
    
    return $ret_arr;
}

function get_all_transactions($db, $driver_id, $date_beg, $date_end, $exclude_arr = NULL) {
	$ret_arr = array();
    $params  = array(
    	$driver_id,
    	$date_beg,
    	$date_end
    );
    
    $query = 'SELECT 
		transactions.*,
		employee.*
		FROM postgres.public.transactions as transactions
		LEFT OUTER JOIN (SELECT * FROM postgres.public.employee)
			AS employee ON  employee.employee_id = transactions.transactions_employee_added_id
		WHERE transactions.transactions_driver_id = $1 AND transactions.transactions_datetime >= $2 AND transactions.transactions_datetime <= $3';
		
	if (is_array($exclude_arr)) {
		foreach($exclude_arr as $value) {
			$query .= ' AND transactions.transactions_subtype is not ' . $value;
		}
	}
		
	$query .= ' ORDER BY transactions.transactions_datetime desc
		';
    
    $query_name = "get_transactions_by_driver_id_query";
    $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
        $query_name
    ));
    
    if (!$result || pg_num_rows($result) == 0) {
        $result = pg_prepare($db, $query_name, $query);
    } //!$result || pg_num_rows( $result ) == 0
    
    $result = pg_execute($db, $query_name, $params);
    
    if ($result) {
        while ($row = pg_fetch_assoc($result)) {
            $ret_arr[] = $row;
        } //$row = pg_fetch_assoc($result)
        
        pg_free_result($result);
    } //$result
    
    return $ret_arr;
}

function get_all_shifts($db, $driver_id, $date_beg, $date_end) {
	$ret_arr = array();
    $params  = array(
    	$driver_id,
    	$date_beg,
    	$date_end
    );
    
    $query = 'SELECT 
		schedule.*,
		vehicle.*
		FROM postgres.public.schedule as schedule
		LEFT OUTER JOIN (SELECT * FROM postgres.public.vehicle)
			AS vehicle ON  vehicle.vehicle_id = schedule.schedule_vehicle_id
		WHERE 
			schedule.schedule_driver_id = $1 AND 
			schedule.schedule_datetime_begin_shift <= $2 AND 
			(schedule.schedule_datetime_end_shift_vehicle >= $3 OR schedule.schedule_datetime_end_shift_vehicle is NULL)
		ORDER BY schedule.schedule_datetime_begin_shift desc
		';
    
    $query_name = "get_shifts_by_driver_id_" . $driver_id . "_query";
    $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
        $query_name
    ));
    
    if (!$result || pg_num_rows($result) == 0) {
        $result = pg_prepare($db, $query_name, $query);
    } //!$result || pg_num_rows( $result ) == 0
    
    $result = pg_execute($db, $query_name, $params);
    
    if ($result) {
        while ($row = pg_fetch_assoc($result)) {
            $ret_arr[] = $row;
        } //$row = pg_fetch_assoc($result)
        
        pg_free_result($result);
    } //$result
    
    return $ret_arr;
}

function get_motivation($db, $date_beg, $date_end) {
	$ret_arr = array();
    $params  = array(
    	$date_beg,
    	$date_end
    );
    
    $query = 'SELECT 
		motivation_entries.*,
		motivation.*
		FROM postgres.public.config_driver_pay_motivation as motivation,
			postgres.public.config_driver_pay_motivation_entries as motivation_entries
		WHERE
			motivation.config_driver_pay_motivation_date_begin >= $1 AND 
			(motivation.config_driver_pay_motivation_date_end <= $2 OR motivation.config_driver_pay_motivation_date_end is NULL) AND
			motivation_entries.config_driver_pay_motivation_entries_config_dpm_id = motivation.config_driver_pay_motivation_id
		ORDER BY motivation_entries.config_driver_pay_motivation_entries_starting_point asc
		';
    
    $query_name = "get_shifts_by_driver_id_query";
    $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
        $query_name
    ));
    
    if (!$result || pg_num_rows($result) == 0) {
        $result = pg_prepare($db, $query_name, $query);
    } //!$result || pg_num_rows( $result ) == 0
    
    $result = pg_execute($db, $query_name, $params);
    
    if ($result) {
        while ($row = pg_fetch_assoc($result)) {
            $ret_arr[] = $row;
        } //$row = pg_fetch_assoc($result)
        
        pg_free_result($result);
    } //$result
    
    return $ret_arr;
}

function calculate_motivation_pay($orders, $motivations, $sums) {
	if (!isset($sums)) {
		$sums = array(
			'TOTAL_ORDERS' => 0,
			'SUM_ORDERS' => 0,
			'SUM_FRAUD' => 0,
			'SUM_TIPS' => 0,
			'SUM_TRANSACTIONS' => 0,
			'SHIFT_PAY_DRIVER' => 0,
			'SHIFT_ORDER_COUNT' => 0,
			'SHIFT_ORDER_SUM' => 0,
			'PERCENT_GETT' => 0,
			'SUM_PERCENT_GETT' => 0,
			'PERCENT_COMPANY' => 0,
			'SUM_PERCENT_COMPANY' => 0,
		);
	}
	
	if (isset($motivations[0])) {
		$correction = $motivations[0]['config_driver_pay_motivation_error'];
	} else {
		$correction = 0;
	}
	
	$sum_orders = 0;
	$count_orders = 0;
	
	foreach ($orders as $key => $value) {
		$tarif = (isset($value['order_fraud_sum']) && $value['order_fraud_sum'] > 0 ? $value['order_fraud_sum'] : $value['order_tarif_driver_overall']);
		// TODO check for fraud flag, if flag is set then take value for adjusted order pay
		$sums['SUM_ORDERS'] += $tarif;
		$sums['TOTAL_ORDERS'] += 1;
		$sums['SUM_FRAUD'] = 0;
		$sums['SUM_TIPS'] += $value['order_tip_driver'];
		
		$sum_orders += $tarif;
	}
	
	if ($correction && $correction != 0) {
		$sum_orders += $correction;
	}
	
	foreach ($motivations as $key => $value) {
		if ($sum_orders <= $value['config_driver_pay_motivation_entries_ending_point']) {
			if ($correction && $correction != 0) {
				$sum_orders -= $correction;
			}
			
			// TODO check for type of entry, percent, rubles etc ... default current is percentage
			$sums['SHIFT_PAY_DRIVER'] = $sum_orders * $value['config_driver_pay_motivation_entries_value'];
			$sums['SHIFT_ORDER_SUM'] = $sum_orders;
			$sums['SHIFT_ORDER_COUNT'] = $count_orders;
			return $sums;
		}
	}
}

function build_document_driver_header($driver) {
	return array(
		'DRIVER_FULL_NAME' => $driver['driver_name_last'] . ' ' . $driver['driver_name_first'] . ' ' . $driver['driver_name_name'],
		'DRIVER_ID' => $driver['driver_id'],
		'GETT_SYSTEM_ID' => $driver['gett_system_id'],
		'BANK_ACCOUNT_NUMBER' => $driver['bank_account_number'],
		'BANK_ACCOUNT_HOLDER_FULL_NAME' => $driver['bank_account_holder_name_last'] . ' ' . $driver['bank_account_holder_name_first'] . ' ' . $driver['bank_account_holder_name_name']
	);
}

$GLOBALS['count_driver_payment'] = 0;
function build_document_information($orders = NULL, $transactions, $driver, $motivation_pay = NULL, $sums = NULL) {
	if (!isset($sums)) {
		$sums = array(
			'TOTAL_ORDERS' => 0,
			'SUM_ORDERS' => 0,
			'SUM_FRAUD' => 0,
			'SUM_TIPS' => 0,
			'SUM_TRANSACTIONS' => 0,
			'PERCENT_GETT' => 0,
			'SUM_PERCENT_GETT' => 0,
			'PERCENT_COMPANY' => 0,
			'SUM_PERCENT_COMPANY' => 0,
			'SUM_PAY_DRIVER' => 0
		);
	}
		
	// TODO : get from configurator
	$percent_gett = 8;
	$percent_company = 7;
	
	$sums['PERCENT_GETT'] = $percent_gett;
	$sums['PERCENT_COMPANY'] = $percent_company;
	
	$rows_order = array();
	$rows_pay = array();
	$rows_transaction = array();
	$header = array();
	
	$bank_payment_info = array(
		'driver' => array(
			'title' => 'Сотрудник',
			'attribute' => array(
				'title' => 'Нпп',
				'value' => $GLOBALS['count_driver_payment']++
			),
			'name_last' => array(
				'title' => 'Фамилия',
				'value' => $driver['driver_name_last']
			),
			'name_first' => array(
				'title' => 'Имя',
				'value' => $driver['driver_name_first']
			),
			'name_name' => array(
				'title' => 'Отчество',
				'value' => $driver['driver_name_name']
			),
			'bank' => array(
				'title' => 'ОтделениеБанка',
				'value' => '8047'
			),
			'bank_branch' => array(
				'title' => 'ФилиалОтделенияБанка',
				'value' => '0603'
			),
			'account_number' => array(
				'title' => 'ЛицевойСчет',
				'value' => $driver['bank_account_number']
			),
			'sum' => array(
				'title' => 'Сумма',
				'value' => null
			)
		)
	);
	
	$header = build_document_driver_header($driver);
	
	if (isset($orders)) {
		foreach ($orders as $key => $value) {
			$delta = $value['order_tarif_driver_overall'] - $value['order_client_money'];
			$order_sum_after_deductions = $value['order_tarif_driver_overall'] - ($value['order_tarif_driver_overall'] * ($percent_company + $percent_gett) / 100);
			$sum_percent_gett = $value['order_tarif_driver_overall'] * $percent_gett / 100;
			$sum_percent_company = $value['order_tarif_driver_overall'] * $percent_company / 100;
			$pay_driver = $order_sum_after_deductions - $value['order_client_money'];
	
			// TODO : fraud
		
			$rows_order[] = array(
				'ORDER_DATE' => date('d.m.Y', strtotime($value['order_datetime_start'])),
				'TYPE_ROW' => DriverPayReportRowType::ORDER,
				'TYPE_ROW_TEXT' => getDriverPayReportRowTypeText(DriverPayReportRowType::ORDER),
				'ORDER_ID' => $value['order_id'],
				'ORDER_CLASS' => $value['order_class'],
				'ORDER_TRIP_START' => date('d.m.Y H:i:s', strtotime($value['order_datetime_start'])),
				'ORDER_TRIP_FINISH' => date('d.m.Y H:i:s', strtotime($value['order_datetime_finish'])),
				'ORDER_ADDRESS_START' => $value['order_address_start'],
				'ORDER_ADDRESS_FINISH' => $value['order_address_finish'],
				'ORDER_TARIF' => $value['order_tarif_driver_overall'],
				'ORDER_CASH' => $value['order_client_money'],
				'ORDER_DELTA' => $delta,
				'ORDER_TIP' => $value['order_tip_driver'],
				'ORDER_TYPE_PAY' => $value['order_type_payment'],
				'PERCENT_GETT' => $percent_gett,
				'SUM_PERCENT_GETT' => $sum_percent_gett,
				'PERCENT_COMPANY' => $percent_company,
				'SUM_PERCENT_COMPANY' => $sum_percent_company,
				'ORDER_SUM_AFTER_DEDUCTIONS' => $order_sum_after_deductions,
				'PAY_DRIVER' => $pay_driver,
			);
		
			$sums['TOTAL_ORDERS'] += 1;
			$sums['SUM_ORDERS'] += $value['order_tarif_driver_overall'];
			$sums['SUM_FRAUD'] = 0; // insert fraud
			$sums['SUM_TIPS'] += $value['order_tip_driver'];
			$sums['SUM_PERCENT_GETT'] += $sum_percent_gett;
			$sums['SUM_PERCENT_COMPANY'] += $sum_percent_company;
			$sums['SUM_PAY_DRIVER'] += $pay_driver;
		}
	} else if (isset($motivation_pay)) {
		foreach ($motivation_pay as $driver_key => $driver_value) {
			foreach ($driver_value as $schedule_key => $schedule_value) {
				$shift = $schedule_value['SHIFT_INFORMATION'];
				
				$rows_pay[] = array(
					'SHIFT_BEGIN_DATE' => $shift['schedule_datetime_begin_shift'],
					'SHIFT_END_DATE' => $shift['schedule_datetime_end_shift_vehicle'],
					'SHIFT_ORDER_COUNT' => $schedule_value['SHIFT_ORDER_COUNT'],
					'SHIFT_ORDER_SUM' => $schedule_value['SHIFT_ORDER_SUM'],
					'SHIFT_PAY_DRIVER' => $schedule_value['SHIFT_PAY_DRIVER'],
					'SHIFT_VEHICLE' => $shift['vehicle_plate_number'],
				);
			}
		}
	}
	
	foreach ($transactions as $key => $value) {
		$rows_transaction[] = array(
			'TRANSACTION_DATE' => date('d.m.Y', strtotime($value['transactions_datetime'])),
			'TYPE_ROW' => DriverPayReportRowType::TRANSACTION,
			'TYPE_ROW_TEXT' => getDriverPayReportRowTypeText(DriverPayReportRowType::TRANSACTION),
			'TRANSACTION_TYPE' => $value['transactions_type'],
			'TRANSACTION_TYPE_TEXT' => getTransactionsTypeText($value['transactions_type']),
			'TRANSACTION_SUBTYPE' => $value['transactions_subtype'],
			'TRANSACTION_SUBTYPE_TEXT' => getTransactionsSubtypeText($value['transactions_subtype']),
			'TRANSACTION_SUM' => $value['transactions_sum'],
			'TRANSACTION_REMARK' => $value['transactions_remark'],
			'TRANSACTION_EMPLOYEE_ADDED_ID' => $value['employee_id'],
			'TRANSACTION_EMPLOYEE_ADDED' => $value['employee_name_last'] . ' ' . $value['employee_name_first'] . ' ' . $value['employee_name_name'],
		);
		
		if ($driver['driver_type'] == DriverType::EMPLOYEE_PAY && 
			($value['transactions_subtype'] == TransactionsSubtype::FRAUD || $value['transactions_subtype'] == TransactionsSubtype::ORDER_CORRECTION)) {
			// skip over fraud and correction transactions if driver type is pay, fraud is counter in the motivation
			continue;
		} else {
			$sums['SUM_TRANSACTIONS'] += ($value['transactions_type'] == TransactionsType::OUTGOING ? (-1) * $value['transactions_sum'] : $value['transactions_sum']);
		}
	}
	
	if (isset($sums['SUM_TRANSACTIONS'])) {
		$sums['SUM_PAY_DRIVER'] += $sums['SUM_TRANSACTIONS'];
	}
	
	$bank_payment_info['driver']['sum']['value'] = $sums['SUM_PAY_DRIVER'];
	
	$header = array_merge ($sums, $header);
	
	$header_title = array(
		'DRIVER_FULL_NAME' => 'фио водителя',
		'DRIVER_ID' => 'id водителя',
		'GETT_SYSTEM_ID' => 'Gett позывной водителя',
		'BANK_ACCOUNT_NUMBER' => 'номер счета',
		'BANK_ACCOUNT_HOLDER_FULL_NAME' => 'фио держателя карты',
		'TOTAL_ORDERS' => 'кол-во заказов',
		'SUM_ORDERS' => 'сумма заказов',
		'SUM_FRAUD' => 'сумма фрод',
		'SUM_TIPS' => 'сумма чаявых',
		'SUM_TRANSACTIONS' => 'сумма проводок',
		'PERCENT_GETT' => 'процент сборов Gett',
		'SUM_PERCENT_GETT' => 'сумма сборов Gett',
		'PERCENT_COMPANY' => 'процент сборов компании',
		'SUM_PERCENT_COMPANY' => 'сумма сборов компании',
		'SUM_PAY_DRIVER' => 'выплата водителю',
	);
	
	$rows_order_title = array(
		'ORDER_DATE' => 'дата заказа',
		'TYPE_ROW' => '',
		'TYPE_ROW_TEXT' => 'тип строки',
		'ORDER_ID' => 'id заказа',
		'ORDER_CLASS' => 'класс',
		'ORDER_TRIP_START' => 'дата начала поездки',
		'ORDER_TRIP_FINISH' => 'дата завершения поездки',
		'ORDER_ADDRESS_START' => 'адрес начала поездки',
		'ORDER_ADDRESS_FINISH' => 'адрес завершения поездки',
		'ORDER_TARIF' => 'общая стоимость',
		'ORDER_CASH' => 'наличные от клиента',
		'ORDER_DELTA' => 'остаток по заказу',
		'ORDER_TIP' => 'чаявые',
		'ORDER_TYPE_PAY' => 'тип оплаты',
		'PERCENT_GETT' => 'процент сборов Gett',
		'SUM_PERCENT_GETT' => 'сборы Gett',
		'PERCENT_COMPANY' => 'процент сборов компании',
		'SUM_PERCENT_COMPANY' => 'сборы компании',
		'ORDER_SUM_AFTER_DEDUCTIONS' => 'стоимость заказа после сборов',
		'PAY_DRIVER' => 'к получению',
	);
	
	$rows_pay_title = array(
		'SHIFT_BEGIN_DATE' => 'дата начала смены',
		'SHIFT_END_DATE' => 'дата завершения смены',
		'SHIFT_ORDER_COUNT' => 'кол-во заказов',
		'SHIFT_ORDER_SUM' => 'сумма заказов',
		'SHIFT_PAY_DRIVER' => 'к выплате водителю',
		'SHIFT_VEHICLE' => 'автомобиль',
	);
	
	$rows_transaction_title = array(
		'TRANSACTION_DATE' => 'дата проводки',
		'TYPE_ROW' => '',
		'TYPE_ROW_TEXT' => 'тип строки',
		'TRANSACTION_TYPE' => '',
		'TRANSACTION_TYPE_TEXT' => 'тип проводки',
		'TRANSACTION_SUBTYPE' => '',
		'TRANSACTION_SUBTYPE_TEXT' => 'подтип проводки',
		'TRANSACTION_SUM' => 'сумма',
		'TRANSACTION_REMARK' => 'примечание',
		'TRANSACTION_EMPLOYEE_ADDED_ID' => 'id кем',
		'TRANSACTION_EMPLOYEE_ADDED' => 'кем',
	);
	
	return array(
		'DOCUMENT_HEADER_TITLE' => $header_title,
		'DOCUMENT_HEADER' => $header,
		'DOCUMENT_ROWS_ORDER_TITLE' => $rows_order_title,
		'DOCUMENT_ROWS_ORDER' => $rows_order,
		'DOCUMENT_ROWS_PAY_TITLE' => $rows_pay_title,
		'DOCUMENT_ROWS_PAY' => $rows_pay,
		'DOCUMENT_ROWS_TRANSACTION_TITLE' => $rows_transaction_title,
		'DOCUMENT_ROWS_TRANSACTION' => $rows_transaction,
		'BANK_PAYMENT' => $bank_payment_info
	);
}

function prepare_document_rows_pay_pdf($pays) {
	$ret_arr[] = array(
		"дата начала смены", 
		"дата завершения смены", 
		"кол-во заказов", 
		"сумма заказов", 
		"к выплате водителю", 
		"автомобиль"
	);

	foreach ($pays as $value) {
		$ret_arr[] = array(
			$value['SHIFT_BEGIN_DATE'],
			$value['SHIFT_END_DATE'],
			$value['SHIFT_ORDER_COUNT'],
			$value['SHIFT_ORDER_SUM'],
			$value['SHIFT_PAY_DRIVER'],
			$value['SHIFT_VEHICLE'],
		);
	}
	
	return $ret_arr;
}

function prepare_document_rows_order_pdf($orders) {
	$ret_arr[] = array(
		"дата заказа", 
		"id заказа", 
		"дата начала поездки", 
		"дата завершения поездки", 
		"адрес начала поездки", 
		"адрес завершения поездки", 
		"общая стоимость", 
		"наличные от клиента", 
		"остаток по заказу", 
		"чаявые", 
		"тип оплаты", 
		"сборы Gett", 
		"сборы компании", 
		"стоимость заказа после сборов", 
		"к получению"
	);
	
	foreach ($orders as $value) {
		$ret_arr[] = array(
			$value['ORDER_DATE'],
			$value['ORDER_ID'],
			$value['ORDER_TRIP_START'],
			$value['ORDER_TRIP_FINISH'],
			$value['ORDER_ADDRESS_START'],
			$value['ORDER_ADDRESS_FINISH'],
			$value['ORDER_TARIF'],
			$value['ORDER_CASH'],
			$value['ORDER_DELTA'],
			$value['ORDER_TIP'],
			$value['ORDER_TYPE_PAY'],
			$value['SUM_PERCENT_GETT'],
			$value['SUM_PERCENT_COMPANY'],
			$value['ORDER_SUM_AFTER_DEDUCTIONS'],
			$value['PAY_DRIVER'],
		);
	}
	
	return $ret_arr;
}

function prepare_document_rows_transaction_pdf($transactions) {
	$ret_arr[] = array(
		"дата проводки",
		"тип проводки", 
		"подтип проводки", 
		"сумма", 
		"примечание",
		"кем"
	);
	
	foreach ($transactions as $value) {
		$ret_arr[] = array(
			$value['TRANSACTION_DATE'],
			$value['TRANSACTION_TYPE_TEXT'],
			$value['TRANSACTION_SUBTYPE_TEXT'],
			$value['TRANSACTION_SUM'],
			$value['TRANSACTION_REMARK'],
			$value['TRANSACTION_EMPLOYEE_ADDED'],
		);
	}
	
	return $ret_arr;
}

function save_json_file($document, $driver_id, $date_script) {
	$pdf_json = array(
		"pageOrientation" => "landscape",
		"content" => array(
			array(
				"text" => "Зарплатная ведомость на $date_script",
				"bold" => true,
				"fontSize" => 18,
				"alignment" => "right",
				"marginBottom" => 20
			),
			array(
				"columns" => array(
					array(
						"width" => "45%",
						"style" => "tableExample",
						"table" => array(
							"headerRows" => 1,
							"body" => array(
								array(
									array(
										"text" => "Информация по водителю:",
										"style" => "header_top"
									),
									array(
										"text" => ""
									),
								),
								array(
									"фио водителя", 
									$document['DOCUMENT_HEADER']['DRIVER_FULL_NAME']
								),
								array(
									"id водителя", 
									$document['DOCUMENT_HEADER']['DRIVER_ID']
								),
								array(
									"Gett позывной водителя", 
									$document['DOCUMENT_HEADER']['GETT_SYSTEM_ID']
								),
								array(
									"фио держателя карты",
									$document['DOCUMENT_HEADER']['BANK_ACCOUNT_HOLDER_FULL_NAME']
								),
								array(
									"номер счета",
									$document['DOCUMENT_HEADER']['BANK_ACCOUNT_NUMBER']
								)
							)
						),
						"layout" => "lightHorizontalLines"
					),
					array(
						"width" => "30%",
						"style" => "tableExample",
						"table" => array(
							"headerRows" => 1,
							"body" => array(
								array(
									array(
										"text" => "Информация по %:",
										"style" => "header_top"
									),
									array(
										"text" => ""
									)
								),
								array(
									"процент сборов компании",
									$document['DOCUMENT_HEADER']['PERCENT_COMPANY']
								),
								array(
									"процент сборов Gett",
									$document['DOCUMENT_HEADER']['PERCENT_GETT']
								)
							)
						),
						"layout" => "lightHorizontalLines"
					),
					array(
						"width" => "25%",
						"style" => "tableExample",
						"table" => array(
							"headerRows" => 1,
							"body" => array(
								array(
									array(
										"text" => "Суммы:",
										"style" => "header_top"
									),
									array(
										"text" => ""
									)
								),
								array(
									"сумма фрод",
									$document['DOCUMENT_HEADER']['SUM_FRAUD']
								),
								array(
									"сумма заказов",
									$document['DOCUMENT_HEADER']['SUM_ORDERS']
								),
								array(
									"кол-во заказов",
									$document['DOCUMENT_HEADER']['TOTAL_ORDERS']
								),
								array(
									"сумма сборов компании",
									$document['DOCUMENT_HEADER']['SUM_PERCENT_COMPANY']
								),
								array(
									"сумма сборов Gett",
									$document['DOCUMENT_HEADER']['SUM_PERCENT_GETT']
								),
								array(
									"сумма чаявых",
									$document['DOCUMENT_HEADER']['SUM_TIPS']
								),
								array(
									"сумма проводок",
									$document['DOCUMENT_HEADER']['SUM_TRANSACTIONS']
								),
								array(
									"выплата водителю",
									$document['DOCUMENT_HEADER']['SUM_PAY_DRIVER']
								)
							)
						),
						"layout" => "lightHorizontalLines"
					)
				)
			),
			array(
				"text" => "Оплаты",
				"style" => "header"
			),
			array(
				"style" => "tableExample",
				"table" => array(
					"headerRows" => 1,
					"body" => prepare_document_rows_pay_pdf($document['DOCUMENT_ROWS_PAY'])
				)
			),
			array(
				"text" => "Заказы",
				"style" => "header"
			),
			array(
				"style" => "tableExample",
				"table" => array(
					"headerRows" => 1,
					"widths" => "auto",
					"body" => prepare_document_rows_order_pdf($document['DOCUMENT_ROWS_ORDER'])
				)
			),
			array(
				"text" => "Проводки",
				"style" => "header"
			),
			array(
				"style" => "tableExample",
				"table" => array(
					"headerRows" => 1,
					"body" => prepare_document_rows_transaction_pdf($document['DOCUMENT_ROWS_TRANSACTION'])
				)
			)
		),
		"styles" => array(
			"header_top" => array(
				"fontSize" => 10,
				"bold" => true
			),
			"header" => array(
				"fontSize" => 18,
				"bold" => true
			),
			"subheader" => array(
				"fontSize" => 15,
				"bold" => true
			),
			"quote" => array(
				"italics" => true
			),
			"small" => array(
				"fontSize" => 8
			),
			"tableExample" => array(
				"fontSize" => 8,
				"margin" => array(-20, 5, 0, 5)
			)
		)
	);
	
	$year = date('Y', strtotime($date_script));
	$month = date('m', strtotime($date_script));
	$path = 'payReportDocuments/' . $year . '/' . $month;
	$file_name = $path . '/' . $driver_id . '_' . $date_script;
	$file_name_pdf = $path . '/' . $driver_id . '_' . $date_script . '_pdf';
	
	if (!file_exists($path)) {
		mkdir($path, 0766, true);
	}
	
	if (isset($document)) {
		$fp = fopen($file_name . '.json', 'w');
		
		if ($fp) {
			fwrite ($fp, json_encode($document));
			fclose ($fp);
		}
	}
	
	if (isset($pdf_json)) {
		$fp = fopen($file_name_pdf . '.json', 'w');
		
		if ($fp) {
			fwrite ($fp, json_encode($pdf_json));
			fclose ($fp);
		}
	}
	
	return $file_name_pdf . '.json';
}

function save_xml_file($xml_arr, $date_script) {
	$year = date('Y', strtotime($date_script));
	$month = date('m', strtotime($date_script));
	$path = 'payReportDocuments/' . $year . '/' . $month;
	$file_name = $path . '/' . $date_script . '.xml';
	
	if (!file_exists($path)) {
		mkdir($path, 0766, true);
	}
	
	$domtree = new DOMDocument('1.0', 'UTF-8');

    /* create the root element of the xml tree and add attributes */
    $xmlRoot = $domtree->createElement($xml_arr['account']['title']);
    
    foreach ($xml_arr['account']['attribute'] as $value) {
	    $xmlAttribute = $domtree->createAttribute($value['title']);
	    $xmlAttribute->value = $value['value'];
	    $xmlRoot->appendChild($xmlAttribute);
    }
    
    /* append it to the document created */
    $xmlRoot = $domtree->appendChild($xmlRoot);
    
    $payments = $domtree->createElement($xml_arr['payments']['title']);
    
    foreach ($xml_arr['payments']['entries'] as $value) {
    	if ($value != NULL) {
	    	$driver = $domtree->createElement($value['driver']['title']);
		    $xmlAttribute = $domtree->createAttribute($value['driver']['attribute']['title']);
	    	$xmlAttribute->value = $value['driver']['attribute']['value'];
		    $driver->appendChild($xmlAttribute);
	    
		    $name_last = $domtree->createElement($value['driver']['name_last']['title'], $value['driver']['name_last']['value']);
	    	$driver->appendChild($name_last);
	    
		    $name_first = $domtree->createElement($value['driver']['name_first']['title'], $value['driver']['name_first']['value']);
		    $driver->appendChild($name_first);
	    
	    	$name_name = $domtree->createElement($value['driver']['name_name']['title'], $value['driver']['name_name']['value']);
		    $driver->appendChild($name_name);
	    
		    $bank = $domtree->createElement($value['driver']['bank']['title'], $value['driver']['bank']['value']);
	    	$driver->appendChild($bank);
	    
		    $bank_branch = $domtree->createElement($value['driver']['bank_branch']['title'], $value['driver']['bank_branch']['value']);
		    $driver->appendChild($bank_branch);
	    
	    	$account_number = $domtree->createElement($value['driver']['account_number']['title'], $value['driver']['account_number']['value']);
		    $driver->appendChild($account_number);
	    
		    $sum = $domtree->createElement($value['driver']['sum']['title'], $value['driver']['sum']['value']);
	    	$driver->appendChild($sum);
	    
		    $payments->appendChild($driver);
	    }
    }
    
    $payments = $xmlRoot->appendChild($payments);

    $payment_order = $domtree->createElement($xml_arr['payment_order']['title'], $xml_arr['payment_order']['value']);
    $payment_order = $xmlRoot->appendChild($payment_order);

    $date_payment_order = $domtree->createElement($xml_arr['date_payment_order']['title'], $xml_arr['date_payment_order']['value']);
    $date_payment_order = $xmlRoot->appendChild($date_payment_order);

    $sum_control = $domtree->createElement($xml_arr['sum_control']['title']);
    $count_entries = $domtree->createElement($xml_arr['sum_control']['count_entries']['title'], $xml_arr['sum_control']['count_entries']['value']);
    $sum_entries = $domtree->createElement($xml_arr['sum_control']['sum_entries']['title'], $xml_arr['sum_control']['sum_entries']['value']);
    $sum_control->appendChild($count_entries);
    $sum_control->appendChild($sum_entries);
    $sum_control = $xmlRoot->appendChild($sum_control);

    /* store xml */
    file_put_contents($file_name, $domtree->saveXML());
}

function convert_arr_to_driver_payout_report($sums, $url, $date_beg, $date_end) {
	return array(	
		'driver_payout_report_datetime_begin' => date('Y-m-d H:i:s', strtotime($date_beg)),
		'driver_payout_report_datetime_end' => date('Y-m-d H:i:s', strtotime($date_end)),
		'driver_payout_report_driver_id' => $sums['DRIVER_ID'],
		'driver_payout_report_total_orders' => $sums['TOTAL_ORDERS'],
		'driver_payout_report_sum_orders' => $sums['SUM_ORDERS'],
		'driver_payout_report_sum_fraud' => $sums['SUM_FRAUD'],
		'driver_payout_report_sum_tips' => $sums['SUM_TIPS'],
		'driver_payout_report_sum_transactions' => $sums['SUM_TRANSACTIONS'],
		'driver_payout_report_percent_gett' => $sums['PERCENT_GETT'],
		'driver_payout_report_sum_percent_gett' => $sums['SUM_PERCENT_GETT'],
		'driver_payout_report_percent_company' => $sums['PERCENT_COMPANY'],
		'driver_payout_report_sum_percent_company' => $sums['SUM_PERCENT_COMPANY'],
		'driver_payout_report_sum_pay_driver' => $sums['SUM_PAY_DRIVER'],
		'driver_payout_report_url' => $url,
	);
}

function build_driver_payout_datestamp($id) {
	return array(
		'driver_payout_datestamp_date' => date('Y-m-d', time()),
		'driver_payout_datestamp_report_id' => $id
	);
}

function build_driver_payout_report_status($id) {
	return array(
		'driver_payout_report_status_remark' => 'автоматическое создание зарплатной ведомости',
		'driver_payout_report_status_status' => DriverPayReportStatus::CREATED,
		'driver_payout_report_status_report_id' => $id
	);
}

function store_driver_payout_report($db, $sums, $url, $date_beg, $date_end) {
	if ($sums['TOTAL_ORDERS'] <= 0) {
		goto ret;
	}
	
	$sums = convert_arr_to_driver_payout_report($sums, $url, $date_beg, $date_end);
	
	pg_query($db, "BEGIN");
	
	$driverPayoutReport = new driverPayoutReportClass($sums);
	$result = $driverPayoutReport->saveDriverPayoutReport($db);
        
    if (!$result) {
		pg_query($db, "ROLLBACK");
		goto ret;
	} //!$result

	$input = build_driver_payout_datestamp($driverPayoutReport->selectDriverPayoutReportId());
	
	$driverPayoutDatestamp = new driverPayoutDatestampClass($input);
	$result = $driverPayoutDatestamp->saveDriverPayoutDatestamp($db);
        
    if (!$result) {
		pg_query($db, "ROLLBACK");
		goto ret;
	} //!$result

	$input = build_driver_payout_report_status($driverPayoutReport->selectDriverPayoutReportId());
	
	$driverPayoutReportStatus = new driverPayoutReportStatusClass($input);
	$result = $driverPayoutReportStatus->saveDriverPayoutReportStatus($db);
        
    if (!$result) {
		pg_query($db, "ROLLBACK");
		goto ret;
	} //!$result
	
	pg_query($db, "COMMIT");
	
	ret:
	
	return $sums;
}

// $driver_type = $argv[1];

// calculate period of pay calculation
// TODO : fix monday calculation, ie if now is monday, then strtotime -1 monday
$date_beg = date('Y-m-d 00:00:00',strtotime('-2 Monday')); // two mondays ago
$date_end = date('Y-m-d 23:59:59',strtotime('-1 Sunday')); // last sunday

$date_script = date('Y-m-d', time());

$document = array();

$today = date('Y-m-d', time());

$xml_arr = array(
	'account' => array(
		'title' => 'СчетаПК',
		'attribute' => array(
			array(
				'title' => 'ДатаФормирования',
				'value' => $today
			),
			array(
				'title' => 'НомерДоговора',
				'value' => '44060952'
			),
			array(
				'title' => 'ДатаДоговора',
				'value' => '2016-01-21'
			),
			array(
				'title' => 'НаименованиеОрганизации',
				'value' => "ООО 'АвтоСтройСервис'"
			),
			array(
				'title' => 'РасчетныйСчетОрганизации',
				'value' => '40702810744050011197'
			),
			array(
				'title' => 'ИНН',
				'value' => '5405961636'
			),
		)
	),
	'payments' => array(
		'title' => 'ЗачислениеЗарплаты',
		'entries' => array()
	),
	'payment_order' => array(
		'title' => 'ПлатежноеПоручение',
		'value' => 1011
	),
	'date_payment_order' => array(
		'title' => 'ДатаПлатежногоПоручения',
		'value' => $today
	),
	'sum_control' => array(
		'title' => 'КонтрольныеСуммы',
		'count_entries' => array(
			'title' => 'КоличествоЗаписей',
			'value' => null
		),
		'sum_entries' => array(
			'title' => 'СуммаИтого',
			'value' => null
		)
	)
);

foreach (DriverType::enumToArray() as $driver_type) {
	if ($driver_type == DriverType::EMPLOYEE_RENT || $driver_type == DriverType::OUTSOURCE) {
		$drivers = get_all_drivers($db, $driver_type);
		foreach($drivers as $key => $value) {
			// TODO : insert check for each driver for the last time of pay report generated
			$orders = get_all_orders($db, $value['gett_id'], $date_beg, $date_end);
			$transactions = get_all_transactions($db, $value['driver_id'], $date_beg, $date_end);
		
			$document[$value['driver_id']] = build_document_information($orders, $transactions, $value);
		
			$url = save_json_file($document[$value['driver_id']], $value['driver_id'], $date_script);
			
			$xml_arr['sum_control']['sum_entries']['value'] += $document[$value['driver_id']]['DOCUMENT_HEADER']['SUM_PAY_DRIVER'];
			$xml_arr['payments']['entries'][] = $document[$value['driver_id']]['BANK_PAYMENT'];
			unset($document[$value['driver_id']]['BANK_PAYMENT']);
			
			var_dump (store_driver_payout_report($db, $document[$value['driver_id']]['DOCUMENT_HEADER'], $url, $date_beg, $date_end));
		}
	} else if ($driver_type == DriverType::EMPLOYEE_PAY) {
		$sums = null;
	
		$drivers = get_all_drivers($db, $driver_type);
		$motivations = get_motivation($db, $date_beg, $date_end);
	
		foreach($drivers as $driver_key => $driver_value) {
			$shifts = get_all_shifts($db, $driver_value['driver_id'], $date_beg, $date_end);
		
			$motivation_pay = array();
		
			foreach($shifts as $shift_key => $shift_value) {
				$orders = get_all_orders_with_fraud($db, $driver_value['gett_id'], $shift_value['schedule_datetime_begin_shift'], $shift_value['schedule_datetime_end_shift_vehicle']);
			
				$sums = calculate_motivation_pay($orders, $motivations, $sums);
			
				$motivation_pay[$driver_value['driver_id']][$shift_value['schedule_date']]['SHIFT_PAY_DRIVER'] = $sums['SHIFT_PAY_DRIVER'];
				$motivation_pay[$driver_value['driver_id']][$shift_value['schedule_date']]['SHIFT_ORDER_COUNT'] = $sums['SHIFT_ORDER_COUNT'];
				$motivation_pay[$driver_value['driver_id']][$shift_value['schedule_date']]['SHIFT_ORDER_SUM'] = $sums['SHIFT_ORDER_SUM'];
				$motivation_pay[$driver_value['driver_id']][$shift_value['schedule_date']]['SHIFT_INFORMATION'] = $shift_value;
			}
		
			// TODO : include fraud transactions in print, but display hint that they are not included in the calculation
			$transactions = get_all_transactions($db, $driver_value['driver_id'], $date_beg, $date_end);//, array(TransactionsSubtype::FRAUD, TransactionsSubtype::ORDER_CORRECTION));
		
			$document[$driver_value['driver_id']] = build_document_information(null, $transactions, $driver_value, $motivation_pay, $sums);
		
			$url = save_json_file($document[$driver_value['driver_id']], $driver_value['driver_id'], $date_script);
			
			$xml_arr['sum_control']['sum_entries']['value'] += $document[$value['driver_id']]['DOCUMENT_HEADER']['SUM_PAY_DRIVER'];
			$xml_arr['payments']['entries'][] = $document[$value['driver_id']]['BANK_PAYMENT'];
			unset($document[$value['driver_id']]['BANK_PAYMENT']);
			
			var_dump (store_driver_payout_report($db, $document[$value['driver_id']]['DOCUMENT_HEADER'], $url, $date_beg, $date_end));
		}
	}
}

$xml_arr['sum_control']['count_entries']['value'] = count($xml_arr['payments']['entries']);

save_xml_file($xml_arr, $date_script);
?>