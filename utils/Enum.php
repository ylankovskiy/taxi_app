<?php
abstract class BasicEnum
{
  private static $constCacheArray = NULL;
  
  private static function getConstants()
  {
    if ( self::$constCacheArray == NULL ) {
      self::$constCacheArray = array();
    } //self::$constCacheArray == NULL
    $calledClass = get_called_class();
    if ( !array_key_exists( $calledClass, self::$constCacheArray ) ) {
      $reflect                               = new ReflectionClass( $calledClass );
      self::$constCacheArray[ $calledClass ] = $reflect->getConstants();
    } //!array_key_exists( $calledClass, self::$constCacheArray )
    return self::$constCacheArray[ $calledClass ];
  }
  
  public static function isValidName( $name, $strict = false )
  {
    $constants = self::getConstants();
    
    if ( $strict ) {
      return array_key_exists( $name, $constants );
    } //$strict
    
    $keys = array_map( 'strtolower', array_keys( $constants ) );
    return in_array( strtolower( $name ), $keys );
  }
  
  public static function isValidValue( $value, $strict = true )
  {
    $values = array_values( self::getConstants() );
    return in_array( $value, $values, $strict );
  }
  
  public static function enumToArray() {
  	return self::getConstants();
  }
}
?>