<?php
$patterns    = array(
   '/views',
  '/controllers',
  '/employee',
  '/driver',
  '/main',
  '/order',
  '/pay',
  '/schedule',
  '/transaction',
  '/vehicle',
  '/utils',
  '/select',
  '/classes',
  '/bank' 
);
$replacement = '';
$path        = str_replace( $patterns, $replacement, getcwd() );

error_log( $path );
chdir( $path );

require_once "db.php";
require_once "utils/helpers/funcs.php";
require_once "utils/config/driverConfig.php";
require_once "utils/config/gettConfig.php";
require_once "utils/helpers/response.php";

session_start();

function get_drivers( $db, $status, $type )
{
  $type_comparison = "=";
  if ($type < 0) {
  	$type_comparison = "!=";
  	$type = abs($type);
  }

  $status_comparison = "=";
  if ($status < 0) {
  	$status_comparison = "!=";
  	$status = abs($status);
  }
    
  $ret_arr = array();
  
  $query = 'SELECT 
			driver.*,
			telephone_book.*
		FROM
			postgres.public.driver as driver
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.telephone_book
		) AS telephone_book ON driver.driver_telephone_book_id=telephone_book.telephone_book_id
		';
  
  if ( isset( $status ) ) {
    switch ( $status ) {
      case DriverStatus::WORKING:
        $query .= 'WHERE
					driver.driver_status' . $status_comparison . DriverStatus::WORKING;
        break;
      case DriverStatus::FIRED:
        $query .= 'WHERE
					driver.driver_status' . $status_comparison . DriverStatus::FIRED;
        break;
      default:
        break;
    } //$status
  } //isset( $status )
  
  if ( isset( $type ) ) {
    switch ( $type ) {
      case DriverType::EMPLOYEE_RENT:
        if ( isset( $status ) ) {
          $query .= ' and ';
        } //isset( $status )
        else {
          $query .= 'WHERE ';
        }
        $query .= 'driver.driver_type' . $type_comparison . DriverType::EMPLOYEE_RENT;
        break;
      case DriverType::EMPLOYEE_PAY:
        if ( isset( $status ) ) {
          $query .= ' and ';
        } //isset( $status )
        else {
          $query .= 'WHERE ';
        }
        $query .= 'driver.driver_type' . $type_comparison . DriverType::EMPLOYEE_PAY;
        break;
      case DriverType::OUTSOURCE:
        if ( isset( $status ) ) {
          $query .= ' and ';
        } //isset( $status )
        else {
          $query .= 'WHERE ';
        }
        $query .= 'driver.driver_type' . $type_comparison . DriverType::OUTSOURCE;
        break;
      default:
        break;
    } //$type
  } //isset( $type )
  
  $query_name = "get_drivers_query$type$status";
  $result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
	$query_name 
  ) );
  
  if ( !$result || pg_num_rows( $result ) == 0 ) {
	$result = pg_prepare( $db, $query_name, $query );
  } //!$result || pg_num_rows( $result ) == 0
		
  $result = pg_execute($db, $query_name, array());
  
  if ( !$result ) {
		$message = 'Произошла ошибка получения данных';
		$ret_arr = prepare_response_error_arr($db, $message, null);
    
    return $ret_arr;
    goto ret;
  } //!$result
  
  while ( $row = pg_fetch_assoc( $result ) ) {
    $ret_arr[] = $row;
//       'name_last' => $row[ 'name_last' ],
//      'name_first' => $row[ 'name_first' ],
//      'name_name' => $row[ 'name_name' ],
//      'telephone' => $row[ 'telephone' ],
//      'driver_status' => $row[ 'driver_status' ],
//      'driver_type' => $row[ 'driver_type' ],
//      'date_birth' => $row[ 'date_birth' ],
//      'driver_remark' => $row[ 'driver_remark' ],
//      'lic_number' => $row[ 'lic_number' ],
//      'passport_series' => $row[ 'passport_series' ],
//      'passport_number' => $row[ 'passport_number' ],
//      'passport_date_given' => $row[ 'passport_date_given' ],
//      'passport_authority_given' => $row[ 'passport_authority_given' ],
//      'passport_address_living' => $row[ 'passport_address_living' ],
//      'actual_address_living' => $row[ 'actual_address_living' ],
//      'driver_datetime_created' => $row[ 'driver_datetime_created' ],
//      'driver_employee_added' => $row[ 'driver_employee_added' ],
//      'telephone_book_id' => $row[ 'telephone_book_id' ],
//      'driver_id' => $row[ 'driver_id' ]
  } //$row = pg_fetch_assoc( $result )
  
  ret:
  
  return $ret_arr;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

if ( isset( $clean_json[ 'method_status' ] ) || isset( $clean_json[ 'method_type' ] ) ) {
  $status = DriverStatus::isValidValue( intval( $clean_json[ 'method_status' ] ) ) ? $clean_json[ 'method_status' ] : null;
  $type   = DriverType::isValidValue( intval( $clean_json[ 'method_type' ] ) ) ? $clean_json[ 'method_type' ] : null;

  if ( isset( $status ) || isset( $type ) ) {
    $result = get_drivers( $db, $status, $type );
    
    echo json_encode( $result );
  } //isset( $status ) || isset( $type )
  else {
    echo json_encode( array ());
  }
  
} //isset( $_REQUEST[ 'method' ] )
?>