<?php
$patterns    = array(
   '/views',
  '/controllers',
  '/employee',
  '/driver',
  '/main',
  '/order',
  '/pay',
  '/schedule',
  '/transaction',
  '/vehicle',
  '/utils',
  '/select',
  '/classes',
  '/bank' 
);
$replacement = '';
$path        = str_replace( $patterns, $replacement, getcwd() );

error_log( $path );
chdir( $path );

require_once "db.php";
require_once "utils/helpers/funcs.php";
require_once "utils/config/vehicleConfig.php";
require_once "utils/helpers/response.php";

session_start();

function get_vehicles( $db, $status, $type )
{
  $ret_arr = array();
  
  $query = 'SELECT 
			vehicle.*
		FROM
			postgres.public.vehicle as vehicle
		';
  
  if ( isset( $status ) ) {
    switch ( $status ) {
      case VehicleStatus::WORKING:
        $query .= 'WHERE
					vehicle.vehicle_status=' . VehicleStatus::WORKING;
        break;
      case VehicleStatus::REPAIR:
        $query .= 'WHERE
					vehicle.vehicle_status=' . VehicleStatus::REPAIR;
        break;
      case VehicleStatus::WRITTEN_OFF:
        $query .= 'WHERE
					vehicle.vehicle_status=' . VehicleStatus::WRITTEN_OFF;
        break;
      default:
        break;
    } //$status
  } //isset( $status )
  
  if ( isset( $type ) ) {
    switch ( $type ) {
      case VehicleType::COMPANY_OWN:
        if ( isset( $status ) ) {
          $query .= ' and ';
        } //isset( $status )
        else {
          $query .= 'WHERE ';
        }
        $query .= 'vehicle.vehicle_type=' . VehicleType::COMPANY_OWN;
        break;
      case VehicleType::DRIVER_OWN:
        if ( isset( $status ) ) {
          $query .= ' and ';
        } //isset( $status )
        else {
          $query .= 'WHERE ';
        }
        $query .= 'vehicle.vehicle_type=' . VehicleType::DRIVER_OWN;
        break;
      default:
        break;
    } //$type
  } //isset( $type )
  
  $query_name = "get_vehicles_query$type$status";
  $result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
	$query_name 
  ) );
  
  if ( !$result || pg_num_rows( $result ) == 0 ) {
	$result = pg_prepare( $db, $query_name, $query );
  } //!$result || pg_num_rows( $result ) == 0
		
  $result = pg_execute($db, $query_name, array());
  
  if ( !$result ) {
		$message = 'Произошла ошибка получения данных';
		$ret_arr = prepare_response_error_arr($db, $message, null);
    
    return $ret_arr;
    exit;
  } //!$result
  
  while ( $row = pg_fetch_assoc( $result ) ) {
  	$ret_arr[] = array(
  			'id' => $row['vehicle_id'],
  			'text' => "$row[vehicle_make] $row[vehicle_model] $row[vehicle_year_production] $row[vehicle_plate_number]"
  	);
//     $ret_arr[] = $row;
  } //$row = pg_fetch_assoc( $result )
  
  return $ret_arr;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

if ( isset( $clean_json[ 'method_status' ] ) || isset( $clean_json[ 'method_type' ] ) ) {
  $status = isset( $clean_json[ 'method_status' ] ) && VehicleStatus::isValidValue( intval( $clean_json[ 'method_status' ] ) ) ? $clean_json[ 'method_status' ] : null;
  $type   = isset( $clean_json[ 'method_type' ] ) && VehicleType::isValidValue( intval( $clean_json[ 'method_type' ] ) ) ? $clean_json[ 'method_type' ] : null;
  
  if ( isset( $status ) || isset( $type ) ) {
    $result = get_vehicles( $db, $status, $type );
    
    echo json_encode( $result );
  } //isset( $status ) || isset( $type )
  else {
    echo json_encode( array ());
  }
} //isset( $clean_json[ 'method_status' ] ) || isset( $clean_json[ 'method_type' ] )
?>