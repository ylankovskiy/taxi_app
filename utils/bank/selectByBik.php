<?php
$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
    '/bank',
    '/configuration'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/response.php";
require_once "utils/helpers/funcs.php";

session_start();

/*
 * 
 * Номер БИК	bik
 * Корреспондентский счет	ks
 * Полное наименование	name
 * Краткое наименование	namemini
 * Индекс	index
 * Населенный пункт	city
 * Адрес	address
 * Телефон	phone
 * Код по ОКАТО	okato
 * Код ОКПО	okpo
 * Регистрационный номер	regnum
 * Срок прохождения документов (дней)	srok
 * Дата добавление информации	dateadd
 * Дата последнего изменения информации	datechange
 * 
 */
function get_bank_in_api($url)
{
    //  Initiate curl
    $ch = curl_init();
    // Disable SSL verification
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    // Will return the response, if false it print the response
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    // Set the url
    curl_setopt($ch, CURLOPT_URL, $url);
    // Execute
    $result = curl_exec($ch);
    // Closing
    curl_close($ch);
    
    return json_decode($result, true);
}

function get_bank_in_db($db, $bik)
{
    $ret_arr = array();
    
    $query = 'SELECT 
			pay_project.*
		FROM
			postgres.public.pay_project as pay_project
		WHERE
			pay_project.pay_project_bank_bik=$1';
    
    $result = pg_prepare($db, "get_pay_project_query", $query);
    $result = pg_execute($db, "get_pay_project_query", array(
        $bik
    ));
    
    if (!$result) {
        $message = 'Произошла ошибка получения данных';
        $ret_arr = prepare_response_error_arr($db, $message, null);
        
        goto ret;
    } //!$result
    
    while ($row = pg_fetch_assoc($result)) {
        $ret_arr[] = $row;
    } //$row = pg_fetch_assoc( $result )
    
ret:
    
    return $ret_arr;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

if (isset($clean_json['bik'])) {
    $bik = $clean_json['bik'];
    
    $result = get_bank_in_db($db, $bik);
    
    if ($result['ERROR_CODE'] == -1 || count($ret_arr) == 0) {
        $url  = "http://www.bik-info.ru/api.html?type=json&bik=$bik";
        $temp = get_bank_in_api($url);
        
        if (isset($temp) && count($temp) > 0) {
            if (isset($temp['name'])) {
                $result = array(
                    'pay_project_bank_name' => $temp['name']
                );
            } //isset($temp['name'])
            else {
                $message = isset($temp['error']) ? $temp['error'] : 'Произошла ошибка получения данных';
                $result  = prepare_response_error_arr($db, $message, null);
            }
        } //isset($temp) && count($temp) > 0
    } //$result['ERROR_CODE'] == -1 || count($ret_arr) == 0
    echo json_encode($result);
    
} //isset($clean_json['bik'])
else {
    $message = 'Отсутствует параметр БИК';
    $result  = prepare_response_error_arr($db, $message, null);
    echo json_encode($result);
}
?>