<?php
	function prepare_response_error_arr($db, $message, $post) {
		$ret_arr = array();
		
		$ret_arr['ERROR_CODE'] = -1;
		$ret_arr['ERROR_MESSAGE'] = $message;
		$ret_arr['DB_ERROR'] = pg_last_error($db);
		$ret_arr['DB_NOTICE'] = pg_last_notice($db);
		$r = pg_get_result($db);
		$ret_arr['RESULT_ERROR'] = pg_result_error($r);
		$ret_arr['ARRAY'] = $post;
		
		return $ret_arr;
	}
	
	function prepare_response_success_arr($db, $message) {
		$ret_arr = array();
				
		$ret_arr['ERROR_CODE'] = 1;
		$ret_arr['ERROR_MESSAGE'] = $message;
		
		return $ret_arr;
	}
?>