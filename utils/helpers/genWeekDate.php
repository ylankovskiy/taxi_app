<?php
date_default_timezone_set("Asia/Krasnoyarsk");

function gen_weeks($start, $end) {

// start and end must be timestamps !!!!
    //$start = 1346976000;  //  Thu 2012-09-06
    //$end   = 1348704000;  //  Tue 2012-09-26

    $i = 0;

// generate the weeks
    $weeks = generateWeeks($start, $end);
// diaplay the weeks
    $html_opt = '';
    //echo 'From: '.fDate($start).'<br>';
    $weeks = array_reverse($weeks);
    foreach ($weeks as $week){

        //echo fDate($week['start']).' '.fDate($week['end']).'<br>';
        $html_opt .=  "<option data-date-beg=".date('d.m.Y', $week['start'])." data-date-end=".date('d.m.Y', $week['end'])." value=".$i.">" . date('d.m.Y', $week['start']) . " - " . date('d.m.Y', $week['end'])  . "</option>";
        $i++;
    }
    //echo 'To: '.fDate($end).'<br>';
    return $html_opt;
}
/*   outputs this:
From: Thu 2012-09-06
Thu 2012-09-06 Sun 2012-09-09
Mon 2012-09-10 Sun 2012-09-16
Mon 2012-09-17 Sun 2012-09-23
Mon 2012-09-24 Wed 2012-09-26
To: Wed 2012-09-26
*/


// $start and $end must be unix timestamps (any range)
//  returns an array of arrays with 'start' and 'end' elements set
//  for each week (or part of week) for the given interval
//  return values are also in timestamps
function generateWeeks($start,$end){
    $ret = array();

    $start = E2D($start);
    $end = E2D($end);
    $start = prevMonday($start);
    $end = nextSunday($end);


    $ns = nextSunday($start);
    while(true){
        if($ns>=$end)
        {
            insert($ret,$start,$end);
            return $ret;
        }
        insert($ret,$start,$ns);
        $start = $ns + 1;
        $ns+=7;
    }
}



// helper function to append the array and convert back to unix timestamp
function insert(&$arr, $start, $end)
{
    $arr[] = array('start'=>D2E($start), 'end'=>D2E($end));
}
// recives any date on CD format returns next Sunday on CD format
function nextSunday($Cdate)
    {
        return $Cdate + 6  - $Cdate % 7;
    }
// recives any date on CD format returns previous Monday on CD format // finaly not used here
function prevMonday($Cdate){return $Cdate      - $Cdate % 7;}
// recives timestamp returns CD
function E2D($what){return floor($what/86400+3);}     // floor may be optional in some circunstances
// recives CD returns timestamp
function D2E($what){return ($what-3)*86400;}          // 24*60*60
// just format the timestamp for output, you can adapt it to your needs
function fDate($what){return date('D Y-m-d',$what);}

//print_r(gen_weeks(strtotime("-8 week"),strtotime("+1 week")));

?>