<?php

/*
*
*	trim spaces from a string, if passed value is an array make a recursive call
*
*/
function trim_array($value) 
{
	if (!is_array($value)) {
    return trim($value);
  }
 
  return array_map('trim_array', $value);
}

/*
*
*	convert all true and false types of postgresql to proper booleans
*
*	support for two levels
*
*/

function convert_bool_to_bit($value) {
	return $value === 't' ? true : false;
}

function parameter_set($value) {
	return isset($value);// && ($value === true);//strlen($value) > 0;
}

function is_not_null($val){
    return !is_null($val) && strlen($val) > 0;
}

function get_json_array_from_request($request) {
	return trim_array(json_decode($request, true));
}

?>