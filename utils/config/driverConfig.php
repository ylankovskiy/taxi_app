<?php
$patterns    = array(
   '/views',
  '/controllers',
  '/employee',
  '/driver',
  '/main',
  '/order',
  '/pay',
  '/schedule',
  '/transaction',
  '/vehicle',
  '/utils',
  '/select',
  '/classes',
  '/bank' 
);
$replacement = '';
$path        = str_replace( $patterns, $replacement, getcwd() );

error_log( $path );
chdir( $path );
require_once "utils/Enum.php";

function getDriverTypeText($type) {
	if ($type == null) {
		return;
	}
	$type_text = '';
	switch($type) {
		case DriverType::EMPLOYEE_RENT:
			$type_text = 'арендная схема';
			break;
		case DriverType::EMPLOYEE_PAY:
			$type_text = 'зарплатная схема';
			break;
		case DriverType::OUTSOURCE:
			$type_text = 'привлеченный';
			break;
		default:
			break;
	}
	
	return $type_text;
}

function getDriverStatusText($status) {
	if ($status == null) {
		return;
	}
	$status_text = '';
	switch($status) {
		case DriverStatus::WORKING:
			$status_text = 'рабочий';
			break;
		case DriverStatus::FIRED:
			$status_text = 'уволен';
			break;
		default:
			break;
	}
	
	return $status_text;
}

abstract class DriverStatus extends BasicEnum
{
  const WORKING = 0;
  const FIRED = 1;
}

abstract class DriverType extends BasicEnum
{
  const EMPLOYEE_RENT = 0;
  const EMPLOYEE_PAY = 1;
  const OUTSOURCE = 2;
  const NOT_EMPLOYEE_RENT=-3;
  const NOT_EMPLOYEE_PAY=-1;
  const NOT_OUTSOURCE=-2;
}
?>