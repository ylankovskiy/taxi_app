<?php
$patterns    = array(
   '/views',
  '/controllers',
  '/employee',
  '/driver',
  '/main',
  '/order',
  '/pay',
  '/schedule',
  '/transaction',
  '/vehicle',
  '/utils',
  '/select',
  '/classes',
  '/bank' 
);
$replacement = '';
$path        = str_replace( $patterns, $replacement, getcwd() );

error_log( $path );
chdir( $path );
require_once "utils/Enum.php";

function getDriverPayReportRowTypeText($type) {
	if ($type == null) {
		return;
	}
	$type_text = '';
	switch($type) {
		case DriverPayReportRowType::ORDER:
			$type_text = 'заказ';
			break;
		case DriverPayReportRowType::TRANSACTION:
			$type_text = 'проводка';
			break;
		case DriverPayReportRowType::MOTIVATION:
			$type_text = 'мотивация';
			break;
		default:
			break;
	}
	
	return $type_text;
}

function getDriverPayReportStatusText($status) {
	if ($status == null) {
		return;
	}
	$status_text = '';
	switch($status) {
		case DriverPayReportStatus::CREATED:
			$status_text = 'сформирован';
			break;
		case DriverPayReportStatus::PAID:
			$status_text = 'проведен';
			break;
		case DriverPayReportStatus::CANCELLED:
			$status_text = 'отклонен';
			break;
		default:
			break;
	}
	
	return $status_text;
}

abstract class DriverPayReportRowType extends BasicEnum
{
  const ORDER = 0;
  const TRANSACTION = 1;
  const MOTIVATION = 2;
}

abstract class DriverPayReportStatus extends BasicEnum
{
  const CREATED = 0;
  const PAID = 1;
  const CANCELLED = 2;
}
?>