<?php
$patterns    = array(
   '/views',
  '/controllers',
  '/employee',
  '/driver',
  '/main',
  '/order',
  '/pay',
  '/schedule',
  '/transaction',
  '/vehicle',
  '/utils',
  '/select',
  '/classes',
  '/bank' 
);
$replacement = '';
$path        = str_replace( $patterns, $replacement, getcwd() );

error_log( $path );
chdir( $path );
require_once "utils/Enum.php";

function getScheduleTransactionsTypeText($type) {
	if ($type == null) {
		return;
	}
	$type_text = '';
	switch($type) {
		case ScheduleTransactionsType::INCOMING:
			$type_text = 'приход';
			break;
		case ScheduleTransactionsType::OUTGOING:
			$type_text = 'расход';
			break;
		default:
			break;
	}
	
	return $type_text;
}

function getScheduleTransactionsSubtypeText($subtype) {
	if ($subtype == null) {
		return;
	}
	$subtype_text = '';
	switch($subtype) {
		case ScheduleTransactionsSubtype::GASOLINE:
			$subtype_text = 'бензин';
			break;
		case ScheduleTransactionsSubtype::MONEY_COLLECTED:
			$subtype_text = 'прием наличных от водителя';
			break;
		case ScheduleTransactionsSubtype::MANUAL_TRANSACTION:
			$subtype_text = 'ручная проводка';
			break;
		case ScheduleTransactionsSubtype::VEHICLE_WASH:
			$subtype_text = 'мойка автомобиля';
			break;
		default:
			break;
	}
	
	return $subtype_text;
}

abstract class ScheduleStatusMask extends BasicEnum
{
  const DRIVER_NOT_SET = 0;
  const DRIVER_SET = 1;
  const DRIVER_ON_SHIFT = 2;
  const DRIVER_OFF_SHIFT_VEHICLE = 4;
  const DRIVER_OFF_SHIFT_MONEY = 8;
}

abstract class ScheduleTabIndex extends BasicEnum
{
  const ASSIGN_DRIVER = 0;
  const ASSIGN_ON_SHIFT = 1;
  const ASSIGN_OFF_SHIFT_VEHICLE = 2;
  const ASSIGN_OFF_SHIFT_MONEY = 3;
}

abstract class ScheduleTransactionsType extends BasicEnum
{
  const INCOMING = 0;
  const OUTGOING = 1;
}

abstract class ScheduleTransactionsSubtype extends BasicEnum
{
  const GASOLINE = 0;
  const MONEY_COLLECTED = 1;
  const MANUAL_TRANSACTION = 2;
  const VEHICLE_WASH = 3;
}
?>