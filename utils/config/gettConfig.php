<?php
$patterns    = array(
   '/views',
  '/controllers',
  '/employee',
  '/driver',
  '/main',
  '/order',
  '/pay',
  '/schedule',
  '/transaction',
  '/vehicle',
  '/utils',
  '/select',
  '/classes',
  '/bank' 
);
$replacement = '';
$path        = str_replace( $patterns, $replacement, getcwd() );

error_log( $path );
chdir( $path );
require_once "utils/Enum.php";

function getGettStatusText($status) {
	if ($status == null) {
		return;
	}
	$status_text = '';
	switch($status) {
		case GettStatus::ACTIVE:
			$status_text = 'активный';
			break;
		case GettStatus::INACTIVE:
			$status_text = 'заблокирован';
			break;
		default:
			break;
	}
	
	return $status_text;
}

abstract class GettStatus extends BasicEnum
{
	const ACTIVE = 0;
	const INACTIVE = 1;
}
?>