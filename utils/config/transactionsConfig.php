<?php
$patterns    = array(
   '/views',
  '/controllers',
  '/employee',
  '/driver',
  '/main',
  '/order',
  '/pay',
  '/schedule',
  '/transaction',
  '/vehicle',
  '/utils',
  '/select',
  '/classes',
  '/bank' 
);
$replacement = '';
$path        = str_replace( $patterns, $replacement, getcwd() );

error_log( $path );
chdir( $path );
require_once "utils/Enum.php";

function getTransactionsStatusText($status) {
	if ($status == null) {
		return;
	}
	$status_text = '';
	switch($status) {
		case TransactionsStatus::COMPLETE:
			$status_text = 'проведено';
			break;
		case TransactionsStatus::CANCELED:
			$status_text = 'отменено';
			break;
		default:
			break;
	}
	
	return $status_text;
}

function getTransactionsTypeText($type) {
	if ($type == null) {
		return;
	}
	$type_text = '';
	switch($type) {
		case TransactionsType::INCOMING:
			$type_text = 'приход';
			break;
		case TransactionsType::OUTGOING:
			$type_text = 'расход';
			break;
		default:
			break;
	}
	
	return $type_text;
}

function getTransactionsSubtypeText($subtype) {
	if ($subtype == null) {
		return;
	}
	$subtype_text = '';
	switch($subtype) {
		case TransactionsSubtype::GASOLINE:
			$subtype_text = 'бензин';
			break;
		case TransactionsSubtype::COMMISSION:
			$subtype_text = 'коммиссия';
			break;
		case TransactionsSubtype::DAILY_FEE:
			$subtype_text = 'ежедневное списание';
			break;
		case TransactionsSubtype::WEEKLY_FEE:
			$subtype_text = 'еженедельное списание';
			break;
		case TransactionsSubtype::MONTHLY_FEE:
			$subtype_text = 'ежемесячное списание';
			break;
		case TransactionsSubtype::VEHICLE_REPAIR:
			$subtype_text = 'ремонт автомобиля';
			break;
		case TransactionsSubtype::PENALTY:
			$subtype_text = 'штраф';
			break;
		case TransactionsSubtype::MOBILE:
			$subtype_text = 'мобильная связь';
			break;
		case TransactionsSubtype::CAB_FAIRS:
			$subtype_text = 'поездки';
			break;
		case TransactionsSubtype::MANUAL_TRANSACTION:
			$subtype_text = 'ручная проводка';
			break;
		case TransactionsSubtype::VEHICLE_WASH:
			$subtype_text = 'мойка';
			break;
		case TransactionsSubtype::FRAUD:
			$subtype_text = 'фрод';
			break;
		case TransactionsSubtype::ORDER_CORRECTION:
			$subtype_text = 'корректировка заказа';
			break;
		case TransactionsSubtype::OTHER:
			$subtype_text = 'другое';
			break;
		default:
			break;
	}
	
	return $subtype_text;
}

abstract class TransactionsStatus extends BasicEnum
{
  const COMPLETE = 0;
  const CANCELED = 1;
}

abstract class TransactionsType extends BasicEnum
{
  const INCOMING = 0;
  const OUTGOING = 1;
}

abstract class TransactionsSubtype extends BasicEnum
{
  const GASOLINE = 0;
  const COMMISSION = 1;
  const DAILY_FEE = 2;
  const WEEKLY_FEE = 3;
  const MONTHLY_FEE = 4;
  const VEHICLE_REPAIR = 5;
  const PENALTY = 6;
  const MOBILE = 7;
  const CAB_FAIRS = 8;
  const MANUAL_TRANSACTION = 9;
  const VEHICLE_WASH = 10;
  const FRAUD = 11;
  const ORDER_CORRECTION = 12;
  const OTHER = 13;
}
?>