<?php
$patterns    = array(
   '/views',
  '/controllers',
  '/employee',
  '/driver',
  '/main',
  '/order',
  '/pay',
  '/schedule',
  '/transaction',
  '/vehicle',
  '/utils',
  '/select',
  '/classes',
  '/bank' 
);
$replacement = '';
$path        = str_replace( $patterns, $replacement, getcwd() );

error_log( $path );
chdir( $path );
require_once "utils/Enum.php";

function getVehicleTypeText($type) {
	if ($type == null) {
		return;
	}
	$type_text = '';
	switch($type) {
		case VehicleType::COMPANY_OWN:
			$type_text = 'принадлежит компании';
			break;
		case VehicleType::DRIVER_OWN:
			$type_text = 'принадлежит водителю';
			break;
		default:
			break;
	}
	
	return $type_text;
}

function getVehicleStatusText($status) {
	if ($status == null) {
		return;
	}
	$status_text = '';
	switch($status) {
		case VehicleStatus::WORKING:
			$status_text = 'рабочий';
			break;
		case VehicleStatus::REPAIR:
			$status_text = 'в ремонте';
			break;
		case VehicleStatus::WRITTEN_OFF:
			$status_text = 'списан';
			break;
		default:
			break;
	}
	
	return $status_text;
}

abstract class VehicleType extends BasicEnum
{
  const COMPANY_OWN = 0;
  const DRIVER_OWN = 1;
  const ANY = 2;
}

abstract class VehicleStatus extends BasicEnum
{
  const WORKING = 0;
  const REPAIR = 1;
  const WRITTEN_OFF = 2;
  const ANY = 3;
}
?>