<?php
	function bot_detected() {
	  if (isset($_SERVER['HTTP_USER_AGENT']) && preg_match('/bot|crawl|slurp|spider/i', $_SERVER['HTTP_USER_AGENT'])) {
		return TRUE;
	  }
	  else {
		return FALSE;
	  }
	}
	
	function detect_mobile() {
		$useragent=$_SERVER['HTTP_USER_AGENT'];
		return preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4));
	}

	$page_names = array(
		'main.php' => 'Главная',
		'views/employee/employee.php' => 'Сотрудники',
		'views/driver/driver.php' => 'Водители',
		'views/vehicle/vehicle.php' => 'Автомобили',
		'views/order/order.php' => 'Заказы',
		'views/pay/pay.php' => 'Выплаты',
		'views/schedule/schedule.php' => 'Расписание',
		'views/transaction/transaction.php' => 'Проводки',
		'views/configuration/configuration.php' => 'Конфигуратор',
	);
	
	$page_url = isset($_REQUEST['url']) ? $_REQUEST['url'] : 'main.php';
	
	$mobile_device = detect_mobile();
?>

<!DOCTYPE html>
<html lang="ru">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=5">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php echo $page_names[$page_url]; ?></title>
    
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link href="font-awesome/css/font-awesome.css" rel="stylesheet">

    <link href="css/animate.css" rel="stylesheet">
    <link href="css/style.css" rel="stylesheet">
    
    <!-- Mainly scripts -->
    <script src="js/jquery-1.11.3.js"></script>
    <script src="js/bootstrap.js"></script>

    <!-- Custom and plugin javascript -->
    <script src="js/inspinia.js"></script>
    <script src="js/plugins/pace/pace.min.js"></script>
    <script src="js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

    <!-- Chosen -->
    <script src="js/plugins/chosen/chosen.jquery.js"></script>

   <!-- JSKnob -->
   <script src="js/plugins/jsKnob/jquery.knob.js"></script>

   <!-- Input Mask-->
    <script src="js/plugins/jasny/jasny-bootstrap.min.js"></script>

   <!-- Data picker -->
   <script src="js/plugins/datapicker/bootstrap-datepicker.js"></script>

   <!-- NouSlider -->
   <script src="js/plugins/nouslider/jquery.nouislider.min.js"></script>

   <!-- Switchery -->
   <script src="js/plugins/switchery/switchery.js"></script>

    <!-- IonRangeSlider -->
    <script src="js/plugins/ionRangeSlider/ion.rangeSlider.min.js"></script>

    <!-- iCheck -->
    <script src="js/plugins/iCheck/icheck.min.js"></script>

    <!-- MENU -->
    <script src="js/plugins/metisMenu/jquery.metisMenu.js"></script>

    <!-- Color picker -->
    <script src="js/plugins/colorpicker/bootstrap-colorpicker.min.js"></script>

    <!-- Clock picker -->
    <script src="js/plugins/clockpicker/clockpicker.js"></script>

    <!-- Image cropper -->
    <script src="js/plugins/cropper/cropper.min.js"></script>

    <!-- Date range use moment.js same as full calendar plugin -->
    <script src="js/plugins/fullcalendar/moment.min.js"></script>

    <!-- Date range picker -->
    <script src="js/plugins/daterangepicker/daterangepicker.js"></script>

    <!-- Select2 -->
    <script src="js/plugins/select2/select2.full.min.js"></script>

    <!-- TouchSpin -->
    <script src="js/plugins/touchspin/jquery.bootstrap-touchspin.min.js"></script>

    <!-- Tags Input -->
    <script src="js/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

    <!-- Dual Listbox -->
    <script src="js/plugins/dualListbox/jquery.bootstrap-duallistbox.js"></script>
    
    <script>
		window.mobileAndTabletcheck = function() {
		  var check = false;
		  (function(a){if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a)||/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0,4))) check = true;})(navigator.userAgent||navigator.vendor||window.opera);
		  return check;
		};
    </script>
    
    <style>
    	.hide {
    		display: none;
    	}
    	
    	.container {
    		margin-left: 5%!important;
    	}
    </style>
</head>

<body>
<?php	
	function get_current_employee($db, $login) {
		$ret_arr = array();
	    $params  = array(
	    	$login
	    );
    
    	$query = 'SELECT
			employee.*,
			position.*
			FROM postgres.public.employee as employee
			LEFT OUTER JOIN (SELECT * FROM postgres.public.position)
				AS position ON  position.position_id = employee.employee_position_id
			WHERE employee.employee_login = $1';
			
	    $query_name = "get_current_employee_query";
    	$result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
        	$query_name
	    ));
    
    	if (!$result || pg_num_rows($result) == 0) {
        	$result = pg_prepare($db, $query_name, $query);
	    } //!$result || pg_num_rows( $result ) == 0
    
    	$result = pg_execute($db, $query_name, $params);

	    if ($result) {
    	    while ($row = pg_fetch_assoc($result)) {
    	    	return $row;
	        } //$row = pg_fetch_assoc($result)
        
    	    pg_free_result($result);
	    } //$result
	}
	
	$employee = get_current_employee($db, getenv('REMOTE_USER'));
	
	$employee_name = $employee['employee_name_first'] . ' ' . $employee['employee_name_last'];
?>
<div id="wrapper">
    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">
                <li class="nav-header">
                    <div class="dropdown profile-element"> <span>
                            <img alt="image" class="img-circle" src="img/profile_small.png" />
                             </span>
                        <a data-toggle="dropdown" class="dropdown-toggle" href="dashboard_5.html#">
                            <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold"><?php echo $employee_name; ?></strong>
                             </span> <span class="text-muted text-xs block"><?php echo $employee['position_name']; ?> <b class="caret"></b></span> </span> </a>
                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
<!-- 
                            <li><a href="profile.html">Profile</a></li>
                            <li><a href="contacts.html">Contacts</a></li>
                            <li><a href="mailbox.html">Mailbox</a></li>
                            <li class="divider"></li>
 -->
                            <li><a href="javascript:logout()">Logout</a></li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        T1C
                    </div>
                </li>
                <li class="<?php echo $page_url == "views/order/order.php" ? 'active' : '';?>">
                    <a href="index.php?url=views/order/order.php"><i class="fa fa-credit-card"></i> <span class="nav-label">Заказы</span>  </a>
                </li>
                <li class="<?php echo $page_url == "views/employee/employee.php" ? 'active' : '';?>">
                    <a href="index.php?url=views/employee/employee.php"><i class="fa fa-users"></i> <span class="nav-label">Сотрудники</span>  </a>
                </li>
                <li class="<?php echo $page_url == "views/driver/driver.php" ? 'active' : '';?>">
                    <a href="index.php?url=views/driver/driver.php"><i class="fa fa-dashboard"></i> <span class="nav-label">Водители</span>  </a>
                </li>
                <li class="<?php echo $page_url == "views/vehicle/vehicle.php" ? 'active' : '';?>">
                    <a href="index.php?url=views/vehicle/vehicle.php"><i class="fa fa-cab"></i> <span class="nav-label">Автомобили</span>  </a>
                </li>
                <li class="<?php echo $page_url == "views/schedule/schedule.php" ? 'active' : '';?>">
                    <a href="index.php?url=views/schedule/schedule.php"><i class="fa fa-calendar"></i> <span class="nav-label">Расписание</span>  </a>
                </li>
                <li class="<?php echo $page_url == "views/pay/pay.php" ? 'active' : '';?>">
                    <a href="index.php?url=views/pay/pay.php"><i class="fa fa-bank"></i> <span class="nav-label">Зарплата</span>  </a>
                </li>
                <li class="<?php echo $page_url == "views/transaction/transaction.php" ? 'active' : '';?>">
                    <a href="index.php?url=views/transaction/transaction.php"><i class="fa fa-rub"></i> <span class="nav-label">Ручные Транзакции</span>  </a>
                </li>
                <li class="<?php echo $page_url == "views/configuration/configuration.php" ? 'active' : '';?>">
                    <a href="index.php?url=views/configuration/configuration.php"><i class="fa fa-gear"></i> <span class="nav-label">Конфигуратор</span>  </a>
                </li>
<!-- 
                <li>
                    <a><i class="fa fa-pie-chart"></i> <span class="nav-label">Отчеты</span> <span class="fa arrow"></span></a>
                    <ul class="nav nav-second-level">
                        <li><a href="index.html">Dashboard v.1</a></li>
                        <li><a href="dashboard_2.html">Dashboard v.2</a></li>
                        <li><a href="dashboard_3.html">Dashboard v.3</a></li>
                        <li><a href="dashboard_4_1.html">Dashboard v.4</a></li>
                        <li><a href="dashboard_5.html">Dashboard v.5</a></li>
                    </ul>
                </li>
 -->
            </ul>

        </div>
    </nav>

        <div id="page-wrapper" class="gray-bg">
        <div class="row border-bottom">
        <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
        <div class="navbar-header">
            <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
<!-- 
            <form role="search" class="navbar-form-custom" action="search_results.html">
                <div class="form-group">
                    <input type="text" placeholder="Search for something..." class="form-control" name="top-search" id="top-search">
                </div>
            </form>
 -->
        </div>
            <ul class="nav navbar-top-links navbar-right">
                <li>
                    <span class="m-r-sm text-muted welcome-message">Taxi 1C</span>
                </li>
<!-- 
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="dashboard_5.html#">
                        <i class="fa fa-envelope"></i>  <span class="label label-warning">16</span>
                    </a>
                    <ul class="dropdown-menu dropdown-messages">
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a7.jpg">
                                </a>
                                <div>
                                    <small class="pull-right">46h ago</small>
                                    <strong>Mike Loreipsum</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">3 days ago at 7:58 pm - 10.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/a4.jpg">
                                </a>
                                <div>
                                    <small class="pull-right text-navy">5h ago</small>
                                    <strong>Chris Johnatan Overtunk</strong> started following <strong>Monica Smith</strong>. <br>
                                    <small class="text-muted">Yesterday 1:21 pm - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="dropdown-messages-box">
                                <a href="profile.html" class="pull-left">
                                    <img alt="image" class="img-circle" src="img/profile.jpg">
                                </a>
                                <div>
                                    <small class="pull-right">23h ago</small>
                                    <strong>Monica Smith</strong> love <strong>Kim Smith</strong>. <br>
                                    <small class="text-muted">2 days ago at 2:30 am - 11.06.2014</small>
                                </div>
                            </div>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="mailbox.html">
                                    <i class="fa fa-envelope"></i> <strong>Read All Messages</strong>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
 -->
<!-- 
                <li class="dropdown">
                    <a class="dropdown-toggle count-info" data-toggle="dropdown" href="dashboard_5.html#">
                        <i class="fa fa-bell"></i>  <span class="label label-primary">8</span>
                    </a>
                    <ul class="dropdown-menu dropdown-alerts">
                        <li>
                            <a href="mailbox.html">
                                <div>
                                    <i class="fa fa-envelope fa-fw"></i> You have 16 messages
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="profile.html">
                                <div>
                                    <i class="fa fa-twitter fa-fw"></i> 3 New Followers
                                    <span class="pull-right text-muted small">12 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="grid_options.html">
                                <div>
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small">4 minutes ago</span>
                                </div>
                            </a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <div class="text-center link-block">
                                <a href="notifications.html">
                                    <strong>See All Alerts</strong>
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </li>
                    </ul>
                </li>
 -->


                <li>
                    <a href="javascript:logout()">
                        <i class="fa fa-sign-out"></i> Log out
                    </a>
                </li>
<!-- 
                <li>
                    <a class="right-sidebar-toggle">
                        <i class="fa fa-tasks"></i>
                    </a>
                </li>
 -->
            </ul>

        </nav>
        </div>

    <!-- Page Content -->
    <div class="container navbar-offset">
        <div class="row">
            <div class="col-md-12 col-xs-18">
<!--             	TODO: здесь используй alert-success для удачного сохранения и alert-danger для не удачного сохранения -->
            	<div class="hidden alert alert-success" id="div_alert">
					<strong>Success!</strong> Indicates a successful or positive action.
				</div>
                <script>
                	function logout() {
						var ajaxTimeout = 10000;
						var async = false;
						var host = window.location.host;
						var pathname = window.location.pathname;
						$.ajax({
							type: 'PUT',
							url: 'http://logout:logout@' + host + pathname,
							timeout: ajaxTimeout,
							async: async
						})
						.done(function(data, textStatus, jqXHR) {
					    	console.log('error performing logout status=' + textStatus);
					    	console.log(jqXHR);
					    	console.log(data);
						})
						.fail(function () {
							// should catch 401 error, in which case we have successfully logged out
							document.execCommand("ClearAuthenticationCache");
							window.location = "/taxi_app/";
						});
                	}
                	
                    function alertShowHide(status, message) {
                    	console.log(status, message, "alert");
                        if (status == 1) {
                            $('#div_alert').addClass('alert-success').html('<strong>' + message + '</strong>').removeClass('hidden');
                        } else if (status == -1){
                            $('#div_alert').addClass('alert-danger').html('<strong>' + message + '</strong>').removeClass('hidden');
                        }
                        setTimeout(
                            function() {
//                                $('#div_alert').hide();
                                $('#div_alert').html('').addClass('hidden').removeClass('alert-danger','alert-success');
                            }, 5000);
                    }
                    window.addEventListener("keydown", function(e){
                        /*
                         keyCode: 8
                         keyIdentifier: "U+0008"
                         */
                        console.log($(document.activeElement)[0])    ;
                        if(e.keyCode === 8 && !document.activeElement) {
                            e.preventDefault();
                            alert('Прейти на предидущую страницу');
                        }
                    });
                </script>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 col-xs-18">
