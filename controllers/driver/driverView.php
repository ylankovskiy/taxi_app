<?php
error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
    '/bank',
    '/configuration'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/response.php";
require_once "utils/helpers/funcs.php";
require_once "utils/config/driverConfig.php";
require_once "utils/config/gettConfig.php";

session_start();

function get_all_drivers($db)
{
    $ret_arr = array();
    
    $query = 'SELECT 
			driver.*,
			gett.*,
			telephone_book.*,
			vehicle.*,
			bank.*,
			pay_project.*
		FROM
			postgres.public.driver as driver
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.gett
		) AS gett ON driver.driver_id=gett.gett_driver_id
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.telephone_book
		) AS telephone_book ON driver.driver_telephone_book_id=telephone_book.telephone_book_id
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.vehicle
		) AS vehicle ON driver.driver_vehicle_id=vehicle.vehicle_id
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.bank
		) AS bank ON driver.driver_id=bank.bank_driver_id
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.pay_project
		  ) AS pay_project ON pay_project.pay_project_id=bank.bank_pay_project_id
		';
    
    $result = pg_prepare($db, "get_all_drivers_query", $query);
    $result = pg_execute($db, "get_all_drivers_query", array());
    
    if (!$result) {
        $message = 'Произошла ошибка получения данных';
        $ret_arr = prepare_response_error_arr($db, $message, null);
        
        return $ret_arr;
        exit;
    } //!$result
    
    while ($row = pg_fetch_assoc($result)) {
        $row['driver_type_text']   = getDriverTypetext($row['driver_type']);
        $row['driver_status_text'] = getDriverStatustext($row['driver_status']);
        $row['gett_status_text']   = getGettStatusText($row['gett_status']);
        
        $ret_arr[] = $row;
    } //$row = pg_fetch_assoc($result)
    
    return $ret_arr;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

if (isset($clean_json['method'])) {
    $method = $clean_json['method'];
    
    if ($method == 'view_all') {
        $result = get_all_drivers($db);
        
        echo json_encode($result);
    } //$method == 'view_all'
    else {
        $message = 'Не верно указан параметр метод';
        $ret_arr = prepare_response_error_arr($db, $message, null);
        
        echo json_encode($ret_arr);
    }
    
} //isset($clean_json['method'])
else {
    $message = 'Не указан параметр метод';
    $ret_arr = prepare_response_error_arr($db, $message, null);
    
    echo json_encode($ret_arr);
}
?>