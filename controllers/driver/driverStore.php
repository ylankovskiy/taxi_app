<?php
error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
    '/bank',
    '/configuration'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";
require_once "utils/helpers/response.php";
require_once "classes/bankClass.php";
require_once "classes/driverClass.php";
require_once "classes/gettClass.php";
require_once "classes/payProjectClass.php";
require_once "classes/telephoneBookClass.php";

session_start();

function store_driver($db, $post)
{
    $ret_arr = array();
    
	pg_query($db, 'BEGIN');
    
    /*
     *	Telephone book information
     *
     */
    if (isset($post['telephone_book_telephone'])) {
        $telephone_book = new telephoneBookClass($post);
        
        $result = $telephone_book->saveTelephoneBook($db);
        
        if (!$result) {
            $message = 'Произошла ошибка сохранения телефонных данных';
            $ret_arr = prepare_response_error_arr($db, $message, $telephone_book->selectParameters());
            
            goto ret;
        } //!$result
        
        if (!$telephone_book->selectTelephoneBookId()) {
            /*
             * TODO: temporary workaround implemented employeeing OIDs, to get the last insterted id
             *
             */
            $last_oid = pg_last_oid($result);
            $telephone_book->selectTelephoneBookFromDB($db, $last_oid);
        } //!$telephone_book->selectTelephoneBookId()
        
        // set telephone id for driver class
        $post['driver_telephone_book_id'] = $telephone_book->selectTelephoneBookId();
    } //isset($post['telephone'])
    
    /*
     *	Driver information
     *
     */
    
    $post['driver_date_birth']          = date('Y-m-d', strtotime($post['driver_date_birth']));
    $post['driver_passport_date_given'] = date('Y-m-d', strtotime($post['driver_passport_date_given']));
    
    $driver = new driverClass($post);
    $result = $driver->saveDriver($db);
    if (!$result) {
        $message = 'Произошла ошибка сохранения данных водителя';
        $ret_arr = prepare_response_error_arr($db, $message, $driver->selectParameters());
        
        goto ret;
    } //!$result
    
    if (!$driver->selectDriverId()) {
        /*
         * TODO: temporary workaround implemented employeeing OIDs, to get the last insterted id
         *
         */
        $last_oid = pg_last_oid($result);
        $driver->selectDriverFromDB($db, $last_oid);
    } //!$driver->selectDriverId()
    
    // set driver id for gett and bank classes
    $post['gett_driver_id'] = $driver->selectDriverId();
    $post['bank_driver_id'] = $driver->selectDriverId();
    
    /*
     *	GETT information
     *
     */
    if (parameter_set($post['gett_system_id']) && parameter_set($post['gett_status']) && parameter_set($post['gett_date_registration'])) {
    	$post['gett_date_registration'] = date('Y-m-d', strtotime($post['gett_date_registration']));
        $gett = new gettClass($post);
        
        $result = $gett->saveGett($db);
        if (!$result) {
            $message = 'Произошла ошибка сохранения данных Gett';
            $ret_arr = prepare_response_error_arr($db, $message, $gett->selectParameters());
            
            goto ret;
        } //!$result
    } //parameter_set($post['gett_number']) && parameter_set($post['gett_status']) && parameter_set($post['gett_date_registration'])
    
    /*
     * Bank information
     *
     */
    if (parameter_set($post['bank_account_number']) && parameter_set($post['bank_account_type']) && parameter_set($post['bank_account_status']) && parameter_set($post['pay_project_bank_bik']) && parameter_set($post['pay_project_bank_name'])) {
        $pay_project = new payProjectClass($post);
        
        $result = $pay_project->savePayProject($db);
        if (!$result) {
            $message = 'Произошла ошибка сохранения банковских данных';
            $ret_arr = prepare_response_error_arr($db, $message, $pay_project);
            
            goto ret;
        } //!$result
        
        if (!$pay_project->selectPayProjectId()) {
            /*
             * TODO: temporary workaround implemented employeeing OIDs, to get the last insterted id
             *
             */
            $last_oid = pg_last_oid($result);
            $pay_project->selectPayProjectFromDB($db, $last_oid);
        } //!$pay_project->selectPayProjectId()
        
        $post['bank_pay_project_id'] = $pay_project->selectPayProjectId();
        
        $bank = new bankClass($post);
        
        $result = $bank->saveBank($db);
        if (!$result) {
            $message = 'Произошла ошибка сохранения банковских данных';
            $ret_arr = prepare_response_error_arr($db, $message, $pay_project);
            
            goto ret;
        } //!$result
    } //parameter_set($post['account_number']) && parameter_set($post['account_type']) && parameter_set($post['account_status']) && parameter_set($post['pay_project_bank_bik']) && parameter_set($post['pay_project_bank_name'])
    
    $message = 'Удачное сохранение данных';
    $ret_arr = prepare_response_success_arr($db, $message);
    
	pg_query($db, 'COMMIT');
    
ret:
    return $ret_arr;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

if (parameter_set($clean_json['method'])) {
    $method = $clean_json['method'];
    if (isset($clean_json['acceptTerms']) && $clean_json['acceptTerms'] == 'on') {
        if ($method == 'store') {
            $result = store_driver($db, $clean_json);
            
            echo json_encode($result);
        } //$method == 'store_new' || $method == 'store_change'
        else {
            $message = 'Указан не верный параметр метод';
            $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
            echo json_encode($ret_arr);
        }
    } //isset($clean_json['acceptTerms']) && $clean_json['acceptTerms'] == 'on'
    else {
        $message = 'Не указан параметр принятия соглашения';
        $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
        echo json_encode($ret_arr);
    }
} //parameter_set($clean_json['method'])
else {
    $message = 'Не указан параметр метод';
    $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
    echo json_encode($ret_arr);
}
?>