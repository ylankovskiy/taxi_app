<?php
error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
    '/bank',
    '/configuration'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/response.php";
require_once "utils/helpers/funcs.php";

session_start();

function get_config($db, $type, $history = 0)
{
    $ret_arr = array();
    $params  = array();
    
    $query = 'SELECT 
			conf.*,
			employee.*
		FROM postgres.public.config_' . $type . ' as conf
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.employee
		) AS employee ON employee.employee_id=conf.config_' . $type . '_employee_added_id
		ORDER BY conf.config_' . $type . '_id DESC ' . ($history == 0 ? '' : 'limit 1')
		;
    
    $query_name = "get_config_" . $type . "_query";
    $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
        $query_name
    ));
    
    if (!$result || pg_num_rows($result) == 0) {
        $result = pg_prepare($db, $query_name, $query);
    } //!$result || pg_num_rows( $result ) == 0
    
    $result = pg_execute($db, $query_name, $params);
    
    if ($result) {
        while ($row = pg_fetch_assoc($result)) {
            $ret_arr[] = $row;
        } //$row = pg_fetch_assoc($result)
        
        pg_free_result($result);
    } //$result
    
//     return $query;
    return $ret_arr;
}

function get_config_driver_payout($db, $driver_type, $history)
{
    $ret_arr = array();
    $params  = array(
        $driver_type
    );
    
    $query = 'SELECT 
			*
		FROM postgres.public.config_driver_payout
		WHERE config_driver_payout_driver_type = $1
		ORDER BY config_driver_payout_id DESC ' . ($history == 0 ? '' : 'limit 1')
		;
    
    if (parameter_set($params)) {
        $query_name = "get_config_driver_payout_query";
        $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
            $query_name
        ));
        
        if (!$result || pg_num_rows($result) == 0) {
            $result = pg_prepare($db, $query_name, $query);
        } //!$result || pg_num_rows( $result ) == 0
        
        $result = pg_execute($db, $query_name, $params);
        
        if ($result) {
            while ($row = pg_fetch_assoc($result)) {
                $ret_arr[] = $row;
            } //$row = pg_fetch_assoc($result)
            
            pg_free_result($result);
        } //$result
    } //parameter_set($params)
    
    return $ret_arr;
}

function get_config_driver_pay_motivation_entries($db, $driver_pay_motivation_id)
{
    $ret_arr = array();
    $params  = array(
        $driver_pay_motivation_id
    );
    
    $query = 'SELECT 
			*
		FROM postgres.public.config_driver_payout
		WHERE config_driver_pay_motivation_entries_config_driver_pay_motivation_id = $1
		';
    
    if (parameter_set($params)) {
        $query_name = "get_config_driver_pay_motivation_entries_query";
        $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
            $query_name
        ));
        
        if (!$result || pg_num_rows($result) == 0) {
            $result = pg_prepare($db, $query_name, $query);
        } //!$result || pg_num_rows( $result ) == 0
        
        $result = pg_execute($db, $query_name, $params);
        
        if ($result) {
            while ($row = pg_fetch_assoc($result)) {
                $ret_arr[] = $row;
            } //$row = pg_fetch_assoc($result)
            
            pg_free_result($result);
        } //$result
    } //parameter_set($params)
    
    return $ret_arr;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);
if (isset($clean_json['method'])) {
    $method = $clean_json['method'];
    $history = $clean_json['history'];
    
    if ($method == 'view_timezone') {
        $result = get_config($db, 'timezone', $history);

        echo json_encode($result);
    } //$method == 'view_timezone'
    else if ($method == 'view_percent_gett') {
        $result = get_config($db, 'percent_gett', $history);
        echo json_encode($result);
    } //$method == 'view_percent_gett'
    else if ($method == 'view_percent_company') {
        $result = get_config($db, 'percent_company', $history);
        
        echo json_encode($result);
    } //$method == 'view_percent_company'
    else if ($method == 'view_gasoline') {
        $result = get_config($db, 'gasoline', $history);
        
        echo json_encode($result);
    } //$method == 'view_gasoline'
    else if ($method == 'view_gas') {
        $result = get_config($db, 'gas', $history);
        
        echo json_encode($result);
    } //$method == 'view_gas'
    else if ($method == 'view_coef_of_distance_travel') {
        $result = get_config($db, 'coef_of_distance_travel', $history);
        echo json_encode($result);
    } //$method == 'view_gas'
    else if ($method == 'view_driver_payout') {
        $driver_type = $clean_json['config_driver_payout_driver_type'];
        
        if (isset($driver_type)) {
            $result = get_config_driver_payout($db, $driver_type, $history);
            
            echo json_encode($result);
        } //$driver_type
        else {
            $message = 'Не указан параметр тип водителя';
            $ret_arr = prepare_response_error_arr($db, $message, null);
            
            echo json_encode($ret_arr);
        }
    } //$method == 'view_driver_payout'
    else if ($method == 'view_driver_pay_motivation') {
        $result = get_config($db, 'driver_pay_motivation', $history);
//        error_log()
        echo json_encode($result);
    } //$method == 'view_driver_pay_motivation'
    else if ($method == 'view_driver_pay_motivation_entries') {
        $driver_pay_motivation_id = $clean_json['config_driver_pay_motivation_id'];
        
        if (isset($driver_pay_motivation_id)) {
            $result = get_config_driver_pay_motivation_entries($db, $driver_pay_motivation_id, $history);
            
            echo json_encode($result);
        } //$driver_pay_motivation_id
        else {
            $message = 'Не указан параметр id мотивации';
            $ret_arr = prepare_response_error_arr($db, $message, null);
            
            echo json_encode($ret_arr);
        }
    } //$method == 'view_driver_pay_motivation_entries'
    else {
        $message = 'Не верно указан параметр метод';
        $ret_arr = prepare_response_error_arr($db, $message, null);
        
        echo json_encode($ret_arr);
    }
    
} //isset($clean_json['method'])
else {
    $message = 'Не указан параметр метод';
    $ret_arr = prepare_response_error_arr($db, $message, null);
    
    echo json_encode($ret_arr);
}
?>