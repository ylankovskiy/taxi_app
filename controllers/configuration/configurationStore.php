<?php
error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
    '/bank',
    '/configuration'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";
require_once "utils/helpers/response.php";
require_once "classes/configTimezoneClass.php";
require_once "classes/configPercentGettClass.php";
require_once "classes/configPercentCompanyClass.php";
require_once "classes/configGasolineClass.php";
require_once "classes/configGasClass.php";
require_once "classes/configDriverPayoutClass.php";
require_once "classes/configDriverPayMotivationClass.php";
require_once "classes/configDriverPayMotivationEntriesClass.php";
require_once "classes/configCoefOfDistanceTravelClass.php";


session_start();

function fix_config_date_end($db, $type, $date_end, $id)
{
    $object_key = array(
        'config_' . $type . '_id' => $id
    );
    
    $object = array(
        'config_' . $type . '_date_end' => $date_end
    );
    
    return pg_update($db, 'public.config_' . $type, $object, $object_key);
}

function get_config($db, $type)
{
    $ret_arr = array();
    $params  = array();
    
    $query = 'SELECT 
			*
		FROM postgres.public.config_' . $type . ' ORDER BY config_' . $type . '_id DESC limit 1
		';
    
    $query_name = "get_config_" . $type . "_query";
    $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
        $query_name
    ));
    
    if (!$result || pg_num_rows($result) == 0) {
        $result = pg_prepare($db, $query_name, $query);
    } //!$result || pg_num_rows( $result ) == 0
    
    $result = pg_execute($db, $query_name, $params);
    
    if ($result) {
        while ($row = pg_fetch_assoc($result)) {
            $ret_arr[] = $row;
        } //$row = pg_fetch_assoc($result)
        
        pg_free_result($result);
    } //$result
    
    return $ret_arr;
}

function get_config_driver_payout($db, $driver_type)
{
    $ret_arr = array();
    $params  = array(
        $driver_type
    );
    
    $query = 'SELECT 
			*
		FROM postgres.public.config_driver_payout
		WHERE config_driver_payout_driver_type = $1
		ORDER BY config_driver_payout_id DESC limit 1
		';
    
    if (parameter_set($params)) {
        $query_name = "get_config_driver_payout_query";
        $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
            $query_name
        ));
        
        if (!$result || pg_num_rows($result) == 0) {
            $result = pg_prepare($db, $query_name, $query);
        } //!$result || pg_num_rows( $result ) == 0
        
        $result = pg_execute($db, $query_name, $params);
        
        if ($result) {
            while ($row = pg_fetch_assoc($result)) {
                $ret_arr[] = $row;
            } //$row = pg_fetch_assoc($result)
            
            pg_free_result($result);
        } //$result
    } //parameter_set($params)
    
    return $ret_arr;
}

function store_config($db, $post, $type)
{
    $ret_arr      = array();
    $result       = null;
    $currentClass = null;
    
    $post['config_' . $type . '_date_begin'] = date('Y-m-d', strtotime($post['config_' . $type . '_date_begin']));
    
    if (isset($post['config_' . $type . '_date_end']) && strlen($post['config_' . $type . '_date_end']) > 0) {
    	$post['config_' . $type . '_date_end'] = date('Y-m-d', strtotime($post['config_' . $type . '_date_end']));
    }
    
	pg_query($db, 'BEGIN');
	
    switch ($type) {
        case 'timezone':
            $currentConfig = get_config($db, 'timezone');
            
            foreach ($currentConfig as $value) {
            	if (!isset($value['config_timezone_date_end'])) {
        	        $dateEnd = date('Y-m-d', strtotime($post['config_timezone_date_begin']));
    	            fix_config_date_end($db, 'timezone', $dateEnd, $value['config_timezone_id']);
	            } //!isset($value['config_timezone_date_end'])
            }
            
            $timezone = new configTimezoneClass($post);
            $result   = $timezone->saveConfigTimezone($db);
            
            $currentClass = $timezone;
            break;
        case 'percent_gett':
            $currentConfig = get_config($db, 'percent_gett');
            
            foreach ($currentConfig as $value) {
            	if (!isset($value['config_percent_gett_date_end'])) {
        	        $dateEnd = date('Y-m-d', strtotime($post['config_percent_gett_date_begin']));
    	            fix_config_date_end($db, 'percent_gett', $dateEnd, $value['config_percent_gett_id']);
	            } //!isset($value['config_percent_gett_date_end'])
            }
            
            $percentGett = new configPercentGettClass($post);
            $result      = $percentGett->saveConfigPercentGett($db);
            
            $currentClass = $percentGett;
            break;
        case 'percent_company':
            $currentConfig = get_config($db, 'percent_company');
            
            foreach ($currentConfig as $value) {
            	if (!isset($currentConfig['config_percent_company_date_end'])) {
        	        $dateEnd = date('Y-m-d', strtotime($post['config_percent_company_date_begin']));
    	            fix_config_date_end($db, 'percent_company', $dateEnd, $currentConfig['config_percent_company_id']);
	            } //!isset($currentConfig['config_percent_company_date_end'])
            }
            
            $percentCompany = new configPercentCompanyClass($post);
            $result         = $percentCompany->saveConfigPercentCompany($db);
            
            $currentClass = $percentCompany;
            break;
        case 'gasoline':
            $currentConfig = get_config($db, 'gasoline');
            
            foreach ($currentConfig as $value) {
            	if (!isset($value['config_gasoline_date_end'])) {
        	        $dateEnd = date('Y-m-d', strtotime($post['config_gasoline_date_begin']));
    	            fix_config_date_end($db, 'gasoline', $dateEnd, $value['config_gasoline_id']);
	            } //!isset($value['config_gasoline_date_end'])
            }
            
            $gasoline = new configGasolineClass($post);
            $result   = $gasoline->saveConfigGasoline($db);
            
            $currentClass = $gasoline;
            break;
        case 'gas':
            $currentConfig = get_config($db, 'gas');
            
            foreach ($currentConfig as $value) {
            	if (!isset($value['config_gas_date_end'])) {
        	        $dateEnd = date('Y-m-d', strtotime($post['config_gas_date_begin']));
    	            fix_config_date_end($db, 'gas', $dateEnd, $value['config_gas_id']);
	            } //!isset($value['config_gas_date_end'])
            }
            
            $gas    = new configGasClass($post);
            $result = $gas->saveConfigGas($db);
            
            $currentClass = $gas;
            break;

        case 'coef_of_distance_travel':
            $currentConfig = get_config($db, 'coef_of_distance_travel');

            foreach ($currentConfig as $value) {
                if (!isset($value['config_coef_of_distance_travel_date_end'])) {
                    $dateEnd = date('Y-m-d', strtotime($post['config_coef_of_distance_travel_date_begin']));
                    fix_config_date_end($db, 'coef_of_distance_travel', $dateEnd, $value['config_coef_of_distance_travel_id']);
                } //!isset($value['config_gas_date_end'])
            }

            $coefDistance   = new configCoefOfDistanceTravelClass($post);
            $result = $coefDistance->saveConfigCoefDistance($db);

            $currentClass = $coefDistance;
            break;

        case 'driver_payout':
            $currentConfig = get_config_driver_payout($db, $post['config_driver_payout_driver_type']);
            
            foreach ($currentConfig as $value) {
            	if (!isset($value['config_driver_payout_date_end'])) {
        	        $dateEnd = date('Y-m-d', strtotime($post['config_driver_payout_date_begin']));
    	            fix_config_date_end($db, 'driver_payout', $dateEnd, $value['config_driver_payout_id']);
	            } //!isset($value['config_driver_payout_date_end'])
            }
            
            $driverPayout = new configDriverPayoutClass($post);
            $result       = $driverPayout->saveConfigDriverPayout($db);
            
            $currentClass = $driverPayout;
            break;
        case 'driver_pay_motivation':
            $currentConfig = get_config($db, 'driver_pay_motivation');
            
            foreach ($currentConfig as $value) {
            	if (!isset($value['config_driver_pay_motivation_date_end'])) {
        	        $dateEnd = date('Y-m-d', strtotime($post['config_driver_pay_motivation_date_begin']));
    	            fix_config_date_end($db, 'driver_pay_motivation', $dateEnd, $value['config_driver_pay_motivation_id']);
	            } //!isset($value['config_driver_pay_motivation_date_end'])
            }
            
            $driverPayMotivation = new configDriverPayMotivationClass($post);
            $result              = $driverPayMotivation->saveConfigDriverPayMotivation($db);
            
            $entries = json_decode($post['entries'], true);
            foreach($entries as $value) {
				$value['config_driver_pay_motivation_entries_config_dpm_id'] = $driverPayMotivation->selectConfigDriverPayMotivationId();
    	        $driverPayMotivationEntries = new configDriverPayMotivationEntriesClass($value);
	            $result                     = $driverPayMotivationEntries->saveConfigDriverPayMotivationEntries($db);
            }
            $currentClass = $driverPayMotivation;
            break;
        default:
            break;
    } //$type
    
    if (!$result) {
        $message = 'Произошла ошибка сохранения конфигуратора';
        $ret_arr = prepare_response_error_arr($db, $message, $currentClass->selectParameters());
        
	    pg_query($db, 'ROLLBACK');
        
        goto ret;
    } //!$result
    
    $message = 'Удачное сохранение данных';
    $ret_arr = prepare_response_success_arr($db, $message);
    pg_query($db, 'COMMIT');
    
ret:
    return $ret_arr;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

if (parameter_set($clean_json['method'])) {
    $method = $clean_json['method'];
    if (isset($clean_json['acceptTerms']) && $clean_json['acceptTerms'] == 'on') {
        if ($method == 'store_timezone') {
            $result = store_config($db, $clean_json, 'timezone');
            
            echo json_encode($result);
        } //$method == 'store_timezone'
        else if ($method == 'store_percent_gett') {
            $result = store_config($db, $clean_json, 'percent_gett');
            
            echo json_encode($result);
        } //$method == 'store_percent_gett'
        else if ($method == 'store_percent_company') {
            $result = store_config($db, $clean_json, 'percent_company');
            
            echo json_encode($result);
        } //$method == 'store_percent_company'
        else if ($method == 'store_gasoline') {
            $result = store_config($db, $clean_json, 'gasoline');
            
            echo json_encode($result);
        } //$method == 'store_gasoline'
        else if ($method == 'store_gas') {
            $result = store_config($db, $clean_json, 'gas');
            
            echo json_encode($result);
        } //$method == 'store_gas'
        else if ($method == 'store_coef_of_distance_travel') {
            $result = store_config($db, $clean_json, 'coef_of_distance_travel');

            echo json_encode($result);
        } //$method == 'store_coef_of_distance_travel'
        else if ($method == 'store_driver_payout') {
            $result = store_config($db, $clean_json, 'driver_payout');
            
            echo json_encode($result);
        } //$method == 'store_driver_payout'
        else if ($method == 'store_driver_pay_motivation') {
            $result = store_config($db, $clean_json, 'driver_pay_motivation');
            
            echo json_encode($result);
        } //$method == 'store_driver_pay_motivation'
        else {
            $message = 'Указан не верный параметр метод';
            $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
            echo json_encode($ret_arr);
        }
    } //isset($clean_json['acceptTerms']) && $clean_json['acceptTerms'] == 'on'
    else {
        $message = 'Не указан параметр принятия соглашения';
        $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
        echo json_encode($ret_arr);
    }
} //parameter_set($clean_json['method'])
else {
    $message = 'Не указан параметр метод';
    $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
    echo json_encode($ret_arr);
}
?>