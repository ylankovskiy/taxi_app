<?php
error_reporting(-1);

$patterns = array('/views', '/controllers', '/employee', '/driver', '/main', '/order', '/pay', '/schedule', '/transaction', '/vehicle', '/utils', '/select',
    '/classes',
    '/bank',
    '/configuration');
$replacement = '';
$path = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";
require_once "utils/helpers/response.php";
require_once "utils/config/transactionsConfig.php";

session_start();

function get_transactions($db, $date_begin, $date_end) {
    $ret_arr = array();

	$params = array(
		date('Y-m-d 00:00:00', strtotime($date_begin)),
		date('Y-m-d 00:00:00', strtotime($date_end))
	);

    $query = 'SELECT 
		transactions.*,
		gett.*,
		driver.*,
		employee.*
		FROM postgres.public.transactions as transactions
		LEFT OUTER JOIN (SELECT * FROM postgres.public.driver)
			AS driver ON transactions.transactions_driver_id = driver.driver_id
		LEFT OUTER JOIN (SELECT * FROM postgres.public.gett)
			AS gett ON  gett.gett_driver_id = driver.driver_id
		LEFT OUTER JOIN (SELECT * FROM postgres.public.employee)
			AS employee ON transactions.transactions_employee_id = employee.employee_id
		WHERE transactions.transactions_datetime >= $1 AND transactions.transactions_datetime <= $2
		ORDER BY transactions.transactions_datetime desc';

	if (parameter_set($params)) {
    	$query_name = "get_all_transactions_query";
    	$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
			$query_name 
		) );
  
		if ( !$result || pg_num_rows( $result ) == 0 ) {
			$result = pg_prepare( $db, $query_name, $query );
		} //!$result || pg_num_rows( $result ) == 0
		
        $result = pg_execute($db, $query_name, $params);
            
        if ($result) {
            while ($row = pg_fetch_assoc($result)) {
            	$row['transactions_type_text'] = getTransactionsTypeText($row['transactions_type']);
            	$row['transactions_subtype_text'] = getTransactionsSubtypeText($row['transactions_subtype']);
            	$row['transactions_status_text'] = getTransactionsStatusText($row['transactions_status']);
            	
                $ret_arr[] = $row;
            }
                
            pg_free_result($result);
        }
    }

    return $ret_arr;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

if (isset($clean_json['method']) && isset($clean_json['date_begin']) && isset($clean_json['date_end'])) {
    $method = $clean_json['method'];
    $date_begin = $clean_json['date_begin'];
    $date_end = $clean_json['date_end'];

	if (strtotime($date_begin) > strtotime($date_end)) {
		$message = 'Не верно указан период';
		$ret_arr = prepare_response_error_arr($db, $message, null);
    
		echo json_encode($ret_arr);
	}
  
    if ($method == 'view') {
        $result = get_transactions($db, $date_begin, $date_end);

        echo json_encode($result);
    } else {
    	$message = 'Не верно указан параметр метод';
		$ret_arr = prepare_response_error_arr($db, $message, null);
		
        echo json_encode($ret_arr);
    }
} //isset($_REQUEST['method'])
else {
    $message = 'Не указан параметр метод или период';
    $ret_arr = prepare_response_error_arr($db, $message, null);
    
    echo json_encode($ret_arr);
}
?>