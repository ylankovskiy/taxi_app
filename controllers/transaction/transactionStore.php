<?php
error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
    '/bank',
    '/configuration'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";
require_once "utils/helpers/response.php";
require_once "classes/transactionsClass.php";

session_start();

function store_transactions($db, $post)
{
    $ret_arr = array();
    
    pg_query($db, 'BEGIN');
            
    $transactions = new transactionsClass($post);
    
    $result = $transactions->saveTransactions($db);
        
    if (!$result) {
		$message = 'Произошла ошибка сохранения проводки';
		$ret_arr = prepare_response_error_arr($db, $message, $transactions->selectParameters());
            
		goto ret;
	} //!$result
    
    $message = 'Удачное сохранение данных';
    $ret_arr = prepare_response_success_arr($db, $message);
    
	pg_query($db, 'COMMIT');
    
ret:
    return $ret_arr;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

if (parameter_set($clean_json['method'])) {
    $method = $clean_json['method'];
    if (isset($clean_json['acceptTerms']) && $clean_json['acceptTerms'] == 'on') {
        if ($method == 'store') {
            $result = store_transactions($db, $clean_json);
            
            echo json_encode($result);
        } //$method == 'store_new' || $method == 'store_change'
        else {
            $message = 'Указан не верный параметр метод';
            $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
            echo json_encode($ret_arr);
        }
    } //isset($clean_json['acceptTerms']) && $clean_json['acceptTerms'] == 'on'
    else {
        $message = 'Не указан параметр принятия соглашения';
        $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
        echo json_encode($ret_arr);
    }
} //parameter_set($clean_json['method'])
else {
    $message = 'Не указан параметр метод';
    $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
    echo json_encode($ret_arr);
}
?>