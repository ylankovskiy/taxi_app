<?php
error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
    '/fraud',
    '/bank',
    '/configuration'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";
require_once "utils/helpers/response.php";
require_once "classes/orderFraudClass.php";

session_start();

function verify_order_existence($db, $input) {
	$ret = false;
    
    $params = array(
        $input['order_fraud_order_id']
    );
    
    $query = 'SELECT 
		ord.*
		FROM postgres.public."order" as ord
		WHERE ord.order_id = $1';
    
    $query_name = "get_orders_" . $input['order_fraud_order_id'] . "_query";
    $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
        $query_name
    ));
    
    if (!$result || pg_num_rows($result) == 0) {
        $result = pg_prepare($db, $query_name, $query);
    } //!$result || pg_num_rows( $result ) == 0
    
    $result = pg_execute($db, $query_name, $params);
    
    if ($result) {
        while ($row = pg_fetch_assoc($result)) {
            $ret = true;
        } //$row = pg_fetch_assoc($result)
        
        pg_free_result($result);
    } //$result
    
    return $ret;
}

function store_fraud($db, $input)
{
    $ret_arr = array();
    
    $verify = verify_order_existence($db, $input);
    
    if ($verify) {
    	pg_query($db, 'BEGIN');
            
	    $orderFraud = new orderFraudClass($input);
    
    	$result = $orderFraud->saveOrderFraud($db);
        
	    if (!$result) {
			$message = 'Произошла ошибка сохранения данных фрод';
			$ret_arr = prepare_response_error_arr($db, $message, $orderFraud->selectParameters());
            
			goto ret;
		} //!$result
    
	    $message = 'Удачное сохранение данных';
    	$ret_arr = prepare_response_success_arr($db, $message);
    
		pg_query($db, 'COMMIT');
	} else {
		$message = 'Не найден заказ по id';
		$ret_arr = prepare_response_error_arr($db, $message, null);
            
		goto ret;
	} //!$result
    
ret:
    return $ret_arr;
}

function extract_sum($string) {
	return (float) filter_var( $string, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION );
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

$IDX_ORDER_ID = 2;
$IDX_REMARK = 3;
$IDX_SUM = 5;

if (parameter_set($clean_json)) {
    $data = $clean_json;
    foreach ($data as $key => $value) {
    	if ($key == 0) {
    		// skip header of csv
    		continue;
    	} else {
    		$sum = extract_sum($value[$IDX_SUM]);
    		$input = array(
    			'order_fraud_order_id' => $value[$IDX_ORDER_ID],
    			'order_fraud_sum' => $sum,
    			'order_fraud_remark' => $value[$IDX_REMARK]
    		);
    		$result = store_fraud($db, $input);
    		
    		echo json_encode($result);
    	}
    }
} else {
    $message = 'Нету значений внутри файла';
    $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
    echo json_encode($ret_arr);
}
?>