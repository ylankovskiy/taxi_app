<?php
error_reporting(-1);
	
$patterns    = array(
   '/views',
  '/controllers',
  '/employee',
  '/driver',
  '/main',
  '/order',
  '/pay',
  '/schedule',
  '/transaction',
  '/vehicle',
  '/utils',
  '/select',
'/classes',
  '/bank' 
);
$replacement = '';
$path        = str_replace( $patterns, $replacement, getcwd() );

error_log( $path );
chdir( $path );

require_once "db.php";
require_once "utils/helpers/response.php";
require_once "utils/helpers/funcs.php";
require_once "utils/config/driverConfig.php";
require_once "utils/config/gettConfig.php";

// session_start();

function get_orders( $url )
{
  //  Initiate curl
  $ch = curl_init();
  // Disable SSL verification
  curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
  // Will return the response, if false it print the response
  curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
  // Set the url
  curl_setopt( $ch, CURLOPT_URL, $url );
  // Execute
  $result = curl_exec( $ch );
  // Closing
  curl_close( $ch );
  
  return $result;
}

function select_gett_driver( $db, $gett_number )
{
  $query = "SELECT *
			FROM 
			  postgres.public.gett
			WHERE
				gett_system_id=$1";
  
  $query_name = "select_gett_driver_query";
  $result     = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
     $query_name 
  ) );
  
  if ( !$result || pg_num_rows( $result ) == 0 ) {
    $result = pg_prepare( $db, $query_name, $query );
  } //!$result || pg_num_rows( $result ) == 0
  
  $result = pg_execute( $db, $query_name, array(
     $gett_number 
  ) );
  
  $gett = array();
  if ( $result ) {
    while ( $row = pg_fetch_assoc( $result ) ) {
      $gett = $row;
    } //$row = pg_fetch_assoc( $result )
    
    pg_free_result( $result );
  } //$result
  
  return $gett;
}

function select_driver_id( $db, $oid, $name_last, $name_name, $name_first, $date_birth )
{
  $params = array();
  
  error_log('ORDERVIEW ERROR LOG: oid=' . $oid . ' last=' . $name_last . ' name=' . $name_name . ' first=' . $name_first . ' birth=' . $date_birth);
  
  
  if ( $name_last != NULL ) {
    $query = 'SELECT driver_id
				FROM 
			  	postgres.public.driver
				WHERE
					name_last=$1 and
					name_name = $2 and
					name_first = $3 and
					date_birth = $4';
    
    $query_name = 'select_driver_id_query';
    
    $params = array(
       $name_last,
      $name_name,
      $name_first,
      $date_birth 
    );
  } //$name_last != NULL
  else if ( $oid != NULL ) {
    $query = 'SELECT driver_id
				FROM 
			  	postgres.public.driver
				WHERE
					oid = $1';
    
    $query_name = 'select_driver_oid_query';
    
    
    $params = array(
       $oid 
    );
  } //$oid != NULL
  
  $result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
     $query_name 
  ) );
  
  if ( !$result || pg_num_rows( $result ) == 0 ) {
    $result = pg_prepare( $db, $query_name, $query );
  } //!$result || pg_num_rows( $result ) == 0
  
  $result = pg_execute( $db, $query_name, $params );
  
  $driver_id = -1;
  if ( $result ) {
    while ( $row = pg_fetch_assoc( $result ) ) {
      $driver_id = $row[ 'driver_id' ];
    } //$row = pg_fetch_assoc( $result )
  } //$result
  
  return $driver_id;
}

function verify_driver_name( $db, $gett_driver_id = NULL, $driver )
{
	$verify = NULL;
	
	if (!$gett_driver_id) {
	  $query = "SELECT *
			FROM 
			  postgres.public.driver
			WHERE
				driver_name_last=$1 and
				driver_name_first=$2 and
				driver_name_name=$3";
  
	  $query_name = "verify_driver_name_1_query";
	  
	  $params = array(
    	$driver[ 'driver_name_last' ],
	    $driver[ 'driver_name_first' ],
    	$driver[ 'driver_name_name' ]
	  );
	} else {
		  $query = "SELECT *
			FROM 
			  postgres.public.driver
			WHERE
				driver_id=$1 and
				driver_name_last=$2 and
				driver_name_first=$3 and
				driver_name_name=$4";
  
	  $query_name = "verify_driver_name_2_query";
	  
	  $params = array(
	     $gett_driver_id,
    	$driver[ 'driver_name_last' ],
	    $driver[ 'driver_name_first' ],
    	$driver[ 'driver_name_name' ]
	  );
	}
	
  $result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
     $query_name 
  ) );
  
  if ( !$result || pg_num_rows( $result ) == 0 ) {
    $result = pg_prepare( $db, $query_name, $query );
  } //!$result || pg_num_rows( $result ) == 0
  
  $result = pg_execute( $db, $query_name, $params );
  
  if ( $result ) {
    while ( $row = pg_fetch_assoc( $result ) ) {
      $verify = $row;
    } //$row = pg_fetch_assoc( $result )
    
    pg_free_result( $result );
  } //$result
  
  return $verify;
}


$time_to   = date( 'Y-m-d H:i:s' );
$time_from = date( 'Y-m-d H:i:s', strtotime( '-7 hours' ) );

echo $time_to . ' ' . $time_from;

$url = 'https://ru-mydbr.gtforge.com/report.php?r=93&p1=1322&u2=' . $time_from . '&u3=' . $time_to . '&h=12a2326b1507e511674d5ec4f63299cdafe6142c&export=json&json_force_object=1';
//var_dump($url);
$result     = get_orders( $url );
// Will dump a beauty json :3
$json       = json_decode( $result, true );
/* trim all strings in an array */
$clean_json = trim_array( $json );

$datetime_created = date( 'Y-m-d H:i:s', time() );

var_dump($clean_json);

foreach ( $clean_json as $key => $value ) {
  if ( $key == 1 ) { // orders
    foreach ( $value as $row => $order ) {
      if ( strlen( $order[ 'ended_at' ] ) > 0 ) {
      	pg_query('BEGIN');
        /*
         * order_id = ['order_id']
         * class = ['division']
         * datetime_start = ['scheduled_at']
         * datetime_finish = ['ended_at']
         * address_start = ['origin_full_address']
         * client_money = ['collected_from_client']
         * tarif_driver_overall = ['cost_for_driver']
         * tip_driver = ['driver_tips']
         * client_coupon = ['coupon']
         * type_payment = ['payment_type']
         * pwg_driver_bonus = [pwg_reward']
         * //Фактическая продолжительность поездки
         * //Дистанция (км)
         * duration_travel = ['\u0424\u0430\u043a\u0442\u0438\u0447\u0435\u0441\u043a\u0430\u044f \u043f\u0440\u043e\u0434\u043e\u043b\u0436\u0438\u0442\u0435\u043b\u044c\u043d\u043e\u0441\u0442\u044c \u043f\u043e\u0435\u0437\u0434\u043a\u0438']
         * gett_id = ['driver_id']
         *
         * name of driver = ['driver_name']
         */
        $str_time = preg_replace( "/^([\d]{1,2})\:([\d]{2})$/", "00:$1:$2", $order[ 'Фактическая продолжительность поездки' ] );
        sscanf( $str_time, "%d:%d:%d", $hours, $minutes, $seconds );
        $duration_seconds = $hours * 3600 + $minutes * 60 + $seconds;
        
        // rename field distance of travel
        $travel_distance = $order['Дистанция (км)'];
        
        $gett_driver = select_gett_driver( $db, $order[ 'driver_id' ] );
        
        $f_create_gett = false;
        $gett          = array();
        $driver        = array();
        
        $driver_name                         = explode( ' ', $order[ 'driver_name' ] );
        $driver[ 'driver_name_last' ]               = $driver_name[ 0 ];
        $driver[ 'driver_name_first' ]              = $driver_name[ 1 ];
        $driver[ 'driver_name_name' ]               = $driver_name[ 2 ];
        $driver[ 'driver_status' ]					= DriverStatus::WORKING;
        $driver[ 'driver_datetime_created' ] = $datetime_created;
        
        error_log( "dump found driver" );
        error_log( var_dump( $gett_driver ) );

        if ( !isset( $gett_driver ) || count( $gett_driver ) == 0 ) {
          // driver is not found by the gett number
          // must register driver in the system and register his gett number
          $gett[ 'gett_status' ]            = GettStatus::ACTIVE;
          $gett[ 'gett_system_id' ]            = $order[ 'driver_id' ];
          $gett[ 'gett_date_registration' ] = date( 'Y-m-d', strtotime( $datetime_created ) );
          $gett[ 'gett_datetime_created' ]  = $datetime_created;
          
          $verify = verify_driver_name( $db, NULL, $driver );
          
          error_log( "dump verify inside found driver" );
          error_log( var_dump( $verify ) );
          
          if ( isset($verify) ) {
          	$object_key = array(
    	        'driver_id' => $verify['driver_id']
	        );
          	$result = pg_update( $db, 'public.driver', $driver, $object_key );
              echo "<br><br>driver update\n";
          	var_dump($driver);
              echo "<br><br>obj key\n";
          	var_dump($object_key);
              echo "<br><br>resault update\n";
              var_dump($result);
          } else {
			$result = pg_insert( $db, 'public.driver', $driver );
              echo "<br><br>driver insert\n";

              var_dump($driver);
              echo "<br><br>resault insert\n";
              var_dump($result);

          }
          
          if ( $result ) {
            
            /*
             * TODO: temporary workaround implemented employeeing OIDs, to get the last insterted id
             *
             */
//            echo "<br><br>error\n";
//            var_dump(pg_last_error($db));
//            echo "<br><br>notice\n";
//            var_dump(pg_last_notice($db));
//            echo "<br><br>\n";
//             var_dump(pg_result_error(pg_get_result($db)));


            $last_oid  = pg_last_oid( $result );
            echo $last_oid . " oid <br></br>\n";
            $driver_id = select_driver_id( $db, $last_oid, NULL, NULL, NULL, NULL );
            echo $driver_id . " id driver<br></br>\n";
            // saved driver found
            if ( $driver_id != -1 ) {
              $gett[ 'gett_driver_id' ] = $driver_id;
              
              $result = pg_insert( $db, 'public.gett', $gett );
              if ( !$result ) {
                $message = 'Произошла ошибка сохранения Gett данных водителя';
                $ret_arr = prepare_response_error_arr( $db, $message, $gett );
                error_log( var_dump( $ret_arr ) );
                
                goto ret;
              } //!$result
              else {
                // driver created, save order
                
                $gett_driver = select_gett_driver( $db, $order[ 'driver_id' ] );
                $order  = array(
                  'order_gett_id' => $gett_driver[ 'gett_id' ],
                  'order_id' => $order[ 'order_id' ],
                  'order_class' => $order[ 'division' ],
                  'order_datetime_start' => $order[ 'scheduled_at' ],
                  'order_datetime_finish' => $order[ 'ended_at' ],
                  'order_address_start' => $order[ 'origin_full_address' ],
                  'order_address_finish' => $order['destination_full_address'],
                  'order_client_money' => floatval( $order[ 'collected_from_client' ] ),
                  'order_tarif_driver_overall' => floatval( $order[ 'cost_for_driver' ] ),
                  'order_tip_driver' => floatval( $order[ 'driver_tips' ] ),
                  'order_client_coupon' => floatval( $order[ 'coupon' ] ),
                  'order_type_payment' => $order[ 'payment_type' ],
                  'order_pwg_driver_bonus' => $order[ 'pwg_reward' ],
                  'order_duration_travel' => floatval( $duration_seconds ),
                  'order_distance_travel' => floatval( $travel_distance )
                );
                var_dump($result);
                $result = pg_insert( $db, 'public.order', $order );

                if ( !$result ) {
                  $message = 'Произошла ошибка сохранения заказа';
                  $ret_arr = prepare_response_error_arr( $db, $message, $order );
                  error_log( var_dump( $ret_arr ) );
                  
                  goto ret;
                } //!$result
              }
            } //$driver_id != -1
          } //$result
          else {
            $message = 'Произошла ошибка сохранения данных водителя';
            $ret_arr = prepare_response_error_arr( $db, $message, $driver );
            error_log( var_dump( $ret_arr ) );
            
            goto ret;
          }
        } //!isset( $gett_driver ) || count( $gett_driver ) == 0
        else {
          // driver is found by the gett number, verify his name
          // TODO : two ways to go about if the name cannot be verified:
          //		1. create a new driver and put a status on the previous driver that matched the
          //			gett number that he was renamed
          //		2. change the existing driver name and save the previous name in history
          //	in any case, the manager should be notified of the change
          
          $verify = verify_driver_name( $db, $gett_driver[ 'gett_driver_id' ], $driver );
          
	        error_log( "dump verify" );
          error_log( var_dump( $verify ) );
          
          if ( isset($verify) ) {
            // driver verified, save order
            
            $gett_driver = select_gett_driver( $db, $order[ 'driver_id' ] );
            
            $order  = array(
              'order_gett_id' => $gett_driver[ 'gett_id' ],
              'order_id' => $order[ 'order_id' ],
              'order_class' => $order[ 'division' ],
              'order_datetime_start' => $order[ 'scheduled_at' ],
              'order_datetime_finish' => $order[ 'ended_at' ],
              'order_address_start' => $order[ 'origin_full_address' ],
              'order_address_finish' => $order['destination_full_address'],
              'order_client_money' => floatval( $order[ 'collected_from_client' ] ),
              'order_tarif_driver_overall' => floatval( $order[ 'cost_for_driver' ] ),
              'order_tip_driver' => floatval( $order[ 'driver_tips' ] ),
              'order_client_coupon' => floatval( $order[ 'coupon' ] ),
              'order_type_payment' => $order[ 'payment_type' ],
              'order_pwg_driver_bonus' => $order[ 'pwg_reward' ],
              'order_duration_travel' => floatval( $duration_seconds ),
              'order_distance_travel' => floatval( $travel_distance )

            );
            
            // TODO: check for duplicate key to prevent warning messages
            $result = pg_insert( $db, 'public.order', $order );
            
            if ( !$result ) {
              $message = 'Произошла ошибка сохранения заказа';
              $ret_arr = prepare_response_error_arr( $db, $message, $order );
              error_log( var_dump( $ret_arr ) );
              
              goto ret;
            } //!$result
          } //$verify
          else {
            // driver not verified, must perform steps from TODO
          }
        }
        // first check if gett_id exists
        // yes --> save order
        // no --> create driver with name and gett id supplied and then save order
        
        pg_query('COMMIT');

        continue;

        ret:        
        pg_query('ROLLBACK');
        
      } //strlen( $order[ 'ended_at' ] ) > 0
    } //$value as $row => $order
  } //$key == 1
} //$json as $key => $value

?>