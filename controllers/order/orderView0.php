<?php
error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
    '/bank',
    '/configuration'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";
require_once "utils/helpers/response.php";

session_start();

function get_all_orders($db, $date_begin, $date_end)
{
    $ret_arr = array();
    
    $params = array(
        date('Y-m-d 00:00:00', strtotime($date_begin)),
        date('Y-m-d 00:00:00', strtotime($date_end))
    );
    
    $query = 'SELECT 
		ord.*,
		gett.*,
		driver.*,
		vehicle.*,
		telephone_book.*,
		order_fraud.*,
		order_correction.*
		FROM postgres.public."order" as ord
		LEFT OUTER JOIN (SELECT DISTINCT ON (order_fraud_order_id) * FROM postgres.public.order_fraud
				GROUP BY order_fraud_id, order_fraud_order_id)
			AS order_fraud ON  order_fraud.order_fraud_order_id = ord.order_id
		LEFT OUTER JOIN (SELECT DISTINCT ON (order_correction_order_id) * FROM postgres.public.order_correction
				GROUP BY order_correction_id, order_correction_order_id)
			AS order_correction ON  order_correction.order_correction_order_id = ord.order_id
		LEFT OUTER JOIN (SELECT * FROM postgres.public.gett)
			AS gett ON  gett.gett_id = ord.order_gett_id
		LEFT OUTER JOIN (SELECT * FROM postgres.public.driver)
			AS driver ON gett.gett_driver_id = driver.driver_id
		LEFT OUTER JOIN (SELECT * FROM postgres.public.vehicle)
			AS vehicle ON driver.driver_vehicle_id = vehicle.vehicle_id
		LEFT OUTER JOIN (SELECT * FROM postgres.public.telephone_book) 
			AS telephone_book ON driver.driver_telephone_book_id=telephone_book.telephone_book_id
		WHERE ord.order_datetime_start >= $1 AND ord.order_datetime_start <= $2
		ORDER BY ord.order_datetime_start desc';
    
    $query_name = "get_all_orders_query";
    $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
        $query_name
    ));
    
    if (!$result || pg_num_rows($result) == 0) {
        $result = pg_prepare($db, $query_name, $query);
    } //!$result || pg_num_rows( $result ) == 0
    
    $result = pg_execute($db, $query_name, $params);
    
    if ($result) {
        while ($row = pg_fetch_assoc($result)) {
            $ret_arr[] = $row;
        } //$row = pg_fetch_assoc($result)
        
        pg_free_result($result);
    } //$result
    
    return $ret_arr;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

if (isset($clean_json['method']) && isset($clean_json['date_begin']) && isset($clean_json['date_end'])) {
    $method     = $clean_json['method'];
    $date_begin = $clean_json['date_begin'];
    $date_end   = $clean_json['date_end'];
    
    if (strtotime($date_begin) > strtotime($date_end)) {
        $message = 'Не верно указан период';
        $ret_arr = prepare_response_error_arr($db, $message, null);
        
        echo json_encode($ret_arr);
    } //strtotime($date_begin) > strtotime($date_end)
    
    if ($method == 'view') {
        $result = get_all_orders($db, $date_begin, $date_end);
        
        echo json_encode($result);
    } //$method == 'view'
    else {
        $message = 'Не верно указан параметр метод';
        $ret_arr = prepare_response_error_arr($db, $message, null);
        
        echo json_encode($ret_arr);
    }
} //isset($clean_json['method']) && isset($clean_json['date_begin']) && isset($clean_json['date_end'])
else {
    $message = 'Не указан параметр метод';
    $ret_arr = prepare_response_error_arr($db, $message, null);
    
    echo json_encode($ret_arr);
}
?>