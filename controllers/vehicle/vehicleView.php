<?php
error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
    '/bank',
    '/configuration'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";
require_once "utils/helpers/response.php";
require_once "utils/config/vehicleConfig.php";

session_start();

function get_all_vehicles($db)
{
    $ret_arr = array();
    
    $query = 'SELECT 
			vehicle.*,
			driver.*,
			telephone_book.*
		FROM
			postgres.public.vehicle as vehicle
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.driver
		) AS driver ON driver.driver_vehicle_id=vehicle.vehicle_id
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.telephone_book
		) AS telephone_book ON driver.driver_telephone_book_id=telephone_book.telephone_book_id';
    
    $result = pg_prepare($db, "get_all_vehicles_query", $query);
    $result = pg_execute($db, "get_all_vehicles_query", array());
    
    if (!$result) {
        $message = 'Произошла ошибка получения данных';
        $ret_arr = prepare_response_error_arr($db, $message, null);
        
        goto ret;
    } //!$result
    
    while ($row = pg_fetch_assoc($result)) {
        $row['vehicle_insurance_kasko'] = convert_bool_to_bit($row['vehicle_insurance_kasko']);
        $row['vehicle_insurance_osago'] = convert_bool_to_bit($row['vehicle_insurance_osago']);
        $row['vehicle_device']          = convert_bool_to_bit($row['vehicle_device']);
        $row['vehicle_gas']             = convert_bool_to_bit($row['vehicle_gas']);
        $row['vehicle_type_text']       = getVehicleTypeText($row['vehicle_type']);
        $row['vehicle_status_text']     = getVehicleStatusText($row['vehicle_status']);
        
        $ret_arr[] = $row;
    } //$row = pg_fetch_assoc($result)
    
ret:
    
    return $ret_arr;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

if (isset($clean_json['method'])) {
    $method = $clean_json['method'];
    
    if ($method == 'view_all') {
        $result = get_all_vehicles($db);
        
        echo json_encode($result);
    } //$method == 'view_all'
    else {
        $message = 'Не верно указан параметр метод';
        $ret_arr = prepare_response_error_arr($db, $message, null);
        
        echo json_encode($ret_arr);
    }
    
} //isset($clean_json['method'])
else {
    $message = 'Не указан параметр метод';
    $ret_arr = prepare_response_error_arr($db, $message, null);
    
    echo json_encode($ret_arr);
}
?>