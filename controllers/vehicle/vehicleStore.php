<?php
error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
    '/bank',
    '/configuration'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";
require_once "utils/helpers/response.php";
require_once "classes/vehicleClass.php";

session_start();

function store_vehicle($db, $post)
{
    $ret_arr = array();
    
    pg_query($db, 'BEGIN');
            
    $vehicle = new vehicleClass($post);
    
    $result = $vehicle->saveVehicle($db);
    
    if (!$result) {
		$message = 'Произошла ошибка сохранения данных автомобиля';
		$ret_arr = prepare_response_error_arr($db, $message, $transactions->selectParameters());
            
		goto ret;
	} //!$result
    
    $message = 'Удачное сохранение данных';
    $ret_arr = prepare_response_success_arr($db, $message);
    
	pg_query($db, 'COMMIT');
    
ret:
    return $ret_arr;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

if (isset($clean_json['method'])) {
    $method = $clean_json['method'];
    
    if (isset($clean_json['vehicle_device'])) {
        if ($clean_json['vehicle_device'] == 'on') {
            $clean_json['vehicle_device'] = "1";
        } //$clean_json['vehicle_device'] == 'on'
        else {
            $clean_json['vehicle_device'] = "0";
        }
    } //isset($clean_json['vehicle_device'])
    if (isset($clean_json['vehicle_gas'])) {
        if ($clean_json['vehicle_gas'] == 'on') {
            $clean_json['vehicle_gas'] = "1";
        } //$clean_json['vehicle_gas'] == 'on'
        else {
            $clean_json['vehicle_gas'] = "0";
        }
    } //isset($clean_json['vehicle_gas'])
    if (isset($clean_json['vehicle_insurance_kasko'])) {
        if ($clean_json['vehicle_insurance_kasko'] == 'on') {
            $clean_json['vehicle_insurance_kasko_date_begin'] = date('Y-m-d', strtotime($clean_json['vehicle_insurance_kasko_date_begin']));
            $clean_json['vehicle_insurance_kasko_date_end']   = date('Y-m-d', strtotime($clean_json['vehicle_insurance_kasko_date_end']));
            $clean_json['vehicle_insurance_kasko']            = "1";
        } //$clean_json['vehicle_insurance_kasko'] == 'on'
        else {
            $clean_json['insurance_kasko_date_begin'] = null;
            $clean_json['insurance_kasko_date_end']   = null;
            $clean_json['insurance_kasko_num']        = null;
            $clean_json['insurance_kasko_series']     = null;
            $clean_json['insurance_kasko']            = "0";
        }
    } //isset($clean_json['vehicle_insurance_kasko'])
    if (isset($clean_json['vehicle_insurance_osago'])) {
        if ($clean_json['ivehicle_nsurance_osago'] == 'on') {
            $clean_json['vehicle_insurance_osago_date_begin'] = date('Y-m-d', strtotime($clean_json['vehicle_vehicle_insurance_osago_date_begin']));
            $clean_json['vehicle_insurance_osago_date_end']   = date('Y-m-d', strtotime($clean_json['vehicle_insurance_osago_date_end']));
            $clean_json['vehicle_insurance_osago']            = "1";
        } //$clean_json['ivehicle_nsurance_osago'] == 'on'
        else {
            $clean_json['vehicle_insurance_osago_date_begin'] = null;
            $clean_json['vehicle_insurance_osago_date_end']   = null;
            $clean_json['vehicle_insurance_osago_num']        = null;
            $clean_json['vehicle_insurance_osago_series']     = null;
            $clean_json['vehicle_insurance_osago']            = "0";
        }
    } //isset($clean_json['vehicle_insurance_osago'])
    
    if (isset($clean_json['acceptTerms']) && $clean_json['acceptTerms'] == 'on') {
        if ($method == 'store') {
            $result = store_vehicle($db, $clean_json);
            
            echo json_encode($result);
        } else {
            $message = 'Указан не верный параметр метод';
            $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
            echo json_encode($ret_arr);
        }
    } //isset($clean_json['acceptTerms']) && $clean_json['acceptTerms'] == 'on'
    else {
        $message = 'Не указан параметр принятия соглашения';
        $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
        echo json_encode($ret_arr);
    }
} //isset($clean_json['method'])
else {
    $message = 'Не указан параметр метод';
    $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
    echo json_encode($ret_arr);
}
?>