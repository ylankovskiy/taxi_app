<?php
	$patterns = array('/views', '/controllers', '/employee', '/driver', '/main', '/order', '/pay', '/schedule', '/transaction', '/vehicle', '/utils', '/select',
'/classes',
  '/bank');
	$replacement = '';
	$path = str_replace($patterns, $replacement, getcwd());
	
	error_log($path);
	chdir($path);

	require_once "db.php";

	session_start();
	
	function get_all_employees($db) {
		$ret_arr = array();
		
		$query = 'SELECT 
  employee.employee_id, 
  employee.employee_name_first, 
  employee.employee_name_name,
  employee.employee_name_last,
  employee.employee_email, 
  employee.employee_login, 
  employee.employee_remark, 
  employee.employee_datetime_modified, 
  status.employee_status_name, 
  department.department_name,
  position.position_name,
  telephone_book.telephone_book_telephone,
  array_agg(parents.employee_name_last ORDER BY parents.employee_name_last) AS superiors 
FROM 
  postgres.public.employee
LEFT OUTER JOIN (
  SELECT * FROM postgres.public.department
) AS department ON employee.employee_department_id=department.department_id
LEFT OUTER JOIN (
  SELECT * FROM postgres.public.position
) AS position ON employee.employee_position_id=position.position_id
LEFT OUTER JOIN (
  SELECT * FROM postgres.public.telephone_book
) AS telephone_book ON employee.employee_telephone_book_id=telephone_book.telephone_book_id
LEFT OUTER JOIN (
  SELECT * FROM postgres.public.employee
) AS parents ON employee.employee_supervisor=parents.employee_id
LEFT OUTER JOIN (
  SELECT * FROM postgres.public.employee_status
) AS status ON employee.employee_status=status.employee_status_id
GROUP BY 
  employee.employee_id, department.department_name, position.position_name, telephone_book.telephone_book_telephone, status.employee_status_name';
		

		$result = pg_prepare ($db, "query", $query);
		$result = pg_execute ($db, "query", array());
	
		if (!$result) {
			$ret_arr['ERROR_CODE'] = -1;
			$ret_arr['ERROR_MESSAGE'] = 'Произошла ошибка получения данных';
			$ret_arr['DB_ERROR'] = pg_last_error($db);
			
			return $ret_arr;
			exit;
		}

		while ($row = pg_fetch_assoc($result)) {
			$ret_arr[] = $row;
		}
		
		return $ret_arr;
	}
	
	if (isset($_REQUEST['method'])) {
		$method = $_REQUEST['method'];
		
		if ($method == 'view_all') {
			$result = get_all_employees($db);
			
			echo json_encode($result);
		}
		
	}
?>