<?php
date_default_timezone_set("Asia/Krasnoyarsk");

error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
    '/bank',
    '/configuration'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";
require_once "utils/helpers/response.php";
require_once "utils/config/driverConfig.php";
require_once "utils/config/scheduleConfig.php";

session_start();
function get_config_params($db, $type) {
    $ret_arr          = array();
    $params  = array();
    $query = 'SELECT 
                conf.*
              FROM postgres.public.' . $type .' as conf
              ORDER BY conf.' . $type .'_id DESC limit 1

            ';
    $query_name = "get_config_" . $type . "_query";
    $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
        $query_name
    ));

    if (!$result || pg_num_rows($result) == 0) {
        $result = pg_prepare($db, $query_name, $query);
    } //!$result || pg_num_rows( $result ) == 0

    $result = pg_execute($db, $query_name, $params);

    if ($result) {
        while ($row = pg_fetch_assoc($result)) {
            $ret_arr[] = $row;
        } //$row = pg_fetch_assoc($result)

        pg_free_result($result);
    } //$result

//     return $query;

    return $ret_arr;

}
function get_rows_for_period($db, $date_begin, $date_end)
{
    $ret_arr          = array();
    $company_vehicles = array();
    
    $query = 'SELECT 
			vehicle.*
		FROM
			postgres.public.vehicle
		WHERE
			vehicle.vehicle_type=0';
    
    
    $result = pg_prepare($db, "get_all_company_vehicles_query", $query);
    $result = pg_execute($db, "get_all_company_vehicles_query", array());
    
    if (!$result) {
        $message = 'Произошла ошибка получения данных';
        $ret_arr = prepare_response_error_arr($db, $message, null);
        
        return $ret_arr;
        exit;
    } //!$result
    else {
        while ($row = pg_fetch_assoc($result)) {
            $company_vehicles[$row['vehicle_id']] = $row;
        } //$row = pg_fetch_assoc($result)
        
        pg_free_result($result);
    }
    
    foreach ($company_vehicles as $vehicle_id => $vehicle) {
        $dt_check = $date_begin;
        
        while (strtotime($dt_check) <= strtotime($date_end)) {
            $params = array(
                $dt_check,
                $vehicle_id
            );
            
            $query = 'SELECT 
				schedule.*,
				driver.*,
				vehicle.*
			FROM
				postgres.public.schedule as schedule
			LEFT OUTER JOIN (
			  SELECT * FROM postgres.public.driver
			) AS driver ON schedule.schedule_driver_id=driver.driver_id
			LEFT OUTER JOIN (
			  SELECT * FROM postgres.public.vehicle
			) AS vehicle ON schedule.schedule_vehicle_id=vehicle.vehicle_id
			WHERE
				schedule.schedule_date=$1 and vehicle.vehicle_id=$2';
            
            if (parameter_set($params)) {
                $query_name = "get_schedule_query";
                $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
                    $query_name
                ));
                
                if (!$result || pg_num_rows($result) == 0) {
                    $result = pg_prepare($db, $query_name, $query);
                } //!$result || pg_num_rows( $result ) == 0
                
                $result = pg_execute($db, $query_name, $params);
                
                if ($result) {
                    $empty_result = true;
                    while ($row = pg_fetch_assoc($result)) {
                        $bg_class = get_bg_schedule_class_name($row['driver_type'], $row['schedule_status']);
                        if (!array_key_exists($vehicle_id, $ret_arr))
                            $ret_arr[$vehicle_id] = array(
                                'vehicle' => $vehicle['vehicle_plate_number'],
                                $dt_check => "<div class='padding-2 driver-assign $bg_class cursor-pointer' data-date='$dt_check' data-type='" . $row['driver_type'] . "' data-status='" . $row['schedule_status'] . "' data-vehicle='$vehicle_id' data-schedule='" . $row['schedule_id'] . "' data-driver='" . $row['schedule_driver_id'] . "'>" . $row['driver_name_last'] . " " . $row['driver_name_first'] . " " . $row['driver_name_name'] . "</div>"
                            );
                        else
                            $ret_arr[$vehicle_id][$dt_check] = "<div class='padding-2 driver-assign $bg_class cursor-pointer' data-date='$dt_check' data-type='" . $row['driver_type'] . "' data-status='" . $row['schedule_status'] . "' data-vehicle='$vehicle_id' data-schedule='" . $row['schedule_id'] . "' data-driver='" . $row['schedule_driver_id'] . "'>" . $row['driver_name_last'] . " " . $row['driver_name_first'] . " " . $row['driver_name_name'] . "</div>";
                        
                        $empty_result = false;
                    } //$row = pg_fetch_assoc($result)
                    
                    if ($empty_result) {
                        if (!array_key_exists($vehicle_id, $ret_arr))
                            $ret_arr[$vehicle_id] = array(
                                'vehicle' => $vehicle['vehicle_plate_number'],
                                $dt_check => "<div class='padding-2 driver-assign bg-danger cursor-pointer' data-date='$dt_check' data-type='' data-status='0' data-vehicle='$vehicle_id' data-schedule='' data-driver=''>Водитель не назначен</div>"
                            );
                        else
                            $ret_arr[$vehicle_id][$dt_check] = "<div class='padding-2 driver-assign bg-danger cursor-pointer' data-date='$dt_check' data-type='' data-status='0' data-vehicle='$vehicle_id' data-schedule='' data-driver=''>Водитель не назначен</div>";
                    } //$empty_result
                    
                    pg_free_result($result);
                } //$result
            } //parameter_set($params)
            
            $dt_check = date('Y-m-d', strtotime('+1 day', strtotime($dt_check)));
        } //strtotime($dt_check) <= strtotime($date_end)
    } //$company_vehicles as $vehicle_id => $vehicle
    
    /*
     *	associative to normal array conversion
     */
    $tmp_arr = array();
    foreach ($ret_arr as $key => $value) {
        $tmp_arr[] = $value;
    } //$ret_arr as $key => $value
    
    return $tmp_arr;
}

function get_bg_schedule_class_name($driver_type, $driver_class_num)
{
    if ($driver_class_num == ScheduleStatusMask::DRIVER_NOT_SET) {
        return "bg-danger";
    } //$driver_class_num == ScheduleStatusMask::DRIVER_NOT_SET
    
    if ($driver_type == DriverType::EMPLOYEE_RENT) {
        if ($driver_class_num & ScheduleStatusMask::DRIVER_OFF_SHIFT_VEHICLE) {
            return "bg-success";
        } //$driver_class_num & ScheduleStatusMask::DRIVER_OFF_SHIFT_VEHICLE
        else if ($driver_class_num & ScheduleStatusMask::DRIVER_ON_SHIFT) {
            return "bg-primary";
        } //$driver_class_num & ScheduleStatusMask::DRIVER_ON_SHIFT
        else if ($driver_class_num & ScheduleStatusMask::DRIVER_SET) {
            return "bg-info";
        } //$driver_class_num & ScheduleStatusMask::DRIVER_SET
    } //$driver_type == DriverType::EMPLOYEE_RENT
    else if ($driver_type == DriverType::EMPLOYEE_PAY) {
        if ($driver_class_num & ScheduleStatusMask::DRIVER_OFF_SHIFT_MONEY) {
            if ($driver_class_num & ScheduleStatusMask::DRIVER_OFF_SHIFT_VEHICLE) {
                return "bg-success";
            } //$driver_class_num & ScheduleStatusMask::DRIVER_OFF_SHIFT_VEHICLE
            else {
                return "bg-warning";
            }
        } //$driver_class_num & ScheduleStatusMask::DRIVER_OFF_SHIFT_MONEY
        else if ($driver_class_num & ScheduleStatusMask::DRIVER_OFF_SHIFT_VEHICLE) {
            return "bg-warning";
        } //$driver_class_num & ScheduleStatusMask::DRIVER_OFF_SHIFT_VEHICLE
        else if ($driver_class_num & ScheduleStatusMask::DRIVER_ON_SHIFT) {
            return "bg-primary";
        } //$driver_class_num & ScheduleStatusMask::DRIVER_ON_SHIFT
        else if ($driver_class_num & ScheduleStatusMask::DRIVER_SET) {
            return "bg-info";
        } //$driver_class_num & ScheduleStatusMask::DRIVER_SET
    } //$driver_type == DriverType::EMPLOYEE_PAY
}

function get_columns_for_period($date_begin, $date_end)
{
    $ret_arr = array();
    
    $ret_arr[] = array(
        'name' => 'vehicle',
        'title' => 'Автомобиль'
    );
    
    $date_begin = date('Y-m-d', strtotime($date_begin));
    $date_end   = date('Y-m-d', strtotime($date_end));
    
    while (strtotime($date_begin) <= strtotime($date_end)) {
        $ret_arr[] = array(
            'name' => $date_begin,
            'title' => $date_begin
        );
        
        $date_begin = date('Y-m-d', strtotime('+1 day', strtotime($date_begin)));
    } //strtotime($date_begin) <= strtotime($date_end)
    
    return $ret_arr;
}

function get_driver_tab($db, $schedule_id)
{
    $ret = null;
    
    $params = array(
        ScheduleTabIndex::ASSIGN_DRIVER,
        $schedule_id
    );
    
    $query = 'SELECT 
			schedule.*,
			schedule_remarks.*
		FROM
			postgres.public.schedule as schedule
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.schedule_remarks
		) AS schedule_remarks ON schedule.schedule_id=schedule_remarks.schedule_remarks_schedule_id and schedule_remarks.schedule_remarks_tab_index=$1
		WHERE
			schedule.schedule_id=$2';
    
    if (parameter_set($params)) {
        $query_name = "get_driver_tab_query";
        $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
            $query_name
        ));
        
        if (!$result || pg_num_rows($result) == 0) {
            $result = pg_prepare($db, $query_name, $query);
        } //!$result || pg_num_rows( $result ) == 0
        
        $result = pg_execute($db, $query_name, $params);
        
        if ($result) {
            while ($row = pg_fetch_assoc($result)) {
                $ret = $row;
            } //$row = pg_fetch_assoc($result)
            
            pg_free_result($result);
        } //$result
    } //parameter_set($params)
    
    return $ret;
}

function get_on_shift_tab($db, $schedule_id)
{
    $ret = null;
    
    $params = array(
        ScheduleTabIndex::ASSIGN_ON_SHIFT,
        $schedule_id
    );
    
    $query = 'SELECT 
			schedule.*,
			schedule_remarks.*,
			vehicle_maintenance.*,
			vehicle_gasoline.*
		FROM
			postgres.public.schedule as schedule
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.schedule_remarks
		) AS schedule_remarks ON schedule.schedule_id=schedule_remarks.schedule_remarks_schedule_id and schedule_remarks.schedule_remarks_tab_index=$1
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.vehicle_maintenance
		) AS vehicle_maintenance ON schedule.schedule_id=vehicle_maintenance.vehicle_maintenance_schedule_id and vehicle_maintenance.vehicle_maintenance_tab_index=$1
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.vehicle_gasoline
		) AS vehicle_gasoline ON schedule.schedule_id=vehicle_gasoline.vehicle_gasoline_schedule_id and vehicle_maintenance.vehicle_maintenance_tab_index=$1
		WHERE
			schedule.schedule_id=$2';
    
    if (parameter_set($params)) {
        $query_name = "get_on_shift_tab_query";
        $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
            $query_name
        ));
        
        if (!$result || pg_num_rows($result) == 0) {
            $result = pg_prepare($db, $query_name, $query);
        } //!$result || pg_num_rows( $result ) == 0
        
        $result = pg_execute($db, $query_name, $params);
        
        if ($result) {
            while ($row = pg_fetch_assoc($result)) {
                $ret = $row;
            } //$row = pg_fetch_assoc($result)
            
            pg_free_result($result);
        } //$result
    } //parameter_set($params)
    
    return $ret;
}

function get_off_shift_vehicle_disabled($db, $schedule_id)
{
    $schedule = null;
    
    $params = array(
        $schedule_id
    );
    
    $query = 'SELECT 
			schedule.*,
			vehicle.*
		FROM
			postgres.public.schedule as schedule
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.vehicle
		) AS vehicle ON vehicle.vehicle_id=schedule.schedule_vehicle_id
		WHERE
			schedule.schedule_id=$1';
    
    if (parameter_set($params)) {
        $query_name = "get_off_shift_vehicle_disabled_1_query";
        $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
            $query_name
        ));
        
        if (!$result || pg_num_rows($result) == 0) {
            $result = pg_prepare($db, $query_name, $query);
        } //!$result || pg_num_rows( $result ) == 0
        
        $result = pg_execute($db, $query_name, $params);
        
        if ($result) {
            while ($row = pg_fetch_assoc($result)) {
                $schedule = $row;
            } //$row = pg_fetch_assoc($result)
            
            pg_free_result($result);
        } //$result
    } //parameter_set($params)
    
    $orders = array();
    
    // TODO: take time adjustment from the config file
    $timezone = get_config_params($db, 'config_timezone');
    $timezone = $timezone[0]['config_timezone_from_moscow'];
//    error_log('timezone ' . var_dump('123'));
    $params = array(
        $schedule['schedule_driver_id'],
        date('Y-m-d H:i:s', strtotime($timezone .' hours', strtotime($schedule['schedule_datetime_begin_shift'])))
    );
    
    $query = 'SELECT 
			o.*
		FROM
			postgres.public."order" as o, postgres.public.gett as g
		WHERE
		g.gett_driver_id = $1 AND
		o.order_gett_id = g.gett_id AND
		o.order_datetime_finish >= $2 and o.order_datetime_finish < now()::timestamp';
    
    if (parameter_set($params)) {
        $query_name = "get_order_for_off_shift_vehicle_disabled_2_query";
        $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
            $query_name
        ));
        
        if (!$result || pg_num_rows($result) == 0) {
            $result = pg_prepare($db, $query_name, $query);
        } //!$result || pg_num_rows( $result ) == 0
        
        $result = pg_execute($db, $query_name, $params);
        
        if ($result) {
            while ($row = pg_fetch_assoc($result)) {
                $orders[] = $row;
            } //$row = pg_fetch_assoc($result)
            
            pg_free_result($result);
        } //$result
    } //parameter_set($params)
    
    $ret_arr                      = array();
    $distance_traveled_for_orders = 0;
    foreach ($orders as $key => $value) {
        if (isset($value['order_distance_travel'])) {
            $distance_traveled_for_orders += $value['order_distance_travel'];
        } //isset($value['order_distance_travel'])
    } //$orders as $key => $value
    
    // TODO: add coefficient of distance travel
    $travel_plan_km                   = $distance_traveled_for_orders * 1.25;
    $ret_arr['travel_plan_km']        = $travel_plan_km;
    // TODO: add coefficient of gas cost
    $gas_cost_per_l              = get_config_params($db, 'config_gas');
    $gas_cost_per_l              = strval($gas_cost_per_l[0]['config_gas_cost']);
    $gasoline_cost_per_l              = get_config_params($db, 'config_gasoline');
    $gasoline_cost_per_l              = strval($gasoline_cost_per_l[0]['config_gasoline_cost']);

//    $gasoline_cost_per_l = 54;
    $gas_plan_payment            = $travel_plan_km / 100 * $schedule['vehicle_l_per_100_km'] * $gas_cost_per_l;

    $gasoline_plan_payment            = $travel_plan_km / 100 * $schedule['vehicle_l_per_100_km'] * $gasoline_cost_per_l;
    $ret_arr['gasoline_plan_payment'] = $gasoline_plan_payment;
    $ret_arr['gas_plan_payment'] = $gas_plan_payment;
    
    return $ret_arr;
}

function get_off_shift_vehicle_tab($db, $schedule_id)
{
    $ret = null;
    
    $params = array(
        ScheduleTabIndex::ASSIGN_OFF_SHIFT_VEHICLE,
        $schedule_id,
        ScheduleTransactionsType::OUTGOING,
        ScheduleTransactionsSubtype::GASOLINE
    );
    
    $query = 'SELECT 
			schedule.*,
			schedule_remarks.*,
			vehicle_maintenance.*,
			vehicle_gasoline.*,
			schedule_transactions.*
		FROM
			postgres.public.schedule as schedule
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.schedule_remarks
		) AS schedule_remarks ON 
			schedule.schedule_id=schedule_remarks.schedule_remarks_schedule_id and 
			schedule_remarks.schedule_remarks_tab_index=$1
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.vehicle_maintenance
		) AS vehicle_maintenance ON 
			schedule.schedule_id=vehicle_maintenance.vehicle_maintenance_schedule_id and 
			vehicle_maintenance.vehicle_maintenance_tab_index=$1
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.vehicle_gasoline
		) AS vehicle_gasoline ON 
			schedule.schedule_id=vehicle_gasoline.vehicle_gasoline_schedule_id and 
			vehicle_maintenance.vehicle_maintenance_tab_index=$1
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.schedule_transactions
		) AS schedule_transactions ON 
			schedule.schedule_id=schedule_transactions.schedule_transactions_schedule_id and 
			schedule_transactions.schedule_transactions_type=$3 and 
			schedule_transactions.schedule_transactions_subtype=$4
		WHERE
			schedule.schedule_id=$2';
    
    if (parameter_set($params)) {
        $query_name = "get_off_shift_vehicle_tab_query";
        $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
            $query_name
        ));
        
        if (!$result || pg_num_rows($result) == 0) {
            $result = pg_prepare($db, $query_name, $query);
        } //!$result || pg_num_rows( $result ) == 0
        
        $result = pg_execute($db, $query_name, $params);
        
        if ($result) {
            while ($row = pg_fetch_assoc($result)) {
                $ret = $row;
            } //$row = pg_fetch_assoc($result)
            
            pg_free_result($result);
        } //$result
    } //parameter_set($params)
    
    return $ret;
}

function get_off_shift_money_disabled($db, $schedule_id)
{
    $schedule = null;
    
    $params = array(
        $schedule_id
    );
    
    $query = 'SELECT 
			schedule.*
		FROM
			postgres.public.schedule as schedule
		WHERE
			schedule.schedule_id=$1';
    
    if (parameter_set($params)) {
        $query_name = "get_off_shift_money_disabled_1_query";
        $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
            $query_name
        ));
        
        if (!$result || pg_num_rows($result) == 0) {
            $result = pg_prepare($db, $query_name, $query);
        } //!$result || pg_num_rows( $result ) == 0
        
        $result = pg_execute($db, $query_name, $params);
        
        if ($result) {
            while ($row = pg_fetch_assoc($result)) {
                $schedule = $row;
            } //$row = pg_fetch_assoc($result)
            
            pg_free_result($result);
        } //$result
    } //parameter_set($params)
    
    $orders = array();
    
    // TODO: take time adjustment from the config file
    $params = array(
        $schedule['schedule_driver_id'],
        date('Y-m-d H:i:s', strtotime('-4 hours', strtotime($schedule['schedule_datetime_begin_shift'])))
    );
    
    $query = 'SELECT 
			o.*
		FROM
			postgres.public."order" as o, postgres.public.gett as g
		WHERE
		g.gett_driver_id = $1 AND
		o.order_gett_id = g.gett_id AND
		o.order_datetime_finish >= $2 and o.order_datetime_finish < now()::timestamp';
    
    if (parameter_set($params)) {
        $query_name = "get_order_for_off_shift_money_disabled_2_query";
        $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
            $query_name
        ));
        
        if (!$result || pg_num_rows($result) == 0) {
            $result = pg_prepare($db, $query_name, $query);
        } //!$result || pg_num_rows( $result ) == 0
        
        $result = pg_execute($db, $query_name, $params);
        
        if ($result) {
            while ($row = pg_fetch_assoc($result)) {
                $orders[] = $row;
            } //$row = pg_fetch_assoc($result)
            
            pg_free_result($result);
        } //$result
    } //parameter_set($params)
    
    $ret_arr = array();
    
    $orders_count = 0;
    $orders_sum   = 0;
    $collect_cash = 0;
    
    foreach ($orders as $key => $value) {
        if (isset($value['order_tarif_driver_overall'])) {
            $orders_sum += $value['order_tarif_driver_overall'];
        } //isset($value['order_tarif_driver_overall'])
        
        if (isset($value['order_client_money'])) {
            $collect_cash += $value['order_client_money'];
        } //isset($value['order_client_money'])
        
        $orders_count++;
    } //$orders as $key => $value
    
    $ret_arr['orders_count'] = $orders_count;
    $ret_arr['orders_sum']   = $orders_sum;
    $ret_arr['collect_cash'] = $collect_cash;
    
    return $ret_arr;
}

function get_off_shift_money_tab($db, $schedule_id)
{
    $ret = null;
    
    $params = array(
        ScheduleTabIndex::ASSIGN_OFF_SHIFT_MONEY,
        $schedule_id,
        ScheduleTransactionsSubtype::MONEY_COLLECTED
    );
    
    $query = 'SELECT 
			schedule.*,
			schedule_remarks.*,
			schedule_transactions.*
		FROM
			postgres.public.schedule as schedule
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.schedule_remarks
		) AS schedule_remarks ON 
			schedule.schedule_id=schedule_remarks.schedule_remarks_schedule_id and 
			schedule_remarks.schedule_remarks_tab_index=$1
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.schedule_transactions
		) AS schedule_transactions ON 
			schedule.schedule_id=schedule_transactions.schedule_transactions_schedule_id and 
			schedule_transactions.schedule_transactions_subtype=$3
		WHERE
			schedule.schedule_id=$2';
    
    if (parameter_set($params)) {
        $query_name = "get_off_shift_money_tab_query";
        $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
            $query_name
        ));
        
        if (!$result || pg_num_rows($result) == 0) {
            $result = pg_prepare($db, $query_name, $query);
        } //!$result || pg_num_rows( $result ) == 0
        
        $result = pg_execute($db, $query_name, $params);
        
        if ($result) {
            while ($row = pg_fetch_assoc($result)) {
                $ret = $row;
            } //$row = pg_fetch_assoc($result)
            
            pg_free_result($result);
        } //$result
    } //parameter_set($params)
    
    return $ret;
}

function get_off_shift_money_manual_tab($db, $schedule_id)
{
    $ret = null;
    
    $params = array(
        $schedule_id,
        ScheduleTransactionsSubtype::MANUAL_TRANSACTION
    );
    
    $query = 'SELECT 
			schedule.*,
			schedule_transactions.*
		FROM
			postgres.public.schedule as schedule
		LEFT OUTER JOIN (
		  SELECT * FROM postgres.public.schedule_transactions
		) AS schedule_transactions ON 
			schedule.schedule_id=schedule_transactions.schedule_transactions_schedule_id and 
			schedule_transactions.schedule_transactions_subtype=$2
		WHERE
			schedule.schedule_id=$1';
    
    if (parameter_set($params)) {
        $query_name = "get_off_shift_money_manual_tab_query";
        $result     = pg_query_params($db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
            $query_name
        ));
        
        if (!$result || pg_num_rows($result) == 0) {
            $result = pg_prepare($db, $query_name, $query);
        } //!$result || pg_num_rows( $result ) == 0
        
        $result = pg_execute($db, $query_name, $params);
        
        if ($result) {
            while ($row = pg_fetch_assoc($result)) {
                $ret = $row;
            } //$row = pg_fetch_assoc($result)
            
            pg_free_result($result);
        } //$result
    } //parameter_set($params)
    
    return $ret;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

if (isset($clean_json['method']) && isset($clean_json['schedule_id'])) {
    $method      = $clean_json['method'];
    $schedule_id = $clean_json['schedule_id'];
    
    if ($method == "viewDriverTab") {
        $result = get_driver_tab($db, $schedule_id);
        $config = get_config_params($db, 'config_gas');
//        array_push($result, $config[0]['config_gas_cost']);
        echo json_encode($result);
    } //$method == "viewDriverTab"
    else if ($method == "viewOnShiftTab") {
        $result = get_on_shift_tab($db, $schedule_id);
        echo json_encode($result);
    } //$method == "viewOnShiftTab"
    else if ($method == "viewOffShiftVehicleDisabled") {
        $result = get_off_shift_vehicle_disabled($db, $schedule_id);
        echo json_encode($result);
    } //$method == "viewOffShiftVehicleDisabled"
    else if ($method == "viewOffShiftVehicleTab") {
        $result = get_off_shift_vehicle_tab($db, $schedule_id);
        echo json_encode($result);
    } //$method == "viewOffShiftVehicleTab"
    else if ($method == "viewOffShiftMoneyDisabled") {
        $result = get_off_shift_money_disabled($db, $schedule_id);
        echo json_encode($result);
    } //$method == "viewOffShiftMoneyDisabled"
    else if ($method == "viewOffShiftMoneyTab") {
        $result = get_off_shift_money_tab($db, $schedule_id);
        echo json_encode($result);
    } //$method == "viewOffShiftMoneyTab"
    else if ($method == "viewOffShiftMoneyManualTab") {
        $result = get_off_shift_money_manual_tab($db, $schedule_id);
        echo json_encode($result);
    } //$method == "viewOffShiftMoneyManualTab"
    //get configuration
//    else if ($method == "get_config"){
//        $result = get_config_params($db, )
//    }
    else {
        $message = 'Не верно указан параметр метод';
        $ret_arr = prepare_response_error_arr($db, $message, null);
        
        echo json_encode($ret_arr);
    }
} //isset($clean_json['method']) && isset($clean_json['schedule_id'])
else if (isset($clean_json['date_begin']) && isset($clean_json['date_end']) && isset($clean_json['type'])) {
    $date_begin = date('Y-m-d', strtotime($clean_json['date_begin']));
    $date_end   = date('Y-m-d', strtotime($clean_json['date_end']));
    $type       = $clean_json['type'];
    
    if (strtotime($date_begin) > strtotime($date_end)) {
        $message = 'Не верно указан период';
        $ret_arr = prepare_response_error_arr($db, $message, null);
        
        echo json_encode($ret_arr);
    } //strtotime($date_begin) > strtotime($date_end)
    
    if ($type === 'columns') {
        $result = get_columns_for_period($date_begin, $date_end);
        echo json_encode($result);
    } //$type === 'columns'
    else if ($type === 'rows') {
        $result = get_rows_for_period($db, $date_begin, $date_end);
        echo json_encode($result);
    } //$type === 'rows'
    else {
        $message = 'Не верно указан параметр тип для загрузки таблицы';
        $ret_arr = prepare_response_error_arr($db, $message, null);
        
        echo json_encode($ret_arr);
    }
} //isset($_REQUEST['method'])
else {
    $message = 'Не указан параметр метод или период';
    $ret_arr = prepare_response_error_arr($db, $message, null);
    
    echo json_encode($ret_arr);
}
?>