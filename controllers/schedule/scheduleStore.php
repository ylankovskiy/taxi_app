<?php
error_reporting(-1);

$patterns    = array(
  '/views',
  '/controllers',
  '/employee',
  '/driver',
  '/main',
  '/order',
  '/pay',
  '/schedule',
  '/transaction',
  '/vehicle',
  '/utils',
  '/select',
  '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";
require_once "utils/helpers/response.php";
require_once "utils/config/driverConfig.php";
require_once "utils/config/scheduleConfig.php";
require_once "utils/config/transactionsConfig.php";
require_once "classes/scheduleClass.php";
require_once "classes/scheduleRemarksClass.php";
require_once "classes/scheduleTransactionsClass.php";
require_once "classes/transactionsClass.php";
require_once "classes/vehicleGasolineClass.php";
require_once "classes/vehicleMaintenanceClass.php";
require_once "classes/driverSaldoClass.php";
require_once "classes/driverClass.php";

session_start();

function prepare_vehicle_gasoline_parameters($json, $tab_index, $post) {
	$json['vehicle_gasoline_vehicle_id'] = $json['schedule_vehicle_id'];
	$json['vehicle_gasoline_schedule_id'] = $json['schedule_id'];
	$json['vehicle_gasoline_date'] = $json['schedule_date'];
	$json['vehicle_gasoline_tab_index'] = $tab_index;
	$json['vehicle_gasoline_datetime_created'] = $json['schedule_datetime_created'];
	$json['vehicle_gasoline_employee_added_id'] = $json['schedule_employee_added_id'];
	$json['vehicle_gasoline_littres'] = $post['vehicle_gasoline_littres'];
	
	return $json;
}

function prepare_vehicle_maintenance_parameters($json, $tab_index, $post) {
	$json['vehicle_maintenance_vehicle_id'] = $json['schedule_vehicle_id'];
	$json['vehicle_maintenance_schedule_id'] = $json['schedule_id'];
	$json['vehicle_maintenance_date'] = $json['schedule_date'];
	$json['vehicle_maintenance_tab_index'] = $tab_index;
	$json['vehicle_maintenance_datetime_created'] = $json['schedule_datetime_created'];
	$json['vehicle_maintenance_employee_added_id'] = $json['schedule_employee_added_id'];
	$json['vehicle_maintenance_km'] = $post['vehicle_maintenance_km'];
	
	return $json;
}

function prepare_schedule_transactions_parameters($json, $tab_index, $post) {
	switch($tab_index) {
		case ScheduleTabIndex::ASSIGN_OFF_SHIFT_VEHICLE:
			$schedule_transactions_sum = $post['schedule_transactions_sum_hidden'] - $post['schedule_transactions_sum'];
			
			if ($schedule_transactions_sum < 0) {
				$json['schedule_transactions_type'] = ScheduleTransactionsType::OUTGOING;			
			} else if ($schedule_transactions_sum > 0) {
				$json['schedule_transactions_type'] = ScheduleTransactionsType::INCOMING;		
			}

			$json['schedule_transactions_subtype'] = ScheduleTransactionsSubtype::GASOLINE;
			
			// YL: save the initial value entered to display in the schedule off shift vehicle tab, in transactions table save the actual transaction value
			$json['schedule_transactions_sum'] = $post['schedule_transactions_sum'];
			break;
		case ScheduleTabIndex::ASSIGN_OFF_SHIFT_MONEY:
			$cash_gett_difference = $post['collect_cash_hidden'] - $post['accepted_cash'];
			
			// YL: save the initial value entered to display in the schedule off shift money tab, in transactions table save the actual transaction value
			if ($cash_gett_difference < 0) {
				$json['schedule_transactions_type'] = ScheduleTransactionsType::OUTGOING;
				$json['schedule_transactions_subtype'] = ScheduleTransactionsSubtype::MONEY_COLLECTED;
				$json['schedule_transactions_sum'] = abs($post['accepted_cash']);
			} else if ($cash_gett_difference > 0) {
				$json['schedule_transactions_type'] = ScheduleTransactionsType::INCOMING;
				$json['schedule_transactions_subtype'] = ScheduleTransactionsSubtype::MONEY_COLLECTED;
				$json['schedule_transactions_sum'] = abs($post['accepted_cash']);
			}
			break;
		default:
			return;
	}
	
	$json['schedule_transactions_schedule_id'] = $json['schedule_id'];
	$json['schedule_transactions_datetime_created'] = $json['schedule_datetime_created'];
	$json['schedule_transactions_datetime'] = $json['schedule_datetime_created'];
	$json['schedule_transactions_employee_added_id'] = $json['schedule_employee_added_id'];
	
	return $json;
}

function prepare_manual_schedule_transactions_parameters($json, $tab_index, $post) {
	switch($tab_index) {
		case ScheduleTabIndex::ASSIGN_OFF_SHIFT_MONEY:
			if ($post['manual_transaction'] == 'on') {
				$json['schedule_transactions_type'] = $post['manual_transaction_type'];
				$json['schedule_transactions_subtype'] = ScheduleTransactionsSubtype::MANUAL_TRANSACTION;
				$json['schedule_transactions_sum'] = abs($post['manual_transaction_sum']);
				$json['schedule_transactions_remark'] = $post['manual_transaction_remark'];
			}
			break;
		default:
			return;
	}
	
	$json['schedule_transactions_schedule_id'] = $json['schedule_id'];
	$json['schedule_transactions_datetime_created'] = $json['schedule_datetime_created'];
	$json['schedule_transactions_datetime'] = $json['schedule_datetime_created'];
	$json['schedule_transactions_employee_added_id'] = $json['schedule_employee_added_id'];
	
	return $json;
}

function prepare_vehicle_wash_schedule_transactions_parameters($json, $tab_index, $post) {	
	switch($tab_index) {
		case ScheduleTabIndex::ASSIGN_OFF_SHIFT_MONEY:
			if ($post['vehicle_wash_transaction'] == 'on') {
				$json['schedule_transactions_type'] = ScheduleTransactionsSubtype::INCOMING;
				$json['schedule_transactions_subtype'] = ScheduleTransactionsSubtype::VEHICLE_WASH;
				// TODO: get vehicle wash sum from the config
				$json['schedule_transactions_sum'] = 150;
			}
			break;
		default:
			return;
	}
	
	$json['schedule_transactions_schedule_id'] = $json['schedule_id'];
	$json['schedule_transactions_datetime_created'] = $json['schedule_datetime_created'];
	$json['schedule_transactions_datetime'] = $json['schedule_datetime_created'];
	$json['schedule_transactions_employee_added_id'] = $json['schedule_employee_added_id'];
	
	return $json;
}

function prepare_transactions_parameters($json, $tab_index, $post) {
	switch($tab_index) {
		case ScheduleTabIndex::ASSIGN_OFF_SHIFT_VEHICLE:
		$transactions_sum = $post['schedule_transactions_sum_hidden'] - $post['schedule_transactions_sum'];
			
			if ($schedule_transactions_sum < 0) {
				$json['transactions_type'] = TransactionsType::OUTGOING;
			} else {
				$json['transactions_type'] = TransactionsType::OUTGOING;
			}

			$json['transactions_subtype'] = TransactionsSubtype::GASOLINE;
			$json['transactions_sum'] = $transactions_sum;
			break;
		case ScheduleTabIndex::ASSIGN_OFF_SHIFT_MONEY:
			$cash_gett_difference = $post['collect_cash_hidden'] - $post['accepted_cash'];
			
			if ($cash_gett_difference < 0) {
				$json['transactions_type'] = TransactionsType::OUTGOING;
				$json['transactions_subtype'] = TransactionsSubtype::CAB_FAIRS;
				$json['transactions_sum'] = abs($cash_gett_difference);
			} else if ($cash_gett_difference > 0) {
				$json['transactions_type'] = TransactionsType::INCOMING;
				$json['transactions_subtype'] = TransactionsSubtype::CAB_FAIRS;
				$json['transactions_sum'] = abs($cash_gett_difference);
			}
			break;
		default:
			return;
	}
	
	$json['transactions_driver_id'] = $json['schedule_driver_id'];
	$json['transactions_datetime_created'] = $json['schedule_datetime_created'];
	$json['transactions_datetime'] = $json['schedule_datetime_created'];
	$json['transactions_employee_added_id'] = $json['schedule_employee_added_id'];
	
	return $json;
}

function prepare_manual_transactions_parameters($json, $tab_index, $post) {	
	switch($tab_index) {
		case ScheduleTabIndex::ASSIGN_OFF_SHIFT_MONEY:
			if ($post['manual_transaction'] == 'on') {
				$json['transactions_type'] = $post['manual_transaction_type'];
				$json['transactions_subtype'] = TransactionsSubtype::MANUAL_TRANSACTION;
				$json['transactions_sum'] = abs($post['manual_transaction_sum']);
				$json['transactions_remark'] = $post['manual_transaction_remark'];
			}
			break;
		default:
			return;
	}
	
	$json['transactions_driver_id'] = $json['schedule_driver_id'];
	$json['transactions_datetime_created'] = $json['schedule_datetime_created'];
	$json['transactions_datetime'] = $json['schedule_datetime_created'];
	$json['transactions_employee_added_id'] = $json['schedule_employee_added_id'];
	
	return $json;
}

function prepare_vehicle_wash_transactions_parameters($json, $tab_index, $post) {	
	switch($tab_index) {
		case ScheduleTabIndex::ASSIGN_OFF_SHIFT_MONEY:
			if ($post['vehicle_wash_transaction'] == 'on') {
				$json['transactions_type'] = TransactionsType::INCOMING;
				$json['transactions_subtype'] = TransactionsSubtype::VEHICLE_WASH;
				// TODO: get vehicle wash sum from the config
				$json['transactions_sum'] = 150;
			}
			break;
		default:
			return;
	}
	
	$json['transactions_driver_id'] = $json['schedule_driver_id'];
	$json['transactions_datetime_created'] = $json['schedule_datetime_created'];
	$json['transactions_datetime'] = $json['schedule_datetime_created'];
	$json['transactions_employee_added_id'] = $json['schedule_employee_added_id'];
	
	return $json;
}

function prepare_schedule_remarks_parameters($json, $tab_index, $post) {	
	$json['schedule_remarks_tab_index'] = $tab_index;
	$json['schedule_remarks_remark'] = $post['schedule_remark'];
	$json['schedule_remarks_schedule_id'] = $json['schedule_id'];
	$json['schedule_remarks_datetime_created'] = $json['schedule_datetime_created'];
	$json['schedule_remarks_employee_added_id'] = $json['schedule_employee_added_id'];
	
	return $json;
}

function store_schedule($db, $post)
{
  $ret_arr = array();
  
	pg_query($db, 'BEGIN');
            
  $schedule = new scheduleClass($post);
  $schedule_remarks_tab_index = null;
  
  	if ($post['schedule_status'] == ScheduleStatusMask::DRIVER_NOT_SET) {
  	
  	} else if ($post['schedule_status'] == ScheduleStatusMask::DRIVER_SET) {
		$tab_index = ScheduleTabIndex::ASSIGN_DRIVER;
		$result = $schedule->saveSchedule($db);
  		
  		if (!$result) {
   			$message = 'Произошла ошибка сохранения данных расписания';
		    $ret_arr = prepare_response_error_arr($db, $message, $schedule->selectParameters());
      
			goto ret;
		}
  	} else if ($post['schedule_status'] >= ScheduleStatusMask::DRIVER_OFF_SHIFT_MONEY) {
  		if ($post['driver_type'] == DriverType::EMPLOYEE_PAY) {
			$tab_index = ScheduleTabIndex::ASSIGN_OFF_SHIFT_MONEY;
  		
	  		$result = $schedule->saveSchedule($db);
  			if (!$result) {
    	 		$message = 'Произошла ошибка сохранения данных расписания';
			    $ret_arr = prepare_response_error_arr($db, $message, $schedule->selectParameters());
      
				goto ret;
			}
		
  			$schedule_transactions_parameters = prepare_schedule_transactions_parameters($schedule->selectParameters(), $tab_index, $post);
  			$transactions_parameters = prepare_transactions_parameters($schedule->selectParameters(), $tab_index, $post);
  		
	  		$scheduleTransactions = new scheduleTransactionsClass($schedule_transactions_parameters);
  			$transactions = new transactionsClass($transactions_parameters);
		
			$result = $scheduleTransactions->saveScheduleTransactions($db);
	  		if (!$result) {
     			$message = 'Произошла ошибка сохранения данных при закрытие смены по деньгам, запись в реестр транзакций для расписаний';
			    $ret_arr = prepare_response_error_arr($db, $message, $scheduleTransactions->selectParameters());
      
				goto ret;
			}
		
			$result = $transactions->saveTransactions($db);
	  		if (!$result) {
     			$message = 'Произошла ошибка сохранения данных при закрытие смены по деньгам, запись в реестр транзакций по компании';
			    $ret_arr = prepare_response_error_arr($db, $message, $transactions->selectParameters());
      	
				goto ret;
			}
			
			if ($post['manual_transaction'] == 'on') {
  				$manual_schedule_transactions_parameters = prepare_manual_schedule_transactions_parameters($schedule->selectParameters(), $tab_index, $post);
	  			$manual_transactions_parameters = prepare_manual_transactions_parameters($schedule->selectParameters(), $tab_index, $post);
	  		
	  			$manualScheduleTransactions = new scheduleTransactionsClass($manual_schedule_transactions_parameters);
  				$manualTransactions = new transactionsClass($manual_transactions_parameters);
  		
				$result = $manualScheduleTransactions->saveScheduleTransactions($db);
  				if (!$result) {
	    	 		$message = 'Произошла ошибка сохранения данных ручной проводки при закрытие смены по деньгам, запись в реестр транзакций для расписаний, сохранение ручной проводки';
		    		$ret_arr = prepare_response_error_arr($db, $message, $scheduleTransactions->selectParameters());
      	
					goto ret;
				}
		
				$result = $manualTransactions->saveTransactions($db);
	  			if (!$result) {
     				$message = 'Произошла ошибка сохранения данных ручной проводки при закрытие смены по деньгам, запись в реестр транзакций по компании, сохранение ручной проводки';
			    	$ret_arr = prepare_response_error_arr($db, $message, $transactions->selectParameters());
      
					goto ret;
				}
  			}
  			
  			if ($post['vehicle_wash_transaction'] == 'on') {
  				$vehicle_wash_schedule_transactions_parameters = prepare_vehicle_wash_schedule_transactions_parameters($schedule->selectParameters(), $tab_index, $post);
	  			$vehicle_wash_transactions_parameters = prepare_vehicle_wash_transactions_parameters($schedule->selectParameters(), $tab_index, $post);
	  		
	  			$vehicleWashScheduleTransactions = new scheduleTransactionsClass($vehicle_wash_schedule_transactions_parameters);
  				$vehicleWashTransactions = new transactionsClass($vehicle_wash_transactions_parameters);
  		
				$result = $vehicleWashScheduleTransactions->saveScheduleTransactions($db);
  				if (!$result) {
	    	 		$message = 'Произошла ошибка сохранения данных ручной проводки при закрытие смены по деньгам, запись в реестр транзакций для расписаний, сохранение мойки автомобиля';
		    		$ret_arr = prepare_response_error_arr($db, $message, $scheduleTransactions->selectParameters());
      	
					goto ret;
				}
		
				$result = $vehicleWashTransactions->saveTransactions($db);
	  			if (!$result) {
     				$message = 'Произошла ошибка сохранения данных ручной проводки при закрытие смены по деньгам, запись в реестр транзакций по компании, сохранение мойки автомобиля';
			    	$ret_arr = prepare_response_error_arr($db, $message, $transactions->selectParameters());
      
					goto ret;
				}
  			}
		} else {
			$message = 'Не верный тип водителя для закрытия смены по денижным средствам';
		    $ret_arr = prepare_response_error_arr($db, $message, $post);
      
			goto ret;
		}
  	} else if ($post['schedule_status'] >= ScheduleStatusMask::DRIVER_OFF_SHIFT_VEHICLE) {
  		if ($post['driver_type'] == DriverType::EMPLOYEE_RENT || $post['driver_type'] == DriverType::EMPLOYEE_PAY) {
			$tab_index = ScheduleTabIndex::ASSIGN_OFF_SHIFT_VEHICLE;
  		
  			$result = $schedule->saveSchedule($db);
	  		if (!$result) {
     			$message = 'Произошла ошибка сохранения данных расписания';
			    $ret_arr = prepare_response_error_arr($db, $message, $schedule->selectParameters());
      
				goto ret;
			}
		
  			$vehicle_gasoline_parameters = prepare_vehicle_gasoline_parameters($schedule->selectParameters(), $tab_index, $post);
  			$vehicle_maintenance_parameters = prepare_vehicle_maintenance_parameters($schedule->selectParameters(), $tab_index, $post);
  		
  			$vehicleGasoline = new vehicleGasolineClass($vehicle_gasoline_parameters);
	  		$vehicleMaintenance = new vehicleMaintenanceClass($vehicle_maintenance_parameters);
  			
  			
			$result = $vehicleMaintenance->saveVehicleMaintenance($db);
  			if (!$result) {
    	 		$message = 'Произошла ошибка сохранения данных автомобиля при закрытие смены';
			    $ret_arr = prepare_response_error_arr($db, $message, $vehicleMaintenance->selectParameters());
      
				goto ret;
			}
		
			$result = $vehicleGasoline->saveVehicleGasoline($db);
	  		if (!$result) {
     			$message = 'Произошла ошибка сохранения данных автомобиля при закрытие смены';
			    $ret_arr = prepare_response_error_arr($db, $message, $vehicleGasoline->selectParameters());
      
				goto ret;
			}

	  		$schedule_transactions_parameters = prepare_schedule_transactions_parameters($schedule->selectParameters(), $tab_index, $post);
  			$transactions_parameters = prepare_transactions_parameters($schedule->selectParameters(), $tab_index, $post);
	  		
  			$scheduleTransactions = new scheduleTransactionsClass($schedule_transactions_parameters);
  			$transactions = new transactionsClass($transactions_parameters);
		
			$result = $scheduleTransactions->saveScheduleTransactions($db);
  			if (!$result) {
	     		$message = 'Произошла ошибка сохранения данных при закрытие смены по деньгам, запись в реестр транзакций для расписаний';
		    	$ret_arr = prepare_response_error_arr($db, $message, $scheduleTransactions->selectParameters());
      	
				goto ret;
			}
		
			$result = $transactions->saveTransactions($db);
	  		if (!$result) {
     			$message = 'Произошла ошибка сохранения данных при закрытие смены по деньгам, запись в реестр транзакций по компании';
			    $ret_arr = prepare_response_error_arr($db, $message, $transactions->selectParameters());
      
				goto ret;
			}
		} else {
			$message = 'Не верный тип водителя для закрытия смены по автомобилю';
		    $ret_arr = prepare_response_error_arr($db, $message, $post);
      
			goto ret;
		}
  	} else if ($post['schedule_status'] > ScheduleStatusMask::DRIVER_ON_SHIFT) {
  		if ($post['driver_type'] == DriverType::EMPLOYEE_RENT || $post['driver_type'] == DriverType::EMPLOYEE_PAY) {
  			$current_date = date("Y-m-d");
  			$compare_date = date("Y-m-d", strtotime($post['schedule_date']));
  			if ($current_date != $compare_date) {
  				$message = 'Произошла ошибка сохранения данных, не возможно создать смену на не текущую дату';
			    $ret_arr = prepare_response_error_arr($db, $message, $vehicleGasoline->selectParameters());
      
      			goto ret;
  			} else {
				$tab_index = ScheduleTabIndex::ASSIGN_ON_SHIFT;
  		
	  			$result = $schedule->saveSchedule($db);
  				if (!$result) {
    		 		$message = 'Произошла ошибка сохранения данных расписания';
				    $ret_arr = prepare_response_error_arr($db, $message, $schedule->selectParameters());
      
					goto ret;
				}
		
  				$vehicle_gasoline_parameters = prepare_vehicle_gasoline_parameters($schedule->selectParameters(), $tab_index, $post);
		  		$vehicle_maintenance_parameters = prepare_vehicle_maintenance_parameters($schedule->selectParameters(), $tab_index, $post);
  		
	  			$vehicleGasoline = new vehicleGasolineClass($vehicle_gasoline_parameters);
  				$vehicleMaintenance = new vehicleMaintenanceClass($vehicle_maintenance_parameters);
		
				$result = $vehicleMaintenance->saveVehicleMaintenance($db);
  				if (!$result) {
	    	 		$message = 'Произошла ошибка сохранения данных автомобиля при назначении на линию';
			    	$ret_arr = prepare_response_error_arr($db, $message, $vehicleMaintenance->selectParameters());
      
					goto ret;
				}
		
				$result = $vehicleGasoline->saveVehicleGasoline($db);
		  		if (!$result) {
     				$message = 'Произошла ошибка сохранения данных автомобиля при назначении на линию';
				    $ret_arr = prepare_response_error_arr($db, $message, $vehicleGasoline->selectParameters());
      
					goto ret;
				}
			}
		} else {
			$message = 'Не верный тип водителя для открытия смены';
		    $ret_arr = prepare_response_error_arr($db, $message, $post);
      
			goto ret;
		}
  	}
  
  $schedule_remarks_parameters = prepare_schedule_remarks_parameters($schedule->selectParameters(), $tab_index, $post);

  $scheduleRemarks = new scheduleRemarksClass($schedule_remarks_parameters);

  $result = $scheduleRemarks->saveScheduleRemarks($db);
  if (!$result) {
  	$message = 'Произошла ошибка сохранения примечания';
	$ret_arr = prepare_response_error_arr($db, $message, $schedule_remarks_parameters);
      
	goto ret;
  }
    
  $message = 'Удачное сохранение данных';
  $ret_arr = prepare_response_success_arr($db, $message);
  
	pg_query($db, 'COMMIT');

  ret:
  
  return $ret_arr;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

if (isset($clean_json['method'])) {
  $method = $clean_json['method'];
  
  if ($method == 'store') {
    if (isset($clean_json['acceptTerms']) && $clean_json['acceptTerms'] == 'on') {
      $result = store_schedule($db, $clean_json);
      
      echo json_encode($result);
    } else {
		$message = 'Нет подтверждения о верности заполненных данных';
    	$ret_arr = prepare_response_error_arr($db, $message, $clean_json);
	    echo json_encode($ret_arr);
    }
  } else {
	$message = 'Указан не верный параметр метод';
    $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
    echo json_encode($ret_arr);
  }
} else {
	$message = 'Не указан параметр метод';
    $ret_arr = prepare_response_error_arr($db, $message, $clean_json);
    echo json_encode($ret_arr);
}
?>