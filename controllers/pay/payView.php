<?php
error_reporting(-1);

$patterns = array('/views', '/controllers', '/employee', '/driver', '/main', '/order', '/pay', '/schedule', '/transaction', '/vehicle', '/utils', '/select',
    '/classes',
    '/bank',
    '/configuration');
$replacement = '';
$path = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";
require_once "utils/helpers/response.php";
require_once "utils/config/driverConfig.php";
require_once "utils/config/driverPayReportConfig.php";

session_start();

function get_driver_payout_reports($db, $date) {
    $ret_arr = array();

	$params = array(
		date('Y-m-d', strtotime($date))
	);

    $query = 'SELECT 
		report.*,
		report_status.*,
		report_datestamp.*,
		driver.*
		FROM postgres.public.driver_payout_report as report
		LEFT OUTER JOIN (SELECT * FROM postgres.public.driver)
			AS driver ON report.driver_payout_report_driver_id = driver.driver_id
		LEFT OUTER JOIN (SELECT * FROM postgres.public.driver_payout_datestamp)
			AS report_datestamp ON report_datestamp.driver_payout_datestamp_report_id = report.driver_payout_report_id
		LEFT OUTER JOIN (select DISTINCT ON (driver_payout_report_status_report_id) * 
				FROM public.driver_payout_report_status GROUP BY driver_payout_report_status_id, driver_payout_report_status_report_id)
			AS report_status ON report_status.driver_payout_report_status_report_id = report.driver_payout_report_id
		WHERE report_datestamp.driver_payout_datestamp_date = $1
		ORDER BY report.driver_payout_report_datetime_created desc';

	if (parameter_set($params)) {
    	$query_name = "get_all_driver_payout_reports_query";
    	$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
			$query_name 
		) );
  
		if ( !$result || pg_num_rows( $result ) == 0 ) {
			$result = pg_prepare( $db, $query_name, $query );
		} //!$result || pg_num_rows( $result ) == 0
		
        $result = pg_execute($db, $query_name, $params);
            
        if ($result) {
            while ($row = pg_fetch_assoc($result)) {
            	$row['driver_payout_report_status_status_text'] = getDriverPayReportStatusText($row['driver_payout_report_status_status']);
            	$row['driver_type_text']   = getDriverTypetext($row['driver_type']);
        		$row['driver_status_text'] = getDriverStatustext($row['driver_status']);
            	
                $ret_arr[] = $row;
            }
                
            pg_free_result($result);
        }
    }

    return $ret_arr;
}

$clean_json = get_json_array_from_request($HTTP_RAW_POST_DATA);

if (isset($clean_json['method']) && isset($clean_json['date'])) {
    $method = $clean_json['method'];
    $date = $clean_json['date'];
  
    if ($method == 'view') {
        $result = get_driver_payout_reports($db, $date);

        echo json_encode($result);
    } else {
    	$message = 'Не верно указан параметр метод';
		$ret_arr = prepare_response_error_arr($db, $message, null);
		
        echo json_encode($ret_arr);
    }
} //isset($_REQUEST['method'])
else {
    $message = 'Не указан параметр метод или период';
    $ret_arr = prepare_response_error_arr($db, $message, null);
    
    echo json_encode($ret_arr);
}
?>