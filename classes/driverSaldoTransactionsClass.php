<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Driver saldo transactions class, follows the public.driver_saldo_transactions table naming convention and structure
 *
 */
class driverSaldoTransactionsClass {
    private $driver_saldo_transactions_id;
    private $driver_saldo_transactions_sum;
    private $driver_saldo_transactions_type;
    private $driver_saldo_transactions_date;
    private $driver_saldo_transactions_driver_id;
    private $driver_saldo_transactions_datetime_created;
    private $driver_saldo_transactions_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['driver_saldo_transactions_sum']) || !isset($json['driver_saldo_transactions_date']) || !isset($json['driver_saldo_transactions_driver_id']) || !isset($json['driver_saldo_transactions_type']))
            return;
        
        if (isset($json['driver_saldo_transactions_id']))
            $this->driver_saldo_transactions_id = $json['driver_saldo_transactions_id'];
        if (isset($json['driver_saldo_transactions_sum']))
            $this->driver_saldo_transactions_sum = $json['driver_saldo_transactions_sum'];
        if (isset($json['driver_saldo_transactions_type']))
            $this->driver_saldo_transactions_type = $json['driver_saldo_transactions_type'];
        if (isset($json['driver_saldo_transactions_date']))
            $this->driver_saldo_transactions_date = $json['driver_saldo_transactions_date'];
        if (isset($json['driver_saldo_transactions_driver_id']))
            $this->driver_saldo_transactions_driver_id = $json['driver_saldo_transactions_driver_id'];
        if (isset($json['driver_saldo_transactions_employee_added_id']))
            $this->driver_saldo_transactions_employee_added_id = $json['driver_saldo_transactions_employee_added_id'];
        
        $this->driver_saldo_transactions_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['driver_saldo_transactions_id']))
            $this->driver_saldo_transactions_id = $row['driver_saldo_transactions_id'];
        if (isset($row['driver_saldo_transactions_sum']))
            $this->driver_saldo_transactions_sum = $row['driver_saldo_transactions_sum'];
        if (isset($row['driver_saldo_transactions_type']))
            $this->driver_saldo_transactions_type = $row['driver_saldo_transactions_type'];
        if (isset($row['driver_saldo_transactions_date']))
            $this->driver_saldo_transactions_date = $row['driver_saldo_transactions_date'];
        if (isset($row['driver_saldo_transactions_driver_id']))
            $this->driver_saldo_transactions_driver_id = $row['driver_saldo_transactions_driver_id'];
        if (isset($row['driver_saldo_transactions_employee_added_id']))
            $this->driver_saldo_transactions_employee_added_id = $row['driver_saldo_transactions_employee_added_id'];
        if (isset($row['driver_saldo_transactions_datetime_created']))
            $this->driver_saldo_transactions_datetime_created = $row['driver_saldo_transactions_datetime_created'];
    }
    
    public function selectDriverSaldoId() {
        return $this->driver_saldo_transactions_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectDriverSaldoTransactionsFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
			FROM 
				postgres.public.driver_saldo_transactions
			WHERE
				oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_driver_saldo_transactions_oid_query";
        } else if (isset($this->driver_saldo_transactions_driver_id)) {
            $query = "SELECT *
			FROM 
				postgres.public.driver_saldo_transactions
			WHERE
				driver_saldo_transactions_driver_id = $1";
            
            $params = array(
                $this->driver_saldo_transactions_driver_id
            );
            
            $query_name = "select_driver_saldo_transactions_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewDriverSaldoTransactions($db, $object) {        
        $result = pg_insert($db, 'public.driver_saldo_transactions', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectDriverSaldoTransactionsFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentDriverSaldoTransactions($db, $object) {
        $object_key = array(
            'driver_saldo_transactions_id' => $object['driver_saldo_transactions_id']
        );
        
        unset($object['driver_saldo_transactions_id']);
        
        return pg_update($db, 'public.driver_saldo_transactions', $object, $object_key);
    }
    
    public function saveDriverSaldoTransactions($db) {
	    $object = $this->selectParameters();
        if (!isset($object['driver_saldo_transactions_id']) || $object['driver_saldo_transactions_id'] === null)
            return $this->saveNewDriverSaldoTransactions($db, $object);
        else
            return $this->saveCurrentDriverSaldoTransactions($db, $object);
    }
}
?>