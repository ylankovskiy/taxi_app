<?php
error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";
require_once "utils/config/scheduleConfig.php";

/*
 *	Schedule class, follows the public.schedule table naming convention and structure
 *
 */
class scheduleClass {
    private $schedule_id;
    private $schedule_date;
    private $schedule_driver_id;
    private $schedule_vehicle_id;
    private $schedule_status;
    private $schedule_datetime_begin_shift;
    private $schedule_datetime_end_shift_vehicle;
    private $schedule_datetime_end_shift_money;
    private $schedule_datetime_created;
    private $schedule_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['schedule_date']) || !isset($json['schedule_driver_id']) || !isset($json['schedule_vehicle_id']) || !isset($json['schedule_status']))
            return;
        
        if (isset($json['schedule_id']))
            $this->schedule_id = $json['schedule_id'];
        if (isset($json['schedule_date']))
            $this->schedule_date = $json['schedule_date'];
        if (isset($json['schedule_driver_id']))
            $this->schedule_driver_id = $json['schedule_driver_id'];
        if (isset($json['schedule_vehicle_id']))
            $this->schedule_vehicle_id = $json['schedule_vehicle_id'];
        if (isset($json['schedule_status'])) {
            $this->schedule_status = $json['schedule_status'];
            
            if ($json['schedule_status'] > ScheduleStatusMask::DRIVER_OFF_SHIFT_MONEY) {
        		$this->schedule_datetime_end_shift_money = date('Y-m-d H:i:s');
        	} else if ($json['schedule_status'] > ScheduleStatusMask::DRIVER_OFF_SHIFT_VEHICLE) {
        		$this->schedule_datetime_end_shift_vehicle = date('Y-m-d H:i:s');
        	} else if ($json['schedule_status'] > ScheduleStatusMask::DRIVER_ON_SHIFT) {
        		$this->schedule_datetime_begin_shift = date('Y-m-d H:i:s');
        	}
        }
        if (isset($json['schedule_employee_added_id']))
            $this->schedule_employee_added_id = $json['schedule_employee_added_id'];
        
        $this->schedule_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['schedule_id']))
            $this->schedule_id = $row['schedule_id'];
        if (isset($row['schedule_date']))
            $this->schedule_date = $row['schedule_date'];
        if (isset($row['schedule_driver_id']))
            $this->schedule_driver_id = $row['schedule_driver_id'];
        if (isset($row['schedule_vehicle_id']))
            $this->schedule_vehicle_id = $row['schedule_vehicle_id'];
        if (isset($row['schedule_status']))
            $this->schedule_status = $row['schedule_status'];
        if (isset($row['schedule_datetime_begin_shift']))
            $this->schedule_datetime_begin_shift = $row['schedule_datetime_begin_shift'];
        if (isset($row['schedule_datetime_end_shift_vehicle']))
            $this->schedule_datetime_end_shift_vehicle = $row['schedule_datetime_end_shift_vehicle'];
        if (isset($row['schedule_datetime_end_shift_money']))
            $this->schedule_datetime_end_shift_money = $row['schedule_datetime_end_shift_money'];
        if (isset($row['schedule_employee_added_id']))
            $this->schedule_employee_added_id = $row['schedule_employee_added_id'];
        if (isset($row['schedule_datetime_created']))
            $this->schedule_datetime_created = $row['schedule_datetime_created'];
    }
    
    public function selectScheduleId() {
        return $this->schedule_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    public function selectScheduleFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
			FROM 
				postgres.public.schedule
			WHERE
				oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_schedule_oid_query";
        } else if (isset($this->schedule_driver_id) && isset($this->schedule_vehicle_id)) {
            $set_query = false;
            
            if (isset($this->schedule_driver_id)) {
                $query = "SELECT *
				FROM 
					postgres.public.schedule
				WHERE
					schedule_driver_id = $1";
                
                $params = array(
                    $this->schedule_driver_id
                );
                
                $query_name = "select_schedule_driver_query";
                
                $set_query = true;
            }
            
            if (isset($this->schedule_vehicle_id)) {
                if ($set_query) {
                    $query .= " and schedule_vehicle_id = $2";
                    
                    $params[1] = $this->schedule_vehicle_id;
                } else {
                    $query = "SELECT *
					FROM 
						postgres.public.schedule
					WHERE
						schedule_vehicle_id = $1";
                    
                    $params = array(
                        $this->schedule_vehicle_id
                    );
                }
                
                $query_name = "select_schedule_vehicle_query";
            }
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewSchedule($db, $object) {
        $result = pg_insert($db, 'public.schedule', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectScheduleFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentSchedule($db, $object) {
        $object_key = array(
            'schedule_id' => $object['schedule_id']
        );
        
        unset($object['schedule_id']);
        
        return pg_update($db, 'public.schedule', $object, $object_key);
    }
    
    public function saveSchedule($db) {
	    $object = $this->selectParameters();
        if (!isset($object['schedule_id']) || $object['schedule_id'] === null)
            return $this->saveNewSchedule($db, $object);
        else
            return $this->saveCurrentSchedule($db, $object);
    }
}
?>