<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Driver class, follows the public.driver table naming convention and structure
 *
 */
class driverClass {
    private $driver_id;
    private $driver_name_first;
    private $driver_name_name;
    private $driver_name_last;
    private $driver_status;
    private $driver_date_birth;
    private $driver_lic_number;
    private $driver_telephone_book_id;
    private $driver_type;
    private $driver_passport_series;
    private $driver_passport_number;
    private $driver_passport_date_given;
    private $driver_passport_authority_given;
    private $driver_passport_address_living;
    private $driver_actual_address_living;
    private $driver_vehicle_id;
    private $driver_remark;
    private $driver_datetime_created;
    private $driver_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['driver_name_first']) || !isset($json['driver_name_name']) || !isset($json['driver_name_last']) || !isset($json['driver_name_first']) || !isset($json['driver_status']))
            return;
        
        if (isset($json['driver_id']))
            $this->driver_id = $json['driver_id'];
        if (isset($json['driver_name_first']))
            $this->driver_name_first = $json['driver_name_first'];
        if (isset($json['driver_name_name']))
            $this->driver_name_name = $json['driver_name_name'];
        if (isset($json['driver_name_last']))
            $this->driver_name_last = $json['driver_name_last'];
        if (isset($json['driver_status']))
            $this->driver_status = $json['driver_status'];
        if (isset($json['driver_date_birth']))
            $this->driver_date_birth = $json['driver_date_birth'];
        if (isset($json['driver_lic_number']))
            $this->driver_lic_number = $json['driver_lic_number'];
        if (isset($json['driver_telephone_book_id']))
            $this->driver_telephone_book_id = $json['driver_telephone_book_id'];
        if (isset($json['driver_type']))
            $this->driver_type = $json['driver_type'];
        if (isset($json['driver_passport_series']))
            $this->driver_passport_series = $json['driver_passport_series'];
        if (isset($json['driver_passport_number']))
            $this->driver_passport_number = $json['driver_passport_number'];
        if (isset($json['driver_passport_date_given']))
            $this->driver_passport_date_given = $json['driver_passport_date_given'];
        if (isset($json['driver_passport_authority_given']))
            $this->driver_passport_authority_given = $json['driver_passport_authority_given'];
        if (isset($json['driver_passport_address_living']))
            $this->driver_passport_address_living = $json['driver_passport_address_living'];
        if (isset($json['driver_actual_address_living']))
            $this->driver_actual_address_living = $json['driver_actual_address_living'];
        if (isset($json['driver_vehicle_id']))
            $this->driver_vehicle_id = $json['driver_vehicle_id'];
        if (isset($json['driver_remark']))
            $this->driver_remark = $json['driver_remark'];
        if (isset($json['driver_employee_added_id']))
            $this->driver_employee_added_id = $json['driver_employee_added_id'];

        $this->driver_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}

    private function fillParametersFromRow($row) {
        if (isset($row['driver_id']))
            $this->driver_id = $row['driver_id'];
        if (isset($row['driver_name_first']))
            $this->driver_name_first = $row['driver_name_first'];
        if (isset($row['driver_name_name']))
            $this->driver_name_name = $row['driver_name_name'];
        if (isset($row['driver_name_last']))
            $this->driver_name_last = $row['driver_name_last'];
        if (isset($row['driver_status']))
            $this->driver_status = $row['driver_status'];
        if (isset($row['driver_date_birth']))
            $this->driver_date_birth = $row['driver_date_birth'];
        if (isset($row['driver_lic_number']))
            $this->driver_lic_number = $row['driver_lic_number'];
        if (isset($row['driver_telephone_book_id']))
            $this->driver_telephone_book_id = $row['driver_telephone_book_id'];
        if (isset($row['driver_type']))
            $this->driver_type = $row['driver_type'];
        if (isset($row['driver_passport_series']))
            $this->driver_passport_series = $row['driver_passport_series'];
        if (isset($row['driver_passport_number']))
            $this->driver_passport_number = $row['driver_passport_number'];
        if (isset($row['driver_passport_date_given']))
            $this->driver_passport_date_given = $row['driver_passport_date_given'];
        if (isset($row['driver_passport_authority_given']))
            $this->driver_passport_authority_given = $row['driver_passport_authority_given'];
        if (isset($row['driver_passport_address_living']))
            $this->driver_passport_address_living = $row['driver_passport_address_living'];
        if (isset($row['driver_actual_address_living']))
            $this->driver_actual_address_living = $row['driver_actual_address_living'];
        if (isset($row['driver_vehicle_id']))
            $this->driver_vehicle_id = $row['driver_vehicle_id'];
        if (isset($row['driver_remark']))
            $this->driver_remark = $row['driver_remark'];
        if (isset($row['driver_datetime_created']))
            $this->driver_datetime_created = $row['driver_datetime_created'];
        if (isset($row['driver_employee_added_id']))
            $this->driver_employee_added_id = $row['driver_employee_added_id'];
    }
    
    public function selectDriverId() {
        return $this->driver_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    public function selectDriverFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        if (isset($this->driver_name_last)) {
            $query = 'SELECT *
				FROM 
			  	postgres.public.driver
				WHERE
					driver_name_last = $1 and
					driver_name_name = $2 and
					driver_name_first = $3 and
					driver_date_birth = $4';
            
            $params = array(
                $this->driver_name_last,
                $this->driver_name_name,
                $this->driver_name_first,
                $this->driver_date_birth
            );
            
            $query_name = "select_driver_query";
        } else if (isset($oid)) {
            $query = 'SELECT *
				FROM 
			  	postgres.public.driver
				WHERE
					oid = $1';
            
            $params = array(
                $oid
            );
            
            $query_name = "select_driver_oid_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
            }
        }
    }
    
    private function saveNewDriver($db, $object) {
        $result = pg_insert($db, 'public.driver', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectDriverFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentDriver($db, $object) {
        $object_key = array(
            'driver_id' => $object['driver_id']
        );
        
        unset($object['driver_id']);
        
        return pg_update($db, 'public.driver', $object, $object_key);
    }
    
    public function saveDriver($db) {
	    $object = $this->selectParameters();
        if (!isset($object['driver_id']) || $object['driver_id'] === null)
            return $this->saveNewDriver($db, $object);
        else
            return $this->saveCurrentDriver($db, $object);
    }
}
?>