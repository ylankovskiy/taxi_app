<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Driver saldo class, follows the public.driver_saldo table naming convention and structure
 *
 */
class driverSaldoClass {
    private $driver_saldo_id;
    private $driver_saldo_sum;
    private $driver_saldo_type;
    private $driver_saldo_date;
    private $driver_saldo_driver_id;
    private $driver_saldo_datetime_created;
    private $driver_saldo_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['driver_saldo_sum']) || !isset($json['driver_saldo_date']) || !isset($json['driver_saldo_driver_id']) || !isset($json['driver_saldo_type']))
            return;
        
        if (isset($json['driver_saldo_id']))
            $this->driver_saldo_id = $json['driver_saldo_id'];
        if (isset($json['driver_saldo_sum']))
            $this->driver_saldo_sum = $json['driver_saldo_sum'];
        if (isset($json['driver_saldo_type']))
            $this->driver_saldo_type = $json['driver_saldo_type'];
        if (isset($json['driver_saldo_date']))
            $this->driver_saldo_date = $json['driver_saldo_date'];
        if (isset($json['driver_saldo_driver_id']))
            $this->driver_saldo_driver_id = $json['driver_saldo_driver_id'];
        if (isset($json['driver_saldo_employee_added_id']))
            $this->driver_saldo_employee_added_id = $json['driver_saldo_employee_added_id'];
        
        $this->driver_saldo_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['driver_saldo_id']))
            $this->driver_saldo_id = $row['driver_saldo_id'];
        if (isset($row['driver_saldo_sum']))
            $this->driver_saldo_sum = $row['driver_saldo_sum'];
        if (isset($row['driver_saldo_type']))
            $this->driver_saldo_type = $row['driver_saldo_type'];
        if (isset($row['driver_saldo_date']))
            $this->driver_saldo_date = $row['driver_saldo_date'];
        if (isset($row['driver_saldo_driver_id']))
            $this->driver_saldo_driver_id = $row['driver_saldo_driver_id'];
        if (isset($row['driver_saldo_employee_added_id']))
            $this->driver_saldo_employee_added_id = $row['driver_saldo_employee_added_id'];
        if (isset($row['driver_saldo_datetime_created']))
            $this->driver_saldo_datetime_created = $row['driver_saldo_datetime_created'];
    }
    
    public function selectDriverSaldoId() {
        return $this->driver_saldo_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectDriverSaldoFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
			FROM 
				postgres.public.driver_saldo
			WHERE
				oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_driver_saldo_oid_query";
        } else if (isset($this->driver_saldo_driver_id)) {
            $query = "SELECT *
			FROM 
				postgres.public.driver_saldo
			WHERE
				driver_saldo_driver_id = $1";
            
            $params = array(
                $this->driver_saldo_driver_id
            );
            
            $query_name = "select_driver_saldo_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewDriverSaldo($db, $object) {        
        $result = pg_insert($db, 'public.driver_saldo', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectDriverSaldoFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentDriverSaldo($db, $object) {
        $object_key = array(
            'driver_saldo_id' => $object['driver_saldo_id']
        );
        
        unset($object['driver_saldo_id']);
        
        return pg_update($db, 'public.driver_saldo', $object, $object_key);
    }
    
    public function saveDriverSaldo($db) {
	    $object = $this->selectParameters();
        if (!isset($object['driver_saldo_id']) || $object['driver_saldo_id'] === null)
            return $this->saveNewDriverSaldo($db, $object);
        else
            return $this->saveCurrentDriverSaldo($db, $object);
    }
}
?>