<?php
error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Vehicle class, follows the public.vehicle table naming convention and structure
 *
 */
 
class vehicleClass {
    private $vehicle_id;
    private $vehicle_year_production;
    private $vehicle_make;
    private $vehicle_model;
    private $vehicle_color;
    private $vehicle_plate_number;
    private $vehicle_type;
    private $vehicle_vin;
    private $vehicle_insurance_kasko;
    private $vehicle_insurance_kasko_series;
    private $vehicle_insurance_kasko_num;
    private $vehicle_insurance_kasko_date_begin;
    private $vehicle_insurance_kasko_date_end;
    private $vehicle_insurance_osago;
    private $vehicle_insurance_osago_series;
    private $vehicle_insurance_osago_num;
    private $vehicle_insurance_osago_date_begin;
    private $vehicle_insurance_osago_date_end;
    private $vehicle_gas;
    private $vehicle_l_per_100_km;
    private $vehicle_maintenance_freq_km;
    private $vehicle_device;
    private $vehicle_device_model;
    private $vehicle_device_cost;
    private $vehicle_status;
    private $vehicle_remark;
    private $vehicle_datetime_created;
    private $vehicle_added_employee_id;
    
    public function __construct($json) {
        if (!isset($json['vehicle_make']) || !isset($json['vehicle_model']) || !isset($json['vehicle_year_production']) || !isset($json['vehicle_plate_number']) || !isset($json['vehicle_type']) || !isset($json['vehicle_status']))
            return;
        
        if (isset($json['vehicle_id']))
            $this->vehicle_id = $json['vehicle_id'];
        if (isset($json['vehicle_year_production']))
            $this->vehicle_year_production = $json['vehicle_year_production'];
        if (isset($json['vehicle_make']))
            $this->vehicle_make = $json['vehicle_make'];
        if (isset($json['vehicle_model']))
            $this->vehicle_model = $json['vehicle_model'];
        if (isset($json['vehicle_color']))
            $this->vehicle_color = $json['vehicle_color'];
        if (isset($json['vehicle_plate_number']))
            $this->vehicle_plate_number = $json['vehicle_plate_number'];
        if (isset($json['vehicle_type']))
            $this->vehicle_type = $json['vehicle_type'];
        if (isset($json['vehicle_vin']))
            $this->vehicle_vin = $json['vehicle_vin'];
        if (isset($json['vehicle_insurance_kasko']))
            $this->vehicle_insurance_kasko = $json['vehicle_insurance_kasko'];
        if (isset($json['vehicle_insurance_kasko_series']))
            $this->vehicle_insurance_kasko_series = $json['vehicle_insurance_kasko_series'];
        if (isset($json['vehicle_insurance_kasko_num']))
            $this->vehicle_insurance_kasko_num = $json['vehicle_insurance_kasko_num'];
        if (isset($json['vehicle_insurance_kasko_date_begin']))
            $this->vehicle_insurance_kasko_date_begin = $json['vehicle_insurance_kasko_date_begin'];
        if (isset($json['vehicle_insurance_kasko_date_end']))
            $this->vehicle_insurance_kasko_date_end = $json['vehicle_insurance_kasko_date_end'];
        if (isset($json['vehicle_insurance_osago']))
            $this->vehicle_insurance_osago = $json['vehicle_insurance_osago'];
        if (isset($json['vehicle_insurance_osago_series']))
            $this->vehicle_insurance_osago_series = $json['vehicle_insurance_osago_series'];
        if (isset($json['vehicle_insurance_osago_num']))
            $this->vehicle_insurance_osago_num = $json['vehicle_insurance_osago_num'];
        if (isset($json['vehicle_insurance_osago_date_begin']))
            $this->vehicle_insurance_osago_date_begin = $json['vehicle_insurance_osago_date_begin'];
        if (isset($json['vehicle_insurance_osago_date_end']))
            $this->vehicle_insurance_osago_date_end = $json['vehicle_insurance_osago_date_end'];
        if (isset($json['vehicle_gas']))
            $this->vehicle_gas = $json['vehicle_gas'];
        if (isset($json['vehicle_l_per_100_km']))
            $this->vehicle_l_per_100_km = $json['vehicle_l_per_100_km'];
        if (isset($json['vehicle_maintenance_freq_km']))
            $this->vehicle_maintenance_freq_km = $json['vehicle_maintenance_freq_km'];
        if (isset($json['vehicle_device']))
            $this->vehicle_device = $json['vehicle_device'];
        if (isset($json['vehicle_device_model']))
            $this->vehicle_device_model = $json['vehicle_device_model'];
        if (isset($json['vehicle_device_cost']))
            $this->vehicle_device_cost = $json['vehicle_device_cost'];
        if (isset($json['vehicle_status']))
            $this->vehicle_status = $json['vehicle_status'];
        if (isset($json['vehicle_remark']))
            $this->vehicle_remark = $json['vehicle_remark'];
        if (isset($json['vehicle_added_employee_id']))
            $this->vehicle_added_employee_id = $json['vehicle_added_employee_id'];
        
        $this->vehicle_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
    	if (isset($row['vehicle_id']))
            $this->vehicle_id = $row['vehicle_id'];
        if (isset($row['vehicle_year_production']))
            $this->vehicle_year_production = $row['vehicle_year_production'];
        if (isset($row['vehicle_make']))
            $this->vehicle_make = $row['vehicle_make'];
        if (isset($row['vehicle_model']))
            $this->vehicle_model = $row['vehicle_model'];
        if (isset($row['vehicle_color']))
            $this->vehicle_color = $row['vehicle_color'];
        if (isset($row['vehicle_plate_number']))
            $this->vehicle_plate_number = $row['vehicle_plate_number'];
        if (isset($row['vehicle_type']))
            $this->vehicle_type = $row['vehicle_type'];
        if (isset($row['vehicle_vin']))
            $this->vehicle_vin = $row['vehicle_vin'];
        if (isset($row['vehicle_insurance_kasko']))
            $this->vehicle_insurance_kasko = $row['vehicle_insurance_kasko'];
        if (isset($row['vehicle_insurance_kasko_series']))
            $this->vehicle_insurance_kasko_series = $row['vehicle_insurance_kasko_series'];
        if (isset($row['vehicle_insurance_kasko_num']))
            $this->vehicle_insurance_kasko_num = $row['vehicle_insurance_kasko_num'];
        if (isset($row['vehicle_insurance_kasko_date_begin']))
            $this->vehicle_insurance_kasko_date_begin = $row['vehicle_insurance_kasko_date_begin'];
        if (isset($row['vehicle_insurance_kasko_date_end']))
            $this->vehicle_insurance_kasko_date_end = $row['vehicle_insurance_kasko_date_end'];
        if (isset($row['vehicle_insurance_osago']))
            $this->vehicle_insurance_osago = $row['vehicle_insurance_osago'];
        if (isset($row['vehicle_insurance_osago_series']))
            $this->vehicle_insurance_osago_series = $row['vehicle_insurance_osago_series'];
        if (isset($row['vehicle_insurance_osago_num']))
            $this->vehicle_insurance_osago_num = $row['vehicle_insurance_osago_num'];
        if (isset($row['vehicle_insurance_osago_date_begin']))
            $this->vehicle_insurance_osago_date_begin = $row['vehicle_insurance_osago_date_begin'];
        if (isset($row['vehicle_insurance_osago_date_end']))
            $this->vehicle_insurance_osago_date_end = $row['vehicle_insurance_osago_date_end'];
        if (isset($row['vehicle_gas']))
            $this->vehicle_gas = $row['vehicle_gas'];
        if (isset($row['vehicle_l_per_100_km']))
            $this->vehicle_l_per_100_km = $row['vehicle_l_per_100_km'];
        if (isset($row['vehicle_maintenance_freq_km']))
            $this->vehicle_maintenance_freq_km = $row['vehicle_maintenance_freq_km'];
        if (isset($row['vehicle_device']))
            $this->vehicle_device = $row['vehicle_device'];
        if (isset($row['vehicle_device_model']))
            $this->vehicle_device_model = $row['vehicle_device_model'];
        if (isset($row['vehicle_device_cost']))
            $this->vehicle_device_cost = $row['vehicle_device_cost'];
        if (isset($row['vehicle_status']))
            $this->vehicle_status = $row['vehicle_status'];
        if (isset($row['vehicle_remark']))
            $this->vehicle_remark = $row['vehicle_remark'];
        if (isset($row['vehicle_added_employee_id']))
            $this->vehicle_added_employee_id = $row['vehicle_added_employee_id'];
        if (isset($row['vehicle_datetime_created']))
            $this->vehicle_datetime_created = $row['vehicle_datetime_created'];
    }
    
    public function selectVehicleId() {
        return $this->vehicle_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectVehicleFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
			FROM 
				postgres.public.vehicle
			WHERE
				oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_vehicle_oid_query";
        } else if (isset($this->vehicle_id)) {
            $query = "SELECT *
			FROM 
				postgres.public.vehicle
			WHERE
				vehicle_id = $1";
            
            $params = array(
                $this->vehicle_id
            );
            
            $query_name = "select_vehicle_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewVehicle($db, $object) {
        $result = pg_insert($db, 'public.vehicle', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectVehicleFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentVehicle($db, $object) {
        $object_key = array(
            'vehicle_id' => $object['vehicle_id']
        );
        
        unset($object['vehicle_id']);
        
        return pg_update($db, 'public.vehicle', $object, $object_key);
    }
    
    public function saveVehicle($db) {
	    $object = $this->selectParameters();
        if (!isset($object['vehicle_id']) || $object['vehicle_id'] === null)
            return $this->saveNewVehicle($db, $object);
        else
            return $this->saveCurrentVehicle($db, $object);
    }
}
?>