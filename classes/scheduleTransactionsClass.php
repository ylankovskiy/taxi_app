<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Schedule transactions class, follows the public.schedule_transactions table naming convention and structure
 *
 */
class scheduleTransactionsClass {
    private $schedule_transactions_id;
    private $schedule_transactions_remark;
    private $schedule_transactions_type;
    private $schedule_transactions_subtype;
    private $schedule_transactions_sum;
    private $schedule_transactions_datetime;
    private $schedule_transactions_schedule_id;
    private $schedule_transactions_datetime_created;
    private $schedule_transactions_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['schedule_transactions_type']) || !isset($json['schedule_transactions_subtype']) || !isset($json['schedule_transactions_sum']) || !isset($json['schedule_transactions_datetime']) || !isset($json['schedule_transactions_schedule_id']))
            return;
        
        if (isset($json['schedule_transactions_id']))
            $this->schedule_transactions_id = $json['schedule_transactions_id'];
        if (isset($json['schedule_transactions_remark']))
            $this->schedule_transactions_remark = $json['schedule_transactions_remark'];
        if (isset($json['schedule_transactions_type']))
            $this->schedule_transactions_type = $json['schedule_transactions_type'];
        if (isset($json['schedule_transactions_subtype']))
            $this->schedule_transactions_subtype = $json['schedule_transactions_subtype'];
        if (isset($json['schedule_transactions_sum']))
            $this->schedule_transactions_sum = $json['schedule_transactions_sum'];
        if (isset($json['schedule_transactions_schedule_id']))
            $this->schedule_transactions_schedule_id = $json['schedule_transactions_schedule_id'];
        if (isset($json['schedule_transactions_employee_added_id']))
            $this->schedule_transactions_employee_added_id = $json['schedule_transactions_employee_added_id'];
        
        $this->schedule_transactions_datetime_created = date('Y-m-d H:i:s');
        $this->schedule_transactions_datetime = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['schedule_transactions_id']))
            $this->schedule_transactions_id = $row['schedule_transactions_id'];
        if (isset($row['schedule_transactions_remark']))
            $this->schedule_transactions_remark = $row['schedule_transactions_remark'];
        if (isset($row['schedule_transactions_type']))
            $this->schedule_transactions_type = $row['schedule_transactions_type'];
        if (isset($row['schedule_transactions_subtype']))
            $this->schedule_transactions_subtype = $row['schedule_transactions_subtype'];
        if (isset($row['schedule_transactions_sum']))
            $this->schedule_transactions_sum = $row['schedule_transactions_sum'];
        if (isset($row['schedule_transactions_datetime']))
            $this->schedule_transactions_datetime = $row['schedule_transactions_datetime'];
        if (isset($row['schedule_transactions_schedule_id']))
            $this->schedule_transactions_schedule_id = $row['schedule_transactions_schedule_id'];
        if (isset($row['schedule_transactions_employee_added_id']))
            $this->schedule_transactions_employee_added_id = $row['schedule_transactions_employee_added_id'];
        if (isset($row['schedule_transactions_datetime_created']))
            $this->schedule_transactions_datetime_created = $row['schedule_transactions_datetime_created'];
    }
    
    public function selectScheduleTransactionsId() {
        return $this->schedule_transactions_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectScheduleTransactionsFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
			FROM 
				postgres.public.schedule_transactions
			WHERE
				oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_schedule_transactions_oid_query";
        } else if (isset($this->schedule_transactions_schedule_id)) {
            $query = "SELECT *
			FROM 
				postgres.public.schedule_transactions
			WHERE
				schedule_transactions_schedule_id = $1";
            
            $params = array(
                $this->schedule_transactions_schedule_id
            );
            
            $query_name = "select_schedule_transactions_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewScheduleTransactions($db, $object) {
        $result = pg_insert($db, 'public.schedule_transactions', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectScheduleTransactionsFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentScheduleTransactions($db, $object) {
        $object_key = array(
            'schedule_transactions_id' => $object['schedule_transactions_id']
        );
        
        unset($object['schedule_transactions_id']);
        
        return pg_update($db, 'public.schedule_transactions', $object, $object_key);
    }
    
    public function saveScheduleTransactions($db) {
	    $object = $this->selectParameters();
        if (!isset($object['schedule_transactions_id']) || $object['schedule_transactions_id'] === null)
            return $this->saveNewScheduleTransactions($db, $object);
        else
            return $this->saveCurrentScheduleTransactions($db, $object);
    }
}
?>