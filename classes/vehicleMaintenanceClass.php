<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Vehicle maintenance class, follows the public.vehicle_maintenance table naming convention and structure
 *
 */
class vehicleMaintenanceClass {
    private $vehicle_maintenance_id;
    private $vehicle_maintenance_tab_index;
    private $vehicle_maintenance_km;
    private $vehicle_maintenance_date;
    private $vehicle_maintenance_vehicle_id;
    private $vehicle_maintenance_schedule_id;
    private $vehicle_maintenance_datetime_created;
    private $vehicle_maintenance_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['vehicle_maintenance_tab_index']) || !isset($json['vehicle_maintenance_km']) || !isset($json['vehicle_maintenance_date']) || !isset($json['vehicle_maintenance_vehicle_id']))
            return;
        
        if (isset($json['vehicle_maintenance_id']))
            $this->vehicle_maintenance_id = $json['vehicle_maintenance_id'];
        if (isset($json['vehicle_maintenance_tab_index']))
            $this->vehicle_maintenance_tab_index = $json['vehicle_maintenance_tab_index'];
        if (isset($json['vehicle_maintenance_km']))
            $this->vehicle_maintenance_km = $json['vehicle_maintenance_km'];
        if (isset($json['vehicle_maintenance_date']))
            $this->vehicle_maintenance_date = $json['vehicle_maintenance_date'];
        if (isset($json['vehicle_maintenance_vehicle_id']))
            $this->vehicle_maintenance_vehicle_id = $json['vehicle_maintenance_vehicle_id'];
        if (isset($json['vehicle_maintenance_schedule_id']))
            $this->vehicle_maintenance_schedule_id = $json['vehicle_maintenance_schedule_id'];
        if (isset($json['vehicle_maintenance_employee_added_id']))
            $this->vehicle_maintenance_employee_added_id = $json['vehicle_maintenance_employee_added_id'];
        
        $this->vehicle_maintenance_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['vehicle_maintenance_id']))
            $this->vehicle_maintenance_id = $row['vehicle_maintenance_id'];
        if (isset($row['vehicle_maintenance_tab_index']))
            $this->vehicle_maintenance_tab_index = $row['vehicle_maintenance_tab_index'];
        if (isset($row['vehicle_maintenance_km']))
            $this->vehicle_maintenance_km = $row['vehicle_maintenance_km'];
        if (isset($row['vehicle_maintenance_date']))
            $this->vehicle_maintenance_date = $row['vehicle_maintenance_date'];
        if (isset($row['vehicle_maintenance_vehicle_id']))
            $this->vehicle_maintenance_vehicle_id = $row['vehicle_maintenance_vehicle_id'];
        if (isset($row['vehicle_maintenance_schedule_id']))
            $this->vehicle_maintenance_schedule_id = $row['vehicle_maintenance_schedule_id'];
        if (isset($row['vehicle_maintenance_employee_added_id']))
            $this->vehicle_maintenance_employee_added_id = $row['vehicle_maintenance_employee_added_id'];
        if (isset($row['vehicle_maintenance_datetime_created']))
            $this->vehicle_maintenance_datetime_created = $row['vehicle_maintenance_datetime_created'];
    }
    
    public function selectVehicleMaintenanceId() {
        return $this->vehicle_maintenance_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectVehicleMaintenanceFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
			FROM 
				postgres.public.vehicle_maintenance
			WHERE
				oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_vehicle_maintenance_oid_query";
        } else if (isset($this->vehicle_maintenance_schedule_id)) {
            $query = "SELECT *
			FROM 
				postgres.public.vehicle_maintenance
			WHERE
				vehicle_maintenance_schedule_id = $1";
            
            $params = array(
                $this->vehicle_maintenance_schedule_id
            );
            
            $query_name = "select_vehicle_maintenance_schedule_query";
        } else if (isset($this->vehicle_maintenance_vehicle_id)) {
            $query = "SELECT *
			FROM 
				postgres.public.vehicle_maintenance
			WHERE
				vehicle_maintenance_vehicle_id = $1";
            
            $params = array(
                $this->vehicle_maintenance_vehicle_id
            );
            
            $query_name = "select_vehicle_maintenance_vehicle_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewVehicleMaintenance($db, $object) {
        $result = pg_insert($db, 'public.vehicle_maintenance', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectVehicleMaintenanceFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentVehicleMaintenance($db, $object) {
        $object_key = array(
            'vehicle_maintenance_id' => $object['vehicle_maintenance_id']
        );
        
        unset($object['vehicle_maintenance_id']);
        
        return pg_update($db, 'public.vehicle_maintenance', $object, $object_key);
    }
    
    public function saveVehicleMaintenance($db) {
	    $object = $this->selectParameters();
        if (!isset($object['vehicle_maintenance_id']) || $object['vehicle_maintenance_id'] === null)
            return $this->saveNewVehicleMaintenance($db, $object);
        else
            return $this->saveCurrentVehicleMaintenance($db, $object);
    }
}
?>