<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Config driver pay motivation class, follows the public.config_driver_pay_motivation table naming convention and structure
 *
 */
class configDriverPayMotivationClass {
    private $config_driver_pay_motivation_id;
    private $config_driver_pay_motivation_error;
    private $config_driver_pay_motivation_date_begin;
    private $config_driver_pay_motivation_date_end;
    private $config_driver_pay_motivation_datetime_created;
    private $config_driver_pay_motivation_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['config_driver_pay_motivation_error']) || !isset($json['config_driver_pay_motivation_date_begin']))
            return;
        
        if (isset($json['config_driver_pay_motivation_id']))
            $this->config_driver_pay_motivation_id = $json['config_driver_pay_motivation_id'];
        if (isset($json['config_driver_pay_motivation_error']))
            $this->config_driver_pay_motivation_error = $json['config_driver_pay_motivation_error'];
        if (isset($json['config_driver_pay_motivation_date_begin']))
            $this->config_driver_pay_motivation_date_begin = $json['config_driver_pay_motivation_date_begin'];
        if (isset($json['config_driver_pay_motivation_date_end']))
            $this->config_driver_pay_motivation_date_end = $json['config_driver_pay_motivation_date_end'];
        if (isset($json['config_driver_pay_motivation_employee_added_id']))
            $this->config_driver_pay_motivation_employee_added_id = $json['config_driver_pay_motivation_employee_added_id'];
        
        $this->config_driver_pay_motivation_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['config_driver_pay_motivation_id']))
            $this->config_driver_pay_motivation_id = $row['config_driver_pay_motivation_id'];
        if (isset($row['config_driver_pay_motivation_error']))
            $this->config_driver_pay_motivation_error = $row['config_driver_pay_motivation_error'];
        if (isset($row['config_driver_pay_motivation_date_begin']))
            $this->config_driver_pay_motivation_date_begin = $row['config_driver_pay_motivation_date_begin'];
        if (isset($row['config_driver_pay_motivation_date_end']))
            $this->config_driver_pay_motivation_date_end = $row['config_driver_pay_motivation_date_end'];
        if (isset($row['config_driver_pay_motivation_employee_added_id']))
            $this->config_driver_pay_motivation_employee_added_id = $row['config_driver_pay_motivation_employee_added_id'];
        if (isset($row['config_driver_pay_motivation_datetime_created']))
            $this->config_driver_pay_motivation_datetime_created = $row['config_driver_pay_motivation_datetime_created'];
    }
    
    public function selectConfigDriverPayMotivationId() {
        return $this->config_driver_pay_motivation_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectConfigDriverPayMotivationFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
				FROM 
				  postgres.public.config_driver_pay_motivation
				WHERE
					oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_config_driver_pay_motivation_oid_query";
        } else if (isset($this->config_driver_pay_motivation_id)) {
            $query = "SELECT *
			FROM 
			  postgres.public.config_driver_pay_motivation
			WHERE
				config_driver_pay_motivation_id = $1";
				
            $params = array(
                $this->config_driver_pay_motivation_id
            );
            
            $query_name = "select_config_driver_pay_motivation_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewConfigDriverPayMotivation($db, $object) {        
        $result = pg_insert($db, 'public.config_driver_pay_motivation', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectConfigDriverPayMotivationFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentConfigDriverPayMotivation($db, $object) {
        $object_key = array(
            'config_driver_pay_motivation_id' => $object['config_driver_pay_motivation_id']
        );
        
        unset($object['config_driver_pay_motivation_id']);
        
        return pg_update($db, 'public.config_driver_pay_motivation', $object, $object_key);
    }
    
    public function saveConfigDriverPayMotivation($db) {
	    $object = $this->selectParameters();
        if (!isset($object['config_driver_pay_motivation_id']) || $object['config_driver_pay_motivation_id'] === null)
            return $this->saveNewConfigDriverPayMotivation($db, $object);
        else
            return $this->saveCurrentConfigDriverPayMotivation($db, $object);
    }
}
?>