<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Gett class, follows the public.gett table naming convention and structure
 *
 */
class gettClass {
    private $gett_id;
    private $gett_system_id;
    private $gett_date_registration;
    private $gett_status;
    private $gett_driver_id;
    private $gett_datetime_created;
    
    public function __construct($json) {
        if (!isset($json['gett_system_id']) || !isset($json['gett_status']) || !isset($json['gett_driver_id']))
            return;
        
        if (isset($json['gett_id']))
            $this->gett_id = $json['gett_id'];
        if (isset($json['gett_system_id']))
            $this->gett_system_id = $json['gett_system_id'];
        if (isset($json['gett_date_registration']))
            $this->gett_date_registration = $json['gett_date_registration'];
        if (isset($json['gett_system_id']))
            $this->gett_system_id = $json['gett_system_id'];
        if (isset($json['gett_driver_id']))
            $this->gett_driver_id = $json['gett_driver_id'];
        
        $this->gett_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['gett_id']))
            $this->gett_id = $row['gett_id'];
        if (isset($row['gett_system_id']))
            $this->gett_system_id = $row['gett_system_id'];
        if (isset($row['gett_date_registration']))
            $this->gett_date_registration = $row['gett_date_registration'];
        if (isset($row['gett_system_id']))
            $this->gett_system_id = $row['gett_system_id'];
        if (isset($row['gett_driver_id']))
            $this->gett_driver_id = $row['gett_driver_id'];
        if (isset($row['gett_datetime_created']))
            $this->gett_datetime_created = $row['gett_datetime_created'];
    }
    
    public function selectGettId() {
        return $this->gett_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectGettFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
				FROM 
				  postgres.public.gett
				WHERE
					oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_gett_oid_query";
        } else if (isset($this->gett_system_id)) {
            $query = "SELECT *
			FROM 
			  postgres.public.gett
			WHERE
				gett_system_id = $1";
				
            $params = array(
                $this->gett_system_id
            );
            
            $query_name = "select_gett_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewGett($db, $object) {        
        $result = pg_insert($db, 'public.gett', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectGettFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentGett($db, $object) {
        $object_key = array(
            'gett_id' => $object['gett_id']
        );
        
        unset($object['gett_id']);
        
        return pg_update($db, 'public.gett', $object, $object_key);
    }
    
    public function saveGett($db) {
	    $object = $this->selectParameters();
        if (!isset($object['gett_id']) || $object['gett_id'] === null)
            return $this->saveNewGett($db, $object);
        else
            return $this->saveCurrentGett($db, $object);
    }
}
?>