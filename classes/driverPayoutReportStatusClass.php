<?php
error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver_payout_report_status',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Driver payout report status class, follows the public.driver_payout_report_status table naming convention and structure
 *
 */
class driverPayoutReportStatusClass {
    private $driver_payout_report_status_id;
    private $driver_payout_report_status_status;
    private $driver_payout_report_status_remark;
    private $driver_payout_report_status_report_id;
    private $driver_payout_report_status_datetime_created;
    private $driver_payout_report_status_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['driver_payout_report_status_status']) || !isset($json['driver_payout_report_status_remark']) || !isset($json['driver_payout_report_status_report_id']))
            return;
        
        if (isset($json['driver_payout_report_status_id']))
            $this->driver_payout_report_status_id = $json['driver_payout_report_status_id'];
        if (isset($json['driver_payout_report_status_status']))
            $this->driver_payout_report_status_status = $json['driver_payout_report_status_status'];
        if (isset($json['driver_payout_report_status_remark']))
            $this->driver_payout_report_status_remark = $json['driver_payout_report_status_remark'];
        if (isset($json['driver_payout_report_status_report_id']))
            $this->driver_payout_report_status_report_id = $json['driver_payout_report_status_report_id'];
        if (isset($json['driver_payout_report_status_employee_added_id']))
            $this->driver_payout_report_status_employee_added_id = $json['driver_payout_report_status_employee_added_id'];

        $this->driver_payout_report_status_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}

    private function fillParametersFromRow($row) {
        if (isset($row['driver_payout_report_status_id']))
            $this->driver_payout_report_status_id = $row['driver_payout_report_status_id'];
        if (isset($row['driver_payout_report_status_status']))
            $this->driver_payout_report_status_status = $row['driver_payout_report_status_status'];
        if (isset($row['driver_payout_report_status_remark']))
            $this->driver_payout_report_status_remark = $row['driver_payout_report_status_remark'];
        if (isset($row['driver_payout_report_status_report_id']))
            $this->driver_payout_report_status_report_id = $row['driver_payout_report_status_report_id'];
        if (isset($row['driver_payout_report_status_datetime_created']))
            $this->driver_payout_report_status_datetime_created = $row['driver_payout_report_status_datetime_created'];
        if (isset($row['driver_payout_report_status_employee_added_id']))
            $this->driver_payout_report_status_employee_added_id = $row['driver_payout_report_status_employee_added_id'];
    }
    
    public function selectDriverPayoutReportStatusId() {
        return $this->driver_payout_report_status_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    public function selectDriverPayoutReportStatusFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        if (isset($this->driver_payout_report_status_report_id)) {
            $query = 'SELECT *
				FROM 
			  	postgres.public.driver_payout_report_status
				WHERE
					driver_payout_report_status_report_id = $1';
            
            $params = array(
                $this->driver_payout_report_status_report_id
            );
            
            $query_name = "select_driver_payout_report_status_query";
        } else if (isset($oid)) {
            $query = 'SELECT *
				FROM 
			  	postgres.public.driver_payout_report_status
				WHERE
					oid = $1';
            
            $params = array(
                $oid
            );
            
            $query_name = "select_driver_payout_report_status_oid_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
            }
        }
    }
    
    private function saveNewDriverPayoutReportStatus($db, $object) {
        $result = pg_insert($db, 'public.driver_payout_report_status', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectDriverPayoutReportStatusFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentDriverPayoutReportStatus($db, $object) {
        $object_key = array(
            'driver_payout_report_status_id' => $object['driver_payout_report_status_id']
        );
        
        unset($object['driver_payout_report_status_id']);
        
        return pg_update($db, 'public.driver_payout_report_status', $object, $object_key);
    }
    
    public function saveDriverPayoutReportStatus($db) {
	    $object = $this->selectParameters();
        if (!isset($object['driver_payout_report_status_id']) || $object['driver_payout_report_status_id'] === null)
            return $this->saveNewDriverPayoutReportStatus($db, $object);
        else
            return $this->saveCurrentDriverPayoutReportStatus($db, $object);
    }
}
?>