<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Vehicle gasoline class, follows the public.vehicle_gasoline table naming convention and structure
 *
 */
class vehicleGasolineClass {
    private $vehicle_gasoline_id;
    private $vehicle_gasoline_tab_index;
    private $vehicle_gasoline_littres;
    private $vehicle_gasoline_date;
    private $vehicle_gasoline_vehicle_id;
    private $vehicle_gasoline_schedule_id;
    private $vehicle_gasoline_datetime_created;
    private $vehicle_gasoline_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['vehicle_gasoline_tab_index']) || !isset($json['vehicle_gasoline_littres']) || !isset($json['vehicle_gasoline_date']) || !isset($json['vehicle_gasoline_vehicle_id']))
            return;
        
        if (isset($json['vehicle_gasoline_id']))
            $this->vehicle_gasoline_id = $json['vehicle_gasoline_id'];
        if (isset($json['vehicle_gasoline_tab_index']))
            $this->vehicle_gasoline_tab_index = $json['vehicle_gasoline_tab_index'];
        if (isset($json['vehicle_gasoline_littres']))
            $this->vehicle_gasoline_littres = $json['vehicle_gasoline_littres'];
        if (isset($json['vehicle_gasoline_date']))
            $this->vehicle_gasoline_date = $json['vehicle_gasoline_date'];
        if (isset($json['vehicle_gasoline_vehicle_id']))
            $this->vehicle_gasoline_vehicle_id = $json['vehicle_gasoline_vehicle_id'];
        if (isset($json['vehicle_gasoline_schedule_id']))
            $this->vehicle_gasoline_schedule_id = $json['vehicle_gasoline_schedule_id'];
        if (isset($json['vehicle_gasoline_employee_added_id']))
            $this->vehicle_gasoline_employee_added_id = $json['vehicle_gasoline_employee_added_id'];
        
        $this->vehicle_gasoline_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['vehicle_gasoline_id']))
            $this->vehicle_gasoline_id = $row['vehicle_gasoline_id'];
        if (isset($row['vehicle_gasoline_tab_index']))
            $this->vehicle_gasoline_tab_index = $row['vehicle_gasoline_tab_index'];
        if (isset($row['vehicle_gasoline_littres']))
            $this->vehicle_gasoline_littres = $row['vehicle_gasoline_littres'];
        if (isset($row['vehicle_gasoline_date']))
            $this->vehicle_gasoline_date = $row['vehicle_gasoline_date'];
        if (isset($row['vehicle_gasoline_vehicle_id']))
            $this->vehicle_gasoline_vehicle_id = $row['vehicle_gasoline_vehicle_id'];
        if (isset($row['vehicle_gasoline_schedule_id']))
            $this->vehicle_gasoline_schedule_id = $row['vehicle_gasoline_schedule_id'];
        if (isset($row['vehicle_gasoline_employee_added_id']))
            $this->vehicle_gasoline_employee_added_id = $row['vehicle_gasoline_employee_added_id'];
        if (isset($row['vehicle_gasoline_datetime_created']))
            $this->vehicle_gasoline_datetime_created = $row['vehicle_gasoline_datetime_created'];
    }
    
    public function selectVehicleGasolineId() {
        return $this->vehicle_gasoline_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectVehicleGasolineFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
			FROM 
				postgres.public.vehicle_gasoline
			WHERE
				oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_vehicle_gasoline_oid_query";
        } else if (isset($this->vehicle_gasoline_schedule_id)) {
            $query = "SELECT *
			FROM 
				postgres.public.vehicle_gasoline
			WHERE
				vehicle_gasoline_schedule_id = $1";
            
            $params = array(
                $this->vehicle_gasoline_schedule_id
            );
            
            $query_name = "select_vehicle_gasoline_schedule_query";
        } else if (isset($this->vehicle_gasoline_vehicle_id)) {
            $query = "SELECT *
			FROM 
				postgres.public.vehicle_gasoline
			WHERE
				vehicle_gasoline_vehicle_id = $1";
            
            $params = array(
                $this->vehicle_gasoline_vehicle_id
            );
            
            $query_name = "select_vehicle_gasoline_vehicle_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewVehicleGasoline($db, $object) {
        $result = pg_insert($db, 'public.vehicle_gasoline', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectVehicleGasolineFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentVehicleGasoline($db, $object) {
        $object_key = array(
            'vehicle_gasoline_id' => $object['vehicle_gasoline_id']
        );
        
        unset($object['vehicle_gasoline_id']);
        
        return pg_update($db, 'public.vehicle_gasoline', $object, $object_key);
    }
    
    public function saveVehicleGasoline($db) {
	    $object = $this->selectParameters();
        if (!isset($object['vehicle_gasoline_id']) || $object['vehicle_gasoline_id'] === null)
            return $this->saveNewVehicleGasoline($db, $object);
        else
            return $this->saveCurrentVehicleGasoline($db, $object);
    }
}
?>