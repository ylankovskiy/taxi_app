<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Bank class, follows the public.bank table naming convention and structure
 *
 */
class bankClass {
    private $bank_id;
    private $account_holder_name_name;
    private $account_holder_name_first;
    private $account_holder_name_last;
    private $account_type;
    private $account_number;
    private $account_remark;
    private $bank_datetime_created;
    private $account_status;
    private $bank_employee_id_added;
    private $bank_driver_id;
    private $bank_pay_project_id;
    
    public function __construct($json) {
        if (!isset($json['account_number']) || !isset($json['account_type']) || !isset($json['account_status']) || !isset($json['account_holder_name_name']) || !isset($json['account_holder_name_first']) || !isset($json['account_holder_name_last']) || !isset($json['bank_driver_id']) || !isset($json['bank_pay_project_id']))
            return;
        
        if (isset($json['bank_id']))
            $this->bank_id = $json['bank_id'];
        if (isset($json['account_holder_name_name']))
            $this->account_holder_name_name = $json['account_holder_name_name'];
        if (isset($json['account_holder_name_first']))
            $this->account_holder_name_first = $json['account_holder_name_first'];
        if (isset($json['account_holder_name_last']))
            $this->account_holder_name_last = $json['account_holder_name_last'];
        if (isset($json['account_type']))
            $this->account_type = $json['account_type'];
        if (isset($json['account_number']))
            $this->account_number = $json['account_number'];
        if (isset($json['account_remark']))
            $this->account_remark = $json['account_remark'];
        if (isset($json['account_status']))
            $this->account_status = $json['account_status'];
        if (isset($json['bank_employee_id_added']))
            $this->bank_employee_id_added = $json['bank_employee_id_added'];
        if (isset($json['bank_driver_id']))
            $this->bank_driver_id = $json['bank_driver_id'];
        if (isset($json['bank_pay_project_id']))
            $this->bank_pay_project_id = $json['bank_pay_project_id'];
        
        $this->bank_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['bank_id']))
            $this->bank_id = $row['bank_id'];
        if (isset($row['account_holder_name_name']))
            $this->account_holder_name_name = $row['account_holder_name_name'];
        if (isset($row['account_holder_name_first']))
            $this->account_holder_name_first = $row['account_holder_name_first'];
        if (isset($row['account_holder_name_last']))
            $this->account_holder_name_last = $row['account_holder_name_last'];
        if (isset($row['account_type']))
            $this->account_type = $row['account_type'];
        if (isset($row['account_number']))
            $this->account_number = $row['account_number'];
        if (isset($row['account_remark']))
            $this->account_remark = $row['account_remark'];
        if (isset($row['account_status']))
            $this->account_status = $row['account_status'];
        if (isset($row['bank_employee_id_added']))
            $this->bank_employee_id_added = $row['bank_employee_id_added'];
        if (isset($row['bank_driver_id']))
            $this->bank_driver_id = $row['bank_driver_id'];
        if (isset($row['bank_pay_project_id']))
            $this->bank_pay_project_id = $row['bank_pay_project_id'];
        if (isset($row['bank_datetime_created']))
            $this->bank_datetime_created = $row['bank_datetime_created'];
    }
    
    public function selectBankId() {
        return $this->bank_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectBankFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
				FROM 
				  postgres.public.bank
				WHERE
					oid = $1";
            
            $params = array(
                $oid
            );
            
	    	$query_name = "select_bank_oid_query";
        } else if (isset($this->account_number)) {
            $query = 'SELECT *
				FROM 
			  	postgres.public.bank
				WHERE
					account_number = $1';
            
            $params = array(
                $this->account_number
            );
            
	    	$query_name = "select_bank_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
            }
        }
    }
    
    private function saveNewBank($db, $object) {   
        $result = pg_insert($db, 'public.bank', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectBankFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentBank($db, $object) {
        $object_key = array(
            'bank_id' => $object['bank_id']
        );
        
        unset($object['bank_id']);
        
        return pg_update($db, 'public.bank', $object, $object_key);
    }
    
    public function saveBank($db) {
	    $object = $this->selectParameters();
        if (!isset($object['bank_id']) || $object['bank_id'] === null)
            return $this->saveNewBank($db, $object);
        else
            return $this->saveCurrentBank($db, $object);
    }
}
?>