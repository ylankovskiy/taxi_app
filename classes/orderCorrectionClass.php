<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Order correction class, follows the public.order_correction table naming convention and structure
 *
 */
class orderCorrectionClass {
    private $order_correction_id;
    private $order_correction_order_id;
    private $order_correction_difference;
    private $order_correction_remark;
    
    public function __construct($json) {
        if (!isset($json['order_correction_order_id']) || !isset($json['order_correction_difference']) || !isset($json['order_correction_remark']))
            return;
        
        if (isset($json['order_correction_id']))
            $this->order_correction_id = $json['order_correction_id'];
        if (isset($json['order_correction_order_id']))
            $this->order_correction_order_id = $json['order_correction_order_id'];
        if (isset($json['order_correction_difference']))
            $this->order_correction_difference = $json['order_correction_difference'];
        if (isset($json['order_correction_remark']))
            $this->order_correction_remark = $json['order_correction_remark'];
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['order_correction_id']))
            $this->order_correction_id = $row['order_correction_id'];
        if (isset($row['order_correction_order_id']))
            $this->order_correction_order_id = $row['order_correction_order_id'];
        if (isset($row['order_correction_difference']))
            $this->order_correction_difference = $row['order_correction_difference'];
        if (isset($row['order_correction_remark']))
            $this->order_correction_remark = $row['order_correction_remark'];
    }
    
    public function selectOrderCorrectionId() {
        return $this->order_correction_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectOrderCorrectionFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
			FROM 
				postgres.public.order_correction
			WHERE
				oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_order_correction_oid_query";
        } else if (isset($this->order_correction_order_id)) {            
            if (isset($this->order_correction_order_id)) {
                $query = "SELECT *
				FROM 
					postgres.public.order_correction
				WHERE
					order_correction_order_id = $1";
                
                $params = array(
                    $this->order_correction_order_id
                );
            }
            
            $query_name = "select_order_correction_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewOrderCorrection($db, $object) {
        $result = pg_insert($db, 'public.order_correction', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectOrderCorrectionFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentOrderCorrection($db, $object) {
        $object_key = array(
            'order_correction_id' => $object['order_correction_id']
        );
        
        unset($object['order_correction_id']);
        
        return pg_update($db, 'public.order_correction', $object, $object_key);
    }
    
    public function saveOrderCorrection($db) {
	    $object = $this->selectParameters();
	    if ($object != null) {
	        if (!isset($object['order_correction_id']) || $object['order_correction_id'] === null)
        	    return $this->saveNewOrderCorrection($db, $object);
    	    else
	            return $this->saveCurrentOrderCorrection($db, $object);
        }
    }
}
?>