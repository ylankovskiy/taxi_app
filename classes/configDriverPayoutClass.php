<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Config driver payout class, follows the public.config_driver_payout table naming convention and structure
 *
 */
class configDriverPayoutClass {
    private $config_driver_payout_id;
    private $config_driver_payout_day_of_week;
    private $config_driver_payout_time;
    private $config_driver_payout_driver_type;
    private $config_driver_payout_date_begin;
    private $config_driver_payout_date_end;
    private $config_driver_payout_datetime_created;
    private $config_driver_payout_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['config_driver_payout_day_of_week']) || !isset($json['config_driver_payout_date_begin']) || !isset($json['config_driver_payout_driver_type']) || !isset($json['config_driver_payout_time']))
            return;
        
        if (isset($json['config_driver_payout_id']))
            $this->config_driver_payout_id = $json['config_driver_payout_id'];
        if (isset($json['config_driver_payout_day_of_week']))
            $this->config_driver_payout_day_of_week = $json['config_driver_payout_day_of_week'];
        if (isset($json['config_driver_payout_time']))
            $this->config_driver_payout_time = $json['config_driver_payout_time'];
        if (isset($json['config_driver_payout_driver_type']))
            $this->config_driver_payout_driver_type = $json['config_driver_payout_driver_type'];
        if (isset($json['config_driver_payout_date_begin']))
            $this->config_driver_payout_date_begin = $json['config_driver_payout_date_begin'];
        if (isset($json['config_driver_payout_date_end']))
            $this->config_driver_payout_date_end = $json['config_driver_payout_date_end'];
        if (isset($json['config_driver_payout_employee_added_id']))
            $this->config_driver_payout_employee_added_id = $json['config_driver_payout_employee_added_id'];
        
        $this->config_driver_payout_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['config_driver_payout_id']))
            $this->config_driver_payout_id = $row['config_driver_payout_id'];
        if (isset($row['config_driver_payout_day_of_week']))
            $this->config_driver_payout_day_of_week = $row['config_driver_payout_day_of_week'];
        if (isset($row['config_driver_payout_time']))
            $this->config_driver_payout_time = $row['config_driver_payout_time'];
        if (isset($row['config_driver_payout_driver_type']))
            $this->config_driver_payout_driver_type = $row['config_driver_payout_driver_type'];
        if (isset($row['config_driver_payout_date_begin']))
            $this->config_driver_payout_date_begin = $row['config_driver_payout_date_begin'];
        if (isset($row['config_driver_payout_date_end']))
            $this->config_driver_payout_date_end = $row['config_driver_payout_date_end'];
        if (isset($row['config_driver_payout_employee_added_id']))
            $this->config_driver_payout_employee_added_id = $row['config_driver_payout_employee_added_id'];
        if (isset($row['config_driver_payout_datetime_created']))
            $this->config_driver_payout_datetime_created = $row['config_driver_payout_datetime_created'];
    }
    
    public function selectConfigDriverPayoutId() {
        return $this->config_driver_payout_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectConfigDriverPayoutFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
				FROM 
				  postgres.public.config_driver_payout
				WHERE
					oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_config_driver_payout_oid_query";
        } else if (isset($this->config_driver_payout_id)) {
            $query = "SELECT *
			FROM 
			  postgres.public.config_driver_payout
			WHERE
				config_driver_payout_id = $1";
				
            $params = array(
                $this->config_driver_payout_id
            );
            
            $query_name = "select_config_driver_payout_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewConfigDriverPayout($db, $object) {        
        $result = pg_insert($db, 'public.config_driver_payout', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectConfigDriverPayoutFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentConfigDriverPayout($db, $object) {
        $object_key = array(
            'config_driver_payout_id' => $object['config_driver_payout_id']
        );
        
        unset($object['config_driver_payout_id']);
        
        return pg_update($db, 'public.config_driver_payout', $object, $object_key);
    }
    
    public function saveConfigDriverPayout($db) {
	    $object = $this->selectParameters();
        if (!isset($object['config_driver_payout_id']) || $object['config_driver_payout_id'] === null)
            return $this->saveNewConfigDriverPayout($db, $object);
        else
            return $this->saveCurrentConfigDriverPayout($db, $object);
    }
}
?>