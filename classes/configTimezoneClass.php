<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Config timezone class, follows the public.config_timezone table naming convention and structure
 *
 */
class configTimezoneClass {	
    private $config_timezone_id;
    private $config_timezone_from_moscow;
    private $config_timezone_date_begin;
    private $config_timezone_date_end;
    private $config_timezone_datetime_created;
    private $config_timezone_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['config_timezone_from_moscow']) || !isset($json['config_timezone_date_begin']))
            return;
        
        if (isset($json['config_timezone_id']))
            $this->config_timezone_id = $json['config_timezone_id'];
        if (isset($json['config_timezone_from_moscow']))
            $this->config_timezone_from_moscow = $json['config_timezone_from_moscow'];
        if (isset($json['config_timezone_date_begin']))
            $this->config_timezone_date_begin = $json['config_timezone_date_begin'];
        if (isset($json['config_timezone_date_end']))
            $this->config_timezone_date_end = $json['config_timezone_date_end'];
        if (isset($json['config_timezone_employee_added_id']))
            $this->config_timezone_employee_added_id = $json['config_timezone_employee_added_id'];
        
        $this->config_timezone_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['config_timezone_id']))
            $this->config_timezone_id = $row['config_timezone_id'];
        if (isset($row['config_timezone_from_moscow']))
            $this->config_timezone_from_moscow = $row['config_timezone_from_moscow'];
        if (isset($row['config_timezone_date_begin']))
            $this->config_timezone_date_begin = $row['config_timezone_date_begin'];
        if (isset($row['config_timezone_date_end']))
            $this->config_timezone_date_end = $row['config_timezone_date_end'];
        if (isset($row['config_timezone_employee_added_id']))
            $this->config_timezone_employee_added_id = $row['config_timezone_employee_added_id'];
        if (isset($row['config_timezone_datetime_created']))
            $this->config_timezone_datetime_created = $row['config_timezone_datetime_created'];
    }
    
    public function selectConfigTimezoneId() {
        return $this->config_timezone_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectConfigTimezoneFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
				FROM 
				  postgres.public.config_timezone
				WHERE
					oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_config_timezone_oid_query";
        } else if (isset($this->config_timezone_id)) {
            $query = "SELECT *
			FROM 
			  postgres.public.config_timezone
			WHERE
				config_timezone_id = $1";
				
            $params = array(
                $this->config_timezone_id
            );
            
            $query_name = "select_config_timezone_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewConfigTimezone($db, $object) {        
        $result = pg_insert($db, 'public.config_timezone', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectConfigTimezoneFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentConfigTimezone($db, $object) {
        $object_key = array(
            'config_timezone_id' => $object['config_timezone_id']
        );
        
        unset($object['config_timezone_id']);
        
        return pg_update($db, 'public.config_timezone', $object, $object_key);
    }
    
    public function saveConfigTimezone($db) {
	    $object = $this->selectParameters();
        if (!isset($object['config_timezone_id']) || $object['config_timezone_id'] === null)
            return $this->saveNewConfigTimezone($db, $object);
        else
            return $this->saveCurrentConfigTimezone($db, $object);
    }
}
?>