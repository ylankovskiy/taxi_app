<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Pay project class, follows the public.pay_project table naming convention and structure
 *
 */
class payProjectClass {
    private $pay_project_id;
    private $pay_project_bank_name;
    private $pay_project_bank_bik;
    private $pay_project_datetime_created;
    
    public function __construct($json) {
        if (!isset($json['pay_project_bank_name']) || !isset($json['pay_project_bank_bik']))
            return;
        
        if (isset($json['pay_project_id']))
            $this->pay_project_id = $json['pay_project_id'];
        if (isset($json['pay_project_bank_name']))
            $this->pay_project_bank_name = $json['pay_project_bank_name'];
        if (isset($json['pay_project_bank_bik']))
            $this->pay_project_bank_bik = $json['pay_project_bank_bik'];
        
        $this->pay_project_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['pay_project_id']))
            $this->pay_project_id = $row['pay_project_id'];
        if (isset($row['pay_project_bank_name']))
            $this->pay_project_bank_name = $row['pay_project_bank_name'];
        if (isset($row['pay_project_bank_bik']))
            $this->pay_project_bank_bik = $row['pay_project_bank_bik'];
        if (isset($row['pay_project_datetime_created']))
            $this->pay_project_datetime_created = $row['pay_project_datetime_created'];
    }
    
    public function selectPayProjectId() {
        return $this->pay_project_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectPayProjectFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
				FROM 
				  postgres.public.pay_project
				WHERE
					oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_pay_project_query";
        } else if (isset($this->pay_project_bank_bik)) {
            $query = 'SELECT *
				FROM 
			  		postgres.public.pay_project
				WHERE
					pay_project_bank_bik = $1';
            
            $params = array(
                $this->pay_project_bank_bik
            );
            
            $query_name = "select_pay_project_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
            }
        }
    }
    
    private function saveNewPayProject($db, $object) {
        $result = pg_insert($db, 'public.pay_project', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectPayProjectFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentPayProject($db, $object) {
        $object_key = array(
            'pay_project_id' => $object['pay_project_id']
        );
        
        unset($object['pay_project_id']);
        
        return pg_update($db, 'public.pay_project', $object, $object_key);
    }
    
    public function savePayProject($db) {
	    $object = $this->selectParameters();
        if (!isset($object['pay_project_id']) || $object['pay_project_id'] === null)
            return $this->saveNewPayProject($db, $object);
        else
            return $this->saveCurrentPayProject($db, $object);
    }
}
?>