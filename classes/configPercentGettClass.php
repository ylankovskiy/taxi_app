<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Config percent gett class, follows the public.config_percent_gett table naming convention and structure
 *
 */
class configPercentGettClass {	
    private $config_percent_gett_id;
    private $config_percent_gett_percent;
    private $config_percent_gett_date_begin;
    private $config_percent_gett_date_end;
    private $config_percent_gett_datetime_created;
    private $config_percent_gett_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['config_percent_gett_percent']) || !isset($json['config_percent_gett_date_begin']))
            return;
        
        if (isset($json['config_percent_gett_id']))
            $this->config_percent_gett_id = $json['config_percent_gett_id'];
        if (isset($json['config_percent_gett_percent']))
            $this->config_percent_gett_percent = $json['config_percent_gett_percent'];
        if (isset($json['config_percent_gett_date_begin']))
            $this->config_percent_gett_date_begin = $json['config_percent_gett_date_begin'];
        if (isset($json['config_percent_gett_date_end']))
            $this->config_percent_gett_date_end = $json['config_percent_gett_date_end'];
        if (isset($json['config_percent_gett_employee_added_id']))
            $this->config_percent_gett_employee_added_id = $json['config_percent_gett_employee_added_id'];
        
        $this->config_percent_gett_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['config_percent_gett_id']))
            $this->config_percent_gett_id = $row['config_percent_gett_id'];
        if (isset($row['config_percent_gett_percent']))
            $this->config_percent_gett_percent = $row['config_percent_gett_percent'];
        if (isset($row['config_percent_gett_date_begin']))
            $this->config_percent_gett_date_begin = $row['config_percent_gett_date_begin'];
        if (isset($row['config_percent_gett_date_end']))
            $this->config_percent_gett_date_end = $row['config_percent_gett_date_end'];
        if (isset($row['config_percent_gett_employee_added_id']))
            $this->config_percent_gett_employee_added_id = $row['config_percent_gett_employee_added_id'];
        if (isset($row['config_percent_gett_datetime_created']))
            $this->config_percent_gett_datetime_created = $row['config_percent_gett_datetime_created'];
    }
    
    public function selectConfigPercentGettId() {
        return $this->config_percent_gett_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectConfigPercentGettFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
				FROM 
				  postgres.public.config_percent_gett
				WHERE
					oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_config_percent_gett_oid_query";
        } else if (isset($this->config_percent_gett_id)) {
            $query = "SELECT *
			FROM 
			  postgres.public.config_percent_gett
			WHERE
				config_percent_gett_id = $1";
				
            $params = array(
                $this->config_percent_gett_id
            );
            
            $query_name = "select_config_percent_gett_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewConfigPercentGett($db, $object) {        
        $result = pg_insert($db, 'public.config_percent_gett', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectConfigPercentGettFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentConfigPercentGett($db, $object) {
        $object_key = array(
            'config_percent_gett_id' => $object['config_percent_gett_id']
        );
        
        unset($object['config_percent_gett_id']);
        
        return pg_update($db, 'public.config_percent_gett', $object, $object_key);
    }
    
    public function saveConfigPercentGett($db) {
	    $object = $this->selectParameters();
        if (!isset($object['config_percent_gett_id']) || $object['config_percent_gett_id'] === null)
            return $this->saveNewConfigPercentGett($db, $object);
        else
            return $this->saveCurrentConfigPercentGett($db, $object);
    }
}
?>