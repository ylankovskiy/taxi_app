<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Transactions class, follows the public.transactions table naming convention and structure
 *
 */
class transactionsClass {
    private $transactions_id;
    private $transactions_remark;
    private $transactions_type;
    private $transactions_subtype;
    private $transactions_status;
    private $transactions_processed_datetime;
    private $transactions_processed_employee_id;
    private $transactions_sum;
    private $transactions_datetime;
    private $transactions_driver_id;
    private $transactions_datetime_created;
    private $transactions_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['transactions_type']) || !isset($json['transactions_subtype']) || !isset($json['transactions_sum']) || !isset($json['transactions_driver_id']))
            return;
        
        if (isset($json['transactions_id']))
            $this->transactions_id = $json['transactions_id'];
        if (isset($json['transactions_remark']))
            $this->transactions_remark = $json['transactions_remark'];
        if (isset($json['transactions_type']))
            $this->transactions_type = $json['transactions_type'];
        if (isset($json['transactions_status']))
            $this->transactions_status = $json['transactions_status'];
        if (isset($json['transactions_processed_datetime']))
            $this->transactions_processed_datetime = $json['transactions_processed_datetime'];
        if (isset($json['transactions_processed_employee_id']))
            $this->transactions_processed_employee_id = $json['transactions_processed_employee_id'];
        if (isset($json['transactions_subtype']))
            $this->transactions_subtype = $json['transactions_subtype'];
        if (isset($json['transactions_sum']))
            $this->transactions_sum = $json['transactions_sum'];
        if (isset($json['transactions_driver_id']))
            $this->transactions_driver_id = $json['transactions_driver_id'];
        if (isset($json['transactions_employee_added_id']))
            $this->transactions_employee_added_id = $json['transactions_employee_added_id'];
        
        $this->transactions_datetime_created = date('Y-m-d H:i:s');
        $this->transactions_datetime = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['transactions_id']))
            $this->transactions_id = $row['transactions_id'];
        if (isset($row['transactions_remark']))
            $this->transactions_remark = $row['transactions_remark'];
        if (isset($row['transactions_type']))
            $this->transactions_type = $row['transactions_type'];
        if (isset($row['transactions_status']))
            $this->transactions_status = $row['transactions_status'];
        if (isset($row['transactions_processed_datetime']))
            $this->transactions_processed_datetime = $row['transactions_processed_datetime'];
        if (isset($row['transactions_processed_employee_id']))
            $this->transactions_processed_employee_id = $row['transactions_processed_employee_id'];
        if (isset($row['transactions_subtype']))
            $this->transactions_subtype = $row['transactions_subtype'];
        if (isset($row['transactions_sum']))
            $this->transactions_sum = $row['transactions_sum'];
        if (isset($row['transactions_datetime']))
            $this->transactions_datetime = $row['transactions_datetime'];
        if (isset($row['transactions_driver_id']))
            $this->transactions_driver_id = $row['transactions_driver_id'];
        if (isset($row['transactions_employee_added_id']))
            $this->transactions_employee_added_id = $row['transactions_employee_added_id'];
        if (isset($row['transactions_datetime_created']))
            $this->transactions_datetime_created = $row['transactions_datetime_created'];
    }
    
    public function selectTransactionsId() {
        return $this->transactions_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectTransactionsFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
			FROM 
				postgres.public.transactions
			WHERE
				oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_transactions_oid_query";
        } else if (isset($this->transactions_driver_id)) {
            $query = "SELECT *
			FROM 
				postgres.public.transactions
			WHERE
				transactions_driver_id = $1";
            
            $params = array(
                $this->transactions_driver_id
            );
            
            $query_name = "select_transactions_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewTransactions($db, $object) {
        $result = pg_insert($db, 'public.transactions', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectTransactionsFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentTransactions($db, $object) {
        $object_key = array(
            'transactions_id' => $object['transactions_id']
        );
        
        unset($object['transactions_id']);
        
        return pg_update($db, 'public.transactions', $object, $object_key);
    }
    
    public function saveTransactions($db) {
	    $object = $this->selectParameters();
        if (!isset($object['transactions_id']) || $object['transactions_id'] === null)
            return $this->saveNewTransactions($db, $object);
        else
            return $this->saveCurrentTransactions($db, $object);
    }
}
?>