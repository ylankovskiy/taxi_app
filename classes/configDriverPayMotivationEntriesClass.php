<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Config driver pay motivation entries class, follows the public.config_driver_pay_motivation_entries table naming convention and structure
 *
 */
class configDriverPayMotivationEntriesClass {
    private $config_driver_pay_motivation_entries_id;
    private $config_driver_pay_motivation_entries_starting_point;
    private $config_driver_pay_motivation_entries_ending_point;
    private $config_driver_pay_motivation_entries_type;
    private $config_driver_pay_motivation_entries_value;
    private $config_driver_pay_motivation_entries_config_dpm_id;
    
    public function __construct($json) {
        if (!isset($json['config_driver_pay_motivation_entries_type']) || !isset($json['config_driver_pay_motivation_entries_value']))
            return;
        
        if (isset($json['config_driver_pay_motivation_entries_id']))
            $this->config_driver_pay_motivation_entries_id = $json['config_driver_pay_motivation_entries_id'];
        if (isset($json['config_driver_pay_motivation_entries_starting_point']))
            $this->config_driver_pay_motivation_entries_starting_point = $json['config_driver_pay_motivation_entries_starting_point'];
        if (isset($json['config_driver_pay_motivation_entries_ending_point']))
            $this->config_driver_pay_motivation_entries_ending_point = $json['config_driver_pay_motivation_entries_ending_point'];
        if (isset($json['config_driver_pay_motivation_entries_type']))
            $this->config_driver_pay_motivation_entries_type = $json['config_driver_pay_motivation_entries_type'];
        if (isset($json['config_driver_pay_motivation_entries_value']))
            $this->config_driver_pay_motivation_entries_value = $json['config_driver_pay_motivation_entries_value'];
        if (isset($json['config_driver_pay_motivation_entries_config_dpm_id']))
            $this->config_driver_pay_motivation_entries_config_dpm_id = $json['config_driver_pay_motivation_entries_config_dpm_id'];
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['config_driver_pay_motivation_entries_id']))
            $this->config_driver_pay_motivation_entries_id = $row['config_driver_pay_motivation_entries_id'];
        if (isset($row['config_driver_pay_motivation_entries_starting_point']))
            $this->config_driver_pay_motivation_entries_starting_point = $row['config_driver_pay_motivation_entries_starting_point'];
        if (isset($row['config_driver_pay_motivation_entries_ending_point']))
            $this->config_driver_pay_motivation_entries_ending_point = $row['config_driver_pay_motivation_entries_ending_point'];
        if (isset($row['config_driver_pay_motivation_entries_type']))
            $this->config_driver_pay_motivation_entries_type = $row['config_driver_pay_motivation_entries_type'];
        if (isset($row['config_driver_pay_motivation_entries_value']))
            $this->config_driver_pay_motivation_entries_value = $row['config_driver_pay_motivation_entries_value'];
        if (isset($row['config_driver_pay_motivation_entries_config_dpm_id']))
            $this->config_driver_pay_motivation_entries_config_dpm_id = $row['config_driver_pay_motivation_entries_config_dpm_id'];
    }
    
    public function selectConfigDriverPayMotivationEntriesId() {
        return $this->config_driver_pay_motivation_entries_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectConfigDriverPayMotivationEntriesFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
				FROM 
				  postgres.public.config_driver_pay_motivation_entries
				WHERE
					oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_config_driver_pay_motivation_entries_oid_query";
        } else if (isset($this->config_driver_pay_motivation_entries_id)) {
            $query = "SELECT *
			FROM 
			  postgres.public.config_driver_pay_motivation_entries
			WHERE
				config_driver_pay_motivation_entries_id = $1";
				
            $params = array(
                $this->config_driver_pay_motivation_entries_id
            );
            
            $query_name = "select_config_driver_pay_motivation_entries_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewConfigDriverPayMotivationEntries($db, $object) {        
        $result = pg_insert($db, 'public.config_driver_pay_motivation_entries', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectConfigDriverPayMotivationEntriesFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentConfigDriverPayMotivationEntries($db, $object) {
        $object_key = array(
            'config_driver_pay_motivation_entries_id' => $object['config_driver_pay_motivation_entries_id']
        );
        
        unset($object['config_driver_pay_motivation_entries_id']);
        
        return pg_update($db, 'public.config_driver_pay_motivation_entries', $object, $object_key);
    }
    
    public function saveConfigDriverPayMotivationEntries($db) {
	    $object = $this->selectParameters();
        if (!isset($object['config_driver_pay_motivation_entries_id']) || $object['config_driver_pay_motivation_entries_id'] === null)
            return $this->saveNewConfigDriverPayMotivationEntries($db, $object);
        else
            return $this->saveCurrentConfigDriverPayMotivationEntries($db, $object);
    }
}
?>