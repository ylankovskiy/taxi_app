<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Order fraud class, follows the public.order_fraud table naming convention and structure
 *
 */
class orderFraudClass {
    private $order_fraud_id;
    private $order_fraud_order_id;
    private $order_fraud_sum;
    private $order_fraud_remark;
    
    public function __construct($json) {
        if (!isset($json['order_fraud_order_id']) || !isset($json['order_fraud_sum']) || !isset($json['order_fraud_remark']))
            return;
        
        if (isset($json['order_fraud_id']))
            $this->order_fraud_id = $json['order_fraud_id'];
        if (isset($json['order_fraud_order_id']))
            $this->order_fraud_order_id = $json['order_fraud_order_id'];
        if (isset($json['order_fraud_sum']))
            $this->order_fraud_sum = $json['order_fraud_sum'];
        if (isset($json['order_fraud_remark']))
            $this->order_fraud_remark = $json['order_fraud_remark'];
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['order_fraud_id']))
            $this->order_fraud_id = $row['order_fraud_id'];
        if (isset($row['order_fraud_order_id']))
            $this->order_fraud_order_id = $row['order_fraud_order_id'];
        if (isset($row['order_fraud_sum']))
            $this->order_fraud_sum = $row['order_fraud_sum'];
        if (isset($row['order_fraud_remark']))
            $this->order_fraud_remark = $row['order_fraud_remark'];
    }
    
    public function selectOrderFraudId() {
        return $this->order_fraud_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectOrderFraudFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
			FROM 
				postgres.public.order_fraud
			WHERE
				oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_order_fraud_oid_query";
        } else if (isset($this->order_fraud_order_id)) {            
            if (isset($this->order_fraud_order_id)) {
                $query = "SELECT *
				FROM 
					postgres.public.order_fraud
				WHERE
					order_fraud_order_id = $1";
                
                $params = array(
                    $this->order_fraud_order_id
                );
            }
            
            $query_name = "select_order_fraud_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewOrderFraud($db, $object) {
        $result = pg_insert($db, 'public.order_fraud', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectOrderFraudFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentOrderFraud($db, $object) {
        $object_key = array(
            'order_fraud_id' => $object['order_fraud_id']
        );
        
        unset($object['order_fraud_id']);
        
        return pg_update($db, 'public.order_fraud', $object, $object_key);
    }
    
    public function saveOrderFraud($db) {
	    $object = $this->selectParameters();
	    if ($object != null) {
	        if (!isset($object['order_fraud_id']) || $object['order_fraud_id'] === null)
        	    return $this->saveNewOrderFraud($db, $object);
    	    else
	            return $this->saveCurrentOrderFraud($db, $object);
        }
    }
}
?>