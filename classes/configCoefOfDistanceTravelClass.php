<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Config gas class, follows the public.config_gas table naming convention and structure
 *
 */
class configCoefOfDistanceTravelClass {
    private $config_coef_of_distance_travel_id;
    private $config_coef_of_distance_travel_val;
    private $config_coef_of_distance_travel_date_begin;
    private $config_coef_of_distance_travel_date_end;
    private $config_coef_of_distance_travel_datetime_created;
    private $config_coef_of_distance_travel_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['config_coef_of_distance_travel_val']) || !isset($json['config_coef_of_distance_travel_date_begin']))
            return;
        
        if (isset($json['config_coef_of_distance_travel_id']))
            $this->config_coef_of_distance_travel_id = $json['config_coef_of_distance_travel_id'];
        if (isset($json['config_coef_of_distance_travel_val']))
            $this->config_coef_of_distance_travel_val = $json['config_coef_of_distance_travel_val'];
        if (isset($json['config_coef_of_distance_travel_date_begin']))
            $this->config_coef_of_distance_travel_date_begin = $json['config_coef_of_distance_travel_date_begin'];
        if (isset($json['config_coef_of_distance_travel_date_end']))
            $this->config_coef_of_distance_travel_date_end = $json['config_coef_of_distance_travel_date_end'];
        if (isset($json['config_coef_of_distance_travel_employee_added_id']))
            $this->config_coef_of_distance_travel_employee_added_id = $json['config_coef_of_distance_travel_employee_added_id'];
        
        $this->config_coef_of_distance_travel_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['config_coef_of_distance_travel_id']))
            $this->config_coef_of_distance_travel_id = $row['config_coef_of_distance_travel_id'];
        if (isset($row['config_coef_of_distance_travel_val']))
            $this->config_coef_of_distance_travel_val = $row['config_coef_of_distance_travel_val'];
        if (isset($row['config_coef_of_distance_travel_date_begin']))
            $this->config_coef_of_distance_travel_date_begin = $row['config_coef_of_distance_travel_date_begin'];
        if (isset($row['config_coef_of_distance_travel_date_end']))
            $this->config_coef_of_distance_travel_date_end = $row['config_coef_of_distance_travel_date_end'];
        if (isset($row['config_coef_of_distance_travel_employee_added_id']))
            $this->config_coef_of_distance_travel_employee_added_id = $row['config_coef_of_distance_travel_employee_added_id'];
        if (isset($row['config_coef_of_distance_travel_datetime_created']))
            $this->config_coef_of_distance_travel_datetime_created = $row['config_coef_of_distance_travel_datetime_created'];
    }
    
    public function selectConfigCoefDistanceId() {
        return $this->config_coef_of_distance_travel_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectConfigCoefDistanceFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
				FROM 
				  postgres.public.config_coef_of_distance_travel
				WHERE
					oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_config_coef_of_distance_travel_oid_query";
        } else if (isset($this->config_coef_of_distance_travel_id)) {
            $query = "SELECT *
			FROM 
			  postgres.public.config_coef_of_distance_travel
			WHERE
				config_gas_id = $1";
				
            $params = array(
                $this->config_coef_of_distance_travel_id
            );
            
            $query_name = "select_config_coef_of_distance_travel_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewConfigCoefDistance($db, $object) {
        $result = pg_insert($db, 'public.config_coef_of_distance_travel', $object);
        $last_oid = pg_last_oid($result);
        $this->selectConfigCoefDistanceFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentConfigCoefDistance($db, $object) {
        $object_key = array(
            'config_coef_of_distance_travel_id' => $object['config_coef_of_distance_travel_id']
        );
        
        unset($object['config_coef_of_distance_travel_id']);
        
        return pg_update($db, 'public.config_coef_of_distance_travel', $object, $object_key);
    }
    
    public function saveConfigCoefDistance($db) {
	    $object = $this->selectParameters();
        if (!isset($object['config_coef_of_distance_travel_id']) || $object['config_coef_of_distance_travel_id'] === null)
            return $this->saveNewConfigCoefDistance($db, $object);
        else
            return $this->saveCurrentConfigCoefDistance($db, $object);
    }
}
?>