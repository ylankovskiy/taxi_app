<?php
error_reporting(-1);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver_payout_report',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Driver payout report class, follows the public.driver_payout_report table naming convention and structure
 *
 */
class driverPayoutReportClass {
    private $driver_payout_report_id;
    private $driver_payout_report_datetime_begin;
    private $driver_payout_report_datetime_end;
    private $driver_payout_report_driver_id;
    private $driver_payout_report_total_orders;
    private $driver_payout_report_sum_orders;
    private $driver_payout_report_sum_fraud;
    private $driver_payout_report_sum_tips;
    private $driver_payout_report_sum_transactions;
    private $driver_payout_report_percent_gett;
    private $driver_payout_report_sum_percent_gett;
    private $driver_payout_report_percent_company;
    private $driver_payout_report_sum_percent_company;
    private $driver_payout_report_sum_pay_driver;
    private $driver_payout_report_url;
    private $driver_payout_report_datetime_created;
    private $driver_payout_report_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['driver_payout_report_datetime_begin']) || 
        	!isset($json['driver_payout_report_datetime_end']) || 
        	!isset($json['driver_payout_report_driver_id']) || 
        	!isset($json['driver_payout_report_total_orders']) || 
        	!isset($json['driver_payout_report_sum_orders']) ||
        	!isset($json['driver_payout_report_sum_transactions']) ||
        	!isset($json['driver_payout_report_percent_gett']) ||
        	!isset($json['driver_payout_report_percent_company']) ||
        	!isset($json['driver_payout_report_url']))
            return;
        
        if (isset($json['driver_payout_report_id']))
            $this->driver_payout_report_id = $json['driver_payout_report_id'];
        if (isset($json['driver_payout_report_datetime_begin']))
            $this->driver_payout_report_datetime_begin = $json['driver_payout_report_datetime_begin'];
        if (isset($json['driver_payout_report_datetime_end']))
            $this->driver_payout_report_datetime_end = $json['driver_payout_report_datetime_end'];
        if (isset($json['driver_payout_report_driver_id']))
            $this->driver_payout_report_driver_id = $json['driver_payout_report_driver_id'];
        if (isset($json['driver_payout_report_total_orders']))
            $this->driver_payout_report_total_orders = $json['driver_payout_report_total_orders'];
        if (isset($json['driver_payout_report_sum_orders']))
            $this->driver_payout_report_sum_orders = $json['driver_payout_report_sum_orders'];
        if (isset($json['driver_payout_report_sum_fraud']))
            $this->driver_payout_report_sum_fraud = $json['driver_payout_report_sum_fraud'];
        if (isset($json['driver_payout_report_sum_tips']))
            $this->driver_payout_report_sum_tips = $json['driver_payout_report_sum_tips'];
        if (isset($json['driver_payout_report_sum_transactions']))
            $this->driver_payout_report_sum_transactions = $json['driver_payout_report_sum_transactions'];
        if (isset($json['driver_payout_report_percent_gett']))
            $this->driver_payout_report_percent_gett = $json['driver_payout_report_percent_gett'];
        if (isset($json['driver_payout_report_sum_percent_gett']))
            $this->driver_payout_report_sum_percent_gett = $json['driver_payout_report_sum_percent_gett'];
        if (isset($json['driver_payout_report_percent_company']))
            $this->driver_payout_report_percent_company = $json['driver_payout_report_percent_company'];
        if (isset($json['driver_payout_report_sum_percent_company']))
            $this->driver_payout_report_sum_percent_company = $json['driver_payout_report_sum_percent_company'];
        if (isset($json['driver_payout_report_sum_pay_driver']))
            $this->driver_payout_report_sum_pay_driver = $json['driver_payout_report_sum_pay_driver'];
        if (isset($json['driver_payout_report_url']))
            $this->driver_payout_report_url = $json['driver_payout_report_url'];
        if (isset($json['driver_payout_report_employee_added_id']))
            $this->driver_payout_report_employee_added_id = $json['driver_payout_report_employee_added_id'];

        $this->driver_payout_report_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}

    private function fillParametersFromRow($row) {
        if (isset($row['driver_payout_report_id']))
            $this->driver_payout_report_id = $row['driver_payout_report_id'];
        if (isset($row['driver_payout_report_datetime_begin']))
            $this->driver_payout_report_datetime_begin = $row['driver_payout_report_datetime_begin'];
        if (isset($row['driver_payout_report_datetime_end']))
            $this->driver_payout_report_datetime_end = $row['driver_payout_report_datetime_end'];
        if (isset($row['driver_payout_report_driver_id']))
            $this->driver_payout_report_driver_id = $row['driver_payout_report_driver_id'];
        if (isset($row['driver_payout_report_total_orders']))
            $this->driver_payout_report_total_orders = $row['driver_payout_report_total_orders'];
        if (isset($row['driver_payout_report_sum_orders']))
            $this->driver_payout_report_sum_orders = $row['driver_payout_report_sum_orders'];
        if (isset($row['driver_payout_report_sum_fraud']))
            $this->driver_payout_report_sum_fraud = $row['driver_payout_report_sum_fraud'];
        if (isset($row['driver_payout_report_sum_tips']))
            $this->driver_payout_report_sum_tips = $row['driver_payout_report_sum_tips'];
        if (isset($row['driver_payout_report_sum_transactions']))
            $this->driver_payout_report_sum_transactions = $row['driver_payout_report_sum_transactions'];
        if (isset($row['driver_payout_report_percent_gett']))
            $this->driver_payout_report_percent_gett = $row['driver_payout_report_percent_gett'];
        if (isset($row['driver_payout_report_sum_percent_gett']))
            $this->driver_payout_report_sum_percent_gett = $row['driver_payout_report_sum_percent_gett'];
        if (isset($row['driver_payout_report_percent_company']))
            $this->driver_payout_report_percent_company = $row['driver_payout_report_percent_company'];
        if (isset($row['driver_payout_report_sum_percent_company']))
            $this->driver_payout_report_sum_percent_company = $row['driver_payout_report_sum_percent_company'];
        if (isset($row['driver_payout_report_sum_pay_driver']))
            $this->driver_payout_report_sum_pay_driver = $row['driver_payout_report_sum_pay_driver'];
        if (isset($row['driver_payout_report_url']))
            $this->driver_payout_report_url = $row['driver_payout_report_url'];
        if (isset($row['driver_payout_report_datetime_created']))
            $this->driver_payout_report_datetime_created = $row['driver_payout_report_datetime_created'];
        if (isset($row['driver_payout_report_employee_added_id']))
            $this->driver_payout_report_employee_added_id = $row['driver_payout_report_employee_added_id'];
    }
    
    public function selectDriverPayoutReportId() {
        return $this->driver_payout_report_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    public function selectDriverPayoutReportFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        if (isset($this->driver_payout_report_driver_id)) {
            $query = 'SELECT *
				FROM 
			  	postgres.public.driver_payout_report
				WHERE
					driver_payout_report_driver_id = $1';
            
            $params = array(
                $this->driver_payout_report_driver_id
            );
            
            $query_name = "select_driver_payout_report_query";
        } else if (isset($oid)) {
            $query = 'SELECT *
				FROM 
			  	postgres.public.driver_payout_report
				WHERE
					oid = $1';
            
            $params = array(
                $oid
            );
            
            $query_name = "select_driver_payout_report_oid_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
            }
        }
    }
    
    private function saveNewDriverPayoutReport($db, $object) {
        $result = pg_insert($db, 'public.driver_payout_report', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectDriverPayoutReportFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentDriverPayoutReport($db, $object) {
        $object_key = array(
            'driver_payout_report_id' => $object['driver_payout_report_id']
        );
        
        unset($object['driver_payout_report_id']);
        
        return pg_update($db, 'public.driver_payout_report', $object, $object_key);
    }
    
    public function saveDriverPayoutReport($db) {
	    $object = $this->selectParameters();
        if (!isset($object['driver_payout_report_id']) || $object['driver_payout_report_id'] === null)
            return $this->saveNewDriverPayoutReport($db, $object);
        else
            return $this->saveCurrentDriverPayoutReport($db, $object);
    }
}
?>