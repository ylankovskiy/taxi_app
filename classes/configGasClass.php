<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Config gas class, follows the public.config_gas table naming convention and structure
 *
 */
class configGasClass {	
    private $config_gas_id;
    private $config_gas_cost;
    private $config_gas_date_begin;
    private $config_gas_date_end;
    private $config_gas_datetime_created;
    private $config_gas_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['config_gas_cost']) || !isset($json['config_gas_date_begin']))
            return;
        
        if (isset($json['config_gas_id']))
            $this->config_gas_id = $json['config_gas_id'];
        if (isset($json['config_gas_cost']))
            $this->config_gas_cost = $json['config_gas_cost'];
        if (isset($json['config_gas_date_begin']))
            $this->config_gas_date_begin = $json['config_gas_date_begin'];
        if (isset($json['config_gas_date_end']))
            $this->config_gas_date_end = $json['config_gas_date_end'];
        if (isset($json['config_gas_employee_added_id']))
            $this->config_gas_employee_added_id = $json['config_gas_employee_added_id'];
        
        $this->config_gas_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['config_gas_id']))
            $this->config_gas_id = $row['config_gas_id'];
        if (isset($row['config_gas_cost']))
            $this->config_gas_cost = $row['config_gas_cost'];
        if (isset($row['config_gas_date_begin']))
            $this->config_gas_date_begin = $row['config_gas_date_begin'];
        if (isset($row['config_gas_date_end']))
            $this->config_gas_date_end = $row['config_gas_date_end'];
        if (isset($row['config_gas_employee_added_id']))
            $this->config_gas_employee_added_id = $row['config_gas_employee_added_id'];
        if (isset($row['config_gas_datetime_created']))
            $this->config_gas_datetime_created = $row['config_gas_datetime_created'];
    }
    
    public function selectConfigGasId() {
        return $this->config_gas_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectConfigGasFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
				FROM 
				  postgres.public.config_gas
				WHERE
					oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_config_gas_oid_query";
        } else if (isset($this->config_gas_id)) {
            $query = "SELECT *
			FROM 
			  postgres.public.config_gas
			WHERE
				config_gas_id = $1";
				
            $params = array(
                $this->config_gas_id
            );
            
            $query_name = "select_config_gas_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewConfigGas($db, $object) {        
        $result = pg_insert($db, 'public.config_gas', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectConfigGasFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentConfigGas($db, $object) {
        $object_key = array(
            'config_gas_id' => $object['config_gas_id']
        );
        
        unset($object['config_gas_id']);
        
        return pg_update($db, 'public.config_gas', $object, $object_key);
    }
    
    public function saveConfigGas($db) {
	    $object = $this->selectParameters();
        if (!isset($object['config_gas_id']) || $object['config_gas_id'] === null)
            return $this->saveNewConfigGas($db, $object);
        else
            return $this->saveCurrentConfigGas($db, $object);
    }
}
?>