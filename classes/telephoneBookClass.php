<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Telephone book class, follows the public.telephone_book table naming convention and structure
 *
 */
class telephoneBookClass {
    private $telephone_book_id;
    private $telephone_book_telephone;
    private $telephone_book_datetime_created;
    
    public function __construct($json) {
        if (!isset($json['telephone_book_telephone']))
            return;
        
        if (isset($json['telephone_book_id']))
            $this->telephone_book_id = $json['telephone_book_id'];
        if (isset($json['telephone_book_telephone']))
            $this->telephone_book_telephone = $json['telephone_book_telephone'];
        
        $this->telephone_book_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['telephone_book_id']))
            $this->telephone_book_id = $row['telephone_book_id'];
        if (isset($row['telephone_book_telephone']))
            $this->telephone_book_telephone = $row['telephone_book_telephone'];
        if (isset($row['telephone_book_datetime_created']))
            $this->telephone_book_datetime_created = $row['telephone_book_datetime_created'];
    }
    
    public function selectTelephoneBookId() {
        return $this->telephone_book_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    public function selectTelephoneBookFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
				FROM 
				  postgres.public.telephone_book
				WHERE
					oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_telephone_book_oid_query";
        } else if (isset($this->telephone_book_telephone)) {
            // check for existing telephone		
            $query = "SELECT *
				FROM 
				  postgres.public.telephone_book
				WHERE
					telephone_book_telephone = $1";
            
            $params = array(
                $this->telephone_book_telephone
            );
            
            $query_name = "select_telephone_book_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewTelephoneBook($db, $object) {
        $result = pg_insert($db, 'public.telephone_book', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectTelephoneBookFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentTelephoneBook($db, $object) {
        $object_key = array(
            'telephone_book_id' => $object['telephone_book_id']
        );
        
        unset($object['telephone_book_id']);
        
        return pg_update($db, 'public.telephone_book', $object, $object_key);
    }
    
    public function saveTelephoneBook($db) {
	    $object = $this->selectParameters();
        if (!isset($object['telephone_book_id']) || $object['telephone_book_id'] === null)
            return $this->saveNewTelephoneBook($db, $object);
        else
            return $this->saveCurrentTelephoneBook($db, $object);
    }
}
?>