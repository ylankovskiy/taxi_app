<?php
// error_reporting(0);

$patterns    = array(
    '/views',
    '/controllers',
    '/employee',
    '/driver',
    '/main',
    '/order',
    '/pay',
    '/schedule',
    '/transaction',
    '/vehicle',
    '/utils',
    '/select',
    '/classes',
  '/bank'
);
$replacement = '';
$path        = str_replace($patterns, $replacement, getcwd());

error_log($path);
chdir($path);

require_once "db.php";
require_once "utils/helpers/funcs.php";

/*
 *	Schedule remarks class, follows the public.schedule_remarks table naming convention and structure
 *
 */
class scheduleRemarksClass {
    private $schedule_remarks_id;
    private $schedule_remarks_schedule_id;
    private $schedule_remarks_tab_index;
    private $schedule_remarks_remark;
    private $schedule_remarks_datetime_created;
    private $schedule_remarks_employee_added_id;
    
    public function __construct($json) {
        if (!isset($json['schedule_remarks_schedule_id']) || !isset($json['schedule_remarks_tab_index']) || !isset($json['schedule_remarks_remark']))
            return;
        
        if (isset($json['schedule_remarks_id']))
            $this->schedule_remarks_id = $json['schedule_remarks_id'];
        if (isset($json['schedule_remarks_schedule_id']))
            $this->schedule_remarks_schedule_id = $json['schedule_remarks_schedule_id'];
        if (isset($json['schedule_remarks_tab_index']))
            $this->schedule_remarks_tab_index = $json['schedule_remarks_tab_index'];
        if (isset($json['schedule_remarks_remark']))
            $this->schedule_remarks_remark = $json['schedule_remarks_remark'];
        if (isset($json['schedule_remarks_employee_added_id']))
            $this->schedule_remarks_employee_added_id = $json['schedule_remarks_employee_added_id'];
        
        $this->schedule_remarks_datetime_created = date('Y-m-d H:i:s');
    }
    
    public function __destruct() {}
    
    private function fillParametersFromRow($row) {
        if (isset($row['schedule_remarks_id']))
            $this->schedule_remarks_id = $row['schedule_remarks_id'];
        if (isset($row['schedule_remarks_schedule_id']))
            $this->schedule_remarks_schedule_id = $row['schedule_remarks_schedule_id'];
        if (isset($row['schedule_remarks_tab_index']))
            $this->schedule_remarks_tab_index = $row['schedule_remarks_tab_index'];
        if (isset($row['schedule_remarks_remark']))
            $this->schedule_remarks_remark = $row['schedule_remarks_remark'];
        if (isset($row['schedule_remarks_employee_added_id']))
            $this->schedule_remarks_employee_added_id = $row['schedule_remarks_employee_added_id'];
        if (isset($row['schedule_remarks_datetime_created']))
            $this->schedule_remarks_datetime_created = $row['schedule_remarks_datetime_created'];
    }
    
    public function selectScheduleRemarksId() {
        return $this->schedule_remarks_id;
    }
    
    public function selectParameters() {
        return array_filter((array) get_object_vars($this), 'is_not_null');
    }
    
    function selectScheduleRemarksFromDB($db, $oid) {
        $params = null;
        $query_name = null;
        
        if (isset($oid)) {
            $query = "SELECT *
			FROM 
				postgres.public.schedule_remarks
			WHERE
				oid = $1";
            
            $params = array(
                $oid
            );
            
            $query_name = "select_schedule_remarks_oid_query";
        } else if (isset($this->schedule_remarks_schedule_id)) {            
            if (isset($this->schedule_remarks_schedule_id)) {
                $query = "SELECT *
				FROM 
					postgres.public.schedule_remarks
				WHERE
					schedule_remarks_schedule_id = $1";
                
                $params = array(
                    $this->schedule_remarks_schedule_id
                );
            }
            
            $query_name = "select_schedule_remarks_query";
        }
        
        if (parameter_set($params)) {
    		$result = pg_query_params( $db, "SELECT name FROM pg_prepared_statements WHERE name = $1", array(
				$query_name 
			) );
  
			if ( !$result || pg_num_rows( $result ) == 0 ) {
				$result = pg_prepare( $db, $query_name, $query );
			} //!$result || pg_num_rows( $result ) == 0
		
            $result = pg_execute($db, $query_name, $params);
            
            if ($result) {
                while ($row = pg_fetch_assoc($result)) {
                    $this->fillParametersFromRow($row);
                }
                
                pg_free_result($result);
            }
        }
    }
    
    private function saveNewScheduleRemarks($db, $object) {
        $result = pg_insert($db, 'public.schedule_remarks', $object);
        
        $last_oid = pg_last_oid($result);
        $this->selectScheduleRemarksFromDB($db, $last_oid);
        
        return $result;
    }
    
    private function saveCurrentScheduleRemarks($db, $object) {
        $object_key = array(
            'schedule_remarks_id' => $object['schedule_remarks_id']
        );
        
        unset($object['schedule_remarks_id']);
        
        return pg_update($db, 'public.schedule_remarks', $object, $object_key);
    }
    
    public function saveScheduleRemarks($db) {
	    $object = $this->selectParameters();
	    if ($object != null) {
	        if (!isset($object['schedule_remarks_id']) || $object['schedule_remarks_id'] === null)
        	    return $this->saveNewScheduleRemarks($db, $object);
    	    else
	            return $this->saveCurrentScheduleRemarks($db, $object);
        }
    }
}
?>