<link href="css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
<link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- In <head> -->
<link href="css/nouislider.min.css" rel="stylesheet">

<!-- In <body> -->




<!-- FooTable -->
<link href="css/plugins/footable/footable.bootstrap.css" rel="stylesheet">
<div class="container-fluid">
    <div class="row">
        <div class="col-lg-12">
            <!-- Nav tabs -->
            <ul id="myTab" class="nav nav-tabs">
                <li><a href="#config_percent" data-toggle="tab">&nbsp;&nbsp;%&nbsp;&nbsp;&nbsp;</a></li>
                <li><a href="#motivation" data-toggle="tab">Мотивация</a></li>
                <li><a href="#vehicle" data-toggle="tab">Автомобили</a></li>
                <li><a href="#settings" data-toggle="tab">Настройки</a></li>
                <li><a href="#pays" data-toggle="tab">Зарплата</a></li>
                <li><a href="#fraud" data-toggle="tab">Фрод</a></li>
                <li><a href="#correction" data-toggle="tab">Корректировки</a></li>

            </ul>

            <!-- Tab panes -->
            <div class="tab-content">
                <div class="tab-pane active" id="config_percent">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Настройки процентных отчислений</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="config_percent_gett_percent">% Gett</label>
                                <form class="form-inline" role="form">
                                    <div class="form-group ">
                                        <input type="number" class="form-control" name="config_percent_gett_percent" id="config_percent_gett_percent" placeholder="% gett" disabled>
                                    </div>
                                    <button type="button" class="btn btn-info btn-sm" name="config_percent_gett_history">История</button>
                                    <button type="button" class="btn btn-warning btn-sm" name="config_percent_gett_change">Изменить</button>
                                </form>
                            </div>
                            <div class="col-lg-6">
                                <label for="config_percent_company">% Компании</label>
                                <form class="form-inline" role="form">
                                    <div class="form-group">
                                        <input type="number" class="form-control" name="config_percent_company_percent" id="config_percent_company" placeholder="% компании" disabled>
                                    </div>
                                    <button type="button" class="btn btn-info btn-sm" name="config_percent_company_history">История</button>
                                    <button type="button" class="btn btn-warning btn-sm" name="config_percent_company_change">Изменить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="motivation">
                    <div class="container-fluid">
                        <div class="row">
                        <div class="col-lg-12">
                            <h2>Настройки мотивации</h2>
                        </div>
                    </div>
                        <div class="row">
                        <div class="col-lg-6">
                            <label for="config_driver_pay_motivation">Срок дейтвия</label>
                            <form class="form-inline" role="form">
                                <div class="form-group">
                                    <input type="number" class="form-control" name="config_driver_pay_motivation_date_begin" id="config_driver_pay_motivation_date_begin" placeholder="срок действия" disabled>
                                    -
                                    <input type="number" class="form-control" name="config_driver_pay_motivation_date_end" id="config_driver_pay_motivation_date_end" placeholder="срок действия" disabled>
                                </div>
                                <button type="button" class="btn btn-info btn-sm" name="config_driver_pay_motivation_history">История</button>
                                <button type="button" class="btn btn-warning btn-sm" name="config_driver_pay_motivation_change">Изменить</button>
                            </form>
                        </div>
                    </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="config_driver_pay_motivation_error">Погрешность</label>
                                <div class="form-inline" role="form">
                                    <input type="number" class="form-control" name="config_driver_pay_motivation_error" id="config_driver_pay_motivation_error" placeholder="погрешность" disabled>
                                        <button type="button" class="btn btn-info btn-sm" name="config_driver_pay_motivation_error_history">История</button>
                                    <button type="button" class="btn btn-warning btn-sm" name="config_driver_pay_motivation_error_change">Изменить</button>
                                </div>

                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-12">
                                    <table id="ft-motivation-head" class="table table-bordered table-responsive table-condensed" data-paging="true" data-sorting="true" data-filtering="true">

                                    </table>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="tab-pane" id="vehicle">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Настройки по автомобилям</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="config_gasoline_cost">Стоимость бензина за литр</label>
                                <form class="form-inline" role="form">
                                    <div class="form-group">
                                        <input type="number" class="form-control" name="config_gasoline_cost" id="config_gasoline_cost" placeholder="стоимость бензина за литр" disabled>
                                    </div>
                                    <button type="button" class="btn btn-info btn-sm" name="config_gasoline_cost_history">История</button>
                                    <button type="button" class="btn btn-warning btn-sm" name="config_gasoline_cost_change">Изменить</button>
                                </form>
                            </div>
                            <div class="col-lg-6">
                                <label for="config_coef_of_distance_travel_val">Стоимость холостого пробега</label>
                                <form class="form-inline" role="form">
                                    <div class="form-group">
                                        <input type="number" class="form-control" name="config_coef_of_distance_travel_val" id="config_coef_of_distance_travel_val" placeholder="коэффицент холостого пробега" disabled>
                                    </div>
                                    <button type="button" class="btn btn-info btn-sm" name="config_coef_of_distance_travel_val_history">История</button>
                                    <button type="button" class="btn btn-warning btn-sm" name="config_coef_of_distance_travel_val_change">Изменить</button>
                                </form>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="config_gas_cost">Стоимость газа за литр</label>
                                <form class="form-inline" role="form">
                                    <div class="form-group">
                                        <input type="number" class="form-control" name="config_gas_cost" id="config_gas_cost" placeholder="стоимость газа, л" disabled>
                                    </div>
                                    <button type="button" class="btn btn-info btn-sm" name="config_gas_cost_history">История</button>
                                    <button type="button" class="btn btn-warning btn-sm" name="config_gas_cost_change">Изменить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="tab-pane" id="settings">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Настройки часового пояса</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="config_timezone_from_moscow">Часовой пояс от г.Москва</label>
                                <form class="form-inline" role="form">
                                    <div class="form-group">
                                        <input type="number" class="form-control" name="config_timezone_from_moscow" id="config_timezone_from_moscow" placeholder="часовой пояс" disabled>
                                    </div>
                                    <button type="button" class="btn btn-info btn-sm" name="config_timezone_history">История</button>
                                    <button type="button" class="btn btn-warning btn-sm" name="config_timezone_change">Изменить</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="pays">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Настройки зарплат</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <form class="form-inline" role="form">
                                    <div class="form-group">
                                        <select data-placeholder="Укажите тип водиеля" class="" id="config_driver_payout_driver_type" name="config_driver_payout_driver_type">
                                            <option selected disabled value="-1">Выбрать тип водителя</option>
                                            <option value="0">наш аренда</option>
                                            <option value="1">наш зарплата</option>
                                            <option value="2">с личным автомобилем</option>
                                        </select>
                                    </div>
                                    <button type="button" class="btn btn-info btn-sm" name="config_driver_payout_history">История</button>
                                    <button type="button" class="btn btn-warning btn-sm" name="config_driver_payout_change">Изменить</button>
                                </form>
                                <form class="form-inline" role="form">
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <label for="config_driver_payout_day_of_week">День недели</label>
                                        </div>
                                        <div class="col-lg-4">
                                            <label for="config_driver_payout_time">Время</label>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-4">
                                            <div class="form-group">
                                                <input type="number" class="form-control" name="config_driver_payout_day_of_week" id="config_driver_payout_day_of_week" placeholder="день недели" disabled>
                                            </div>
                                        </div>
                                        <div class="col-lg-4">
                                            <div class="form-group" >
                                                <input type="text" class="form-control" name="config_driver_payout_time" id="config_driver_payout_time"  placeholder="время" disabled>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-6">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="fraud">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Загрузка данных по фроду</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="config_upload_fraud">Выберите файл по фроду для загрузки на сервер</label>
                                <form name="upload_fraud" class="form-inline" role="form">
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" name="config_upload_fraud" id="config_upload_fraud">
                                    </div>
<!--                                    <button type="submit" class="btn btn-info btn-sm" name="config_upload_frod_upload">Загрузить</button>-->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="correction">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12">
                                <h2>Загрузка данных корректировок</h2>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-6">
                                <label for="config_upload_correction">Выберите файл корректировок для загрузки на сервер</label>
                                <form class="form-inline" role="form">
                                    <div class="form-group">
                                        <input type="file" class="form-control-file" name="config_upload_correction" id="config_upload_correction">
                                    </div>
<!--                                    <button type="button" class="btn btn-info btn-sm" name="config_upload_correction_upload">Загрузить</button>-->
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
</div>
<button class="btn btn-primary btn-lg hide" data-toggle="modal" data-target="#myModal">
    Посмотреть демо
</button>
<!--<div id="range"></div>-->
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

        </div>
    </div>
</div>


<!-- Sparkline -->
<script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- FooTable -->
<script src="js/plugins/footable/footable.js"></script>
<!-- Jquery Validate -->
<script src="js/plugins/validate/jquery.validate.min.js"></script>
<script src="js/plugins/validate/localization/messages_ru.min.js"></script>
<!-- Steps -->
<script src="js/plugins/staps/jquery.steps.js"></script>
<!-- Chosen -->
<script src="js/plugins/chosen/chosen.jquery.js"></script>

<script src="js/nouislider.min.js"></script>
<script src="js/wNumb.js"></script>
<!--PapaParse-->
<script src="js/plugins/PapaParse-4.3.2/papaparse.js"></script>

<script>
    var config_modal = {
        controllers: {
            url_view: "controllers/configuration/configurationView.php",
            url_save: "controllers/configuration/configurationStore.php"
        },

        percent: {
            gett: {
                table: ".history",
                columns: "controllers/configuration/configurationViewAllColumnsPercentGettHistory.json",
                method: {
                    save: "store_percent_gett",
                    view: "view_percent_gett"
                }
            },
            company: {
                table: ".history",
                columns: "controllers/configuration/configurationViewAllColumnsPercentCompanyHistory.json",
                method: {
                    save: "store_percent_company",
                    view: "view_percent_company"
                }
            }
        },
        motivation: {
            table: ".history",
            columns: "controllers/configuration/configurationViewAllColumnsMotivationHistory.json",
            method: {
                save: "store_driver_pay_motivation",
                view: "view_driver_pay_motivation"
            }
        },
        motivation_error: {
            table: ".history",
            columns: "controllers/configuration/configurationViewAllColumnsMotivationErrorHistory.json",
            method: {
                save: "store_driver_pay_motivation_error",
                view: "view_driver_pay_motivation_error"
            }
        },
        fuel: {
            gas: {
                table: ".history",
                columns: "controllers/configuration/configurationViewAllColumnsCostGasHistory.json",
                method: {
                    save: "store_gas",
                    view: "view_gas"
                }
            },
            gasoline: {
                table: ".history",
                columns: "controllers/configuration/configurationViewAllColumnsCostGasolineHistory.json",
                method: {
                    save: "store_gasoline",
                    view: "view_gasoline"
                }
            },
            coef_of_distance_travel: {
                table: ".history",
                columns: "controllers/configuration/configurationViewAllColumnsCoefDistanceHistory.json",
                method: {
                    save: "store_coef_of_distance_travel",
                    view: "view_coef_of_distance_travel"
                }
            }
        },
        settings: {
            timezone: {
                table: ".history",
                columns: "controllers/configuration/configurationViewAllColumnsTimezoneHistory.json",
                method: {
                    save: "store_timezone",
                    view: "view_timezone"
                }
            }
        },
        payout: {
            table: ".history",
            columns: "controllers/configuration/configurationViewAllColumnsPayoutHistory.json",
            method: {
                save: "store_driver_payout",
                view: "view_driver_payout"
            }
        }
    };

    Object.freeze( config_modal );
    var ft = null;
    var  ft_motivation = null;
    var  ft_motivation_modal = null;

    function init_table(table, columns, controller, method) {
        console.log(table,columns);
        ft = FooTable.init(table, {
            "columns": $.get(columns, null, null, "json"),
            "rows": $.ajax({
                url: controller,
                type: 'post',
                dataType: 'json',
                cache: false,
                async: false,
                contentType: 'application/json;charset=utf-8',
                data: JSON.stringify({
                    method: method


                })
            })
        });

        return ft;
    }
    function StoreDataToBase(JsonForm) {
        $.ajax({
            url: "controllers/configuration/configurationStore.php",
            type: 'post',
            dataType: 'json',
            cache: false,
            async: false,
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify(JsonForm),

            success: function (data) {
//                console.log(data, '123');
                alertShowHide(data['ERROR_CODE'], data['ERROR_MESSAGE']);
            },
//            error: function (error) {
//                console.log('error',error);
//            },
//            complete: function (txt) {
//                console.log(txt);
//            }
        });
    }
    function loadFraud(resaults, file) {
        $.ajax({
            url: "controllers/fraud/fraudStore.php",
            type: 'post',
            dataType: 'json',
            cache: false,
            async: false,
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify(resaults.data)
        });
    }
    function loadCorrection(resaults, file) {
        $.ajax({
            url: "controllers/correction/correctionStore.php",
            type: 'post',
            dataType: 'json',
            cache: false,
            async: false,
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify(resaults.data)
        });
    }
    //noUIRange

    function addRange(range, intervals, max) {
//        var range = document.getElementById('range');
        range.style.height = '40px';
        range.style.margin = '40px auto 30px';
        noUiSlider.create(range, {
            start: intervals, // 4 handles, starting at...
//            margin: 300, // Handles must be at least 300 apart
//            limit: 600, // ... but no more than 600
            connect: true, // Display a colored bar between the handles
            direction: 'ltr', // Put '0' at the bottom of the slider
            orientation: 'horizontal', // Orient the slider vertically
            behaviour: 'tap-drag', // Move handle on tap, bar is draggable
            step: 1,
            tooltips: true,
            format: wNumb({
                decimals: 0
            }),
            range: {
                'min': 0,
                'max': max
            },
//            pips: { // Show a scale with the slider
//                mode: 'steps',
//                stepped: true,
//                density: 4
//            },
            format: wNumb({
                decimals: 3,
                thousand: '.',
                postfix: ' Руб.',
            })
        });

        return range;
    }
    function removeRange(range) {
        range.noUiSlider.destroy();
       return range;
    }
    $(document).ready(function () {

        console.log(config_modal);
        $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {
            var tmp_vals = null;
            function getVals (controller = config_modal.controllers.url_view, method, param){
                var ret = null;
                var data = {method: method};
                if (method == 'view_driver_payout') {
                    data.config_driver_payout_driver_type = param;
                }
                $.ajax({
                    url: controller,
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    async: false,
                    contentType: 'application/json;charset=utf-8',
                    data: JSON.stringify(data),
                    success: function (d) {
                        ret = d;
                    }
                });
                return ret;
            }
            if ($(e.target).attr("href") == '#motivation') {
                if (ft_motivation != null) {
                    ft_motivation.destroy();
                }
                ft_motivation = FooTable.init('#ft-motivation-head', {
                    "columns": $.get("controllers/configuration/configurationViewAllColumnsMotivationHistory.json", null, null, "json"),
                    "rows": $.ajax({
                        url: config_modal.controllers.url_view,
                        type: 'post',
                        dataType: 'json',
                        cache: false,
                        async: false,
                        contentType: 'application/json;charset=utf-8',
                        data: JSON.stringify({
                            method: config_modal.motivation.method.view
                        })
                    })
                });
            }
            if ($(e.target).attr("href") == '#config_percent') {
                tmp_vals = null;
                tmp_vals = getVals(config_modal.controllers.url_view ,config_modal.percent.gett.method.view );
                if (!$.isEmptyObject(tmp_vals)) {
                    $('[name=config_percent_gett_percent]').val(tmp_vals[0].config_percent_gett_percent);
                } else {
                    $('[name=config_percent_gett_percent]').val('');

                }

                tmp_vals = getVals(config_modal.controllers.url_view ,config_modal.percent.company.method.view );
                    if (!$.isEmptyObject(tmp_vals)) {
                        $('[name=config_percent_company_percent]').val(tmp_vals[0].config_percent_company_percent);
                    } else {
                        $('[name=config_percent_company_percent]').val('');

                    }
            }

            if ($(e.target).attr("href") == '#vehicle') {
                tmp_vals = null;
                tmp_vals = getVals(config_modal.controllers.url_view ,config_modal.fuel.gasoline.method.view );
                if (!$.isEmptyObject(tmp_vals)) {
                    $('[name=config_gasoline_cost]').val(tmp_vals[0].config_gasoline_cost);
                } else {
                    $('[name=config_gasoline_cost]').val('');

                }
                tmp_vals = getVals(config_modal.controllers.url_view ,config_modal.fuel.gas.method.view);
                if (!$.isEmptyObject(tmp_vals)) {
                    $('[name=config_gas_cost]').val(tmp_vals[0].config_gas_cost);
                } else {
                    $('[name=config_gas_cost]').val('');

                }
                tmp_vals = getVals(config_modal.controllers.url_view ,config_modal.fuel.coef_of_distance_travel.method.view);
                if (!$.isEmptyObject(tmp_vals)) {
                    $('[name=config_coef_of_distance_travel_val]').val(tmp_vals[0].config_coef_of_distance_travel_val);
                } else {
                    $('[name=config_coef_of_distance_travel_val]').val('');

                }
            }
            if ($(e.target).attr("href") == '#settings') {
                tmp_vals = null;
                tmp_vals = getVals(config_modal.controllers.url_view ,config_modal.settings.timezone.method.view );
                if (!$.isEmptyObject(tmp_vals)) {
                    $('[name=config_timezone_from_moscow]').val(tmp_vals[0].config_timezone_from_moscow);
                } else {
                    $('[name=config_timezone_from_moscow]').val('');
                }
            }

            if ($(e.target).attr("href") == '#pays') {
                $( "#config_driver_payout_driver_type" ).change(function() {
                tmp_vals = null;
                tmp_vals = getVals(config_modal.controllers.url_view,
                                    config_modal.payout.method.view ,
                                    $('#config_driver_payout_driver_type').val()
                );
                if (!$.isEmptyObject(tmp_vals)) {
                    $('[name=config_driver_payout_day_of_week]').val(tmp_vals[0].config_driver_payout_day_of_week);
                    $('[name=config_driver_payout_time]').val(tmp_vals[0].config_driver_payout_time);
                } else {
                    $('[name=config_driver_payout_day_of_week]').val('');
                    $('[name=config_driver_payout_time]').val('');
                }
                });
                $( "#config_driver_payout_driver_type" ).change();
            }

//            if ($(e.target).attr("href") == '#motivation') {
//                    tmp_vals = null;
//                    tmp_vals = getVals(config_modal.controllers.url_view,
//                        config_modal.payout.method.view ,
//                        $('#config_driver_payout_driver_type').val()
//                    );
//                    if (!$.isEmptyObject(tmp_vals)) {
//                        $('[name=config_driver_payout_day_of_week]').val(tmp_vals[0].config_driver_payout_day_of_week);
//                        $('[name=config_driver_payout_time]').val(tmp_vals[0].config_driver_payout_time);
//                    } else {
//                        $('[name=config_driver_payout_day_of_week]').val('');
//                        $('[name=config_driver_payout_time]').val('');
//                    }
//                $( "#config_driver_payout_driver_type" ).change();
//                }


        });
        $("#myTab a:first").tab('show');


        $('[name=config_percent_gett_change]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentPercentGettChange.php'
            });
        });
        $('[name=config_percent_gett_history]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentPercentGettHistory.php'
            });
        });
        $('[name=config_percent_company_change]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentPercentCompanyChange.php'
            });
        });
        $('[name=config_percent_company_history]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentPercentCompanyHistory.php'
            });
        });

        $('[name=config_gasoline_cost_change]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentCostGasolineChange.php'
            });
        });
        $('[name=config_gasoline_cost_history]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentCostGasolineHistory.php'
            });
        });
        $('[name=config_gas_cost_change]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentCostGasChange.php'
            });
        });
        $('[name=config_gas_cost_history]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentCostGasHistory.php'
            });
        });
        $('[name=config_coef_of_distance_travel_val_change]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentCoefDistanceChange.php'
            });
        });
        $('[name=config_coef_of_distance_travel_val_history]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentCoefDistanceHistory.php'
            });
        });


        $('[name=config_driver_pay_motivation_change]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentMotivationChange.php'
            });
        });
        $('[name=config_driver_pay_motivation_history]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentMotivationHistory.php'
            });
        });
        $('[name=config_timezone_history]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentSettingsTimezoneHistory.php'
            });
        });
        $('[name=config_timezone_change]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentSettingsTimezoneChange.php'
            });
        });
        $('[name=config_driver_payout_history]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentDriverPayoutHistory.php'
            });
        });
        $('[name=config_driver_payout_change]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentDriverPayoutChange.php'
            });
        });
        $('[name=config_driver_pay_motivation_error_history]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentDriverPayMotivationErrorHistory.php'
            });
        });
        $('[name=config_driver_pay_motivation_error_change]').click(function () {
            $('#myModal').modal({
                remote: 'views/configuration/configurationModalContentDriverPayMotivationErrorChange.php'
            });
        });




        $('#myModal').on('hidden.bs.modal', function () {
            $('#myModal').removeData('bs.modal');
            $('#myModal .modal-content').html('');
            $('.modal-dialog').removeClass('modal-sm');
            $('.modal-dialog').removeClass('modal-lg');
            if (ft != null) {
                ft.destroy();
                $(".footable-empty").remove();

            };

        });
        $('#myModal').on('shown.bs.modal', function (e) {
            var id_modal = $(e.target).find('.modal-header').attr('id');

            console.log("123", $(e.target).find('.modal-header').attr('id'));
            $('#save').on('click', function (e) {
                var $form = $('#form-data');
                var jsonForm = JSON.stringify($form.serializeObject());
                var tmp_form = $form.serializeObject();
                if (id_modal == 'coef-of-distance-travel-change') {
                    tmp_form.method = config_modal.fuel.coef_of_distance_travel.method.save;
                    console.log(tmp_form);
                    StoreDataToBase(tmp_form);
                }
                if (id_modal == 'gas-change') {
                    tmp_form.method = config_modal.fuel.gas.method.save;
                    console.log(tmp_form);
                    StoreDataToBase(tmp_form);
                }
                if (id_modal == 'gasoline-change') {
                    tmp_form.method = config_modal.fuel.gasoline.method.save;
                    console.log(tmp_form);
                    StoreDataToBase(tmp_form);
                }
                if (id_modal == 'payout-change') {
                    tmp_form.method = config_modal.payout.method.save;
                    console.log(tmp_form);
                    StoreDataToBase(tmp_form);
                }
                if (id_modal == 'driver-pay-motivation-error-change') {
                    tmp_form.method = config_modal.motivation_error.method.save;
                    console.log(tmp_form);
                    StoreDataToBase(tmp_form);
                }
                if (id_modal == 'motivation-change') {
                    function RangeValStore(start, end, value) {
                        this.config_driver_pay_motivation_entries_starting_point = start;
                        this.config_driver_pay_motivation_entries_ending_point = end;
                        this.config_driver_pay_motivation_entries_type = "0";
                        this.config_driver_pay_motivation_entries_value = value;

                    }
                    var entries_tmp = [];
                    $('#ft-motivation tbody tr').each(function (index, element) {
                        console.log(index, element);
                        console.log($('td:eq(2) input', this).val());
                        entries_tmp.push(new RangeValStore(
                            $('td:eq(0) input', this).val(),
                            $('td:eq(1) input', this).val(),
                            $('td:eq(2) input', this).val()
                        ));
                    });
                    var data;
                    data = {
                        method: config_modal.motivation.method.save,
                        entries: JSON.stringify(entries_tmp),
                        config_driver_pay_motivation_date_begin: tmp_form.config_driver_pay_motivation_date_begin,
                        config_driver_pay_motivation_date_end: tmp_form.config_driver_pay_motivation_date_end,
                        config_driver_pay_motivation_error: tmp_form.config_driver_pay_motivation_error,
                        acceptTerms: tmp_form.acceptTerms
                };
                    StoreDataToBase(data);
                }
                if (id_modal == 'percent-company-change') {
                    tmp_form.method = config_modal.percent.company.method.save;
                    console.log(tmp_form);
                    StoreDataToBase(tmp_form);
                }
                if (id_modal == 'percent-gett-change') {
                    tmp_form.method = config_modal.percent.gett.method.save;
//                    console.log(tmp_form);
                    StoreDataToBase(tmp_form);
                }
                if (id_modal == 'timezone-change') {
                    tmp_form.method = config_modal.settings.timezone.method.save;
                    console.log(tmp_form);
                    StoreDataToBase(tmp_form);
                }


//                console.log(jsonForm);
//                console.log('save');
//                alert(' Сохранено');
                $('#myModal').modal('hide');

            });

            //history
            if (id_modal == 'percent-gett-history') {
                ft = init_table(
                    config_modal.percent.gett.table,
                    config_modal.percent.gett.columns,
                    config_modal.controllers.url_view,
                    config_modal.percent.gett.method.view
                );
                console.log(config_modal.controllers.url_view);
                console.log(ft);

            }
            if (id_modal == 'percent-company-history') {
                ft = init_table(
                    config_modal.percent.company.table,
                    config_modal.percent.company.columns,
                    config_modal.controllers.url_view,
                    config_modal.percent.company.method.view
                );
            }
            if (id_modal == 'driver-pay-motivation-error-history') {
                ft = init_table(
                    config_modal.motivation_error.table,
                    config_modal.motivation_error.columns,
                    config_modal.controllers.url_view,
                    config_modal.motivation_error.method.view
                );
            }
            if (id_modal == 'motivation-history') {
                ft = init_table(
                    config_modal.motivation.table,
                    config_modal.motivation.columns,
                    config_modal.controllers.url_view,
                    config_modal.motivation.method.view
                );
            }
            if (id_modal == 'coef-of-distance-travel-history') {
                ft = init_table(
                    config_modal.fuel.coef_of_distance_travel.table,
                    config_modal.fuel.coef_of_distance_travel.columns,
                    config_modal.controllers.url_view,
                    config_modal.fuel.coef_of_distance_travel.method.view
                );
            }
            if (id_modal == 'gas-history') {
                ft = init_table(
                    config_modal.fuel.gas.table,
                    config_modal.fuel.gas.columns,
                    config_modal.controllers.url_view,
                    config_modal.fuel.gas.method.view
                );
            }

            if (id_modal == 'gasoline-history') {
                ft = init_table(
                    config_modal.fuel.gasoline.table,
                    config_modal.fuel.gasoline.columns,
                    config_modal.controllers.url_view,
                    config_modal.fuel.gasoline.method.view
                );
            }
            if (id_modal == 'timezone-history') {
                ft = init_table(
                    config_modal.settings.timezone.table,
                    config_modal.settings.timezone.columns,
                    config_modal.controllers.url_view,
                    config_modal.settings.timezone.method.view
                );
            }
            if (id_modal == 'payout-history') {
                ft = init_table(
                    config_modal.payout.table,
                    config_modal.payout.columns,
                    config_modal.controllers.url_view,
                    config_modal.payout.method.view
                );
            }
            function RangeVal(start,end,value) {
                this.start = '<input     type="number">';
                this.end = '<input type="number">';
                this.motivation = '<input type="number">';

            }
            function Intervals(range_conf) {
                var ret_intervals = [];
                for (var i=0;i<range_conf.length;i++ ) {
                    ret_intervals.push(range_conf[i].start);
                }
                ret_intervals.push(range_conf[range_conf.length -1 ].end);
                return ret_intervals;
            }
            function addEventTooltip(range) {
                $('.noUi-tooltip').on('click', function(e) {
////                    $($(this).removeClass('noUi-tooltip'));
//                    var changeEl = document.createElement("div");
//                    changeEl.innerHTML="<input type='number'>";
//                    $(this).after(changeEl)
                    var value = prompt('Сколько вам лет?', 100);
                    console.log(range.noUiSlider.get());
                    range.noUiSlider.set(value);

                    console.log($(this), value);

                });
            }
            //change
            if (id_modal == 'motivation-change') {
                $("#create-table-motivation").on('click', function () {

                    console.log(JSON.stringify(range_conf));
                    if (ft_motivation_modal != null) {
                        ft_motivation_modal.destroy();
                    }
                    var temp = JSON.stringify(range_conf);
                    ft_motivation_modal = FooTable.init('#ft-motivation', {
                        "columns": $.get("controllers/configuration/configurationViewAllColumnsMotivationHistory.json", null, null, "json"),
                        "rows": JSON.parse(temp)
                    });
                })
                $("#create-table-motivation").addClass('hide');
//                var range = document.getElementById('range');
                var range_conf = [];
                range_conf.push(new RangeVal(0,1000));
                range_conf.push(new RangeVal(range_conf[0].end,2000));
                $("#create-table-motivation").click();

//                range_conf.push(new RangeVal(range_conf[1].end,2000,0));
//                range_conf.push(new RangeVal(range_conf[2].end,3000,0));
//                $.when(range = addRange(range, Intervals(range_conf),range_conf[range_conf.length - 1].end +10000 * 1.3)).done(addEventTooltip(range));
//                range = addRange(range, Intervals(range_conf),range_conf[range_conf.length - 1].end +10000 * 1.3)
                $('#add-slider').on('click', function () {
//                    range = removeRange(range);
//                    console.log("123", range_conf);
                    range_conf.push(new RangeVal(range_conf[range_conf.length - 1].end,range_conf[range_conf.length - 1].end + 1000,0));
//                    range = addRange(range, Intervals(range_conf), range_conf[range_conf.length - 1].end +10000 * 1.3);
//                    $.when(range = addRange(range, Intervals(range_conf), range_conf[range_conf.length - 1].end +10000 * 1.3)).done(addEventTooltip(range));
                    $("#create-table-motivation").click();
                });
                $('#remove-slider').on('click', function () {
                    if (range_conf.length > 2) {
                        range_conf.pop();
                    }
//                    range = removeRange(range);
//                    range = addRange(range, Intervals(range_conf),range_conf[range_conf.length - 1].end +10000 * 1.3);
                    $("#create-table-motivation").click();
                    console.log(range_conf);
//                    range = removeRange(range);
//                    console.log(range);
                });






            }
            $('div.date').datepicker({
                format: "dd.mm.yyyy",
                todayBtn: "linked",
                clearBtn: true,
                language: "ru"
            });
        });
        function loadFraud(results, file) {
            $.ajax({
                url: "controllers/fraud/fraudStore.php",
                type: 'post',
                dataType: 'json',
                cache: false,
                async: false,
                contentType: 'application/json;charset=utf-8',
                data: JSON.stringify(results.data)
            });
        }
        function loadCorrection(results, file) {
        	var rows = results.data;
        	var rows_copy = [];
        	var len = rows.length;
        	
        	// collect only rows with difference value set, otherwise too much data and the controller may crash
			for (var i = 0; i < len; i++) {
				if (typeof(rows[i]) !== 'undefined') {
					if (rows[i][18] != "") {
						rows_copy.push(rows[i]);
					}
				}
			}
			
            $.ajax({
                url: "controllers/correction/correctionStore.php",
                type: 'post',
                dataType: 'json',
                cache: false,
                async: false,
                contentType: 'application/json;charset=utf-8',
                data: JSON.stringify(rows_copy)
            });
        }
        $('input[name=config_upload_fraud]').change(function() {
            $(this).parse({
                config: {
                    complete: loadFraud
                    // base config to use for each file
                },
                before: function (file, inputElem) {
                    // executed before parsing each file begins;
                    // what you return here controls the flow
                },
                error: function (err, file, inputElem, reason) {
                    // executed if an error occurs while loading the file,
                    // or if before callback aborted for some reason
                },
                complete: function () {
                    // executed after all files are complete
                }
            });
        });
            $('input[name=config_upload_correction]').change(function(){
                var parse = $(this).parse({
                    config: {
                        complete: loadCorrection
                        // base config to use for each file
                    },
                    before: function(file, inputElem)
                    {
                        // executed before parsing each file begins;
                        // what you return here controls the flow
                    },
                    error: function(err, file, inputElem, reason)
                    {
                        // executed if an error occurs while loading the file,
                        // or if before callback aborted for some reason
                    },
                    complete: function()
                    {
                        // executed after all files are complete
                    }
                });
        });


    });



</script>



