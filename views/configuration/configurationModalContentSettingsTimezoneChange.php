<div id="timezone-change"class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Изменение часового пояса</h4>
</div>
<div class="modal-body">
<!--    <form role="form">-->
<!--        <div class="form-group">-->
<!--            <label for="config_timezone_from_moscow">Новый часовой пояс</label>-->
<!---->
<!--        </div>-->
<!--    </form>-->
    <form id="form-data" role="form">
        <div class="form-group">
            <label for="config_timezone_from_moscow_modal">Новый часовой пояс</label>
<!--            <input class="" name="config_timezone_from_moscow" id="config_timezone_from_moscow_modal">-->
            <select name="config_timezone_from_moscow" id="config_timezone_from_moscow">
                <option selected disabled value=''>Выберите часовой пояс</option>
                <option value="-12">-12</option>
                <option value="-11">-11</option>
                <option value="-10">-10</option>
                <option value="-9">-9</option>
                <option value="-8">-8</option>
                <option value="-7">-7</option>
                <option value="-6">-6</option>
                <option value="-5">-5</option>
                <option value="-4">-4</option>
                <option value="-3">-3</option>
                <option value="-2">-2</option>
                <option value="-1">-1</option>
                <option value="0">0</option>
                <option value="1">+1</option>
                <option value="2">+2</option>
                <option value="3">+3</option>
                <option value="4">+4</option>
                <option value="5">+5</option>
                <option value="6">+6</option>
                <option value="7">+7</option>
                <option value="8">+8</option>
                <option value="9">+9</option>
                <option value="10">+10</option>
                <option value="11">+11</option>
                <option value="12">+12</option>

            </select>
        </div>
        <div class="form-group">
            <label for="config_timezone_date_begin_modal">Дата начала действия часового пояса</label>
            <div class="input-group date" data-provide="datepicker">
                <input type="text" name="config_timezone_date_begin" id="config_timezone_date_begin_modal" class="form-control input-sm">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
        </div>
        <hr>
        <div class="form-group required">
            <label for="acceptTerms" class="">Я подтверждаю достоверность введенных мною данных</label>
            <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required">
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Отмена</button>
    <button id="save" type="button" class="btn btn-primary btn-sm">Сохранить изменения</button>
</div>
<script>
    $('.modal-dialog').addClass('modal-sm');
</script>