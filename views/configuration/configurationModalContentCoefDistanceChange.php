<div id="coef-of-distance-travel-change" class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Изменение коэффицента холостого пробега</h4>
</div>
<div class="modal-body">
    <form id="form-data" role="form">
        <div class="form-group">
            <label for="config_coef_of_distance_travel_val_change_modal">Новое значение коэффицента холостого пробега</label>
            <input class="" name="config_coef_of_distance_travel_val" id="config_coef_of_distance_travel_val_modal">
        </div>
        <div class="form-group">
            <label for="config_coef_of_distance_travel_date_begin_modal">Дата начала действия коэффицента</label>
            <div class="input-group date" data-provide="datepicker">
                <input type="text" name="config_coef_of_distance_travel_date_begin" id="config_coef_of_distance_travel_date_begin_modal" class="form-control input-sm">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
        </div>
        <hr>
        <div class="form-group required">
            <label for="acceptTerms" class="">Я подтверждаю достоверность введенных мною данных</label>
            <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required">
        </div>

    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Отмена</button>
    <button id="save" type="button" class="btn btn-primary btn-sm">Сохранить изменения</button>
</div>
<script>
    $('.modal-dialog').addClass('modal-sm');
</script>