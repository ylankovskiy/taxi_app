<div id="timezone-history" class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">История изменений часового пояса</h4>
</div>
<div class="modal-body">
    <table class="table history table-bordered table-responsive table-condensed" data-paging="true" data-sorting="true" data-filtering="true">

    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Закрыть</button>
<!--    <button type="button" class="btn btn-primary btn-sm">Сохранить изменения</button>-->
</div>