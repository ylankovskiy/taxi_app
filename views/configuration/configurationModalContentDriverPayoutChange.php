<div id="payout-change" class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Изменение настроек зарплаты</h4>
</div>
<div class="modal-body">
    <form id="form-data" role="form">
        <div class="form-group">
            <select data-placeholder="Укажите тип водиеля" class="" name="config_driver_payout_driver_type" id="config_driver_payout_driver_type_modal">
                <option selected disabled value="-1">Выбрать тип водителя</option>
                <option value="0">наш аренда</option>
                <option value="1">наш зарплата</option>
                <option value="2">с личным автомобилем</option>
            </select>
        </div>
            <div class="form-group">
                <select data-placeholder="Укажите день недели" class="" name="config_driver_payout_day_of_week">
                    <option selected disabled value="0">Выбрать день недели</option>
                    <option value="1">понедельник</option>
                    <option value="2">вторник</option>
                    <option value="3">среда</option>
                    <option value="4">четверг</option>
                    <option value="5">пятница</option>
                    <option value="6">суббота</option>
                    <option value="7">воскресенье</option>
                </select>
            </div>
            <div class="form-group">
                <select data-placeholder="Укажите время" class="" name="config_driver_payout_time">
                    <option selected disabled value="-1">Выбрать время</option>
                    <option value="00:00:00">00:00</option>
                    <option value="01:00:00">01:00</option>
                    <option value="02:00:00">02:00</option>
                    <option value="03:00:00">03:00</option>
                    <option value="04:00:00">04:00</option>
                    <option value="05:00:00">05:00</option>
                    <option value="06:00:00">06:00</option>
                    <option value="07:00:00">07:00</option>
                    <option value="08:00:00">08:00</option>
                    <option value="09:00:00">09:00</option>
                    <option value="10:00:00">10:00</option>
                    <option value="11:00:00">11:00</option>
                    <option value="12:00:00">12:00</option>
                    <option value="13:00:00">13:00</option>
                    <option value="14:00:00">14:00</option>
                    <option value="15:00:00">15:00</option>
                    <option value="16:00:00">16:00</option>
                    <option value="17:00:00">17:00</option>
                    <option value="18:00:00">18:00</option>
                    <option value="19:00:00">19:00</option>
                    <option value="20:00:00">20:00</option>
                    <option value="21:00:00">21:00</option>
                    <option value="22:00:00">22:00</option>
                    <option value="23:00:00">23:00</option>
                </select>
            </div>
        <div class="form-group">
            <label for="config_driver_payout_date_begin_modal">Дата начала действия зарплаты</label>
            <div class="input-group date" data-provide="datepicker">
                <input type="text" name="config_driver_payout_date_begin" id="config_driver_payout_date_begin_modal" class="form-control input-sm">
                <div class="input-group-addon">
                    <span class="glyphicon glyphicon-th"></span>
                </div>
            </div>
        </div>
        <hr>
        <div class="form-group required">
            <label for="acceptTerms" class="">Я подтверждаю достоверность введенных мною данных</label>
            <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required">
        </div>
    </form>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Отмена</button>
    <button id="save" type="button" class="btn btn-primary btn-sm">Сохранить изменения</button>
</div>
<script>
    $('.modal-dialog').addClass('modal-sm');
</script>