<style>
    .full-height {
        height: 100%;
    }
</style>
<div id="motivation-change" class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h4 class="modal-title" id="myModalLabel">Изменение параметров мотивации</h4>
</div>
<div class="modal-body">

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="form-group">
                        <label for="range">Мотивации</label>
                        <div class="" name="range" id="range"></div>
                    </div>
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-md-2">
                    <div class="btn-group">
                        <button id="create-table-motivation" type="button" class="btn btn-success btn-sm">Мотивации</button>
                    </div>
                </div>
                <div class="col-md-3 col-md-offset-7">
                    <div class="btn-group">
                        <button type="button" id="add-slider" class="btn btn-primary btn-sm">Добавить</button>
                    </div>
                    <div class="btn-group">
                        <button type="button" id="remove-slider" class="btn btn-danger btn-sm">Удалить</button>
                    </div>
                </div>
            </div>
            <br>
        <div class="row">
            <div class="col-md-6">
                <table id="ft-motivation" class="table table-bordered table-responsive table-condensed" data-paging="false" data-sorting="false" data-filtering="false">

                </table>
            </div>
            <div class="col-md-4 col-md-offset-2">
                <form id="form-data" role="form">
                <div class="form-group">
                    <label for="config_motivation_date_begin_modal">Дата начала действия мотивации</label>
                    <div class="input-group date" data-provide="datepicker">
                        <input type="text" name="config_driver_pay_motivation_date_begin" id="config_motivation_date_begin_modal" class="form-control input-sm">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-th"></span>
                        </div>
                    </div>
                </div>
               <div class="form-group">
                   <label for="infelicity">Погрешность</label>
                   <input type="number" name="config_driver_pay_motivation_error" id="infelicity">
               </div>
                    <hr>
                    <div class="form-group required">
                        <label for="acceptTerms" class="">Я подтверждаю достоверность введенных мною данных</label>
                        <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required">
                    </div>
                </form>
            </div>

        </div>
        </div>


</div>

<div class="modal-footer">
    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Отмена</button>
    <button id="save" type="button" class="btn btn-primary btn-sm">Сохранить изменения</button>
</div>
<script>
    $('.modal-dialog').addClass('modal-lg');
</script>