<link href="css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
<link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- FooTable -->
<link href="css/plugins/footable/footable.bootstrap.css" rel="stylesheet">
<style>
    button.footable-add {
    display: none!important;
    }
    button.footable-delete {
    display: none!important;
    }
</style>
<style>
    .select2-close-mask{
    z-index: 2099;
    }
    .select2-dropdown{
    z-index: 3051;
    }
</style>
<!--max height form style-->
<style type="text/css">
    /* Adjust the height of section */
    .profileFormDriver .content {
    min-height: 100px;
    }
    .profileFormDriver .content > .body {
    width: 100%;
    height: auto;
    padding: 15px;
    position: relative;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Новая проводка</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                        <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <h2>
                    Создание новой проводки
                </h2>
                <p>
                    Заполните все указанные поля со звездочкой.
                </p>
                <form id="form" action="#" class="wizard-big profileFormDriver">
<!--                    //добавить класс profileFormTransaction-->
                    <h1>Проводка</h1>
<!---->
<!--                    ransactions_type —> тип, select-->
<!--                    transactions_subtype —> подтип, select-->
<!--                    transactions_sum —> сумма-->
<!--                    transactions_driver_id—> выбор водителя-->
<!--                    transactions_remark —> примечание-->
<!--                    transactions_employee_added_id —> id сотрудника кто добавил, сейчас 0-->
                    <fieldset>
                        <h2>Проводка</h2>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label class="control-label">Тип транзакции</label>
                                    <select data-placeholder="Укажите тип транзакции..." class="form-control required" name="transactions_type" tabindex="2">
                                        <option value="">Выбрать...</option>
                                        <option value="0">приход</option>
                                        <option value="1">расход</option>
<!--                                        из объета-->
                                    </select>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Подтип транзакции</label>
                                    <select data-placeholder="Укажите подтип..." class="form-control required" name="transactions_subtype" tabindex="2">
                                        <option value="">Выбрать...</option>
                                        <option value="0">безнин</option>
                                        <option value="1">коммисия</option>
                                        <option value="2">ежедневное списание</option>
                                        <option value="3">еженедельное списание</option>
                                        <option value="4">ежемесечное списание</option>
                                        <option value="5">ремонт автомобиля</option>
                                        <option value="6">штраф</option>
                                        <option value="7">мобильная связь</option>
                                        <option value="8">поездки</option>
                                        <option value="9">ручная проводка</option>
                                        <option value="10">мойка</option>
                                        <option value="11">другое</option>
                                    </select>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Сумма</label>
                                    <input  name="transactions_sum" type="text" class="form-control required">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label class="control-label">Выбор водителя</label>
                                    <select data-placeholder="Выбирите водителя..." id="form_driver_select" name="transactions_driver_id" tabindex="2">
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Комментарий</label>
<!--                                    <input  name="transactions_remark" type="text" class="form-control required">-->
                                    <textarea class="form-control" rows="3" name="transactions_remark"></textarea>

                                </div>
                            </div>
                    </fieldset>
                    <h1>Подтверждение</h1>
                    <fieldset>
                        <h2>Достоверность ввода данных</h2>
                        <div class="form-group required">
                            <input id="acceptTerms_form" name="acceptTerms" type="checkbox" class="required">
                            <label for="acceptTerms_form" class="control-label">Я подтверждаю достоверность введенных мною данных.</label>
                        </div>
                    </fieldset>
                    <input id="method" class="hidden" name="method" type="text" value="store" />
<!--                    //метод сохранения транзакции-->
                    <input class="hidden" name="transactions_added_employee_id" type="number" value="0" />
<!--                    //исправить где еще на acceptterms-->
                </form>
            </div>
        </div>
    </div>
</div>
<div class="row editor-status hidden">
    <div class="col-lg-12">
        <div class="ibox">
        </div>
    </div>
</div>
<div class="modal fade" id="transaction-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title">
    <style>
        /* provides a red astrix to denote required fields - this should be included in common stylesheet */
        .form-group.required .control-label:after {
            content: "*";
            color: red;
            margin-left: 4px;
        }
    </style>
    <div class="modal-dialog" role="document">
        <form class="modal-content form-horizontal" id="editor" method="POST">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="editor-title">Add Row</h4>
            </div>
            <div class="modal-body">
                <input class="hidden" name="method" type="text" value="store" />
                <input class="hidden" name="transactions_added_employee_id" type="number" value="0" />
                <input class="hidden" name="transactions_id" type="number" value="0" />
                <div class="form-group">
                    <h3>Данный о проводке</h3>
                    <hr>
                </div>
                    <div class="form-group required">
                        <label for="transaction_type" class="col-sm-3 control-label">Тип транзакции</label>
                        <div class="col-sm-9">
                            <select data-placeholder="Выбирите тип транзакции..." id="transaction_type" class="form-control" name="transaction_type" required>
                                <option value="">Выбрать...</option>
                                <option value="0">приход</option>
                                <option value="1">расход</option>
                            </select>
                        </div>
                    </div>
                <div class="form-group required">
                    <label for="transaction_subtype" class="col-sm-3 control-label">Подтип транзакции</label>
                    <div class="col-sm-9">
                        <select data-placeholder="Выбирите подтип транзакции..." id="transaction_subtype" class="form-control" name="transaction_subtype" required>
                            <option value="">Выбрать...</option>
                            <option value="0">безнин</option>
                            <option value="1">коммисия</option>
                            <option value="2">ежедневное списание</option>
                            <option value="3">еженедельное списание</option>
                            <option value="4">ежемесечное списание</option>
                            <option value="5">ремонт автомобиля</option>
                            <option value="6">штраф</option>
                            <option value="7">мобильная связь</option>
                            <option value="8">поездки</option>
                            <option value="9">ручная проводка</option>
                            <option value="10">мойка</option>
                            <option value="11">другое</option>
                        </select>
                    </div>
                </div>

                <div class="form-group required">
                    <label for="transactions_sum" class="col-sm-3 control-label">Сумма</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control " id="transactions_sum" name="transactions_sum" placeholder="Сумма" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="transactions_driver_id" class="col-sm-3 control-label">Выбор водителя</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control " id="transactions_driver_id" name="transactions_driver_id" placeholder="Выбор водителя" required>
                    </div>
                    
                </div>
                <div class="form-group required">
                    <label for="transactions_remark" class="col-sm-3 control-label">Комментарий</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control " id="transactions_remark" name="transactions_remark" placeholder="Комментарий" required>
                    </div>
                </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
</div>
</div>


<div class="row">
	<div class="col-lg-12">
	    <div class="ibox">
	    	<div class="col-lg-6">
                <div class="form-group">
			    	<label class="control-label">Период</label>
				    <div class="input-daterange input-group" id="datepicker">
						<input type="text" class="input-sm form-control" name="date_begin" />
						<span class="input-group-addon">по</span>
						<input type="text" class="input-sm form-control" name="date_end" />
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="row">
    <div class="col-lg-12">
   	    <div class="ibox">
	        <table class="table table-bordered table-responsive table-condensed" data-paging="true" data-sorting="true" data-filtering="true">
    	    </table>
    	</div>
    </div>
</div>
</div>
<!-- FooTable -->
<script src="js/plugins/footable/footable.js"></script>
<!-- Steps -->
<script src="js/plugins/staps/jquery.steps.js"></script>
<!-- Jquery Validate -->
<script src="js/plugins/validate/jquery.validate.min.js"></script>
<script src="js/plugins/validate/localization/messages_ru.min.js"></script>
<script>
var $FORM = null;
var $WIZARD = null;
var date_begin = null;
var date_end = null;
var ft = null;

function init_table() {
	console.log("init");
    ft = FooTable.init('.table', {
        "columns": $.get("controllers/transaction/transactionViewAllColumns.json", null, null, "json"),
        "rows": $.ajax({
            url: "controllers/transaction/transactionView.php",
            type: 'post',
            dataType: 'json',
            cache: false,
            async: false,
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify({
                method: 'view',
                date_begin: date_begin,
                date_end: date_end
            })
        })
    });
    
    return ft;
}

$(document).ready(function () {
	$('.input-daterange').datepicker({
	    format: "dd.mm.yyyy",
    	todayBtn: "linked",
	    clearBtn: true,
    	language: "ru"
	}).on('hide', function(e) {
    	date_begin = $('[name=date_begin]').val();
	    date_end = $('[name=date_end]').val();
        if (ft != null) {
            ft.destroy();
            $(".footable-empty").remove();
        }
	    if (typeof(date_begin) !== 'undefined' && date_begin && typeof(date_end) !== 'undefined' && date_end) {
	        ft = init_table();
	    }
    });
    
    $FORM = $("#form");
    $("a.collapse-link").click();
    $WIZARD = $("#wizard").steps();
    $FORM.steps({
        bodyTag: "fieldset",
        onStepChanging: function (event, currentIndex, newIndex) {
            console.log("changing : " + currentIndex + ' ' + newIndex);
            // Always allow going backward even if the current step contains invalid fields!
            if (currentIndex > newIndex) {
                return true;
            }

            var form = $(this);

            // Clean up if user went backward before
            if (currentIndex < newIndex) {
                // To remove error styles
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
            }

            // Disable validation on fields that are disabled or hidden.
            form.validate().settings.ignore = ":disabled,:hidden";

            // Start validation; Prevent going forward if false
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            console.log("changed : " + currentIndex + ' ' + priorIndex);

            // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3) {
                $(this).steps("previous");
            }
        },
        onFinishing: function (event, currentIndex) {
            var form = $(this);

            // Disable validation on fields that are disabled.
            // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
            form.validate().settings.ignore = ":disabled,:hidden";

            // Start validation; Prevent form submission if false
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            var form = $(this);
            var jsonForm = JSON.stringify(form.serializeObject());
            console.log(jsonForm);
            $.ajax({
                url: 'controllers/transaction/transactionStore.php',
                type: 'post',
                dataType: 'json',
                cache: false,
                async: false,
                contentType: 'application/json;charset=utf-8',
                success: function (data) {
                    console.log(data);
                    //location.reload();
                    alertShowHide(data['ERROR_CODE'], data['ERROR_MESSAGE']);
                    if (ft != null) {
                        ft.destroy();
                        $(".footable-empty").remove();
                    }
                   
                   if (typeof(date_begin) !== 'undefined' && date_begin && typeof(date_end) !== 'undefined' && date_end) {
                   	ft = init_table();
                   }
					$('.collapse-link').click();
                   $('#form').trigger( 'reset' );
                },
                data: jsonForm
            });

//             Submit form input
//            form.submit();
        },
        labels: {
            cancel: "Отмена",
            current: "Текущий шаг:",
            pagination: "Нумерация",
            finish: "Завершить",
            next: "Вперед",
            previous: "Назад",
            loading: "Загрузка ..."
        }
    }).validate({
        errorPlacement: function (error, element) {
            element.before(error);
        }
    });
    date_begin = $('[name=date_begin]').val();
    date_end = $('[name=date_end]').val();

    if (typeof(date_begin) !== 'undefined' && date_begin && typeof(date_end) !== 'undefined' && date_end) {
    console.log('call');
       ft = init_table();
    }
    
    $.ajax({
        url: 'utils/select/selectDriver.php',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=utf-8',
        cache: false,
        async: false,
        data: JSON.stringify({
            method_status: 0
        }),
        success: function (data) {
            var data_select = [];
            $.each(data, function (key, value) {
				data_select.push({
					"id": value.driver_id,
					"text": value.driver_name_last + ' ' + value.driver_name_first + ' ' + value.driver_name_name
                });
            });
            console.log(data_select);
            $("#form_driver_select", $FORM).select2({
                data: data_select
            });
        }
    });
});</script>

