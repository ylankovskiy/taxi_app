<link href="css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
<link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- In <head> -->
<link href="css/nouislider.min.css" rel="stylesheet">

<!-- In <body> -->

<!-- FooTable -->
<link href="css/plugins/footable/footable.bootstrap.css" rel="stylesheet">
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="row">
                <div class="col-lg-2">
                    <div class="form-group required" id="">
                        <label class="control-label">Ведомости на дату:</label>
                        <div id="pay-date"  class="input-group date">

                            <input type="text" class="form-control required" value="">
                            <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </span>
                        </div>
                    </div>
                </div>


            </div>
            <div class="row">
                <table id="pay-table"
                    data-toggle="table"
                    class="display table table-bordered table-responsive table-condensed"
                    data-pagination="true"
                    data-search="true">
                </table>
            </div>

        </div>
    </div>
</div>

<!--pay-modal-->
<div class="modal fade" id="pay-modal-print" tabindex="-1" role="dialog" aria-labelledby="editor-title">
    <div class="modal-dialog" role="document">
        <form class="modal-content form-horizontal" id="editor">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="editor-title">Печать доумента</h4>
            </div>
            <div class="modal-body">
                <h3>Предпросмотре печати</h3>
                <div class="row">
                <h2>Header</h2>
                    <table id="pay-table-header"
                           data-toggle="table"
                           class="display table table-bordered table-responsive table-condensed"
                           data-pagination="true"
                           data-search="true">
                    </table>
                </div>

                <div class="row">
                    <h2>Content</h2>
                    <table id="pay-table-content"
                           data-toggle="table"
                           class="display table table-bordered table-responsive table-condensed"
                           data-pagination="true"
                           data-search="true">
                    </table>
                </div>

                <div class="row">
                    <h2>Transaction</h2>
                    <table id="pay-table-transaction"
                           data-toggle="table"
                           class="display table table-bordered table-responsive table-condensed"
                           data-pagination="true"
                           data-search="true">
                    </table>
                </div>



            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Печать</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
            </div>
        </form>
    </div>
</div>

<div class="modal fade" id="pay-modal-edit-status" tabindex="-1" role="dialog" aria-labelledby="editor-title">
    <div class="modal-dialog" role="document">
        <form class="modal-content form-horizontal" id="form-data">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="editor-title">Изменение статуса ведомости</h4>
            </div>
            <div class="modal-body">
<!--                <h3>Изменить статус ведоомости</h3>-->
<!--                <form id="form-data" role="form">-->
                    <div class="form-group">
                        <label for="driver_payout_report_status_status">Новый статусс</label>
                        <select name="driver_payout_report_status_status" id="driver_payout_report_status_status">
                            <option selected disabled value=''>Выберите статсу</option>
                            <option value="0">сформирован</option>
                            <option value="1">проведен</option>
                            <option value="2">отклонен</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="driver_payout_report_status_remark">Примечание:</label>

                            <textarea class="form-control" rows="3" id="driver_payout_report_status_remark" name="driver_payout_report_status_remark"></textarea>

                    </div>
                    <div class="form-group required">
                        <label for="acceptTerms" class="">Я подтверждаю достоверность введенных мною данных</label>
                        <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required">
                    </div>
<!--                    <div class="form-group">-->
<!--                        <label for="pay-report-sum">Сумма</label>-->
<!--                        <input type="number" id="pay-report-sum" name="pay-report-sum">-->
<!---->
<!--                    </div>-->
            </div>

            <div class="modal-footer">
                <button id="save" type="button" class="btn btn-primary">Сохранить</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Отменить</button>
            </div>
        </form>
    </div>
</div>

</div>
<!-- Sparkline -->
<script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- FooTable -->
<script src="js/plugins/footable/footable.js"></script>
<!-- Jquery Validate -->
<script src="js/plugins/validate/jquery.validate.min.js"></script>
<script src="js/plugins/validate/localization/messages_ru.min.js"></script>
<!-- Steps -->
<script src="js/plugins/staps/jquery.steps.js"></script>
<!-- Chosen -->
<script src="js/plugins/chosen/chosen.jquery.js"></script>

<script src="js/nouislider.min.js"></script>
<script src="js/wNumb.js"></script>

<!--PDFMake-->
<script src='js/plugins/pdfMake/pdfmake.min.js'></script>
<script src='js/plugins/pdfMake/vfs_fonts.js'></script>
<script>

    $(document).ready(function () {
        $('.input-group.date').datepicker({
            format: "dd.mm.yyyy",
            startDate: "-50d",
            todayBtn: "linked",
            clearBtn: true,
            language: "ru"
        });
        var ft = null;
        var $modal_print = $('#pay-modal-print'),
            $modal_edit_status = $('#pay-modal-edit-status');
        $form_data = $('#form-data', $modal_edit_status);
        $('#pay-date').datepicker()
            .on("changeDate", function(e) {
                if (ft != null) {
                    ft.destroy();
                    $(".footable-empty").remove();
                }
                console.log(e);
                var t = new Date(e.date);
                console.log( t );
                var day = t.getDate();
                var monthIndex = t.getMonth() + 1;
                var year = t.getFullYear();
                var date_input = day + '.' + monthIndex + '.' + year;
                console.log(ft);
                ft = FooTable.init('#pay-table', {
                    "columns": $.get("controllers/pay/payViewAllColumns.json", null, null, "json"),
                    "rows": $.ajax({
                        url: "controllers/pay/payView.php",
                        type: 'post',
                        dataType: 'json',
                        cache: false,
                        async: false,
                        contentType: 'application/json;charset=utf-8',
                        data: JSON.stringify({
                            method: 'view',
                            date: date_input

                        })
                    }),
                    editing: {
                        deleteText: '<span class="fa fa-print" aria-hidden="true"></span>',
                        enabled: true,
                        allowAdd: false,
                        allowEdit: true,
                        allowDelete: true,
                        allowView: false,
                        alwaysShow: true,

                        deleteRow: function (row) {
                            console.log('row', row.val());
                            $.ajax({
                                dataType: "json",
                                url: row.val().driver_payout_report_url,
                                success: function (data) {
                                    console.log(data);
                                    var docDefinition = data;
                                    pdfMake.createPdf(docDefinition).download('optionalName.pdf');
                                }
                            });

                        },

                        editRow: function (row) {
                            $modal_edit_status.data('row', row.val());
                            $modal_edit_status.modal('show'); // display the modal


                        }
                    },

                });

            });
//        $('[name=pay-date]').off('change');
//        $('[name=pay-date]').on('change', function () {
//            console.log($(this).val());
////            var date = $(this).val();


//        });

        $('#pay-modal-edit-status').on('shown.bs.modal', function () {
            console.log($modal_edit_status.data('row'));
//            var
            $("#save").on('click', function () {
//                console.log();
                var data = $form_data.serializeObject();
                data.method = 'store';
                data.driver_payout_report_status_report_id = $modal_edit_status.data('row').driver_payout_report_id;
            console.log(data);
//                + "&driver_payout_report_id=" + $modal_edit_status.data('row').driver_payout_report_id
                $.ajax({
                    url: "controllers/pay/payStore.php",
                    type: 'post',
                    dataType: 'json',
                    cache: false,
                    async: false,
                    contentType: 'application/json;charset=utf-8',
                    data: JSON.stringify(data),
                    success: function (data) {
                        alertShowHide(data['ERROR_CODE'], data['ERROR_MESSAGE']);

                    }

                });
                $('#pay-modal-edit-status').modal('hide');

            });
        });
//            $('.modal-dialog').addClass('modal-lg');
//        $editor_status.on('submit', function (e) {
//        });

//        $('#pay-modal-print').on('shown.bs.modal', function () {
//            $('.modal-dialog').addClass('modal-lg');
//            var ft_table_modal_header = null;
//            var ft_table_modal_content = null;
//            var ft_table_modal_transaction= null
//
//            var column_head = null,
//                column_content = null,
//                column_transaction = null;
//            var ret_arr = null;
//            function getColumn(data) {
//                var column = [];
//                for (var key in data) {
//                    var t = {
//                        name: key,
//                        title: data[key]
//                    };
//                    column.push(new Object(t));
//                }
//                return column;
//            }
//            function ititTableModal (ft_table, table_id ,columns, rows) {
//                ft_table = FooTable.init(table_id, {
//                    "columns": columns,
//                    "rows": rows
//                })
//            }
//            $.ajax({
//                dataType: "json",
//                url: "payReportDocuments/2017/05/1_2007-05-13_test1.json",
//                success: function (data) {
//                    console.log(data);
//                    var docDefinition = data;
//                    pdfMake.createPdf(docDefinition).download('optionalName.pdf');
//
////                    column_head = getColumn(data.DOCUMENT_HEADER_TITLE);
////                    column_content = getColumn(data.DOCUMENT_ROWS_ORDER_TITLE);
////                    column_transaction = getColumn(data.DOCUMENT_ROWS_TRANSACTION_TITLE);
////
////                    ititTableModal(ft_table_modal_header, '#pay-table-header',column_head,[data.DOCUMENT_HEADER]);
////                    ititTableModal(ft_table_modal_content, '#pay-table-content',column_content,[data.DOCUMENT_ROWS_ORDER]);
////                    ititTableModal(ft_table_modal_transaction, '#pay-table-transaction',column_transaction,data.DOCUMENT_ROWS_TRANSACTION);
////                    function genRowsColumns(title, data) {
////                        var rows = [];
////                        var columns = [];
////                        var ret_arr = {};
////
////                        for (var key1 in title) {
////                            columns.push(title[key1]);
////                            $.each(data,function (key,value) {
////                                if (rows[key] == undefined){
//////                                    var t = {
//////                                         []
//////                                    };
////                                    rows.push([]);
////                                }
////                                for (var key2 in value) {
////                                    if (key2 == key1) {
////                                        rows[key].push(value[key1]);
////                                    }
////                                }
////                            });
////                        }
////
////                        ret_arr.rows = rows;
////                        ret_arr.columns = columns;
////                        return ret_arr;
////                    }
////                    function genRowsColumnsV2(title,data) {
////                        var rows = [];
////                        var columns = [];
////                        var ret_arr = {};
////                        for (var key in title) {
////                            var t = {
////                                title: title[key],
////                                dataKey: key
////                            };
////                            columns.push(new Object(t));
////                        }
////
////                        $.each(data,function (key1,value) {
////
//////                                if (typeof(key1) == 'string') {
//////                                    rows.push(key1 + ":"  + value);
//////                                } else {
////                                    if (rows[key1] == undefined){
////                                        rows.push(new Object());
////                                    }
////
////                                    for (var key2 in value) {
////                                        var r = rows[key1];
////                                        r[key2] = value[key2];
////
////                                    }
////
////
////
////
////                            });
////                        console.log('123',columns, rows);
////
////                        ret_arr.rows = rows;
////                        ret_arr.columns = columns;
////                        return ret_arr;
////                    }
//
////
//                }
//            });
//        });


    });

</script>

