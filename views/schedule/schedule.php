<!-- FooTable -->
<?php require_once('utils/helpers/genWeekDate.php'); ?>
<link href="css/plugins/footable/footable.bootstrap.css" rel="stylesheet">
<link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<style>
    button.footable-add {
    display: none!important;
    }
    button.footable-delete {
    display: none!important;
    }
    .padding-2 {
    padding: 2px;
    }
    .padding-5 {
    padding: 5px;
    }
</style>
<div class="row hidden">
    <div class="col-lg-12">
        <div class="ibox">
        </div>
    </div>
</div>
<div class="row date_range">
    <div class="col-lg-12">
        <select class="selectpicker" name="weeks">
        <?php echo gen_weeks(strtotime("-8 week"),strtotime("+1 week")) ; ?>
        </select>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <table class="table table-bordered table-responsive table-condensed" data-paging="true">
            </table>
        </div>
    </div>
</div>
</div>
<div class="modal fade" id="shift-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title">
    <style scoped>
        /* provides a red astrix to denote required fields - this should be included in common stylesheet */
        .form-group.required .control-label:after {
        content: "*";
        color: red;
        margin-left: 4px;
        }
    </style>
    <div class="modal-dialog" role="document">
        <div class="modal-content form-horizontal" id="editor">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="editor-title">Add Row</h4>
            </div>
            <div class="modal-body">
                <ul class="nav nav-tabs" id="tabContent">
                    <li name="tab-pick-driver"><a href="#pick-driver" data-toggle="tab">Выбрать водителя</a></li>
                    <li name="tab-on-shift"><a href="#on-shift" data-toggle="tab">Поставить на линию</a></li>
                    <li name="tab-off-shift-vehicle"><a href="#off-shift-vehicle" data-toggle="tab">Прием автомобиля</a></li>
                    <li name="tab-off-shift-money"><a href="#off-shift-money" data-toggle="tab">Прием денежных средств</a></li>
                </ul>
                <br/>
                <!--          tab Select Driver-->
                <div class="tab-content">
                    <div class="tab-pane" id="pick-driver">
                        <form class="modal-content form-horizontal padding-5" id="form_pick_driver" method="POST">
                            <input type="number"  id="schedule_status" name="schedule_status" class="hidden" value="1"/>
                            <input type="text"  id="schedule_date" name="schedule_date" class="hidden" value=""/>
                            <input type="text"  id="method" name="method" class="hidden" value="store"/>
                            <input type="number" id="schedule_vehicle_id" name="schedule_vehicle_id" class="hidden" />
                            <input type="number" id="schedule_id" name="schedule_id" class="hidden" />
                            <input type="number" id="schedule_employee_added_id" name="schedule_employee_added_id" class="hidden" value="0" />
                            <div class="control-group">
                                <div class="form-group required">
                                    <label class="col-sm-3 control-label" for="schedule_driver_id">Водитель</label>
                                    <div class="col-sm-9">
                                        <select class="form-control driver_select" id="schedule_driver_id" name="schedule_driver_id" required></select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="schedule_remark">Примечание:</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="3" id="schedule_remark" name="schedule_remark"></textarea>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label col-sm-3" for="acceptTerms">Подтверждение</label>
                                    <div class="col-sm-9">
                                        <input id="acceptTerms" name="acceptTerms" type="checkbox" required>&nbsp;Я подтверждаю достоверность введенных мною данных.
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                            </div>
                        </form>
                    </div>
                    <!--            end tab select driver-->
                    <!--            tab set line-->
                    <div class="tab-pane" id="on-shift">
                        <form class="modal-content form-horizontal padding-5" id="form_on_shift" method="POST" >
                            <input type="number"  id="schedule_status" name="schedule_status" class="hidden" value="3"/>
                            <input type="text"  id="schedule_date" name="schedule_date" class="hidden" value=""/>
                            <input type="text"  id="method" name="method" class="hidden" value="store"/>
                            <input type="number" id="schedule_vehicle_id" name="schedule_vehicle_id" class="hidden" />
                            <input type="number" id="schedule_driver_id" name="schedule_driver_id" class="hidden" />
                            <input type="number" id="schedule_id" name="schedule_id" class="hidden" />
                            <input type="number" id="schedule_employee_added_id" name="schedule_employee_added_id" class="hidden" value="0" />
                            <input type="number" id="driver_type" name="driver_type" class="hidden" />
                            <div class="control-group">
                                <div class="form-group required">
                                    <label class="col-sm-3 control-label" for="vehicle_gasoline_littres">Текущий остаток бензина, л</label>
                                    <div class="col-sm-9">
                                        <input type="number" step="0.01" min="0" max="100" class="form-control" id="vehicle_gasoline_littres" name="vehicle_gasoline_littres" placeholder="Текущий остаток бензина, л" required>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-3 control-label" for="vehicle_maintenance_km">Текущий пробег автомобиля</label>
                                    <div class="col-sm-9">
                                        <input type="number" min="0" class="form-control" id="vehicle_maintenance_km" name="vehicle_maintenance_km" placeholder="Текущий пробег автомобиля" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="schedule_remark">Примечание:</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="3" id="schedule_remark" name="schedule_remark"></textarea>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label col-sm-3" for="acceptTerms">Подтверждение</label>
                                    <div class="col-sm-9">
                                        <input id="acceptTerms" name="acceptTerms" type="checkbox" required>&nbsp;Я подтверждаю достоверность введенных мною данных.
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                            </div>
                        </form>
                    </div>
                    <!--            end tab set line-->
                    <!--            tab acceptence vehicle-->
                    <div class="tab-pane" id="off-shift-vehicle">
                        <form class="modal-content form-horizontal padding-5" id="form_off_shift_vehicle" method="POST">
                            <input type="number"  id="schedule_status" name="schedule_status" class="hidden" value="4"/>
                            <input type="text"  id="schedule_date" name="schedule_date" class="hidden" value=""/>
                            <input type="text"  id="method" name="method" class="hidden" value="store"/>
                            <input type="number" id="schedule_vehicle_id" name="schedule_vehicle_id" class="hidden" />
                            <input type="number" id="schedule_driver_id" name="schedule_driver_id" class="hidden" />
                            <input type="number" id="schedule_id" name="schedule_id" class="hidden" />
                            <input type="number" id="schedule_employee_added_id" name="schedule_employee_added_id" class="hidden" value="0" />
                            <input type="number" id="driver_type" name="driver_type" class="hidden" />
                            <div class="control-group">
                                <div class="form-group required">
                                    <label class="col-sm-3 control-label" for="vehicle_gasoline_littres">Текущий остаток бензина, л</label>
                                    <div class="col-sm-9">
                                        <input type="number" step="0.01" min="0" max="100" class="form-control" id="vehicle_gasoline_littres" name="vehicle_gasoline_littres" placeholder="Текущий остаток бензина, л" required>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-3 control-label" for="vehicle_maintenance_km">Текущий пробег автомобиля</label>
                                    <div class="col-sm-9">
                                        <input type="number" min="0" class="form-control" id="vehicle_maintenance_km" name="vehicle_maintenance_km" placeholder="Текущий пробег автомобиля" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="travel_plan_km">Плановый пробег автомобиля</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="travel_plan_km" name="travel_plan_km" placeholder="Плановй пробег автомобиля" disabled>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-3 control-label" for="schedule_transactions_sum">Оплата за ГСМ</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="schedule_transactions_sum" name="schedule_transactions_sum" placeholder="Оплата за ГСМ" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="gasoline_plan_payment">Плановая оплата ГСМ</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="gasoline_plan_payment" name="gasoline_plan_payment" placeholder="Плановая оплата ГСМ" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="schedule_remark">Примечание:</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="3" id="schedule_remark" name="schedule_remark"></textarea>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label col-sm-3" for="acceptTerms">Подтверждение</label>
                                    <div class="col-sm-9">
                                        <input id="acceptTerms" name="acceptTerms" type="checkbox" required>&nbsp;Я подтверждаю достоверность введенных мною данных.
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <button type="submit" id= "btn-save" name="btn-save" class="btn btn-primary">Сохранить изменения</button>
                            </div>
                        </form>
                    </div>
                    <!--            end tab accpetence vehicle-->
                    <!--            tab acceptence money-->
                    <div class="tab-pane" id="off-shift-money">
                        <form class="modal-content form-horizontal padding-5" id="form_off_shift_money" method="POST">
                            <input type="number"  id="schedule_status" name="schedule_status" class="hidden" value="8"/>
                            <input type="text"  id="schedule_date" name="schedule_date" class="hidden" value=""/>
                            <input type="text"  id="method" name="method" class="hidden" value="store"/>
                            <input type="number" id="schedule_vehicle_id" name="schedule_vehicle_id" class="hidden" />
                            <input type="number" id="schedule_driver_id" name="schedule_driver_id" class="hidden" />
                            <input type="number" id="schedule_id" name="schedule_id" class="hidden" />
                            <input type="number" id="schedule_employee_added_id" name="schedule_employee_added_id" class="hidden" value="0" />
                            <input type="number" id="driver_type" name="driver_type" class="hidden" />
                            <div class="control-group">
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="orders_count">Количество заказов Gett</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="orders_count" name="orders_count" placeholder="Количество заказов Gett" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="orders_sum">Сумма по заказам Gett</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="orders_sum" name="orders_sum" placeholder="Сумма по заказам Gett" disabled>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="collect_cash">Забрать сумму наличными</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="collect_cash" name="collect_cash" placeholder="Забрать сумму наличными" disabled>
                                        <input type="number" class="form-control hidden" id="collect_cash_hidden" name="collect_cash_hidden" placeholder="Забрать сумму наличными">
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="col-sm-3 control-label" for="accepted_cash">Принял сумму наличными</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="accepted_cash" name="accepted_cash" placeholder="Сдал сумму наличными" min="0" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="car_wash">Мойка:</label>
                                    <div class="col-sm-9">
                                        <input id="car_wash" name="vehicle_wash_transaction" type="checkbox">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="schedule_remark">Примечание:</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="3" id="schedule_remark" name="schedule_remark"></textarea>
                                    </div>
                                </div>
<!--                                <div class="form-group col-sm-12">-->
<!--                                    <input id="transaction" name="manual_transaction" type="checkbox">-->
<!--                                    <label class="control-label" for="manual_transaction">Ручная проводка</label>-->
<!--                                </div>-->

                                <div class="form-group">
                                    <label class="col-sm-3 control-label" for="transaction">Ручная проводка:</label>
                                    <div class="col-sm-9">
                                        <input id="transaction" name="manual_transaction" type="checkbox">
                                    </div>
                                </div>

                                <div class="form-group col-sm-12" name="div_transaction_type">
                                    <label class="col-sm-3 control-label" for="manual_transaction_type">Тип транзакции</label>
                                    <div class="col-sm-9">
                                        <select data-placeholder="Выберите тип транзакции..." class="form-control" id="manual_transaction_type" name="manual_transaction_type">
                                            <option value="-1" selected="selected" disabled>Выберите тип транзакции</option>
                                            <option value="0">Приход</option>
                                            <option value="1">Расход</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group col-sm-12" name="div_transaction_sum">
                                    <label class="col-sm-3 control-label" for="manual_transaction_sum">Сумма</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="manual_transaction_sum" name="manual_transaction_sum" placeholder="Сумма" >
                                    </div>
                                </div>
                                <div class="form-group col-sm-12" name="div_transaction_remark">
                                    <label class="col-sm-3 control-label" for="manual_transaction_remark">Примечание:</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" rows="3" id="manual_transaction_remark" name="manual_transaction_remark"></textarea>
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label col-sm-3" for="acceptTerms">Подтверждение</label>
                                    <div class="col-sm-9">
                                        <input id="acceptTerms" name="acceptTerms" type="checkbox" required>&nbsp;Я подтверждаю достоверность введенных мною данных.
                                    </div>
                                </div>
                            </div>
                            <div class="control-group">
                                <button type="submit" id="btn-save" name="btn-save" class="btn btn-primary">Сохранить изменения</button>
                            </div>
                        </form>
                    </div>
                    <!--            end tab accepnence money-->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>
<!-- Jquery Validate -->
<script src="js/plugins/validate/jquery.validate.min.js"></script>
<script src="js/plugins/validate/localization/messages_ru.min.js"></script>
<!-- FooTable -->
<script src="js/plugins/footable/footable.js"></script>
<script>
var $driverModal = $('#shift-modal');
// the below initializes FooTable and returns the created instance for later use

var ft = null;
var driver_id = null;
var status = null;
var type = null;
var vehicle_id = null;
var schedule_id = null;
var plan_km = null;
var plan_gasoline_cost = null;
var orders_count = null;
var orders_sum = null;
var shift = null;

var $editor = null;
var $editorTitle = null;

Object.freeze(
    SCHEDULE_STATUS_MASK = {
        ASSIGNED_EMPTY: 0,
        ASSIGNED_DRIVER: 1,
        ASSIGNED_ON_SHIFT: 2,
        ASSIGNED_OFF_SHIFT_VEHICLE: 4,
        ASSIGNED_OFF_SHIFT_MONEY: 8
    }
);

function initFooTable() {
    if ($dateRangeSelect == null)
        return;

    var date_begin = $('option:selected', $dateRangeSelect).attr('data-date-beg');
    var date_end = $('option:selected', $dateRangeSelect).attr('data-date-end');

    ft = FooTable.init('.table', {
        "rows": $.ajax({
            url: "controllers/schedule/scheduleView.php",
            type: 'post',
            dataType: 'json',
            cache: false,
            async: false,
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify({
                type: 'rows',
                date_begin: date_begin,
                date_end: date_end
            }),
        }),
        "columns": $.ajax({
            url: "controllers/schedule/scheduleView.php",
            type: 'post',
            dataType: 'json',
            cache: false,
            async: false,
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify({
                type: 'columns',
                date_begin: date_begin,
                date_end: date_end
            }),
        })


    }, init_table_edit);
}

function TabStatus(tab0Show, tab1Show, tab2Show, tab3Show, tab0Disabled, tab1Disabled, tab2Disabled, tab3Disabled) {
    $("#select2-schedule_driver_id-container").hide();
    this._tabDriverShow = tab0Show,
        this._tabOnShiftShow = tab1Show,
        this._tabOffShiftVehicileShow = tab2Show,
        this._tabOffShiftMoneyShow = tab3Show,
        this._tabDriverDisabled = tab0Disabled,
        this._tabOnShiftDisabled = tab1Disabled,
        this._tabOffShiftVehicileDisabled = tab2Disabled,
        this._tabOffShiftMoneyDisabled = tab3Disabled,

        this.showTab = function ($driverModal) {
            this._tabDriverShow ? $('#tabContent a[href="#pick-driver"]', $driverModal).show() :
                $('#tabContent a[href="#pick-driver"]', $driverModal).hide();
            this._tabDriverShow ? $('#tabContent li:eq(0) a', $driverModal).tab('show') : null;

            this._tabOnShiftShow ? $('#tabContent a[href="#on-shift"]', $driverModal).show() :
                $('#tabContent a[href="#on-shift"]', $driverModal).hide();
            this._tabOnShiftShow ? $('#tabContent li:eq(1) a', $driverModal).tab('show') : null;

            this._tabOffShiftVehicileShow ? $('#tabContent a[href="#off-shift-vehicle"]', $driverModal).show() :
                $('#tabContent a[href="off-shift-vehicle"]', $driverModal).hide();
            this._tabOffShiftVehicileShow ? $('#tabContent li:eq(2) a', $driverModal).tab('show') : null;

            this._tabOffShiftMoneyShow ? $('#tabContent a[href="#off-shift-money"]', $driverModal).show() :
                $('#tabContent a[href="#off-shift-money"]', $driverModal).hide();
            this._tabOffShiftMoneyShow ? $('#tabContent li:eq(3) a', $driverModal).tab('show') : null;
        };
    this.disableTab = function (tabView, $driverModal) {
        console.log(tabView);
        console.log(this._tabDriverShow, this._tabOnShiftShow, this._tabOffShiftVehicileShow, this._tabOffShiftMoneyShow, this._tabDriverDisabled, this._tabOnShiftDisabled, this._tabOffShiftVehicileDisabled, this._tabOffShiftMoneyDisabled);
        if (tabView == '#pick-driver') {

            if (this._tabDriverDisabled) {
                $(tabView, $driverModal).find('input').prop('disabled', true);
                $(tabView, $driverModal).find('select').prop('disabled', true);
                $(tabView, $driverModal).find('textarea').prop('disabled', true);
                $(tabView, $driverModal).find('button').hide();
            } else {
                $(tabView, $driverModal).find('input').prop('disabled', false);
                $(tabView, $driverModal).find('select').prop('disabled', false);
                $(tabView, $driverModal).find('textarea').prop('disabled', false);
                $(tabView, $driverModal).find('button').show();
            }
        } else if (tabView == '#on-shift') {
            if (this._tabOnShiftDisabled) {
                console.log('disable on shift');
                $(tabView, $driverModal).find('input').prop('disabled', true);
                $(tabView, $driverModal).find('textarea').prop('disabled', true);
                $(tabView, $driverModal).find('button').hide();
            } else {
                console.log('enable on shift');
                $(tabView, $driverModal).find('input').prop('disabled', false);
                $(tabView, $driverModal).find('textarea').prop('disabled', false);
                $(tabView, $driverModal).find('button').show();
            }
        } else if (tabView == '#off-shift-vehicle') {
            if (this._tabOffShiftVehicileDisabled) {
                console.log('vehicle ' + tabView);
                $(tabView, $driverModal).find('input').prop('disabled', true);
                $(tabView, $driverModal).find('textarea').prop('disabled', true);
                $(tabView, $driverModal).find('button').hide();
            } else {
                $(tabView, $driverModal).find('input').prop('disabled', false);
                $(tabView, $driverModal).find('textarea').prop('disabled', false);
                $(tabView, $driverModal).find('[name = travel_plan_km]').prop('disabled', true);
                $(tabView, $driverModal).find('[name = gasoline_plan_payment]').prop('disabled', true);
                $(tabView, $driverModal).find('button').show();
            }
        } else if (tabView == '#off-shift-money') {
            if (this._tabOffShiftMoneyDisabled) {
                console.log('money ' + tabView);
                $(tabView, $driverModal).find('input').prop('disabled', true);
                $(tabView, $driverModal).find('textarea').prop('disabled', true);
                $(tabView, $driverModal).find('select').prop('disabled', true);
                $(tabView, $driverModal).find('button').hide();
            } else {
                $(tabView, $driverModal).find('input').prop('disabled', false);
                $(tabView, $driverModal).find('textarea').prop('disabled', false);
                $(tabView, $driverModal).find('select').prop('disabled', false);
                $(tabView, $driverModal).find('[name = orders_count]').prop('disabled', true);
                $(tabView, $driverModal).find('[name = orders_sum]').prop('disabled', true);
                $(tabView, $driverModal).find('[name = collect_cash]').prop('disabled', true);
                $(tabView, $driverModal).find('button').show();
            }
        }
    };
}

function checkStatus(status, type) {
    var tabStat = null;
    if (type == 0) {
        //rental company vehicle
        if (status & 0x4) {
            tabStat = new TabStatus(true, true, true, false, true, true, true, true);
            return tabStat
        } else if (status & 0x2) {
            tabStat = new TabStatus(true, true, true, false, true, true, false, false);
            return tabStat
        } else if (status & 0x1) {
            tabStat = new TabStatus(true, true, false, false, false, false, false, false);
            return tabStat
        } else if (status == 0) {
            tabStat = new TabStatus(true, false, false, false, false, false, false, false);
            return tabStat
        }
    } else {
        //fixed pay company vehicle
        if ((status & 0xC) == 0xC) { // shift complete
            tabStat = new TabStatus(true, true, true, true, true, true, true, true);
            return tabStat
        } else if (status & 0x8) {
            tabStat = new TabStatus(true, true, true, true, true, true, false, true);
            return tabStat
        } else if (status & 0x4) {
            tabStat = new TabStatus(true, true, true, true, true, true, true, false);
            return tabStat
        } else if (status & 0x2) {
            tabStat = new TabStatus(true, true, true, true, true, true, false, false);
            return tabStat
        } else if (status & 0x1) {
            tabStat = new TabStatus(true, true, false, false, false, false, false, false);
            return tabStat
        } else if (status == 0) {
            tabStat = new TabStatus(true, false, false, false, false, false, false, false);
            return tabStat
        }
    }
}

function init_table_edit() {
    $('.driver-assign').on('click', function () {
        $.ajax({
            url: 'utils/select/selectDriver.php',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json;charset=utf-8',
            async: false,
            cache: false,
            data: JSON.stringify({
                method_status: 0,
                method_type: -2
            }),
            success: function (data) {
                var data_select = [];
                $.each(data, function (key, value) {
                    data_select.push({
                        "id": value.driver_id,
                        "text": value.driver_name_last + ' ' + value.driver_name_first + ' ' + value.driver_name_name
                    });
                });
                $(".driver_select").select2({
                    data: data_select
                });
            }
        });
        //        console.log(this);
        driver_id = $(this).attr('data-driver');
        status = $(this).attr('data-status');
        type = $(this).attr('data-type');
        vehicle_id = $(this).attr('data-vehicle');
        schedule_id = $(this).attr('data-schedule');
        shift = $(this).attr('data-shift');
        date = $(this).attr('data-date');
        
        $driverModalForm = $('form', $driverModal);
        $editorTitle = $('#editor-title', $driverModal);

        $editorTitle.html('Расписание на дату ' + date);
        $driverModal.modal('show'); // display the modal
        $driverModal.on('hidden.bs.modal', function () {
            $('form', $driverModal).trigger('reset');

        });
        $driverModal.off('shown.bs.modal');
        $driverModal.on('shown.bs.modal', function () {
            $('#tabContent a', $driverModal).hide();
            $('#tabContent a', $driverModal).click(function (e) {
                e.preventDefault();
                $(this).tab('show');
            });
            var tabs = checkStatus(status, type);

            $('#tabContent a').off('shown.bs.tab');
            $('#tabContent a').on('shown.bs.tab', function (e) {
                var tabViewContent = $(this).attr('href');
                if (tabViewContent == '#pick-driver') {
                    tabs.disableTab(tabViewContent, $driverModal);
                    if (status > SCHEDULE_STATUS_MASK.ASSIGNED_EMPTY) {
// TODO: нужно сделать одну функцию ajax назовем ее schedule_view_request(data, successCallback)
// входные параметры это дата и функция отработки удачного запроса
//
// для всех закладок модального окна применять эту логику!
                        $.ajax({
                            url: 'controllers/schedule/scheduleView.php',
                            type: 'post',
                            dataType: 'json',
                            contentType: 'application/json;charset=utf-8',
                            async: false,
                            cache: false,
                            data: JSON.stringify({
                                method: "viewDriverTab",
                                schedule_id: schedule_id
                            }),
                            success: function (data) {
                                if (data != null) {
                                    $(tabViewContent, $driverModal).find('select[name = schedule_driver_id]').val(data.schedule_driver_id);
                                    $(tabViewContent, $driverModal).find('textarea[name = schedule_remark]').val(data.schedule_remarks_remark);
                                    $(tabViewContent, $driverModal).find('input[name = acceptTerms]').prop('checked', true);
                                } else {
                                    $(tabViewContent, $driverModal).find('input[name = acceptTerms]').prop('checked', false);
                                }
                            }
                        });
                    }

                    $(tabViewContent, $driverModal).find('input[name = schedule_vehicle_id]').val(vehicle_id);
                    $(tabViewContent, $driverModal).find('input[name = schedule_date]').val(date);
                    $(tabViewContent, $driverModal).find('input[name = schedule_id]').val(schedule_id);
                    $(tabViewContent, $driverModal).find('input[name = driver_type]').val(type);

                }
                if (tabViewContent == '#on-shift') {

                    tabs.disableTab(tabViewContent, $driverModal);
                    if (status > SCHEDULE_STATUS_MASK.ASSIGNED_DRIVER) {
                        $.ajax({
                            url: 'controllers/schedule/scheduleView.php',
                            type: 'post',
                            dataType: 'json',
                            contentType: 'application/json;charset=utf-8',
                            async: false,
                            cache: false,
                            data: JSON.stringify({
                                method: "viewOnShiftTab",
                                schedule_id: schedule_id
                            }),
                            success: function (data) {
                                if (data != null) {
                                    $(tabViewContent, $driverModal).find('input[name = vehicle_gasoline_littres]').val(data.vehicle_gasoline_littres);
                                    $(tabViewContent, $driverModal).find('input[name = vehicle_maintenance_km]').val(data.vehicle_maintenance_km);
                                    $(tabViewContent, $driverModal).find('textarea[name = schedule_remark]').val(data.schedule_remarks_remark);
                                    $(tabViewContent, $driverModal).find('input[name = acceptTerms]').prop('checked', true);
                                } else {
                                    $(tabViewContent, $driverModal).find('input[name = acceptTerms]').prop('checked', false);
                                }
                            }
                        });
                    }
                    $(tabViewContent, $driverModal).find('input[name = schedule_vehicle_id]').val(vehicle_id);
                    $(tabViewContent, $driverModal).find('input[name = schedule_driver_id]').val(driver_id);
                    $(tabViewContent, $driverModal).find('input[name = schedule_date]').val(date);
                    $(tabViewContent, $driverModal).find('input[name = schedule_id]').val(schedule_id);
                    $(tabViewContent, $driverModal).find('input[name = driver_type]').val(type);
                }
                if (tabViewContent == '#off-shift-vehicle') {
                    tabs.disableTab(tabViewContent, $driverModal);
                    $.ajax({
                        url: 'controllers/schedule/scheduleView.php',
                        type: 'post',
                        dataType: 'json',
                        contentType: 'application/json;charset=utf-8',
                        async: false,
                        cache: false,
                        data: JSON.stringify({
                            method: "viewOffShiftVehicleDisabled",
                            schedule_id: schedule_id
                        }),
                        success: function (data) {
                            $(tabViewContent, $driverModal).find('input[name = travel_plan_km]').val(data.travel_plan_km);
                            $(tabViewContent, $driverModal).find('input[name = gasoline_plan_payment]').val(data.gasoline_plan_payment);
                        }
                    });

                    if (status > SCHEDULE_STATUS_MASK.ASSIGNED_DRIVER) {
                        $.ajax({
                            url: 'controllers/schedule/scheduleView.php',
                            type: 'post',
                            dataType: 'json',
                            contentType: 'application/json;charset=utf-8',
                            async: false,
                            cache: false,
                            data: JSON.stringify({
                                method: "viewOffShiftVehicleTab",
                                schedule_id: schedule_id
                            }),
                            success: function (data) {
                                if (data != null && data.schedule_datetime_end_shift_vehicle != null) {
                                    $(tabViewContent, $driverModal).find('input[name = vehicle_gasoline_littres]').val(data.vehicle_gasoline_littres);
                                    $(tabViewContent, $driverModal).find('input[name = vehicle_maintenance_km]').val(data.vehicle_maintenance_km);
                                    $(tabViewContent, $driverModal).find('textarea[name = schedule_remark]').val(data.schedule_remarks_remark);
                                    $(tabViewContent, $driverModal).find('input[name = schedule_transactions_sum]').val(data.schedule_transactions_sum);
                                    $(tabViewContent, $driverModal).find('input[name = acceptTerms]').prop('checked', true);
                                } else {
                                    $(tabViewContent, $driverModal).find('input[name = acceptTerms]').prop('checked', false);
                                }
                            }
                        });
                    }

                    $(tabViewContent, $driverModal).find('input[name = schedule_vehicle_id]').val(vehicle_id);
                    $(tabViewContent, $driverModal).find('input[name = schedule_driver_id]').val(driver_id);
                    $(tabViewContent, $driverModal).find('input[name = schedule_date]').val(date);
                    $(tabViewContent, $driverModal).find('input[name = schedule_id]').val(schedule_id);
                    $(tabViewContent, $driverModal).find('input[name = driver_type]').val(type);

                    $current_schedule_status = $(tabViewContent, $driverModal).find('input[name = schedule_status]').val();
                    $(tabViewContent, $driverModal).find('input[name = schedule_status]').val($current_schedule_status + status);
                }
                if (tabViewContent == '#off-shift-money') {
                    tabs.disableTab(tabViewContent, $driverModal);
                    $.ajax({
                        url: 'controllers/schedule/scheduleView.php',
                        type: 'post',
                        dataType: 'json',
                        contentType: 'application/json;charset=utf-8',
                        async: false,
                        cache: false,
                        data: JSON.stringify({
                            method: "viewOffShiftMoneyDisabled",
                            schedule_id: schedule_id
                        }),
                        success: function (data) {
                            $(tabViewContent, $driverModal).find('input[name = orders_count]').val(data.orders_count);
                            $(tabViewContent, $driverModal).find('input[name = orders_sum]').val(data.orders_sum);
                            $(tabViewContent, $driverModal).find('input[name = collect_cash]').val(data.collect_cash);
                            $(tabViewContent, $driverModal).find('input[name = collect_cash_hidden]').val(data.collect_cash);
                        }
                    });

                    if (status > SCHEDULE_STATUS_MASK.ASSIGNED_DRIVER) {
                        $.ajax({
                            url: 'controllers/schedule/scheduleView.php',
                            type: 'post',
                            dataType: 'json',
                            contentType: 'application/json;charset=utf-8',
                            async: false,
                            cache: false,
                            data: JSON.stringify({
                                method: "viewOffShiftMoneyTab",
                                schedule_id: schedule_id
                            }),
                            success: function (data) {
                                if (data != null && data.schedule_transactions_id != null) {
                                    $(tabViewContent, $driverModal).find('textarea[name = schedule_remark]').val(data.schedule_remarks_remark);
                                    $(tabViewContent, $driverModal).find('input[name = accepted_cash]').val(data.schedule_transactions_sum);
                                    $(tabViewContent, $driverModal).find('input[name = acceptTerms]').prop('checked', true);
                                } else {
                                    $(tabViewContent, $driverModal).find('input[name = acceptTerms]').prop('checked', false);
                                }
                            }
                        });
                        $.ajax({
                            url: 'controllers/schedule/scheduleView.php',
                            type: 'post',
                            dataType: 'json',
                            contentType: 'application/json;charset=utf-8',
                            async: false,
                            cache: false,
                            data: JSON.stringify({
                                method: "viewOffShiftMoneyManualTab",
                                schedule_id: schedule_id
                            }),
                            success: function (data) {
                                if (data != null && data.schedule_transactions_id != null) {
                                    $(tabViewContent, $driverModal).find('input[name = manual_transaction]').prop('checked', true);
                                    $(tabViewContent, $driverModal).find('select[name = manual_transaction_type]').val(data.schedule_transactions_type);
                                    $(tabViewContent, $driverModal).find('input[name = manual_transaction_sum]').val(data.schedule_transactions_sum);
                                    $(tabViewContent, $driverModal).find('textarea[name = manual_transaction_remark]').val(data.schedule_transactions_remark);
                                    $(tabViewContent, $driverModal).find('input[name = manual_transaction]').change();
                                } else {
                                    $(tabViewContent, $driverModal).find('input[name = manual_transaction]').prop('checked', false);
                                }
                            }
                        });
                    }


                    $(tabViewContent, $driverModal).find('input[name = schedule_vehicle_id]').val(vehicle_id);
                    $(tabViewContent, $driverModal).find('input[name = schedule_driver_id]').val(driver_id);
                    $(tabViewContent, $driverModal).find('input[name = schedule_id]').val(schedule_id);
                    $(tabViewContent, $driverModal).find('input[name = schedule_date]').val(date);
                    $(tabViewContent, $driverModal).find('input[name = driver_type]').val(type);

                    $current_schedule_status = $(tabViewContent, $driverModal).find('input[name = schedule_status]').val();
                    $(tabViewContent, $driverModal).find('input[name = schedule_status]').val($current_schedule_status + status);

                    $(tabViewContent, $driverModal).find('input[name = manual_transaction]').off("change");
                    $(tabViewContent, $driverModal).find('input[name = manual_transaction]').on("change", function (e) {
                        if ($(this).is(':checked')) {
                            $('[name = div_transaction_type]', $driverModal).show();
                            $('[name = div_transaction_sum]', $driverModal).show();
                            $('[name = div_transaction_remark]', $driverModal).show();
                        } else {
                            $('[name = div_transaction_type]', $driverModal).hide();
                            $('[name = div_transaction_sum]', $driverModal).hide();
                            $('[name = div_transaction_remark]', $driverModal).hide();
                        }
                    });

                    $(tabViewContent, $driverModal).find('input[name = manual_transaction]').change();

                }
            });

            tabs.showTab($driverModal);

            $driverModalForm.off('submit');
            $driverModalForm.on('submit', function (e) {
                e.preventDefault(); // stop the default post back from a form submit
                var $form = $(this);
                var jsonForm = JSON.stringify($form.serializeObject());

                $.ajax({
                    url: 'controllers/schedule/scheduleStore.php',
                    type: 'post',
                    dataType: 'json',
                    contentType: 'application/json;charset=utf-8',
                    cache: false,
                    async: false,
                    data: jsonForm,
                    success: function (data) {
                        alertShowHide(data['ERROR_CODE'], data['ERROR_MESSAGE']);
                        if (ft != null) {
                            ft.destroy();
                        }
                        initFooTable();
                        //TODO: complite
// TODO : сделать принт в статусную строку и перегрузить данные таблицы в случаи ERROR_CODE = 1 (удачное сохранение)
                    }
                });

                $driverModal.modal('hide');
            });
        });
    });
}


var $dateRangeSelect = null;
$(document).ready(function () {
    $('.date_range option[value="1"]').prop('selected', true);
    $dateRangeSelect = $('.date_range select');
    $dateRangeSelect.off('change');
    $dateRangeSelect.on('change', function () {
        initFooTable();
    });

    initFooTable();
});</script>