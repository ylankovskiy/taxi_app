<link href="css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
<link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- FooTable -->
<link href="css/plugins/footable/footable.bootstrap.css" rel="stylesheet">
<style>
    button.footable-add {
    display: none!important;
    }
    button.footable-delete {
    display: none!important;
    }
</style>
<!--max height form style-->
<style type="text/css">
    /* Adjust the height of section */
    .profileFormVihicle .content {
    min-height: 100px;
    }
    .profileFormVihicle .content > .body {
    width: 100%;
    height: auto;
    padding: 15px;
    position: relative;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Новый автомобиль</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <h2>
                    Заведение нового автомобиля
                </h2>
                <p>
                    Заполните все указанные поля со звездочкой.
                </p>
                <form id="form" action="#" class="wizard-big profileFormVihicle">
                    <h1>Автомобиль</h1>
                    <fieldset>
                        <h2>Информация по автомобилю</h2>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label class="control-label">Марка</label>
                                    <input  name="vehicle_make" type="text" class="form-control required">
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Модель</label>
                                    <input  name="vehicle_model" type="text" class="form-control required">
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Цвет</label>
                                    <input  name="vehicle_color" type="text" class="form-control required">
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Год выпуска</label>
                                    <input  name="vehicle_year_production" type="number" class="form-control required">
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Регистрационный номер</label>
                                    <input  name="vehicle_plate_number" type="text" class="form-control required">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">VIN номер</label>
                                    <input  name="vehicle_vin" type="text" class="form-control">
                                </div>
                                <div class="form-group">
                                    <div class="form-group">
                                        <label class="control-label">Наличие газового оборудования</label>
                                        <input  name="vehicle_gas" type="checkbox" class="">
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Тип</label>
                                    <select data-placeholder="Выбирите тип автомобиля..." class="form-control required" name="vehicle_type">
                                        <option value="">Выбрать ...</option>
                                        <option value="0">автомобиль компании</option>
                                        <option value="1">личный автомобиль водителя</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label class="control-label">Статус</label>
                                    <select data-placeholder="Выбирите статус автомобиля..." class="form-control required"  name="vehicle_status">
                                        <option value="">Выбрать ...</option>
                                        <option value="0">рабочий</option>
                                        <option value="1">в ремонте</option>
                                        <option value="2">списанный</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Примечание</label>
<!--                                    <input  name="vehicle_remark" type="text" class="form-control">-->
                                    <textarea class="form-control" rows="3" id="" name=""></textarea>

                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <h1>Регистрация и страхование</h1>
                    <fieldset>
                        <h2>Информация о регистрации и страховании автомобиля</h2>
                        <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="form-group">
                                        <div class="checkbox">
                                            <label class="control-label">
                                            <input  name="vehicle_insurance_osago" type="checkbox">ОСАГО</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group required">
                                            <label class="control-label">Серия</label>
                                            <input  name="vehicle_insurance_osago_series" type="text" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group required">
                                            <label class="control-label">Номер</label>
                                            <input  name="vehicle_insurance_osago_num" type="text" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group required" id="div_insurance_osago_date_begin">
                                            <label class="control-label">Дата начала действия</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="vehicle_insurance_osago_date_begin" class="form-control " value="" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group required" id="div_insurance_osago_date_end">
                                            <label class="control-label">Дата окончания действия</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="vehicle_insurance_osago_date_end" class="form-control" value="" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="row">
                                    <div class="form-group ">
                                        <div class="checkbox">
                                            <label class="control-label"><input  name="vehicle_insurance_kasko" type="checkbox" value="false">КАСКО</label>
                                        </div>
                                    </div>
                                    <div class="col-lg-4">
                                        <div class="form-group required">
                                            <label class="control-label">Серия</label>
                                            <input  name="vehicle_insurance_kasko_series" type="text" class="form-control" required>
                                        </div>
                                    </div>
                                    <div class="col-lg-8">
                                        <div class="form-group required">
                                            <label class="control-label">Номер</label>
                                            <input name="vehicle_insurance_kasko_num" type="text" class="form-control" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-6">
                                        <div class="form-group required" id="div_insurance_kasko_date_begin">
                                            <label class="control-label">Дата начала действия</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                <input type="text" name="vehicle_insurance_kasko_date_begin" class="form-control " value="" required>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-6">
                                        <div class="form-group required" id="div_insurance_kasko_date_end">
                                            <label class="control-label">Дата окончания действия</label>
                                            <div class="input-group date">
                                                <span class="input-group-addon">
                                                <i class="fa fa-calendar"></i>
                                                </span>
                                                <input type="text" name="vehicle_insurance_kasko_date_end" class="form-control " value="" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                    </fieldset>
                    <h1>Техническое обслуживание</h1>
                    <fieldset>
                        <h2>Информация о техническом обслуживании автомобиля</h2>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label class="control-label">Расход топлива, л/100км</label>
                                    <input  name="vehicle_l_per_100_km" type="number" class="form-control required">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label class="control-label">Периодичность ТО, км</label>
                                    <input  name="vehicle_maintenance_freq_km" type="number" class="form-control required">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <h1>Дополнительно</h1>
                    <fieldset>
                        <h2>Дополнительная информация об автомобиле</h2>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                        <input class="checkbox" name="vehicle_device" id="vehicle_device_checkbox" type="checkbox">
                                        <label for="vehicle_device_checkbox" class="control-label">Наличие устройства</label>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group required" required="">
                                    <label class="control-label">Модель устройства</label>
                                    <input  name="vehicle_device_model" type="text" class="form-control" required>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Цена устройства</label>
                                    <input  name="vehicle_device_cost" type="number" class="form-control" required>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <h1>Подтверждение</h1>
                    <fieldset>
                        <h2>Достоверность ввода данных</h2>
                        <div class="form-group required">
                            <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required">
                            <label for="acceptTerms" class="control-label">Я подтверждаю достоверность введенных мною данных.</label>
                        </div>
                    </fieldset>
                    <input class="hidden" name="method" type="text" value="store" />
                    <input class="hidden" name="vehicle_added_employee_id" type="number" value="0" />
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<div class="row editor-status hidden">
    <div class="col-lg-12">
        <div class="ibox">
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <table class="table table-bordered table-responsive table-condensed" data-paging="true" data-sorting="true" data-filtering="true">
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="vehicle-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title">
    <style>
        /* provides a red astrix to denote required fields - this should be included in common stylesheet */
        .form-group.required .control-label:after {
        content: "*";
        color: red;
        margin-left: 4px;
        }
    </style>
    <div class="modal-dialog" role="document">
        <form class="modal-content form-horizontal" id="editor" method="POST">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="editor-title">Add Row</h4>
            </div>
            <div class="modal-body">
                <input class="hidden" name="method" type="text" value="store" />
                <input class="hidden" name="vehicle_added_employee_id" type="number" value="0" />
                <input class="hidden" type="number" id="vehicle_id" name="vehicle_id" value="" />
                <div class="form-group">
                    <h3>Информация по автомобилю</h3>
                    <hr>
                </div>
                <div class="form-group required">
                    <label for="vehicle_make" class="col-sm-3 control-label">Марка</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="vehicle_make" name="vehicle_make" placeholder="Марка" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="vehicle_model" class="col-sm-3 control-label">Модель</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control " id="vehicle_model" name="vehicle_model" placeholder="Модель" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="vehicle_color" class="col-sm-3 control-label">Цвет</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control " id="vehicle_color" name="vehicle_color" placeholder="Цвет" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="vehicle_year_production" class="col-sm-3 control-label">Год выпуска</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control " id="vehicle_year_production" name="vehicle_year_production" placeholder="Год выпуска" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="vehicle_plate_number" class="col-sm-3 control-label">Регистрационный номер</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control " id="vehicle_plate_number" name="vehicle_plate_number" placeholder="Регистрационный номер" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="vehicle_vin" class="col-sm-3 control-label">VIN номер</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control " id="vehicle_vin" name="vehicle_vin" placeholder="VIN номер">
                    </div>
                </div>
                <div class="form-group">
                    <label for="vehicle_gas" class="col-sm-3 control-label">Наличие газового оборудования</label>
                    <div class="col-sm-9">

                            <input type="checkbox" class="" id="vehicle_gas" name="vehicle_gas" placeholder="Наличие газового оборудования">

                    </div>
                </div>
                <div class="form-group required">
                    <label for="vehicle_type" class="col-sm-3 control-label">Тип</label>
                    <div class="col-sm-9">
                        <select data-placeholder="Выбирите тип автомобиля..." id="vehicle_type" class="form-control " name="vehicle_type" required>
                            <option value="">Выбрать ...</option>
                            <option value="0" selected>автомобиль компании</option>
                            <option value="1">личный автомобиль водителя</option>
                        </select>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="vehicle_status" class="col-sm-3 control-label">Статус</label>
                    <div class="col-sm-9">
                        <select data-placeholder="Выбирите статус автомобиля..." id="vehicle_status" class="form-control" name="vehicle_status" required>
                            <option value="">Выбрать ...</option>
                            <option value="0">рабочий</option>
                            <option value="1">в ремонте</option>
                            <option value="2">списанный</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="vehicle_remark" class="col-sm-3 control-label">Примечание</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="vehicle_remark" name="vehicle_remark" placeholder="Примечание">
                    </div>
                </div>
                <div class="form-group">
                    <h3>Информация о регистрации и страховании автомобиля</h3>
                    <hr>
                </div>
                <div class="form-group">
                    <label for="vehicle_insurance_osago" class="col-sm-3 control-label">Выберите тип страхования</label>
                    <div class="col-sm-9">
                        <div class="col-sm-6">
                            <div class="checkbox">
                                <label for="vehicle_insurance_osago"><input class="" id="vehicle_insurance_osago" name="vehicle_insurance_osago" type="checkbox">ОСАГО
                                </label>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="checkbox">
                                <label for="vehicle_insurance_kasko">
                                    <input class="" id="vehicle_insurance_kasko" name="vehicle_insurance_kasko" type="checkbox">
                                    КАСКО
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group" id="insurance_osago_title" name="insurance_osago_title">
                    <h4>Страховой полис ОСАГО</h4>
                    <hr>
                </div>
                <div class="form-group">
                    <label for="vehicle_insurance_osago_series" class="col-sm-3 control-label">Серия страхового полиса</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control " id="vehicle_insurance_osago_series" name="vehicle_insurance_osago_series" placeholder="Серия страхового полиса" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="vehicle_insurance_osago_num" class="col-sm-3 control-label">Номер страхового полиса</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control " id="vehicle_insurance_osago_num" name="vehicle_insurance_osago_num" placeholder="Номер страхового полиса" required>
                    </div>
                </div>
                <div class="form-group" name="div_insurance_osago_date_begin">
                    <label for="input-group date" class="col-sm-3 control-label">Дата начала действия</label>
                    <div class="col-sm-9">
                        <div class="input-group date">
                            <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </span>
                            <input type="text" name="vehicle_insurance_osago_date_begin" class="form-control" value="">
                        </div>
                    </div>
                </div>
                <div class="form-group" name="div_insurance_osago_date_end">
                    <label for="input-group date" class="col-sm-3 control-label">Дата окончания действия</label>
                    <div class="col-sm-9">
                        <div class="input-group date">
                            <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </span>
                            <input type="text" name="vehicle_insurance_osago_date_end" class="form-control" value="">
                        </div>
                    </div>
                </div>
                <div class="form-group" id="insurance_kasko_title" name="insurance_kasko_title">
                    <h4>Страховой полис КАСКО</h4>
                    <hr>
                </div>
                <div class="form-group">
                    <label for="vehicle_insurance_kasko_series" class="col-sm-3 control-label">Серия страхового полиса</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control " id="vehicle_insurance_kasko_series" name="vehicle_insurance_kasko_series" placeholder="Серия страхового полиса" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="vehicle_insurance_kasko_num" class="col-sm-3 control-label">Номер страхового полиса</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control " id="vehicle_insurance_kasko_num" name="vehicle_insurance_kasko_num" placeholder="Номер страхового полиса" required>
                    </div>
                </div>
                <div class="form-group" name="div_insurance_kasko_date_begin">
                    <label for="input-group date" class="col-sm-3 control-label">Дата начала действия</label>
                    <div class="col-sm-9">
                        <div class="input-group date">
                            <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </span>
                            <input type="text" name="vehicle_insurance_kasko_date_begin" class="form-control" value="">
                        </div>
                    </div>
                </div>
                <div class="form-group" name="div_insurance_kasko_date_end">
                    <label for="input-group date" class="col-sm-3 control-label">Дата окончания действия</label>
                    <div class="col-sm-9">
                        <div class="input-group date">
                            <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </span>
                            <input type="text" name="vehicle_insurance_kasko_date_end" class="form-control " value="">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <h3>Информация о техническом обслуживании автомобиля</h3>
                    <hr>
                </div>
                <div class="form-group required">
                    <label for="vehicle_l_per_100_km" class="col-sm-3 control-label">Расход топлива, л/100км</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control " id="vehicle_l_per_100_km" name="vehicle_l_per_100_km" placeholder="Расход топлива, л/100км" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="vehicle_maintenance_freq_km" class="col-sm-3 control-label">Периодичность ТО, км</label>
                    <div class="col-sm-9">
                        <input type="number" class="form-control " id="vehicle_maintenance_freq_km" name="vehicle_maintenance_freq_km" placeholder="Периодичность технического обслуживания, 1000km" required>
                    </div>
                </div>
                <div class="form-group">
                    <h3>Дополнительная информация об автомобиле</h3>
                    <hr>
                </div>
                <div class="form-group">
                    <label for="vehicle_device" class="col-sm-3 control-label">Наличие устройства</label>
                    <div class="col-sm-9">

                            <input class="" type="checkbox" id="vehicle_device" name="vehicle_device" placeholder="Наличие устройства">

                    </div>
                </div>
                <div class="form-group">
                    <label for="vehicle_device_model" class="col-sm-3 control-label">Модель устройства</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="vehicle_device_model" name="vehicle_device_model" placeholder="Модель устройства">
                    </div>
                </div>
                <div class="form-group">
                    <label for="vehicle_device_cost" class="col-sm-3 control-label">Цена устройства</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="vehicle_device_cost" name="vehicle_device_cost" placeholder="Цена устройства">
                    </div>
                </div>
                <div class="form-group">
                    <h3>Подтверждение данных</h3>
                    <hr>
                </div>
                <div class="form-group required">
                    <label for="acceptTerms" class="col-sm-3 control-label">Я подтверждаю достоверность введенных мною данных.</label>
                    <div class="col-sm-9">
                        <input type="checkbox" class="" id="acceptTerms" name="acceptTerms" placeholder="" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
</div>
<!-- Sparkline -->
<script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- FooTable -->
<script src="js/plugins/footable/footable.js"></script>
<!-- Jquery Validate -->
<script src="js/plugins/validate/jquery.validate.min.js"></script>
<script src="js/plugins/validate/localization/messages_ru.min.js"></script>
<!-- Steps -->
<script src="js/plugins/staps/jquery.steps.js"></script>
<!-- Chosen -->
<script src="js/plugins/chosen/chosen.jquery.js"></script>
<script>
//TODO: завел для тебя глобальные переменные формы и таблицы
var $FORM = null;
var ft = null;
function initFootable() {
    return FooTable.init('.table', {
        "rows": $.ajax({
            url: "controllers/vehicle/vehicleView.php",
            type: 'post',
            dataType: 'json',
            cache: false,
            async: false,
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify({
                method: 'view_all'
            }),
        }),
        "columns": $.get("controllers/vehicle/vehicleViewAllColumns.json", null, null, "json"),
        editing: {
            "deleteText": '<span class="fa fa-print" aria-hidden="true"></span>',
            showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Редактировать',
            hideText: 'Назад',
            enabled: true,
            allowAdd: false,
            allowEdit: true,
            allowDelete: true,
            allowView: true,
            deleteRow: function (row) {
                console.log('print option');
            },
            editRow: function (row) {
                var values = row.val();
                $editorTitle = $('#editor-title', $modal);
                console.log(row);

                var title = 'Изменение данных автомобиля';
                var flagEdit = true;

                viewVehicleModal(values, $editor, $editorTitle, title, flagEdit);

                $modal.data('row', row); // set the row data value for use later
                $modal.modal('show'); // display the modal

                $modal.on('shown.bs.modal', function () {
                    // HIDE/SHOW OSAGO/KASKO only after checking specific value fields

                    attachModalEvents($modal, values);
                });
                var form = document.getElementById('editor');
                form.onsubmit = function (event) {
                    event.preventDefault();

                    var $form = $(this);
                    var jsonForm = JSON.stringify($form.serializeObject());
                    console.log(jsonForm);
                    $.ajax({
                        url: 'controllers/vehicle/vehicleStore.php',
                        type: 'post',
                        dataType: 'json',
			            cache: false,
            			async: false,
			            contentType: 'application/json;charset=utf-8',
                        success: function (data) {
                            console.log(data);
                            alertShowHide(data['ERROR_CODE'], data['ERROR_MESSAGE']);
//                            location.reload();
                        },
                        data: jsonForm
                    });
                }
            },
            viewRow: function (row) {
                var values = row.val();

                var $editorTitle = $('#editor-title', $modal);
                var title = 'Данные автомобиля';
                var flagEdit = false;

                viewVehicleModal(values, $editor, $editorTitle, title, flagEdit);

                $modal.data('row', row); // set the row data value for use later
                $modal.modal('show'); // display the modal

                $modal.on('shown.bs.modal', function () {
                    // HIDE/SHOW OSAGO/KASKO only after checking specific value fields

                    attachModalEvents($modal, values);
                });
            }
        }
    });
}


//TODO: у тебя есть enable/disable пары для формы и модального окна, они ОЧЕНЬ похожи! объедени и передавай $modal либо $FORM элемент чтобы открывать / скрывать нужные тебе элементы
// если тебе нужно открыть / скрыть элементы в модале которых нету в форме, на пример title, то заведи отдельную функцию для этого дела

function modalEnableOsago($modal) {
    $('[name = vehicle_insurance_osago_series]', $modal).closest(".form-group").show();
    $('[name = vehicle_insurance_osago_num]', $modal).closest(".form-group").show();
    $('[name = vehicle_insurance_osago_date_begin]', $modal).closest(".form-group").show();
    $('[name = vehicle_insurance_osago_date_end]', $modal).closest(".form-group").show();
    $('[name = insurance_osago_title]', $modal).show();
    //add required
    $('[name = vehicle_insurance_osago_series]', $modal).prop('required',true).closest(".form-group").addClass('required');
    $('[name = vehicle_insurance_osago_num]', $modal).prop('required',true).closest(".form-group").addClass('required');
    $('[name = vehicle_insurance_osago_date_begin]', $modal).prop('required',true).closest(".form-group").addClass('required');
    $('[name = vehicle_insurance_osago_date_end]', $modal).prop('required',true).closest(".form-group").addClass('required');
}

function modalDisableOsago($modal) {
    $('[name = vehicle_insurance_osago_series]', $modal).closest(".form-group").hide();
    $('[name = vehicle_insurance_osago_num]', $modal).closest(".form-group").hide();
    $('[name = vehicle_insurance_osago_date_begin]', $modal).closest(".form-group").hide();
    $('[name = vehicle_insurance_osago_date_end]', $modal).closest(".form-group").hide();
    //remove required
    $('[name = vehicle_insurance_osago_series]', $modal).prop('required',false).closest(".form-group").removeClass('required');
    $('[name = vehicle_insurance_osago_num]', $modal).prop('required',false).closest(".form-group").removeClass('required');
    $('[name = vehicle_insurance_osago_date_begin]', $modal).prop('required',false).closest(".form-group").removeClass('required');
    $('[name = vehicle_insurance_osago_date_end]', $modal).prop('required',false).closest(".form-group").removeClass('required');



    $('[name = insurance_osago_title]', $modal).hide();
    $('[name = vehicle_insurance_osago_series]', $modal).val('');
    $('[name = vehicle_insurance_osago_num]', $modal).val('');
    $('[name = vehicle_insurance_osago_date_begin] .input-group.date', $modal).datepicker('update', '');
    $('[name = vehicle_insurance_osago_date_end] .input-group.date', $modal).datepicker('update', '');
}

function modalEnableKasko($modal) {
    $('[name = vehicle_insurance_kasko_series]', $modal).closest(".form-group").show();
    $('[name = vehicle_insurance_kasko_num]', $modal).closest(".form-group").show();
    $('[name = vehicle_insurance_kasko_date_begin]', $modal).closest(".form-group").show();
    $('[name = vehicle_insurance_kasko_date_end]', $modal).closest(".form-group").show();
    $('[name = insurance_kasko_title]', $modal).show();
    //add required
    $('[name = vehicle_insurance_kasko_series]', $modal).prop('required',true).closest(".form-group").addClass('required');
    $('[name = vehicle_insurance_kasko_num]', $modal).prop('required',true).closest(".form-group").addClass('required');
    $('[name = vehicle_insurance_kasko_date_begin]', $modal).prop('required',true).closest(".form-group").addClass('required');
    $('[name = vehicle_insurance_kasko_date_end]', $modal).prop('required',true).closest(".form-group").addClass('required');
}

function modalDisableKasko($modal) {
    $('[name = vehicle_insurance_kasko_series]', $modal).closest(".form-group").hide();
    $('[name = vehicle_insurance_kasko_num]', $modal).closest(".form-group").hide();
    $('[name = vehicle_insurance_kasko_date_begin]', $modal).closest(".form-group").hide();
    $('[name = vehicle_insurance_kasko_date_end]', $modal).closest(".form-group").hide();
    //remove reuired
    $('[name = vehicle_insurance_kasko_series]', $modal).prop('required',false).closest(".form-group").removeClass('required');
    $('[name = vehicle_insurance_kasko_num]', $modal).prop('required',false).closest(".form-group").removeClass('required');
    $('[name = vehicle_insurance_kasko_date_begin]', $modal).prop('required',false).closest(".form-group").removeClass('required');
    $('[name = vehicle_insurance_kasko_date_end]', $modal).prop('required',false).closest(".form-group").removeClass('required');

    $('[name = insurance_kasko_title]', $modal).hide();
    $('[name = vehicle_insurance_kasko_series]', $modal).val('');
    $('[name = vehicle_insurance_kasko_num]', $modal).val('');
    $('[name = vehicle_insurance_kasko_date_begin] .input-group.date', $modal).datepicker('update', '');
    $('[name = vehicle_insurance_kasko_date_end] .input-group.date', $modal).datepicker('update', '');
}

function modalEnableDeviceVehicle($modal) {
    $('[name = vehicle_device_model]', $modal).closest(".form-group").show();
    $('[name = vehicle_device_cost]', $modal).closest(".form-group").show();
    //add required
    $('[name = vehicle_device_model]', $modal).prop('required',true).closest(".form-group").addClass('required');
    $('[name = vehicle_device_cost]', $modal).prop('required',true).closest(".form-group").addClass('required');
}

function modalDisableDeviceVehicle($modal) {
    $('[name = vehicle_device_model]', $modal).closest(".form-group").hide();
    $('[name = vehicle_device_cost]', $modal).closest(".form-group").hide();
    //remove required
    $('[name = vehicle_device_model]', $modal).prop('required',false).closest(".form-group").removeClass('required');
    $('[name = vehicle_device_cost]', $modal).prop('required',false).closest(".form-group").removeClass('required');
}

function viewVehicleModal(values, $editor, $editorTitle, title, flagEdit) {
    $editorTitle.html(title);

    // we need to find and set the initial value for the editor inputs
    $editor.find('[name = vehicle_id]').val(values.vehicle_id);
    $editor.find('[name = vehicle_make]').val(values.vehicle_make);
    $editor.find('[name = vehicle_model]').val(values.vehicle_model);

    if (values.vehicle_gas == 'false') {
        $editor.find('[name = vehicle_gas]').prop('checked', false);
    } else {
        $editor.find('[name = vehicle_gas]').prop('checked', true);
    }
    
    $editor.find('[name = vehicle_color]').val(values.vehicle_color);
    $editor.find('[name = vehicle_year_production]').val(values.vehicle_year_production);
    $editor.find('[name = vehicle_vin]').val(values.vehicle_vin);
    $editor.find('[name = vehicle_type] option').val(values.vehicle_type);
    $editor.find('[name = vehicle_status]').val(values.vehicle_status);
    $editor.find('[name = vehicle_remark]').val(values.vehicle_remark);

    if (values.vehicle_insurance_osago == 'false') {
        $editor.find('[name = vehicle_insurance_osago]').prop('checked', false);
    } else {
    	$editor.find('[name = vehicle_insurance_osago]').prop('checked', true);
    }
    $editor.find('[name = vehicle_insurance_osago_series]').val(values.vehicle_insurance_osago_series);
    $editor.find('[name = vehicle_insurance_osago_num]').val(values.vehicle_insurance_osago_num);
    $editor.find('[name = vehicle_insurance_osago_date_begin]').val(values.insurance_kasko_osago_begin);
    $editor.find('[name = vehicle_insurance_osago_date_end]').val(values.insurance_kasko_osago_end);

    if (values.vehicle_insurance_kasko == 'false') {
        $editor.find('[name = vehicle_insurance_kasko]').prop('checked', false);
    } else {
        $editor.find('[name = vehicle_insurance_kasko]').prop('checked', true);
    }
    $editor.find('[name = vehicle_insurance_kasko_series]').val(values.vehicle_insurance_kasko_series);
    $editor.find('[name = vehicle_insurance_kasko_num]').val(values.vehicle_insurance_kasko_num);
    $editor.find('[name = vehicle_insurance_kasko_date_begin]').val(values.vehicle_insurance_kasko_date_begin);
    $editor.find('[name = vehicle_insurance_kasko_date_end]').val(values.vehicle_insurance_kasko_date_end);

    $editor.find('[name = vehicle_plate_number]').val(values.vehicle_plate_number);
    $editor.find('[name = vehicle_l_per_100_km]').val(values.vehicle_l_per_100_km);
    $editor.find('[name = vehicle_maintenance_freq_km]').val(values.vehicle_maintenance_freq_km);

    if (values.vehicle_device == 'false') {
        $editor.find('[name = vehicle_device]').prop('checked', false);
    } else {
        $editor.find('[name = vehicle_device]').prop('checked', true);
    }
    $editor.find('[name = vehicle_device_model]').val(values.vehicle_device_model);
    $editor.find('[name = vehicle_device_cost]').val(values.vehicle_device_cost);

    if (!flagEdit) {
        $editor.find("input").prop('disabled', true);
        $editor.find("select").prop('disabled', true);
        $editor.find('button[type=submit]').hide();
    } else {
        $editor.find("input").prop('disabled', false);
        $editor.find("select").prop('disabled', false);
        $editor.find('button[type=submit]').show();
    }
}

function attachModalEvents($modal, values) {
    if (values.vehicle_insurance_osago == 'false') {
        modalDisableOsago($modal);
    } else {
        modalEnableOsago($modal);
    }

    if (values.vehicle_insurance_kasko == 'false') {
        modalDisableKasko($modal);
    } else {
        modalEnableKasko($modal);
    }

    if (values.vehicle_device == 'false') {
        modalDisableDeviceVehicle($modal);
    } else {
        modalEnableDeviceVehicle($modal);
    }

    $('[name = vehicle_insurance_osago]', $modal).on('change', function () {
        $(this).prop("checked") ? modalEnableOsago($modal) : modalDisableOsago($modal);
    });

    $('[name = vehicle_insurance_kasko]', $modal).on('change', function () {
        $(this).prop("checked") ? modalEnableKasko($modal) : modalDisableKasko($modal);
    });

    $('[name = vehicle_device]', $modal).on('change', function () {
        $(this).prop("checked") ? modalEnableDeviceVehicle($modal) : modalDisableDeviceVehicle($modal);
    });

    $('[name = div_insurance_kasko_date_begin] .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('[name = div_insurance_kasko_date_end] .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('[name = div_insurance_osago_date_begin] .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('[name = div_insurance_osago_date_end] .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
}

var $modal = $('#vehicle-modal'),
    $editor = $('#editor', $modal);
    // the below initializes FooTable and returns the created instance for later use
    ft = initFootable();

$editor.on('submit', function (e) {
    if (this.checkValidity && !this.checkValidity()) return; // if validation fails exit early and do nothing.
    e.preventDefault(); // stop the default post back from a form submit
    var row = $modal.data('row'), // get any previously stored row object
        values = { // create a hash of the editor row values
            firstName: $editor.find('#firstName').val(),
            lastName: $editor.find('#lastName').val()
        };

    if (row instanceof FooTable.Row) { // if we have a row object then this is an edit operation
        // here you can execute an ajax call to the server and then only update the row once the result is
        // retrieved. This example simply updates the row straight away.
        row.val(values);
    } else { // otherwise this is an add operation
        // here you can execute an ajax call to the server to save the values and get the new row id and then
        // only add the row once the result is retrieved. This example simply adds the row straight away using
        // a basic integer id.
        values.id = uid++;
        ft.rows.add(values);
    }
    $modal.modal('hide');
});
$(document).ready(function () {
	$FORM = $("#form");
	$('#div_alert').hide();
    $("a.collapse-link").click();
    $("#wizard").steps();
    $FORM.steps({
        bodyTag: "fieldset",
        onStepChanging: function (event, currentIndex, newIndex) {
            console.log("changing : " + currentIndex + ' ' + newIndex);
            // Always allow going backward even if the current step contains invalid fields!
            if (currentIndex > newIndex) {
                return true;
            }

            var form = $(this);

            // Clean up if user went backward before
            if (currentIndex < newIndex) {
                // To remove error styles
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
            }

            // Disable validation on fields that are disabled or hidden.
            form.validate().settings.ignore = ":disabled,:hidden";

            // Start validation; Prevent going forward if false
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            console.log("changed : " + currentIndex + ' ' + priorIndex);

            // Suppress (skip) "Warning" step if the user is old enough and wants to the previous step.
            if (currentIndex === 2 && priorIndex === 3) {
                $(this).steps("previous");
            }
        },
        onFinishing: function (event, currentIndex) {
            var form = $(this);

            // Disable validation on fields that are disabled.
            // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
            form.validate().settings.ignore = ":disabled,:hidden";

            // Start validation; Prevent form submission if false
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            var form = $(this);
            var jsonForm = JSON.stringify(form.serializeObject());
            console.log(jsonForm);
            $.ajax({
                url: 'controllers/vehicle/vehicleStore.php',
                type: 'post',
                dataType: 'json',
                cache: false,
                async: false,
                contentType: 'application/json;charset=utf-8',
                success: function (data) {
                    console.log(data);
                    //location.reload();
                    if (ft != null) {
                        ft.destroy();
                        $(".footable-empty").remove();

                    }
                    ft = initFootable();
                    alertShowHide(data['ERROR_CODE'], data['ERROR_MESSAGE']);
                    $('.collapse-link').click();
                    $('#form').trigger( 'reset' );
// TODO: что это? если идет сохранение и перезагрузка таблицы то действовать следующим путем...
// TODO : сделать принт в статусную строку и перегрузить данные таблицы в случаи ERROR_CODE = 1 (удачное сохранение)
// TODO: complite
// TODO: complite в моем потеряном коммите было как теперь тут)))
// если необходимо перегрузить табличные данные то не зачем писать инициализацию таблицы заново, пере используй код!



                    //            $('.table').trigger('footable_redraw');
                },
                data: jsonForm
            });

            // Submit form input
            //form.submit();
        },
        labels: {
            cancel: "Отмена",
            current: "Текущий шаг:",
            pagination: "Нумерация",
            finish: "Завершить",
            next: "Вперед",
            previous: "Назад",
            loading: "Загрузка ..."
        }
    }).validate({
        errorPlacement: function (error, element) {
                element.before(error);
            }
    });

    function formEnableOsago() {
        $('[name = vehicle_insurance_osago_series]').closest(".form-group").show();
        $('[name = vehicle_insurance_osago_num]').closest(".form-group").show();
        $("#div_insurance_osago_date_begin").show();
        $("#div_insurance_osago_date_end").show();
    }

    function formDisableOsago() {
        $('[name = vehicle_insurance_osago_series]').closest(".form-group").hide();
        $('[name = vehicle_insurance_osago_num]').closest(".form-group").hide();
        $('#div_insurance_osago_date_begin').hide();
        $('#div_insurance_osago_date_end').hide();
        $('[name = vehicle_insurance_osago_series]').val('');
        $('[name = vehicle_insurance_osago_num]').val('');
        $('[name = vehicle_insurance_osago_date_begin] .input-group.date').datepicker('update', '');
        $('[name = vehicle_insurance_osago_date_end] .input-group.date').datepicker('update', '');
    }

    function formEnableKasko() {
        $('[name = vehicle_insurance_kasko_series]').closest(".form-group").show();
        $('[name = vehicle_insurance_kasko_num]').closest(".form-group").show();
        $('#div_insurance_kasko_date_begin').show();
        $('#div_insurance_kasko_date_end').show();

    }

    function formDisableKasko() {
        $('[name = vehicle_insurance_kasko_series]').closest(".form-group").hide();
        $('[name = vehicle_insurance_kasko_num]').closest(".form-group").hide();
        $('#div_insurance_kasko_date_begin').hide();
        $('#div_insurance_kasko_date_end').hide();
        $('[name = vehicle_insurance_kasko_series]').val('');
        $('[name = vehicle_insurance_kasko_num]').val('');
        $('[name = vehicle_insurance_kasko_date_begin] .input-group.date').datepicker('update', '');
        $('[name = vehicle_insurance_kasko_date_end] .input-group.date').datepicker('update', '');
    }

    function formEnableDeviceVehicle() {
        $('[name = vehicle_device_model]').closest(".form-group").show();
        $('[name = vehicle_device_cost]').closest(".form-group").show();
    }

    function formDisableDeviceVehicle() {
        $('[name = vehicle_device_model]').closest(".form-group").hide();
        $('[name = vehicle_device_cost]').closest(".form-group").hide();
    }
    formDisableOsago();
    formDisableKasko();
    formDisableDeviceVehicle();
    $('[name = vehicle_insurance_osago]').on('change', function () {
        $(this).prop("checked") ? formEnableOsago() : formDisableOsago();
    });

    $('[name = vehicle_insurance_kasko]').on('change', function () {
        $(this).prop("checked") ? formEnableKasko() : formDisableKasko();
    });

    $('[name = vehicle_device]').on('change', function () {
        $(this).prop("checked") ? formEnableDeviceVehicle() : formDisableDeviceVehicle();
    });

    $('#div_insurance_kasko_date_begin .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
    $('#div_insurance_kasko_date_end .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
    $('#div_insurance_osago_date_begin .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
    $('#div_insurance_osago_date_end .input-group.date').datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
    console.log(ft);
});
</script>