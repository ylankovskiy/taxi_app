<link href="css/plugins/footable/footable.bootstrap.css" rel="stylesheet">
<link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<style>
    button.footable-add {
        display: none!important;
    }
    button.footable-delete {
        display: none!important;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="col-lg-6">
                <div class="form-group">
			    	<label class="control-label">Период</label>
				    <div class="input-daterange input-group" id="datepicker">
						<input type="text" class="input-sm form-control" name="date_begin" />
						<span class="input-group-addon">по</span>
						<input type="text" class="input-sm form-control" name="date_end" />
					</div>
				</div>
            </div>
            <div class="col-lg-6">
                <label class="control-label">Фрод/Корретировки</label>
                <div class="col-sm-9">
                    <input name="fraud-filtr" type="checkbox">&nbsp;Фрод фильтр
                </div>
                <div class="col-sm-9">
                    <input name="correct-filtr" type="checkbox">&nbsp;Корректировки фильтр
                </div>

            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <table class="table table-bordered table-responsive table-condensed" data-paging="true" data-sorting="true" data-filtering="true">

            </table>
        </div>
    </div>
</div>
</div>
<!-- FooTable -->
<script src="js/plugins/footable/footable.js"></script>
<!-- Jquery Validate -->
<script src="js/plugins/validate/jquery.validate.min.js"></script>
<script src="js/plugins/validate/localization/messages_ru.min.js"></script>
<script>
var date_begin = null;
var date_end = null;
var ft = null;

$(function () {
    $('.input-daterange').datepicker({
	    format: "dd.mm.yyyy",
    	todayBtn: "linked",
	    clearBtn: true,
    	language: "ru"
	}).on('hide', function(e) {
    	date_begin = $('[name=date_begin]').val();
	    date_end = $('[name=date_end]').val();
        if (ft != null) {
            ft.destroy();
            $(".footable-empty").remove();
        };
	    if (typeof(date_begin) !== 'undefined' && date_begin && typeof(date_end) !== 'undefined' && date_end) {
	        ft = init_table();
//            console.log($(".footable-filtering"))
//            $(".footable-filtering").hide();


        }
    });
});

function init_table() {
    ft = FooTable.init('.table', {
//        "filtering": {
//            "position": "center",
//            "enabled": true,
//            "filters": [{
//                "name": "my-filter",
//                "query": "The value",
//                "columns": ["order_fraud_sum","order_correction_difference"]
//            }]
//        },
        "columns": $.get("controllers/order/orderViewAllColumns.json", null, null, "json"),
        "rows": $.ajax({
            url: "controllers/order/orderView0.php",
            type: 'post',
            dataType: 'json',
            cache: false,
            async: false,
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify({
                method: 'view',
                date_begin: date_begin,
                date_end: date_end
            })
        }),


});

    return ft;

}

$(document).ready(function () {
    date_begin = $('[name=date_begin]').val();
    date_end = $('[name=date_end]').val();
    $("[name=fraud-filtr]").change(function(){
        if $(this).is(":checked") {
            if (typeof(date_begin) !== 'undefined' && date_begin && typeof(date_end) !== 'undefined' && date_end) {
                ft = init_table(fraud);
            }
        } else {
            ft = init_table();

        }
        $("[name=correct-filtr]").change(function(){
            if ($(this).is(":checked") && typeof(date_begin) !== 'undefined' && date_begin && typeof(date_end) !== 'undefined' && date_end) {
                if  $("[name=fraud-filtr]").is(":checked") {
                    ft = init_table();
                } else {
                    ft = init_table(correct);
                }
            }
             else {
                ft = init_table(fraud);
            }

    });
    if (ft != null) {
        ft.destroy();
        $(".footable-empty").remove();
    }
    if (typeof(date_begin) !== 'undefined' && date_begin && typeof(date_end) !== 'undefined' && date_end) {
        ft = init_table();
    }
});
</script>

