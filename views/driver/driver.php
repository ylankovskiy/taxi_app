<link href="css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
<link href="css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<link href="css/plugins/select2/select2.min.css" rel="stylesheet">
<!-- FooTable -->
<link href="css/plugins/footable/footable.bootstrap.css" rel="stylesheet">
<style>
    .select2-close-mask{
    z-index: 2099;
    }
    .select2-dropdown{
    z-index: 3051;
    }
</style>
<!--max height form style-->
<style type="text/css">
    /* Adjust the height of section */
    .profileFormDriver .content {
    min-height: 100px;
    }
    .profileFormDriver .content > .body {
    width: 100%;
    height: auto;
    padding: 15px;
    position: relative;
    }
</style>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <div class="ibox-title">
                <h5>Новый водитель</h5>
                <div class="ibox-tools">
                    <a class="collapse-link">
                    <i class="fa fa-chevron-up"></i>
                    </a>
                </div>
            </div>
            <div class="ibox-content">
                <h2>
                    Заведение нового водителя
                </h2>
                <p>
                    Заполните все указанные поля со звездочкой.
                </p>
                <form id="form" action="#" class="wizard-big profileFormDriver">
                    <h1>Водитель</h1>
                    <fieldset>
                        <h2>Личные данные водителя</h2>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label class="control-label">Фамилия</label>
                                    <input name="driver_name_last" type="text" class="form-control required">
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Имя</label>
                                    <input  name="driver_name_first" type="text" class="form-control required">
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Отчество</label>
                                    <input  name="driver_name_name" type="text" class="form-control required">
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Примечание</label>
                                    <input name="driver_remark" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label class="control-label">Номер водительского удостоверения</label>
                                    <input name="driver_lic_number" type="text" class="form-control required">
                                </div>
                                <div class="form-group required" id="div_date_birth">
                                    <label class="control-label">Дата рождения</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </span>
                                        <input type="text" name="driver_date_birth"  class="form-control required" value="03/04/2014">
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Телефон</label>
                                    <input type="text" class="form-control required" name="telephone_book_telephone" data-mask="7(999)999-9999" placeholder="">
                                    <span class="help-block">7(999)999-9999</span>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label class="control-label">Статус</label>
                                    <select data-placeholder="Укажите статус..." class="driver_status form-control required" name="driver_status" tabindex="2">
                                        <option disabled value="-1">Выбрать...</option>
                                        <option value="0">работает</option>
                                        <option value="1">уволен</option>
                                    </select>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label" required>Тип</label>
                                    <select data-placeholder="Укажите тип..." class="driver_type form-control required" name="driver_type"  tabindex="2">
                                        <option value="">Выбрать...</option>
                                        <option value="0">наш аренда</option>
                                        <option value="1">наш зарплата</option>
                                        <option value="2">с личным автомобилем</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <h1>Паспортные данные</h1>
                    <fieldset>
                        <h2>Паспортные данные водителя</h2>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label class="control-label">Серия</label>
                                    <input name="driver_passport_series" type="text" class="form-control required">
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Номер</label>
                                    <input  name="driver_passport_number" type="text" class="form-control required">
                                </div>
                                <div class="form-group required" id="div_passport_date_given">
                                    <label class="control-label">Дата выдачи</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                        </span><input type="text" name="driver_passport_date_given" class="form-control required" value="03/04/2014">
                                    </div>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Выдан</label>
                                    <input name="driver_passport_authority_given" type="text" class="form-control required">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label class="control-label">Адрес прописки</label>
                                    <input  name="driver_passport_address_living" type="text" class="form-control required">
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Адрес проживания</label>
                                    <input  name="driver_actual_address_living" type="text" class="form-control required">
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <h1>Информация по банку</h1>
                    <fieldset>
                        <h2>Информация по банку</h2>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label class="control-label">Фамилия</label>
                                    <input name="bank_account_holder_name_last" type="text" class="form-control required">
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Имя</label>
                                    <input name="bank_account_holder_name_first" type="text" class="form-control required">
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Отчество</label>
                                    <input name="bank_account_holder_name_name" type="text" class="form-control required">
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Наименование банка</label>
                                    <input name="pay_project_bank_name" type="text" class="form-control required" disabled>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Номер счета</label>
                                    <input name="bank_account_number" type="number" class="form-control required">
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">БИК</label>
                                    <input name="pay_project_bank_bik" type="number" class="form-control required">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group required">
                                    <label class="control-label">Тип счета</label>
                                    <select data-placeholder="Выбирите тип счета..." class="form-control required" name="bank_account_type"  tabindex="2">
                                        <option value="">Выбрать...</option>
                                        <option value="0">свой</option>
                                        <option value="1">чужой</option>
                                    </select>
                                </div>
                                <div class="form-group required">
                                    <label class="control-label">Статус</label>
                                    <select data-placeholder="Выбирите статус счета..." class="form-control required" name="bank_account_status"  tabindex="2">
                                        <option value="">Выбрать...</option>
                                        <option value="0">активный</option>
                                        <option value="1">не активный</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label class="control-label">Примечание</label>
<!--                                    <input name="bank_account_remark" type="text" class="form-control">-->
                                    <textarea class="form-control" rows="3" name="bank_account_remark"></textarea>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <h1>Gett</h1>
                    <fieldset>
                        <h2>Информация Gett для водителя</h2>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Позывной для водителя  Gett</label>
                                    <input name="gett_system_id" type="text" class="form-control" disabled>
                                </div>
                                <div class="form-group" id="div_gett_date_registration">
                                    <label class="control-label">Дата регистрации</label>
                                    <div class="input-group date">
                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="gett_date_registration" class="form-control" value="03/04/2014" disabled>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Статус</label>
                                    <select data-placeholder="Выбирите статус водителя в Gett..." class="gett_status form-control" name="gett_status" tabindex="2">
                                        <option value="">Выбрать...</option>
                                        <option value="0">активный</option>
                                        <option value="1">заблокирован</option>
                                        <option value="2">переименован</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <h1>Автомобиль</h1>
                    <fieldset>
                        <h2>Информация по автомобилю водителя</h2>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <label class="control-label">Выбор автомобиля</label>
                                    <select data-placeholder="Выбирите автомобиль..." id="form_vehicle_select" name="driver_vehicle_id" tabindex="2">
                                    </select>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    <h1>Подтверждение</h1>
                    <fieldset>
                        <h2>Достоверность ввода данных</h2>
                        <div class="form-group required">
                            <input id="acceptTerms" name="acceptTerms" type="checkbox" class="required">
                            <label for="acceptTerms" class="">Я подтверждаю достоверность введенных мною данных.</label>
                        </div>
                    </fieldset>
                    <input id="method" class="hidden" name="method" type="text" value="store" />
                    <input id="driver_employee_added_id" class="hidden" name="driver_employee_added_id" type="number" value="0" />
                </form>
            </div>
        </div>
    </div>
</div>
</div>
<div class="row">
    <div class="col-lg-12">
        <div class="ibox">
            <table class="table table-bordered table-responsive table-condensed" data-paging="true" data-sorting="true" data-filtering="true">
            </table>
        </div>
    </div>
</div>
<div class="modal fade" id="driver-modal" tabindex="-1" role="dialog" aria-labelledby="editor-title">
    <style>
        /* provides a red astrix to denote required fields - this should be included in common stylesheet */
        .form-group.required .control-label:after {
        content: "*";
        color: red;
        margin-left: 4px;
        }
    </style>
    <div class="modal-dialog" role="document">
        <form class="modal-content form-horizontal" id="editor">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
                <h4 class="modal-title" id="editor-title">Add Row</h4>
            </div>
            <div class="modal-body">
                <input name="method" class="hidden" type="text" value="store" />
                <input class="hidden" name="driver_employee_added_id" type="number" value="0" />
                <input type="number" name="driver_id" class="hidden" value="" />
                <input type="number" name="bank_id" class="hidden" value="" />
                <input type="number" name="gett_id" class="hidden" value="" />
                <input type="number" name="driver_vehicle_id" class="hidden" value="" />
                <input type="number" name="driver_telephone_book_id" class="hidden" value="" />
                <div class="form-group">
                    <h3>Водитель</h3>
                    <hr>
                </div>
                <div class="form-group required">
                    <label for="driver_name_last" class="col-sm-3 control-label">Фамилия</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control required" id="driver_name_last" name="driver_name_last" placeholder="Фамилия" >
                    </div>
                </div>
                <div class="form-group required">
                    <label for="driver_name_first" class="col-sm-3 control-label">Имя</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="driver_name_first" name="driver_name_first" placeholder="Имя" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="driver_name_name" class="col-sm-3 control-label">Отчество</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="driver_name_name" name="driver_name_name" placeholder="Отчество" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="driver_remark" class="col-sm-3 control-label">Примечание</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="driver_remark" name="driver_remark" placeholder="Примечание">
                    </div>
                </div>
                <div class="form-group required">
                    <label for="driver_lic_number" class="col-sm-3 control-label">Номер водительского удостоверения</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="driver_lic_number" name="driver_lic_number" placeholder="Номер водительского удостоверения" required>
                    </div>
                </div>
                <div class="form-group required" name="div_date_birth">
                    <label for="driver_date_birth" class="col-sm-3 control-label">Дата рождения</label>
                    <div class="col-sm-9">
                        <div class="input-group date">
                            <span class="input-group-addon"><i class="fa fa-calendar"></i>
                            </span>
                            <input type="text" id="driver_date_birth" name="driver_date_birth" class="form-control" value="03/04/2014" required>
                        </div>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="telephone_book_telephone" class="col-sm-3 control-label">Телефон</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="telephone_book_telephone" name="telephone_book_telephone" data-mask="7(999)999-9999" placeholder="" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="driver_status" class="col-sm-3 control-label">Статус</label>
                    <div class="col-sm-9">
                        <select data-placeholder="Укажите статус..." id="driver_status" name="driver_status" tabindex="2" required>
                            <option value="">Выбрать...</option>
                            <option value="0">работает</option>
                            <option value="1">уволен</option>
                        </select>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="driver_type" class="col-sm-3 control-label">Тип</label>
                    <div class="col-sm-9">
                        <select data-placeholder="Выбирите тип водителя..." id="driver_type" name="driver_type" tabindex="2" required>
                            <option value="">Выбрать...</option>
                            <option value="0">наш аренда</option>
                            <option value="1">наш зарплата</option>
                            <option value="2">с личным автомобилем</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <h3>Паспортные данные</h3>
                    <hr>
                </div>
                <div class="form-group required">
                    <label for="driver_passport_series" class="col-sm-3 control-label">Серия паспорта</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="driver_passport_series" name="driver_passport_series" placeholder="Серия" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="driver_passport_number" class="col-sm-3 control-label">Номер паспорта</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="driver_passport_number" name="driver_passport_number" placeholder="Номер" required>
                    </div>
                </div>
                <div class="form-group required" name="div_passport_date_given">
                    <label for="driver_passport_date_given" class="col-sm-3 control-label">Дата выдачи паспорта</label>
                    <div class="col-sm-9">
                        <div class="input-group date">
                            <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </span>
                            <input type="text" id="driver_passport_date_given" name="driver_passport_date_given" class="form-control" value="03/04/2014" required>
                        </div>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="driver_passport_authority_given" class="col-sm-3 control-label">Паспорт выдан</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="driver_passport_authority_given" name="driver_passport_authority_given" placeholder="Выдан" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="driver_passport_address_living" class="col-sm-3 control-label">Адрес прописки</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="driver_passport_address_living" name="driver_passport_address_living" placeholder="Адрес прописки" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="driver_actual_address_living" class="col-sm-3 control-label">Адрес проживания</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="driver_actual_address_living" name="driver_actual_address_living" placeholder="Адрес проживания" required>
                    </div>
                </div>
                <div class="form-group">
                    <h3>Информация по банку</h3>
                    <hr>
                </div>
                <div class="form-group required">
                    <label for="bank_account_holder_name_last" class="col-sm-3 control-label">Фамилия держателя карты</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="bank_account_holder_name_last" name="bank_account_holder_name_last" placeholder="Фамилия держателя карты" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="bank_account_holder_name_first" class="col-sm-3 control-label">Имя держателя карты</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="bank_account_holder_name_first" name="bank_account_holder_name_first" placeholder="Имя держателя карты" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="bank_account_holder_name_name" class="col-sm-3 control-label">Отчество держателя карты</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="bank_account_holder_name_name" name="bank_account_holder_name_name" placeholder="Отчество держателя карты" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="pay_project_bank_name" class="col-sm-3 control-label">Наименование банка</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="pay_project_bank_name" name="pay_project_bank_name" placeholder="Наименование банка" required disabled>
                        <input type="text" class="hidden form-control" name="pay_project_bank_name" placeholder="Наименование банка скрыто">
                    </div>
                </div>
                <div class="form-group required">
                    <label for="bank_account_number" class="col-sm-3 control-label">Номер счета</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="bank_account_number" name="bank_account_number" placeholder="Номер счета" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="pay_project_bank_bik" class="col-sm-3 control-label">БИК</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="pay_project_bank_bik" name="pay_project_bank_bik" placeholder="БИК" required>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="bank_account_type" class="col-sm-3 control-label">Тип счета</label>
                    <div class="col-sm-9">
                        <select data-placeholder="Выбирите тип счета..." id="bank_account_type" name="bank_account_type" tabindex="2" required>
                            <option value="">Выбрать...</option>
                            <option value="0">свой</option>
                            <option value="1">чужой</option>
                        </select>
                    </div>
                </div>
                <div class="form-group required">
                    <label for="bank_account_status" class="col-sm-3 control-label">Статус счета</label>
                    <div class="col-sm-9">
                        <select data-placeholder="Выбирите статус счета..." id="bank_account_status" name="bank_account_status" tabindex="2" required>
                            <option value="">Выбрать...</option>
                            <option value="0">активный</option>
                            <option value="1">не активный</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="bank_account_remark" class="col-sm-3 control-label">Примечание по счету</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="bank_account_remark" name="bank_account_remark" placeholder="Примечание по счету">
                    </div>
                </div>
                <div class="form-group">
                    <h3>Gett</h3>
                    <hr>
                </div>
                <div class="form-group">
                    <label for="gett_system_id" class="col-sm-3 control-label">Позывной Gett для водителя</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="gett_system_id" name="gett_system_id" placeholder="Позывной Gett для водителя" disabled>
                    </div>
                </div>
                <div class="form-group" name="div_gett_date_registration">
                    <label for="gett_date_registration" class="col-sm-3 control-label">Дата регистрации Gett</label>
                    <div class="col-sm-9">
                        <div class="input-group date">
                            <span class="input-group-addon">
                            <i class="fa fa-calendar"></i>
                            </span>
                            <input type="text" id="gett_date_registration" name="gett_date_registration" class="form-control" value="03/04/2014">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="gett_status" class="col-sm-3 control-label">Статус Gett</label>
                    <div class="col-sm-9">
                        <select data-placeholder="Выбирите статус Gett..." id="gett_status" name="gett_status" tabindex="2">
                            <option value="">Выбрать...</option>
                            <option value="0">активный</option>
                            <option value="1">заблокирован</option>
                            <option value="2">переименован</option>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <h3>Автомобиль</h3>
                    <hr>
                </div>
                <div class="form-group">
                    <label for="modal_vehicle_select" class="col-sm-3 control-label">Выбор автомобиля</label>
                    <div class="col-sm-9">
                        <select data-placeholder="Выбирите автомобиль..." id="modal_vehicle_select" name="driver_vehicle_id" tabindex="2">
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <h3>Подтверждение данных</h3>
                    <hr>
                </div>
                <div class="form-group required">
                    <label for="acceptTerms" class="col-sm-3 control-label">Я подтверждаю достоверность введенных мною данных.</label>
                    <div class="col-sm-9">
                        <input type="checkbox" class="" id="acceptTerms" name="acceptTerms"  required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Save changes</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
            </div>
        </form>
    </div>
</div>

<!-- Sparkline -->
<script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
<!-- FooTable -->
<script src="js/plugins/footable/footable.js"></script>
<!-- Jquery Validate -->
<script src="js/plugins/validate/jquery.validate.min.js"></script>
<script src="js/plugins/validate/localization/messages_ru.min.js"></script>
<!-- Steps -->
<script src="js/plugins/staps/jquery.steps.js"></script>
<!-- Chosen -->
<script src="js/plugins/chosen/chosen.jquery.js"></script>
<!--pdfMake-->
<script src='js/plugins/pdfMake/pdfmake.min.js'></script>
<script src='js/plugins/pdfMake/vfs_fonts.js'></script>
<!--jsZip-->
<script src='js/plugins/jsZip/jszip.js'></script>
<script>
var $FORM = null;
var ft = null;

function viewDriverModal(values, $editor, $editorTitle, title, flagEdit) {
    $editorTitle.html(title);
    console.log(values);
    $editor.find('[name = driver_id]').val(values.driver_id);
    $editor.find('[name = driver_vehicle_id]').val(values.driver_vehicle_id);
    $editor.find('[name = bank_id]').val(values.bank_id);
    $editor.find('[name = driver_telephone_book_id]').val(values.driver_telephone_book_id);
    $editor.find('[name = gett_id]').val(values.gett_id);
    $editor.find('[name = driver_name_last]').val(values.driver_name_last);
    $editor.find('[name = driver_name_first]').val(values.driver_name_first);
    $editor.find('[name = driver_name_name]').val(values.driver_name_name);
    $editor.find('[name = driver_remark]').val(values.driver_remark);
    $editor.find('[name = driver_lic_number]').val(values.driver_lic_number);
    $editor.find('[name = driver_date_birth]').val(values.driver_date_birth);
    $editor.find('[name = telephone_book_telephone]').val(values.telephone_book_telephone);
    $editor.find('[name = driver_status]').val(values.driver_status);
    $editor.find('[name = driver_type]').val(values.driver_type);
    $editor.find('[name = driver_passport_series]').val(values.driver_passport_series);
    $editor.find('[name = driver_passport_number]').val(values.driver_passport_number);
    $editor.find('[name = driver_passport_date_given]').val(values.driver_passport_date_given);
    $editor.find('[name = driver_passport_authority_given]').val(values.driver_passport_authority_given);
    $editor.find('[name = driver_passport_address_living]').val(values.driver_passport_address_living);
    $editor.find('[name = driver_actual_address_living]').val(values.driver_actual_address_living);
    $editor.find('[name = bank_account_holder_name_last]').val(values.bank_account_holder_name_last);
    $editor.find('[name = bank_account_holder_name_first]').val(values.bank_account_holder_name_first);
    $editor.find('[name = bank_account_holder_name_name]').val(values.bank_account_holder_name_name);
    $editor.find('[name = pay_project_bank_name]').val(values.pay_project_bank_name);
    $editor.find('[name = pay_project_bank_bik]').val(values.pay_project_bank_bik);
    $editor.find('[name = bank_account_number]').val(values.bank_account_number);
    $editor.find('[name = bank_account_type]').val(values.bank_account_type);
    $editor.find('[name = bank_account_status]').val(values.bank_account_status);
    $editor.find('[name = bank_account_remark]').val(values.bank_account_remark);
    $editor.find('[name = gett_system_id]').val(values.gett_system_id);
    $editor.find('[name = gett_status]').val(values.gett_status);
    $editor.find('[name = gett_date_registration]').val(values.gett_date_registration);
    $editor.find('[name = vehicle_select]').val(values.vehicle_select);

    if (!flagEdit) {
        $editor.find("input").prop('disabled', true);
        $editor.find("select").prop('disabled', true);
        $editor.find('button[type=submit]').hide();
    } else {
        $editor.find("input").prop('disabled', false);
        $editor.find("select").prop('disabled', false);
        $editor.find('button[type=submit]').show();
    }
    
    $editor.find('[name = pay_project_bank_name]').prop('disabled', true);
    $editor.find('[name = gett_system_id]').prop('disabled', true);
    $editor.find('[name = gett_date_registration]').prop('disabled', true);


    $('select[name=bank_account_type]', $modal).change(function () {
        console.log('change', this);
        if ($('[name=bank_account_type]', $modal).val() == 0) {
            $('[name=bank_account_holder_name_last]',$modal).val($('[name=driver_name_last]', $modal).val()).prop('disabled', true);
            $('[name=bank_account_holder_name_first]',$modal).val($('[name=driver_name_first]', $modal).val()).prop('disabled', true);
            $('[name=bank_account_holder_name_name]',$modal).val($('[name=driver_name_name]', $modal).val()).prop('disabled', true);
        } else {
            $('[name=bank_account_holder_name_last]',$modal).val('').prop('disabled', false);
            $('[name=bank_account_holder_name_first]',$modal).val('').prop('disabled', false);
            $('[name=bank_account_holder_name_name]',$modal).val('').prop('disabled', false);
        }
    });
}

function attachModalEvents($modal, values) {
    $('[name = div_date_birth] .input-group.date', $modal).datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('[name = div_passport_date_given] .input-group.date', $modal).datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('[name = div_gett_date_registration] .input-group.date', $modal).datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
    $.ajax({
        url: 'utils/select/selectVehicle.php',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=utf-8',
        cache: false,
        async: false,
        data: JSON.stringify({
            method_status: 0,
//             method_type: 0
        }),
        success: function (data) {
            $("#modal_vehicle_select", $modal).select2({
                data: data
            });
        },
    });
    $('[name = pay_project_bank_bik]', $modal).on("change", function () {
        var bik = $(this).val();
        $.ajax({
            url: 'utils/bank/selectByBik.php',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json;charset=utf-8',
            cache: false,
            async: false,
            data: JSON.stringify({
                bik: bik
            }),
            success: function (data) {
                $('[name = pay_project_bank_name]', $modal).val(data['pay_project_bank_name']);
                //console.log(data);
            },
        });
    });

}

var $modal = $('#driver-modal'),
    $editor = $('#editor', $modal),
    // the below initializes FooTable and returns the created instance for later use
    ft = FooTable.init('.table', {
        "rows": $.ajax({
            url: "controllers/driver/driverView.php",
            type: 'post',
            dataType: 'json',
            cache: false,
            async: false,
            contentType: 'application/json;charset=utf-8',
            data: JSON.stringify({
                method: 'view_all'
            }),
        }),
        "columns": $.get("controllers/driver/driverViewAllColumns.json", null, null, "json"),
        editing: {
            deleteText: '<span class="fa fa-print" aria-hidden="true"></span>',
            showText: '<span class="fooicon fooicon-pencil" aria-hidden="true"></span> Редактировать',
            hideText: 'Назад',
            enabled: true,
            allowAdd: false,
            allowEdit: true,
            allowDelete: true,
            allowView: true,
            deleteRow: function (row) {
                console.log('print option');
                function getDoc(url_pdf_src, name) {
                    console.log(url_pdf_src);
                    $.ajax({
                        dataType: "json",
                        url: url_pdf_src,
                        success: function (data) {
                            console.log(data);
                            var docDefinition = data;
                            pdfMake.createPdf(docDefinition).download(name + '.pdf');
//
                        }
                    });
                }
                getDoc('payReportDocuments/2017/Зарплатники.json',"Зарплатники");
                getDoc('payReportDocuments/2017/Доверенности.json',"Доверенности");
                getDoc('payReportDocuments/2017/Своя карта Формула.json',"Своя карта Формула");
                getDoc('payReportDocuments/2017/Своя карта.json',"Своя карта");
                getDoc('payReportDocuments/2017/Чужая карта.json',"Чужая карта");
                getDoc('payReportDocuments/2017/Чужая карта Формула.json',"Чужая карта Формула");



            },
            editRow: function (row) {
                var values = row.val();

                var $editorTitle = $('#editor-title', $modal);
                var title = 'Изменение данных водителя';
                var flagEdit = true;

                viewDriverModal(values, $editor, $editorTitle, title, flagEdit);

                $modal.data('row', row); // set the row data value for use later
                $modal.modal('show'); // display the modal

                $modal.on('shown.bs.modal', function () {
// TODO: search why select2 does not work in modal
// пока убрал select2() по причине его не корректной работы в модале
//                     $editor.find("[name = driver_status]").select2();
//                     $editor.find("[name = driver_type]").select2();
//                     $editor.find("[name = bank_account_status]").select2();
//                     $editor.find("[name = bank_account_type]").select2();
//                     $editor.find("[name = gett_status]").select2();
//                     $editor.find("[name = vehicle_status]").select2();
                    attachModalEvents($modal, values);
                });
            },
            viewRow: function (row) {
                var values = row.val();

                var $editorTitle = $('#editor-title', $modal);
                var title = 'Данные водителя';
                var flagEdit = false;

                viewDriverModal(values, $editor, $editorTitle, title, flagEdit);

                $modal.data('row', row); // set the row data value for use later
                $modal.modal('show'); // display the modal

                $modal.on('shown.bs.modal', function () {
// TODO: search why select2 does not work in modal
// пока убрал select2() по причине его не корректной работы в модале
//                     $editor.find("[name = driver_status]").select2();
//                     $editor.find("[name = driver_type]").select2();
//                     $editor.find("[name = bank_account_status]").select2();
//                     $editor.find("[name = bank_account_type]").select2();
//                     $editor.find("[name = gett_status]").select2();
//                     $editor.find("[name = vehicle_status]").select2();
                });
            }
        }
    });

$editor.on('submit', function (e) {
    if (this.checkValidity && !this.checkValidity()) return; // if validation fails exit early and do nothing.
    e.preventDefault(); // stop the default post back from a form submit
    var row = $modal.data('row');
    var $form = $(this);
    var jsonForm = JSON.stringify($form.serializeObject());
    console.log(jsonForm);
    $.ajax({
        url: 'controllers/driver/driverStore.php',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=utf-8',
        cache: false,
        async: false,
        success: function (data) {
            //      	console.log(data);
            if (ft != null) {
                ft.destroy();
                $(".footable-empty").remove();

            }
            ft = initFootable();
            alertShowHide(data['ERROR_CODE'], data['ERROR_MESSAGE']);
            $('.collapse-link').click();
            $('#form').trigger( 'reset' );
        },
        data: jsonForm
    });

    $modal.modal('hide');
});

$(document).ready(function () {
	$FORM = $("#form");



    $("a.collapse-link").click();
    $("#wizard").steps();
    $FORM.steps({
        bodyTag: "fieldset",
        onStepChanging: function (event, currentIndex, newIndex) {
            // Always allow going backward even if the current step contains invalid fields!
            if (currentIndex > newIndex) {
                return true;
            }

            var form = $(this);

            // Clean up if user went backward before
            if (currentIndex < newIndex) {
                // To remove error styles
                $(".body:eq(" + newIndex + ") label.error", form).remove();
                $(".body:eq(" + newIndex + ") .error", form).removeClass("error");
            }

            // Disable validation on fields that are disabled or hidden.
            form.validate().settings.ignore = ":disabled,:hidden";

            // Start validation; Prevent going forward if false
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {},
        onFinishing: function (event, currentIndex) {
            var form = $(this);

            // Disable validation on fields that are disabled.
            // At this point it's recommended to do an overall check (mean ignoring only disabled fields)
            form.validate().settings.ignore = ":disabled,:hidden";

            // Start validation; Prevent form submission if false
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            var form = $(this);
            var jsonForm = JSON.stringify(form.serializeObject());

            $.ajax({
                url: 'controllers/driver/driverStore.php',
                type: 'post',
                dataType: 'json',
                contentType: 'application/json;charset=utf-8',
                cache: false,
                async: false,
                success: function (data) {
                    console.log(data);
                },
                data: jsonForm
            });
        },

        labels: {
            cancel: "Отмена",
            current: "Текущий шаг:",
            pagination: "Нумерация",
            finish: "Завершить",
            next: "Вперед",
            previous: "Назад",
            loading: "Загрузка ..."
        }
    }).validate({
        errorPlacement: function (error, element) {
                element.before(error);
            }
    });
    
    $.ajax({
        url: 'utils/select/selectVehicle.php',
        type: 'post',
        dataType: 'json',
        contentType: 'application/json;charset=utf-8',
        cache: false,
        async: false,
        data: JSON.stringify({
            method_status: 0
//             method_type: 0
        }),
        success: function (data) {
            $("#form_vehicle_select", $FORM).select2({
                data: data
            });
        },
    });
    
    $("[name = pay_project_bank_bik]", $FORM).on("change", function () {
        var bik = $(this).val();
        $.ajax({
            url: 'utils/bank/selectByBik.php',
            type: 'post',
            dataType: 'json',
            contentType: 'application/json;charset=utf-8',
            cache: false,
            async: false,
            data: JSON.stringify({
                bik: bik
            }),
            success: function (data) {
                $("[name = pay_project_bank_name]", $FORM).val(data['pay_project_bank_name']);
                //console.log(data);
            },
        });
    });


    $('#div_date_birth .input-group.date', $FORM).datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('#div_passport_date_given .input-group.date', $FORM).datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

    $('#div_gett_date_registration .input-group.date', $FORM).datepicker({
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });

//    $("[name = driver_status]", $FORM).select2();
//    $("[name = driver_type]", $FORM).select2();

//    $("[name = bank_account_status]", $FORM).select2();
//    $("[name = bank_account_type]", $FORM).select2();

//    $("[name = gett_status]", $FORM).select2();

//    $("[name = vehicle_status]", $FORM).select2();
    $('select[name=bank_account_type]', $FORM).change(function () {
        console.log('change', this);
        if ($('[name=bank_account_type]', $FORM).val() == 0) {
            $('[name=bank_account_holder_name_last]',$FORM).val($('[name=driver_name_last]', $FORM).val()).prop('disabled', true);
            $('[name=bank_account_holder_name_first]',$FORM).val($('[name=driver_name_first]', $FORM).val()).prop('disabled', true);
            $('[name=bank_account_holder_name_name]',$FORM).val($('[name=driver_name_name]', $FORM).val()).prop('disabled', true);
        } else {
            $('[name=bank_account_holder_name_last]',$FORM).val('').prop('disabled', false);
            $('[name=bank_account_holder_name_first]',$FORM).val('').prop('disabled', false);
            $('[name=bank_account_holder_name_name]',$FORM).val('').prop('disabled', false);
        }
    });


});</script>