<link href="css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="css/plugins/steps/jquery.steps.css" rel="stylesheet">
<div class="row">
   <div class="col-lg-12">
      <div class="ibox">
         <table 
            data-toggle="table" 
            class="display table table-bordered table-responsive table-condensed" 
            data-url="controllers/employee/employeeView.php" 
            data-query-params="queryParams" 
            data-pagination="true" 
            data-search="true">
            <thead>
               <tr>
                  <th data-field="employee_name_last" data-sortable="true">Фамилия</th>
                  <th data-field="employee_name_first" data-sortable="true">Имя</th>
                  <th data-field="employee_name_name" data-sortable="true">Отчество</th>
                  <th data-field="department_name" data-sortable="true">Отдел</th>
                  <th data-field="position_name" data-sortable="true">Должность</th>
                  <th data-field="employee_email">Почта</th>
                  <th data-field="telephone_book_telephone">Телефон</th>
                  <th data-field="employee_login">Login</th>
                  <th data-field="employee_status_name" data-sortable="true">Статус</th>
                  <th data-field="superiors" data-sortable="true">Руководитель</th>
               </tr>
            </thead>
         </table>
      </div>
   </div>
</div>
</div>
<!-- Mainly scripts -->
<script src="js/bootstrap-table.js"></script>
<link href="css/bootstrap-table.css" rel="stylesheet">
<!-- Sparkline -->
<script src="js/plugins/sparkline/jquery.sparkline.min.js"></script>
<script>
   function queryParams() {
    return {
      		method: 'view_all'
    };
   }
</script>